<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 function scpAppMainConnect(&$scp)
{
	/*! \remark untuk Load Driver harus dynamic */
	app_func_memcopy($cfg, $scp); /*! test copy address */
	
	// $s =& $scp;
	
	// if( app_func_memcopy($s, $scp) )
	// {
		//app_func_pointer
		// $s = &$scp;	
	// }
	
	$cfg = 'mysql'; // get from config <scp_cfg_dbs.type>
	/**
	
	s->AppDbType , 
	s->AppDbHost, 
	s->AppDbPort, 
	s->AppDbName 
	 
	*/
	if (!app_loader_driver($scp, '__DBS__', $cfg))
	{
		exit(0);	
	}
	
	if( app_conn_handler() )
	{
			
	}
	
	// app_loader_modul( 'driver', 'mysql', 'app_active_handler');
	
	// include ('/opt/kawani/bin/chatpro/driver/mysql/app_active_handler');
	// call < app_active_handler >
 } 