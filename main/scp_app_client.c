<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */


/**
 * \brief  scpAppClientHandler 
 *
 * \remark fungsi yang betugas untuk Mengatur Route Client yang connect ke Server 
 *  memastikan apakah client connect melalui Port WebHook , Atau Connect melalui port 
 *  Khusus untuk Komunikasi 2 Arah , 
 *
 * \param  	Global Object
 *
 * \retval 	int(0) 
 *
 */
 function scpAppClientHandler(&$scp = NULL)
{
	/*! \remark check data jika bukan array exit saja */
	if ( !app_func_sizeof($scp->AppProgClient) )
	{
		return 0;
	}
	
	/*! \retval Iterator for All client connect */ 
	foreach ($scp->AppProgClient as $i => $f)
	{
		
		if ( app_func_retval($f, $s) )
		{
			/*! \remark handler: Jika Connect via socket HTTP Atau Web Hook */
			if (!strcmp($s->type, 'tcp_app') &&(app_func_search($s->id, $scp->AppProgSocket)))
			{
				scpAppMainApplication($scp, $s);
				break;
			}
			
			/*! \remark handler: Jika Connect via socket HTTP Atau Web Hook */
			if (!strcmp($s->type, 'tcp_pub') &&(app_func_search($s->id, $scp->AppProgSocket)))
			{
				scpAppMainWebhook($scp, $s);
				break;
			}
			
			
			/*! \remark handler: Client Websocket connect from Browser */
			if (!strcmp($s->type, 'wss_vst') &&(app_func_search($s->id, $scp->AppProgSocket)))
			{
				scpAppMainWebsock($scp, $s);  
				continue;
			}
		}
	}
	
	/*! \retval finaly false */
	return 0;
}
 
/**
 * \brief  	scpAppClientCollected
 *
 * \remark  Description Of Method 
 *
 * \param  	<\ p > Global Parameter 
 * \param	<\ s > Global Parameter 
 *
 * \retval 	int(0) 
 *
 */
 function scpAppClientCollected(&$p= NULL, &$s= NULL) 
{ 
	if ( isset($p->AppProgClient[$s->id]) )
	{
	   /*! 
		* \remark jika PID dari client belum ada tambahkan ke sini,
		*  Untuk slenjutnya akan di monitor Oleh server WebHook
		*
		*/
		if ( !$p->AppProgClient[$s->id]['pid'] )
		{
			$p->AppProgClient[$s->id]['pid'] = $s->pid;	
		}
	}
	
	return $s->id;
}

/**
 * \brief  	scpAppClientDeleted
 * \remark  Destruct / Destroy Client Session from Iterator Process 
 *
 * \param  <smt> Global Parameters  	  	 
 * \param  <s>	Object Client Session  
 *
 * \retval 	int(0) 
 *
 */
 function scpAppClientDeleted( &$scp= NULL, &$s= NULL) 
{
	if (isset($scp->AppProgClient[$s->id])) 
	{
		unset($scp->AppProgClient[$s->id]);	
	}
	return $s->id;
}

/**
 * \brief  	scpAppClientIterator
 *
 * \remark  Description Of Method 
 *
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function scpAppClientIterator(&$scp = NULL)
{
	if (app_func_sizeof($scp->AppProgClient)<1)
	{
		return 0;	
	}
	
	// push for peear 
	foreach ($scp->AppProgClient as $j => $f)
	{ 
		if ( app_func_retval($f, $s) )
		{
			app_func_copy($scp->AppProgSocket, $s->id, $s->sock);   
		} 
		app_func_free($s);
	} 
	
	return 0;
}
