<?php 

/**
 * \brief  scpAppPeerDisconected 
 *
 * \remark fungsi yang betugas untuk Mengatur Route Client yang connect ke Server 
 *  memastikan apakah client connect melalui Port WebHook , Atau Connect melalui port 
 *  Khusus untuk Komunikasi 2 Arah , 
 *
 * \param  	Global Object
 *
 * \retval 	int(0) 
 *
 */
 
 function scpAppPeerDisconected(&$scp = NULL, $s= NULL)
{
	if ( !sockTcpClose($s->sock) )
	{
		$scp->AppPeerClients[$s->peer]['id']   = 0;
		$scp->AppPeerClients[$s->peer]['sock'] = -1;
		$scp->AppPeerClients[$s->peer]['time'] = app_func_time();
	} 
	
	return $s->id;
} 
 
 
 
/**
 * \brief  	scpAppPeerIterator
 *
 * \remark  Description Of Method 
 *
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function scpAppPeerIterator(&$scp = NULL)
{
	if ( app_func_sizeof($scp->AppPeerClients)<1 )
	{
		return 0;	
	}
	
	
	/*! \remark Iterasi untuk semua jenis peers yang connect ke socket server */
	foreach ( $scp->AppPeerClients as $j => $f)
	{
		app_func_copy($s, false);
		if ( app_func_retval($f, $s) )
		{
			if ( app_func_sock($s->sock) )
			{
				app_func_copy($scp->AppProgSocket, $s->id, $s->sock);
				app_func_free($s);
				continue;
			}
			
			/*! jika peers terjadi putus koneksi */
			if ( app_func_sock($s->sock)<1 && app_func_interval($s->time) >=2  )
			{
				scp_log_app(sprintf("Peer '%s' host= %s port= %s Reconnecting", $s->peer, $s->host, $s->port));
				if (sockTcpClientCreate(1, $s->id, $s->sock))
				{
					/* \remark  success connect and next iterator_apply */
					if ( sockTcpClientConnect($s->sock, $s->host, $s->port) <0  ) 
					{
						/*! \remark if failed create connection */
						scp_log_app(sprintf("Peer '%s' host= %s port= %s Reconnecting Fail.", $s->peer, $s->host, $s->port));
						scp_log_app("\t");
						
						
						$scp->AppPeerClients[$s->peer]['id']   = 0;
						$scp->AppPeerClients[$s->peer]['sock'] = -1;
						$scp->AppPeerClients[$s->peer]['time'] = app_func_time();
					
						/*! \remark clean after process */
						app_func_free($s);
						continue; 
					}
					
					/*! \remark If Success Reconnecting to Peer register */
					scp_log_app(sprintf("Peer '%s' host= %s port= %s Reconnecting Success.", $s->peer, $s->host, $s->port));
					scp_log_app("\t");
						
					
					$scp->AppPeerClients[$s->peer]['id']   = $s->id;
					$scp->AppPeerClients[$s->peer]['sock'] = $s->sock;
					$scp->AppPeerClients[$s->peer]['time'] = app_func_time();
					
					/*! \remark Masukan ke List socket untuk di Iterasi */
					app_func_copy($scp->AppProgSocket, $s->id, $s->sock);
					app_func_free($s);
					
				}
			} 
		}
		
		/*! \remark clean after process */
		app_func_free($s);	
	}
	
	return 0;
}

/**
 * \brief  scpAppPeerHandler 
 *
 * \remark fungsi yang betugas untuk Mengatur Route Client yang connect ke Server 
 *  memastikan apakah client connect melalui Port WebHook , Atau Connect melalui port 
 *  Khusus untuk Komunikasi 2 Arah , 
 *
 * \param  	Global Object
 *
 * \retval 	int(0) 
 *
 */
 function scpAppPeerHandler(&$scp = NULL)
{
	/*! \remark check data jika bukan array exit saja */ 
	if ( !app_func_sizeof($scp->AppPeerClients) )
	{
		return 0;
	}
	
	/*! \remark test Untuk Iterasi Connect Ke Peer */ 
	foreach ($scp->AppPeerClients as $i => $f)
	{
		if (app_func_retval($f, $s))
		{
			/*! \remark Handler response Untuk Client Peer */
			if (!strcmp($s->type, 'peer') &&(app_func_search($s->id, $scp->AppProgSocket)))
			{
				scpAppMainPeerSock($scp, $s);
				break;
			}
		}
		
		app_func_free($s); 
	}
	return 0;
			
}