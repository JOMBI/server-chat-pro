<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */


  
/**
 * \brief  	scpAppMainSigAlarm
 * \remark  Description Of Method 
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function scpAppMainSigAlarm()
{
	Global $scp;	
	
	scp_log_app("Caught SIGALRM");
	foreach ($scp->AppProgThread as $pid)
	{
		posix_kill($pid, SIGINT);
	}
	return 0;
} 

/**
 * \brief  	scpAppMainSigClient
 *
 * \remark  Process Ini Tidak di Perlukana jika process hanya bersifat state Less  
 * 	artinya one request one Response ini di perlukan jiga model transaksi Bersifat Realtime 
 *  semisal dengan websocket atu real time protokol process ini sangat di perlukan karena 
 *  terkadang client tidak me request 'Close' namun karena indikasi lain semisal koneksi 
 *  buruk yang menyebabkan koneksi terputus . 
 *
 * \param  	{smt} 	Global Object Handler 
 * \param  	{pid} 	Client PID Signal Trap  
 *
 * \retval 	int(0) 
 *
 */
 function scpAppMainSigClient(&$scp = NULL, &$pid = NULL)
{
	foreach ($scp->AppProgClient as $id => $c)
	{
		if ( app_func_retval($c, $s) )
		{
			if ($s->pid &&( !strcmp($s->pid, $pid) )) {
				unset($scp->AppProgClient[$s->id]);
			}
		}	
		app_func_free($c);	
	}
	return 0;
 }
 
/**
 * \brief  	scpAppMainSigRetval
 *
 * \remark  fungsi yang bertujuan untuk menangkap / Trap ketika ada sebuah porcess yang mati 
 *  bisa di karenakan exit Norma atau abnormal , ketika hal ini terjadi maka pid yang sebelumnya 
 *  di register ke object global harus di delete agar tidak terjadi penumpukan 
 *
 * \param  	<\ scp > Global Object parameter 
 * \param  	<\ signo > Code signal yang datang untuk di check  
 *
 * \retval 	int(0) 
 *
 */
 
 function scpAppMainSigRetval(&$scp = NULL, $signo = NULL)
{
	/*! \remark create Local variable */
	app_func_struct($out);
	app_func_copy($out->pid, 0); 
	app_func_copy($out->wait, 0);
	app_func_copy($out->status, 0);
	
	/*! \remark iteration process collected */ 
	while ( ($out->pid = pcntl_wait($out->status, WNOHANG)) >0 )
	{
	   
	   /*! \remark clean up pids from SCP Global */
		$scp->AppProgThread = array_diff($scp->AppProgThread, array($out->pid)); 
		app_func_copy( $out->wait, 	
			pcntl_wifexited($out->status)
		); 
		
	   /*! 
	    * \remark check pid is whohang from process 
	    * like for zombie process on here please set change on this 
		* the next level fro kill 
		*
		* Please Use for debug Olny !
		*
		*/
		if ($scp->AppProgScr)
		{
			if (!$out->wait)
			{ 
				// scp_log_info(sprintf("Signal Collected Kill PID(%d)", $out->pid), __FILE__, __LINE__ ); 
			} 
			else if($out->wait)
			{ 
				// scp_log_info(sprintf("Signal Collected Exited PID(%d)", $out->pid), __FILE__, __LINE__ ); 
			}
		}
	}
	
	/*! \retval int(0) */
	app_func_free($out);
	return 0;
} 
 
/*!
 * \brief  	scpAppMainSigHandler
 *
 * \remark  Description Of Method 
 * \param  	-
 *
 * \retval 	int(0) 
 *
 */
 function scpAppMainSigHandler($signo = NULL)
{ 
	Global $scp;
	
	switch($signo)
	{
	   /*! \retval SIGTERM : shutdown process: */
		case SIGTERM: 
			scp_log_info("Signal: SIGTERM", __FILE__, __LINE__ );
			exit(0);
		break; 
		
		/*! \retval SIGHUP : signal restarting process from system */
		case SIGHUP:
			scp_log_info("Signal: SIGHUP", __FILE__, __LINE__ );
			exit(0);
		break;
		
		/*! \retval SIGINT: Signal Interupted by Sytem */
		case SIGINT: 
			scp_log_info("Signal: SIGINT", __FILE__, __LINE__ );
			exit(0);
		break; 
		
		/*! \retval SIGQUIT : Quit Signal from Process */
		case SIGQUIT:
			scp_log_info("Signal: SIGQUIT", __FILE__, __LINE__ );
			exit(0);
		break;
		
		/*! \retval SIGPIPE : Broken Pipe Like Socket connected:*/
		case SIGPIPE:
			scp_log_info("Signal: SIGPIPE", __FILE__, __LINE__ ); 
		break;
		
		/*! \retval SIGCHLD : signal from child */
		case SIGCHLD:
			if (function_exists( 'scpAppMainSigRetval' )) {
				scpAppMainSigRetval($scp, SIGCHLD);
			}
		break;
		
		/*! \retval No Signal Define set On here : */ 
		default:  
			scp_log_info("Signal: Default", __FILE__, __LINE__ ); 
		break; 
   } 
   /*! \retval int(0) === false */
   return 0;
}  

 
/*!
 * \brief  	smtAppMainSignal
 * \remark  Description Of Method 
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function scpAppMainSignal(&$scp = NULL)
{ 	
    /*! \retval if posix methode is not fail */
	if (!function_exists('pcntl_signal'))
	{
		app_func_copy($scp->AppSignal, 0);
		return 0;
	}
	
	/*! \retval Copy Signal If Success */
	if (app_func_copy($scp->AppSignal, 1))
	{
		pcntl_signal(SIGTERM, 'scpAppMainSigHandler'); 
		pcntl_signal(SIGHUP,  'scpAppMainSigHandler');
		pcntl_signal(SIGINT,  'scpAppMainSigHandler');
		pcntl_signal(SIGPIPE, 'scpAppMainSigHandler');
		pcntl_signal(SIGQUIT, 'scpAppMainSigHandler');
		pcntl_signal(SIGCHLD, 'scpAppMainSigHandler'); 
		pcntl_signal(SIGALRM, 'scpAppMainSigAlarm');
	}
	
	/*! \retval signal true */
	
	return app_func_int($scp->AppSignal);
}  