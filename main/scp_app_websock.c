<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 function scpAppMainWebsock(&$scp= NULL, &$s)
{
	$s->size = ((1024) * 3);
	$s->buf  = @socket_read($s->sock, $s->size, PHP_BINARY_READ);
	if (($s->buf ===  false ) OR ( $s->buf === '' ) OR (strlen($s->buf) < 1)) 
	{
		app_visitor_loger($s, sprintf("Visitor Session #%d Client Disconnected", $s->id)); 
		if ( app_websock_visitor_client_sighup($scp, $s) ) {
			app_func_free($s);
		} 
		return 0;
	}
	
   /*! \retval  Jika belum pernah Upgrade Websocket maka upgrade dulu headernya  
	* biar support websocket jika sudah upgrade baru process iterasi. 
	*
	*/
	if (!$s->shak) 
	{
		sockWebSocketUpgrade($scp, $s);
		/*! \remark check apakh sudah Punya PID  Jika Belum akan di buat handler Thread ? */
		if ( !$s->pid && app_websock_visitor_client_thread($scp, $s) ) { 
			app_websock_visitor_client_collected($scp,$s);
		}
		return 0;
	}
	
	/*! \remark if socking upgrade websocket then wil commit this channels for long time */
	if ($s->shak)
	{
		if (app_websock_visitor_ontrace($scp, $s)) {
			app_func_free($s->buf); 
		}
	}
	
	return 0;	
 }  