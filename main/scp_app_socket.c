<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 

/*!
 * \brief  	scpAppCoreSocket
 *
 * \remark  Socket Khusus Untuk Application /core socket jika 
 *	next kedepannya ada interaksi semisal via Telnet Atau Komunikasi lain semisal 
 *	dengan Applikasi Telephony .
 *	
 * \param  	<\ scp > Global Parameters 
 * \retval 	int(0) 
 *
 */
 function scpAppCoreSocket(&$scp = NULL)
{
	scp_log_info(sprintf("TCP Socket Bind 'scpAppCoreSocket()' port: %s", $scp->AppTcpPort), 
	__FILE__, __LINE__);
	
	if (sockTcpServerCreate($scp->AppTcpLink, $scp->AppTcpHost, $scp->AppTcpPort, $scp->AppTcpSign)<0 )
	{
		scp_log_error("Created 'scpAppCoreSocket()' Error.", 
		__FILE__, __LINE__);
		
		return 0;
	} 
	return $scp->AppTcpLink;
} 

/*!
 * \brief  	scpAppManagerSocket
 *
 * \remark  Socket Server yang berperan Untuk Pertukaran data antara thread satu dengan 
 * yang lainya karena ini hanya bisa di aksess secara internal saja dan kemungkinan paket data 
 * yang di proces akan jauh lebih besar maka trasport yang di gunakan Bukan TCP melainkan 
 * UDP agar bisa menampung process data dalam jumlah yang besar .
 *
 * \param  	<\ scp > Global Parameters 
 * \retval 	int(0) 
 *
 */
 function scpAppMgrSocket(&$scp = NULL)
{
	scp_log_info(sprintf("TCP Socket Bind 'scpAppMgrSocket()' port: %s", $scp->MgrUdpPort), 
	__FILE__, __LINE__);
	
	if (sockUdpServerCreate($scp->MgrUdpLink, $scp->MgrUdpHost, $scp->MgrUdpPort)<0)
	{
		scp_log_error("Created 'scpAppMgrSocket()' Error", 
		__FILE__, __LINE__);
		return 0;
	}
	
	return $scp->MgrUdpLink;
}


/*!
 * \brief  scpAppPubSocket
 *
 * \remark Server Socket yang berperan Untuk Transaksi dengan menggunakan  
 * Fiture web http via port 80, Ini sifatnya Global yang akan di Gunakan oleh semua part 
 * baik Internal< admin > , Visitor maupun Agent. 
 
 * Publik socket atau di sebut juga sebagai Web Hook memiliki Peran sebagai <next_fallback> action dari websocket. 
 * Sehingga process yang terjadi di server Untuk Pertukaran / Komunikasi antara Visitor dan Agent 
 * tidak Hanya mengandalkan Webscoket, dan Web Hook berperan juga membantu process Webscoket dalam pertukaran data .
 *
 * Websocket hanya akan melakukan triger ke client dan Kemudian client akan merequest Via Web Http / Web Hook 
 * Untuk Process lanjutan .
 * 
 * Hal Ini Juga Sekaligus Untuk penangan Pengiriman data yang cukup besar seperti file gambar, dokumen 
 * dan file lain yang berkaitan dengan process komunikasi antara Visitor dan Agent 
 *
 * \param  	<\ scp > Global Object Parameter 
 * \retval 	int(0) 
 *
 */
 function scpAppPubSocket(&$scp = NULL)
{
	scp_log_info(sprintf("TCP Socket Bind 'scpAppPubSocket() port: %s'", $scp->PubTcpPort), 
	__FILE__, __LINE__);
	
	if (sockTcpServerCreate($scp->PubTcpLink, $scp->PubTcpHost, $scp->PubTcpPort, $scp->PubTcpSign)<0 )
	{
		scp_log_error("Created 'scpAppPubSocket()' Error.", 
		__FILE__, __LINE__);
		
		return 0;
	} 
	return $scp->PubTcpLink;
}

/*!
 * \brief  	scpAppVstSocket
 *
 * \remark 	Selain Socket Internal dan Public ada juga Socket Yang Khusus Untuk Menangani Masing Object .
 * Soket Visitor Server Memiliki Tujuan Untuk melayani client yang berasal dari Visitor / Guest / Tamu 
 * Yang kemudian akan di Process di server dan server akan melakukan Routing ke Agent Yang ada di Websocket   
 * Agent mengapa di Buat banyak jalur ? , Tujuannya tentu agar process transaksi tidak terhambat dan tidak 
 * saling bersingungan walaupun memiliki keterkaitan antara satu dengan yang lainnya .
 * 
 * \param  	<\ scp > Global Object Parameter
 * \retval 	int(0) 
 *
 */
 function scpAppVstSocket(&$scp = NULL)
{
	scp_log_info(sprintf("TCP Socket Bind 'scpAppVstSocket()' port: %s", $scp->VstTcpPort),
	__FILE__, __LINE__ );
	
	if (sockTcpServerCreate($scp->VstTcpLink, $scp->VstTcpHost, $scp->VstTcpPort, $scp->VstTcpSign)<0 )
	{
		scp_log_error("Created 'scpAppVstSocket()' Error.", 
		__FILE__, __LINE__ );
		
		return 0;
	} 
	return $scp->VstTcpLink;
}


/*!
 * \brief  	smtAppMainSignal
 * \remark  Description Of Method 
 * \param  	-
 * \retval 	int(0) 
 *
 */
 function scpAppMainSocket(&$scp = NULL)
{
	/*! \remark RunSock */
	if (!isset($scp->RunSock))
	{
		app_func_copy($scp->RunSock, 0);
	}
	
	/*! \remark tcp:scpAppCoreSocket */
	if (scpAppCoreSocket($scp))
	{
		app_func_next($scp->RunSock);
		if (function_exists('sockTcpSetNonblock'))
		{
			sockTcpSetNonblock($scp->AppTcpLink);
		}
	}
	
	/*! \remark udp:scpAppMgrSocket */
	if (scpAppMgrSocket($scp))
	{
		app_func_next($scp->RunSock);
	}
	
	/*! \remark tcp:scpAppPubSocket */
	if (scpAppPubSocket($scp))
	{
		app_func_next($scp->RunSock);
		if (function_exists('sockTcpSetNonblock'))
		{
			sockTcpSetNonblock($scp->PubTcpLink);
		}
	}
	
	/*! \remark tcp:scpAppVstSocket */
	if (scpAppVstSocket($scp))
	{
		app_func_next($scp->RunSock);
		if (function_exists('sockTcpSetNonblock')) 
		{
			sockTcpSetNonblock($scp->VstTcpLink);
		}
	}
	/*! \retval finally is false */
	return $scp->RunSock;
}