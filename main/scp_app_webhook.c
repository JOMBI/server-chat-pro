<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 function scpAppMainWebhook(&$scp =NULL, &$s = NULL)
{
	sockTcpRead($s->sock, $s->nread, 1024);
	if (($s->nread ===  false ) OR ( $s->nread === '' ) OR (strlen($s->nread) < 1)) 
	{
		app_web_hook_loger($s, sprintf("Session #%d Client Disconnected", $s->id));
		if ( app_webhook_client_sighup($scp, $s) ) {
			app_func_free($s);
		}
		
		return 0;
	}
	
   /*! 
	* \remark Jika data Ada Isinya maka teruskan Ke Porcess handler selanjutnya 
	* Process Manipulasi dan response akan di lakukan pada modul webhook 
	*
	* \remark Untuk Paralel Process dengan Aksess User Yang Lebih dari satu client sebaiknya 
	* process client webhook di Buat thread sendiri tujuannya agar tidak membuat process 
	* harus Mengantri , Atau Menerapkan Multi thread 
	*
	*/
	else 
	{
	    if ( app_webhook_client_thread($scp, $s) )
		{
			if ( app_webhook_client_collected($scp,$s) ){
				
				app_func_free($s->nread);
			}
		}		
	} 
	
	return 0;
 }   