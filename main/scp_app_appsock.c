<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * FLOW: onTrace->Meta->Action->Event 
 */
 
/*! \retval  scpAppMainApplication */
 function scpAppMainApplication(&$m= NULL, &$s, $buf = 1024)
{
	$s->size = (($buf) * 2); /*! \remark increments */
	$s->buf  = @socket_read($s->sock, $s->size, PHP_BINARY_READ);
	if (($s->buf ===  false ) OR ( $s->buf === '' ) OR (strlen($s->buf) < 1)) 
	{
		app_appsock_client_disconnect($m, $s);
		if (app_appsock_client_deleted($m, $s))
		{
			scp_log_app(sprintf("Peer Client '%s' Host= %s Port= %s Disconnected.", $s->id, $s->host, $s->port ));  
			app_func_free($s);
		}
		
		return 0;
	}
	
	/*! \remark sent data to iterator process */
	if (app_appsock_ontrace($m, $s))
	{
		app_func_free($s->buf); 		
	} 
	
	return 0; 
}