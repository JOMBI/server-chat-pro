<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 function scpAppMainBanner(&$scp = NULL)
{
	/*! \note verbose false then 
	will hide if on Production  arg "v" */	 
	app_func_struct($app);
	app_func_copy($app->scr, "\n");  
	
	if (app_func_sizeof($app->scr))
	{
		app_func_output($app->scr, sprintf("%s (%s) - Version %s\n",$scp->AppProgName,$scp->AppProgUid, $scp->AppProgVer));
		app_func_output($app->scr, sprintf("%s.\n",$scp->AppProgCopy));
		app_func_output($app->scr, sprintf("Auth < %s >\n", $scp->AppProgAuth));
		app_func_output($app->scr, sprintf("Build on %s\n", $scp->AppProgBuild));
		app_func_output($app->scr, "\n"); 
		
		/*! \remark show data to user */
		if ( !scp_log_scr($scp, $app->scr) )
		{
			app_func_free($app);
		} 
	}
	
	return 0;
}