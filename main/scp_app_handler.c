<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/*! \retval scpAppMainHandler */ 
 function scpAppMainHandler(&$scp)
{
	if (php_sapi_name() != 'cli')
	{ 
		exit(0);	
	} 
	return $scp;
 } 
 
 
/*! \retval scpAppUnregHandler */
 function scpAppUnregHandler(&$scp)
{
   /*! 
	* \remark 
	* Ketika program stop atau mati maka perlu lakukan ini 
	* 1. Close Semua Socket  
	* 2. Delete File PID	
	* 3. Close Stream Loger  
	* 4. Close Koneksi Ke database , etc 
	*
	*/ 
	
	scp_log_info(sprintf("Remove File PID '%s'", $scp->AppProgLck), 
	__FILE__, __LINE__ );
	
	
	/*! \retval Go to Process */
	app_func_event('app_fork_del' , array(&$scp), __FILE__, __LINE__);
	app_func_event('scp_log_close', array(&$scp), __FILE__, __LINE__);
	return 0;
 }  