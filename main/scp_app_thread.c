<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/*! \def scpAppCreateThread < scp, event, param, pid > */ 
 function scpAppCreateThread(&$scp = NULL, $event = NULL, $param = NULL, &$pid = 0 )
{
	/*!\retval ceate multi thread  management pararel process*/
	app_func_struct($out);
	app_func_copy($out->pid, pcntl_fork());
	
	
	/*! \retval if false process then */
	if ($out->pid == -1) 
		die('could not fork');
		
	else if ($out->pid)
	{ 
		/*! \retval masukan PID ke Object SCP Application Untuk di Monitoring */
		app_signal_register($scp, $out->pid);
		
	}
	
	/*! \def fork child worker process */
	else 
	{
		/*! \remark Create Or Call Method on child process */
		if ( app_signal_worker($scp, $event, $param) )
		{
		   
		   /*! \remark setelah selesai menjalankan sebuah method maka kill pid tersebut agar tidak menjadi  
			*  zombi yang berdampak buruk pada performa device / server 
			*/
			if ( $scp->pid &&( function_exists( 'posix_kill' )) )
			{
				if ( posix_kill($scp->pid, SIGKILL) )
				{
					exit(0);
				}
			}
		}
	} 
	
	/*! \retval Return PID for Global Master */ 
	if (app_func_copy($pid, $out->pid)) 
	{
		app_func_free($out);
	}
	
	return $pid;
}  