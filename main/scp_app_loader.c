<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/*!
 * \brief smtAppEventLoader
 *
 * \remark Event function get Subloader process 
 * base program Under CLI console .
 *
 * \param 	<\scp> Global Object reference 
 * \param 	<\works> iniated dalam stream 
 * \param 	<\path> directory atau path lokasi yang akan di include
 *
 * \retval  *int(0)
 */ 
 function scpAppEventLoader(&$scp, $works= '', $path = '')
{
	if ( !is_dir($path) )
	{
		return 0;
	}
	
	/*! \remark next for all scan dir */
	$paths = scandir($path);
	if (is_array($paths)) foreach ($paths as $i => $f)
	{
		/*! \remark jika hasil iterasi ini bukan file melainkan directory 
		tidak perlu di load lewat saja */
		
		if ( is_dir($f) ){
			continue;
		}	
		
		
		/*! \remark Go to Process */
		if (!is_dir($f) && stristr($f, '.c'))
		{
			$cfgLoader = sprintf('%s/%s', $path, $f);
			
			if ( @file_exists($cfgLoader) )
			{
				if ( !isset($scp->AppProgLoader[$works][$f]) )
				{
					$scp->AppProgLoader[$works][$f] = $cfgLoader;
				}
				
				/*! \retval then will start loader */
				include ($cfgLoader);
			}
		}			
	}
	
	/*! \retval finally is int === 0 */
	return 0;
 } 
 
 
/*!
 * \brief 	scpAppMainLoader
 *
 * \remark 	Main Program for Application Loader Process 
 *	from this will auto load all files processs 
 *
 * \param 	<\scp> Global Object 
 *
 * \retval 	running KepAlive(0)
 */  
 
 function scpAppMainLoader(&$scp)
{
   /*! 
	* \remark Untuk Loader Pertma kali yang di iniated adalah Modul Library 
	* Karena Ini bagian terpeting Yang selanjutnya akan ada Sub Event Loader Untuk Masing
	* directory yang menyesuaikan dengan ke Butuhan 
	*
	*/
	if ( function_exists('scpAppEventLoader') )
	{
		scpAppEventLoader($scp, '__LIB__', $scp->AppProgEnviroment['__LIB__']);
		scpAppEventLoader($scp, '__DBS__', $scp->AppProgEnviroment['__DBS__']);  
		scpAppEventLoader($scp, '__SCK__', $scp->AppProgEnviroment['__SCK__']);
		scpAppEventLoader($scp, '__MGR__', $scp->AppProgEnviroment['__MGR__']);
		scpAppEventLoader($scp, '__WHK__', $scp->AppProgEnviroment['__WHK__']);
		scpAppEventLoader($scp, '__VST__', $scp->AppProgEnviroment['__VST__']);
		scpAppEventLoader($scp, '__WSK__', $scp->AppProgEnviroment['__WSK__']);
		scpAppEventLoader($scp, '__SES__', $scp->AppProgEnviroment['__SES__']);
		scpAppEventLoader($scp, '__CHN__', $scp->AppProgEnviroment['__CHN__']);   
		scpAppEventLoader($scp, '__PSK__', $scp->AppProgEnviroment['__PSK__']);  
		scpAppEventLoader($scp, '__ASK__', $scp->AppProgEnviroment['__ASK__']);  
		scpAppEventLoader($scp, '__WKR__', $scp->AppProgEnviroment['__WKR__']);  
		 			
	}
	
	/*! \remark finally retval */
	return $scp;
 }
 
 
 