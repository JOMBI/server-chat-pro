<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
 function scpAppMainConfig(&$scp)
{
	/*! \def App config */
	$scp->AppProgVer   		= '1.1.0 Betha';								// Version Program
	$scp->AppProgUid   		= 'SCP';									  	// UID program 	
	$scp->AppProgName  		= 'Server Chat Pro';			 	 		  	// Name of Program
	
	$scp->AppProgTitle 		= 'SCP - Server Chat Pro';			 	  		// Title Description
	$scp->AppProgCopy  		= '(C) 2021 - Peranti Digital Solusindo, PT'; 	// Copy Right 
	$scp->AppProgLine  		= '2021 - Peranti Digital Solusindo, PT'; 		// Copy Right 	
	$scp->AppProgAuth  		= 'PDS Developer Team';						  	// Program Auth 	
	$scp->AppProgLink  		= 'http://www.perantidigital.co.id';		  	// Program Auth 
	$scp->AppProgBranch 	= 'Chat Pro';			 	 		  			// Name of Brach
	$scp->AppProgBuild 		= '2021-09-26';								  	// Program Build 	
	$scp->AppProgId   		= 0;											// server Identfification
	$scp->AppProgCfg   		= 0;											// Config file 
	$scp->AppProgPid   		= 0;										  	// PID program 
	$scp->AppProgLck   		= 0;										  	// Lock file 
	$scp->AppProgRun   		= 0;										  	// Run Or Not 
	$scp->AppProgScr   		= 1;										  	// for screen 
	$scp->AppProgVar   		= NULL;										  	// all config var 	
	$scp->AppProgSpl   		= array();									  	// Auto Loader  
	
	/*! \def App Config  Sokcet */
	$scp->AppProgStorage 	= array();									 	// All Information set on this stream 		
	$scp->AppProgService 	= array();									 	// All Service To Monitoring 
	$scp->AppProgWorker  	= array();									 	// All Worker PID 
	$scp->AppProgThread  	= array();									 	// All PID register 
	$scp->AppProgSignal  	= 0;											// Signal Not Work ?
	$scp->AppProgClient  	= array();									 	// socket Client Udp:port, Tcp:sid Pub:sid
	$scp->AppProgSocket  	= array();									 	// socket Master 
	$scp->AppProgTimout  	= 30;										 	// Timeout connected Sokcet 
	$scp->AppProgMaxCon  	= 1024;									 		// Max Client Connect 
	$scp->AppProgProc	 	= NULL;									 		// Open Worker Class Register Proc
	
	
	
	
	/*! \def Local App config */
	$scp->AppTcpHost   = NULL; 
	$scp->AppTcpPort   = NULL;
	$scp->AppTcpSign   = NULL;
	$scp->AppTcpBuff   = 65000;
	$scp->AppTcpLink   = -1;
	
	/*! \def Manager config */
	$scp->MgrUdpHost   = NULL; 
	$scp->MgrUdpPort   = NULL;
	$scp->MgrUdpSign   = NULL;
	$scp->MgrUdpBuff   = 65000;
	$scp->MgrUdpLink   = -1;
	
	/*! \def Public config */
	$scp->PubTcpHost   = NULL; 
	$scp->PubTcpPort   = NULL;
	$scp->PubTcpSign   = NULL;
	$scp->PubTcpBuff   = 65000;
	$scp->PubTcpLink   = -1;
	
	/*! \def Visitor config */
	$scp->VstTcpHost   = NULL; 
	$scp->VstTcpPort   = NULL;
	$scp->VstTcpSign   = NULL;
	$scp->VstTcpBuff   = 65000;
	$scp->VstTcpLink   = -1;
	 
	/*! \def config for Loger */
	$scp->LogPathVerb  	= 0;
	$scp->LogPathLink  	= -1;  
	$scp->LogPathCfg   	= '/var/log/scp';
	$scp->LogPathDts   	= date('Ymd');
	$scp->LogPathSize  	= 1024;
	$scp->LogPathUid  	= 'scp';
	
	
	/*! \def for Alert & Worker */
	$scp->AppWorker   	= NULL;
	$scp->AppAlert 	  	= NULL; 
	$scp->AppSec      	= 2;
	$scp->AppUsec	  	= 2000;
	
	/*! \def box_session */ 
	$scp->AppBoxTime	 = array();	// box_time 
	$scp->AppBoxLoger    = array();  // <from, to >
	$scp->AppBoxSesion   = array();  // <from, to >
	$scp->AppUserSesion  = array();	 // User Session 	
 	$scp->AppChanSession = array();	 // channels session per user 
	$scp->AppChanState	 = array();	 // channels status for Update 
	
	/*! \def for dynamic Webhook*/
	$scp->WebHooks 		 = NULL;
	$scp->CreateId 		 = 0;
	$scp->AppBoxId 		 = 0;
	$scp->AppMsgId 		 = 0;
	
	
	/*! for Route client */
	$scp->AppPeerPartner = NULL;
	$scp->AppPeerClients = array(); 
	if (isset($scp->AppProgEnviroment['__LOG__']))
	{
		$scp->LogPathCore = $scp->AppProgEnviroment['__LOG__'];
	}
	
	/*! Retval */
	return $scp; 
 }
 
 
 /*! \brief < \ scpAppInitStream > */ 
 function scpAppInitStream(&$scp)
{
	
	/*! \def copy program ID */
	app_func_copy($scp->AppProgId, app_cfg_param('scp_cfg_prog', 'id'));
	
	/*! \def App config */ 
	app_func_copy($scp->AppTcpHost, app_cfg_param('scp_cfg_app','host'));
	app_func_copy($scp->AppTcpPort, app_cfg_param('scp_cfg_app','port'));
	app_func_copy($scp->AppTcpSign, app_cfg_param('scp_cfg_app','sign')); 
	 
	 
	/*! \def Manager config */ 
	app_func_copy($scp->MgrUdpHost, app_cfg_param('scp_cfg_mgr','host'));
	app_func_copy($scp->MgrUdpPort, app_cfg_param('scp_cfg_mgr','port'));
	app_func_copy($scp->MgrUdpSign, app_cfg_param('scp_cfg_mgr','sign')); 
	  
	
	/*! \def Public config */
	app_func_copy($scp->PubTcpHost, app_cfg_param('scp_cfg_pub','host'));
	app_func_copy($scp->PubTcpPort, app_cfg_param('scp_cfg_pub','port'));
	app_func_copy($scp->PubTcpSign, app_cfg_param('scp_cfg_pub','sign')); 
	   
	/*! \def Agent config */
	app_func_copy($scp->AgtTcpHost, app_cfg_param('scp_cfg_agt','host'));
	app_func_copy($scp->AgtTcpPort, app_cfg_param('scp_cfg_agt','port'));
	app_func_copy($scp->AgtTcpSign, app_cfg_param('scp_cfg_agt','sign')); 
	    
	
	/*! \def Visitor config */
	app_func_copy($scp->VstTcpHost, app_cfg_param('scp_cfg_vst','host'));
	app_func_copy($scp->VstTcpPort, app_cfg_param('scp_cfg_vst','port'));
	app_func_copy($scp->VstTcpSign, app_cfg_param('scp_cfg_vst','sign'));  
	return 0;
 }
 
  /*! \brief < \ scpAppInitPartner > */ 
 function scpAppInitPartner(&$scp)
{
	app_func_copy($retval, 0);
	if ( !app_cfg_partner('scp_cfg_peer', 'peer', $partners) )
	{
		return 0;
	}
	
	
	if( app_func_copy($scp->AppPeerPartner, $partners) )
	{
		foreach( $scp->AppPeerPartner as $i => $peer )
		{
			app_func_free($s);
			if ( app_cfg_peers($peer, $s) )
			{
				/*! \ Add new default data */
				app_func_copy($s->id  , 0);
				app_func_copy($s->sock, false);
				app_func_copy($s->peer, $peer);
				app_func_copy($s->type, 'peer');
				app_func_copy($s->time, strtotime('now'));
				
				
				/*! get server ID */
				app_func_copy($s->sid , app_cfg_param('scp_cfg_prog','sid'));
				if ( sockTcpClientCreate(1, $s->id, $s->sock) > 0 )
				{
					$scp->AppPeerClients[$s->peer]['id']   = $s->id;
					$scp->AppPeerClients[$s->peer]['sock'] = $s->sock;
					
					if ( sockTcpClientConnect($s->sock, $s->host, $s->port) < 0 )
					{
						if ( !sockTcpClose($s->sock) )
						{
							$scp->AppPeerClients[$s->peer]['id'] = 0;
							$scp->AppPeerClients[$s->peer]['sock'] = -1;
						}
						
					}
					
					/*! \ Add New data Reference */
					$scp->AppPeerClients[$s->peer]['sid']  = $s->sid;
					$scp->AppPeerClients[$s->peer]['cid']  = $s->cid;
					$scp->AppPeerClients[$s->peer]['host'] = $s->host;
					$scp->AppPeerClients[$s->peer]['port'] = $s->port;
					$scp->AppPeerClients[$s->peer]['type'] = $s->type;
					$scp->AppPeerClients[$s->peer]['peer'] = $s->peer;
					$scp->AppPeerClients[$s->peer]['pass'] = $s->pass;
					$scp->AppPeerClients[$s->peer]['time'] = $s->time;
					app_func_next($retval);
				}  
				
				app_func_free($s);
			}
		}
	}
	// print_r($scp->AppPeerPartner);
	/*! \remark finaly return this */
	return $retval;
}
 
/*! \brief < \ scpAppInitConfig > */ 
 function scpAppInitConfig(&$scp)
{
	/*! \remark Set Program PID to Local */
	app_func_struct($app);
	app_func_copy($app->cfg, 0);
	
	/*! \remark next Iterator Process */
	if ( $scp->AppProgPid = getmypid() )
	{
		app_func_copy($scp->AppProgRun, 
			app_func_next($scp->AppProgRun)
		);
	}
	
	/*! \remark while Read Config file program process */
	
	app_cfg_main($scp);
	
	/*! \remark create fork run '/var/run/scp/.pid' */
	
	app_fork_run($scp);
	
	
	/*! \remark Open All Configuration */
	
	if ( app_cfg_parser($scp->AppProgCfg, $app->cfg) )
	{
		app_func_copy($scp->AppProgVar, $app->cfg);
	} 
	
	/*! \retval create socket */
	scpAppInitStream($scp);
	
	
	/*! \retval get Peers connect to this server */
	scpAppInitPartner($scp);
	
	/*! finaly return this */ 
	return ( is_array($app->cfg) ? sizeof($app->cfg) : 0 );
} 