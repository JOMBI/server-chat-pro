/*!
 * SCP -- An open source Server Chat Pro
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/*! \retval Global Object */
 window.ws = null;
 window.sockTimeout = 10;
 window.sockTimeoutId = null;
 window.sockReconnecting = 0;
 window.sockInterupted = 0;
 
/**
 * \remark 	code Error on socket 
 * \link  	https://developer.mozilla.org/en-US/docs/Web/API/WebSocket/readyState
 */ 
 const WS_CODE_CONNECT = 0;
 const WS_CODE_OPENED  = 1;
 const WS_CODE_CLOSING = 2;
 const WS_CODE_CLOSED  = 3;

/*! \retval for Utilitize str output format */
 window.sprintf = function( str ) 
{
	for ( var i=1; i < arguments.length; i++) 
	{
		str = str.replace( /%s/, arguments[i] );
	}
	
	return str;
};

/*! \retval for Utilitize str writelog format */
 window.writeloger = function( str ) 
{
	console.log( sprintf("WS >> %s\n", str ));
	return 0;
}

/*! \retval for Utilitize str writelog format */
 window.writeobj = function( obj ) 
{
	console.log(obj);
	return 0;
}

/*! \link http://www.navioo.com/javascript/tutorials/Javascript_microtime_1583.html */
 window.makeuid = function(get_as_float) 
{  
    // Returns either a string or a float containing the current time in seconds and microseconds 
    var now = new Date().getTime() / 1000;  
    var s = parseInt(now);  
    var retval = (get_as_float) ? now : s+''+(((now - s) * 1000)/1000).toFixed(4);  
	return retval.toString().replace(/\./i, ''); 
}  

/*! \link http://www.navioo.com/javascript/tutorials/Javascript_microtime_1583.html */
 window.microtime = function(get_as_float) 
{  
    // Returns either a string or a float containing the current time in seconds and microseconds 
    var now = new Date().getTime() / 1000;  
    var s = parseInt(now);  
    return (get_as_float) ? now : (((now - s) * 1000)/1000).toFixed(2) + ' ' + s;  
}  

/*! \retval for Utilitize for check to second  */
 window.app_func_second = function(time)
{
	var time_to_show = parseInt(time); // unix timestamp in seconds
	var t = new Date(time_to_show * 1000);
	var formatted = ('0' + t.getHours()).slice(-2) + ':' + ('0' + t.getMinutes()).slice(-2) + ':' + ('0' + t.getSeconds()).slice(-2);
	return formatted;
}

/*! \retval for Utilitize for check to second  */
 window.app_func_minute = function(time)
{
	var time_to_show = parseInt(time); // unix timestamp in seconds
	var t = new Date(time_to_show * 1000);
	var formatted = ('0' + t.getHours()).slice(-2) + ':' + ('0' + t.getMinutes()).slice(-2);
	return formatted;
}


/*! \retval for Utilitize for check method */
 window.app_func_isfunc = function(str)
{
	if( typeof(str) == 'function' ){
		return true;
	}
	return false;
}

/*! \retval for Utilitize for check method */
 window.app_func_isobj = function(data)
{
	if( typeof(data) == 'object' ){
		return true;
	}
}

/*! \retval for Utilitize for check sizeof */
 window.app_func_sizeof = function(str)
{
	var str = str.toString();
	return str.length;
}

/*! \retval for Utilitize for check sizeof */
 window.app_func_int = function(str)
{
	return parseInt(str);
}

/*! \retval Parent Of Socket CP */
 function chatPro() 
{
	/*! \remark set default parameter */
	this.onClientId	 = null;
	this.onSupport 	 = null;
	this.onSession 	 = null;
	this.onNavigator = null;
	this.onHost		 = null;
	this.onPort		 = null;
	this.onUrl	     = null;
	this.onConected  = 0;
	this.onSocket	 = false;
	this.onSecWait	 = 5000;
	 
	/*! \remark check websocket support on client browser */
	if ('WebSocket' in window) 
	{
		if (this.onSupport = true){
			window.writeloger("WebSocket is supported by your Browser!");
		}
		return this;
	}
	
	/*! socket Not Supperted */
	if (this.onSupport == false)
	{
		window.writeloger("WebSocket NOT supported by your Browser!");	
	}
	
	return this;
}


/*! \retval cpSock.onSetSockHandler */
 chatPro.prototype.onSetSockHandler = function(onConnect, onError, onThread, onClose, onTimeout)
{
	this.onConnect 	 = onConnect;
	this.onError 	 = onError;
	this.onThread    = onThread;
	this.onClose 	 = onClose;
	this.onTimeout   = onTimeout; 
 } 
 
 /*! \retval cpSock.onSetStartHandler */
 chatPro.prototype.onSetStartHandler = function(onStart)
 {
	this.onStart    = onStart;
	return this;
 }
 
/*! \retval cpSock.onSetThreadHandler */
 chatPro.prototype.onSetThreadHandler = function(onReceiveMessage, onErrorMessage)
 {
	this.onReceiveMessage = onReceiveMessage;
	this.onErrorMessage = onErrorMessage;
	return this;
 }
 
/*! \retval cpSock.onSetSockClose */
 chatPro.prototype.onSetSockClose = function(Interupt)
 {
	window.sockInterupted = Interupt;
	if (window.sockInterupted)
	{
		this.onSocket.close();
	}
	return 0;
 }
 
/*! \retval cpSock.onSetError */
 chatPro.prototype.onSetSockError = function( chat, error )
{
	
	/*! \note customize local Error */
	var onEvents = 
	{
		meta  : 300,
		type  : 'event',
		event : 'error',
		error : 1, 
		data  : {
			text : error
		}
	}; 
	
	if ( app_func_isobj(onEvents) && app_func_isfunc(chat.onReceiveMessage) ){
		chat.onReceiveMessage.apply(this, [onEvents,chat]);
	}
	return 0;
} 

/*! \retval chatPro.onSetSockPrepare */
 chatPro.prototype.onSetSockPrepare = function(callback)
{
	window.sprintf("%s", 'onSetSockPrepare');
	var config = {};
	if ( app_func_isfunc(callback))
	{
		callback.apply(this, [this, config]);
		return ;
	}
	
	return ;
} 
 
/*! \retval cpSock.onSetSockConnect */
 chatPro.prototype.onSetSockConnect = function(Host, Port) 
{
	this.onProt = 'ws';
	this.onHost = Host;
	this.onPort = Port; 
	window.writeloger(window.sprintf("Host = %s Port = %s", this.onHost, this.onPort));
	 
	/*! \remark validation data host, port */
	if ( typeof this.onHost  == 'undefined' ){
		return 0;
	}
	
	/*! \remark netxt iteration process */
	this.onUrl = window.sprintf("%s://%s:%s/echo", this.onProt, this.onHost, this.onPort);
	if (this.onUrl == '' )
	{
		return 0;
	}
	
	/*! \remark if sucess socket */
	
	this.onSocket = new WebSocket(this.onUrl);
	var onParents = this;
	
	/*! \retval if socket is open will call my function onConected */
	this.onSocket.onopen = function(evt)
	{ 
		writeloger("Initialize open ws:socket");
		if (app_func_isobj(evt) && app_func_isfunc(window.clearTimeout))
		{
			window.clearTimeout(window.sockTimeout);
			window.sockReconnecting = 0;
			
			onParents.onConected = true;
			if (onParents.onConected)
			{
				onParents.onSetSockError(onParents, "Connected");
			}
		}
		
		// next if success connect call mya method 
		if ( onParents.onConected && app_func_isfunc(onParents.onConnect) )
		{
			onParents.onConnect.apply(this, [onParents, evt]);	
		}
		
		return 0;
	}
	
	
	this.onSocket.onmessage = function(evt)
	{
		writeloger(sprintf('Initialize Receive message ws:socket'));  
		if ( app_func_isfunc(onParents.onThread) )
		{
			onParents.onThread.apply(this, [onParents, evt.data]);
			return 0;
		}	
	}
	
	this.onSocket.onclose = function(evt)
	{
		writeloger('Initialize close ws:socket');
		if( window.sockInterupted )
		{
			writeloger('Interupt close ws:socket');
			window.sockInterupted = 0;
			return 0;
		}
		
		window.sockTimeout = window.setTimeout(() => {
			onParents.onConected = false;
			if (!onParents.onConected && app_func_isfunc(onParents.onTimeout))
			{
				onParents.onTimeout.apply(this, [onParents, evt]);
			}
		}, onParents.onSecWait );
		
		return 0;
	} 
	
	this.onSocket.onerror = function(evt)
	{
		if( app_func_isfunc(onParents.onError))
		{
			onParents.onError.apply(this, [onParents, evt]);
		}
	}
	
	return 0;
}  

 
/*! \retval chatPro.onConnect */
 chatPro.prototype.onConnect = function(chat, event)
{
	writeloger("Initialize Connect ws:socket");
	if ( app_func_isfunc(chat.onStart) ){
		chat.onStart.apply(this, [chat, event]);
	}
	return 0;
};

/*! \retval chatPro.onThread */
 chatPro.prototype.onThread  = function(chat, data)
{
	var onEvents = null;
	try 
	{
		onEvents = JSON.parse(data);
		if ( app_func_isobj(onEvents) && app_func_isfunc(chat.onReceiveMessage) ){
			chat.onReceiveMessage.apply(this, [onEvents,chat]);
		}
	}
	/*! \remark ter indikasi terjadi sebuah kesalahan process */
	catch(error)
	{
		writeloger("dump.socket.error");
		console.log(error);
		onEvents = data;
		if (app_func_isfunc(chat.onErrorMessage)){
			chat.onErrorMessage.apply(this, [onEvents, chat, error]);
		}
	}
	
	return 0;
							
};  


/*! \retval chatPro.onTimeout */
 chatPro.prototype.onTimeout = function(chat, event)
{
	
	writeloger('Initialize Timeout ws:socket');  
	window.clearTimeout(window.sockTimeout);
	
	/*! \remark create indikator */
	window.sockReconnecting = parseInt(window.sockReconnecting)+1;
	
	writeloger("Try Connected"); 
	chat.onSetSockError( chat, 
		sprintf("Reconnecting (%s) ...", window.sockReconnecting)
	);
	
	/*! \remark try connected */
	window.sockTimeout = window.setTimeout(() => {
		chat.onSetSockConnect(chat.onHost, chat.onPort);
	}, 3000);	 
};

/*! \retval chatPro.onError */
/*! \link https://developer.mozilla.org/en-US/docs/Web/API/WebSocket/readyState 
	0:CONNECTING, 1:OPEN, 2:CLOSING, 3:CLOSED
*/
 chatPro.prototype.onError = function(chat, event)
{
	writeloger('Initialize error ws:socket'); 
	if (event.originalTarget.readyState == WS_CODE_CLOSED ) {
		writeloger(sprintf("Scoket Event State '%s'", 'CLOSED')); 
	} else if(event.originalTarget.readyState == WS_CODE_CLOSING ){ 
		writeloger(sprintf("Scoket Event State '%s'", 'CLOSING'));  
	}
	return 0;
};


/*! \retval chatPro.onSend */
 chatPro.prototype.onSend = function(str, callback)
{
	this.onSocket.send(str);
	if( typeof callback == 'function' ) {
		callback.apply(this, [this, str]);
	}
	
	return 0; 
};


/*! \retval chatPro.onWrite */
 chatPro.prototype.onWrite = function(str)
{
	writeloger('Initialize error ws:socket'); 
	window.writeobj(chat);	
	window.writeobj(event);	
}; 