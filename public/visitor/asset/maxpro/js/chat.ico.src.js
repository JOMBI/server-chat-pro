/*!
 * SCP -- An open source Server Chat Pro
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
// https://www.w3schools.com/charsets/ref_emoji_skin_tones.asp
"use strict"

// list object char emoticon on 'Hexadecimal'
const elementsEmojis = 
[	
	0x1F642,	// &#128578
	0x1F601, 	// &#128513
	0x1F602, 	// &#128514
	0x1F603, 	// &#128515
	0x1F60D, 	// &#128525
	0x1F60A, 	// &#128522
	0x1F614, 	// &#128532
	0x1F613, 	// &#128531
	0x1F633, 	// &#128563
	0x1F631, 	// &#128561
	0x1F621,  	// &#128545
	0x1F648, 	// &#128584
	0x1F62D, 	// &#128557
	0x1F44D, 	// &#128077
	0x1F389,	// &#127881
	0x1F605,	// &#128517
	0x1F932,	// &#129330
	0x1F64F		// &#128591	
];
 

// list object from hexa to Html Char "elementsEmotHexa"
 const elementsEmotHexa = 
{
	'1f642' : '&#128578;',
	'1f601' : '&#128513;',
	'1f602' : '&#128514;',
	'1f603' : '&#128515;',
	'1f60d' : '&#128525;',
	'1f60a' : '&#128522;',
	'1f614' : '&#128532;',
	'1f613' : '&#128531;',
	'1f633' : '&#128563;',
	'1f631' : '&#128561;',
	'1f621' : '&#128545;',
	'1f648' : '&#128584;',
	'1f62d' : '&#128557;',
	'1f44d' : '&#128077;',
	'1f389' : '&#127881;',
	'1f605' : '&#128517;',
	'1f932' : '&#129330;',
	'1f64f' : '&#128591;'
 };             

 
// https://stackoverflow.com/questions/59918250/how-to-replace-all-emoji-in-string-to-unicode-js
// for Expresion find process 	
 const generateElementsRegexUnicode = (uniqcode, callback) => {
	const str = uniqcode;
	const rex = /[\u{1f300}-\u{1f5ff}\u{1f900}-\u{1f9ff}\u{1f600}-\u{1f64f}\u{1f680}-\u{1f6ff}\u{2600}-\u{26ff}\u{2700}-\u{27bf}\u{1f1e6}-\u{1f1ff}\u{1f191}-\u{1f251}\u{1f004}\u{1f0cf}\u{1f170}-\u{1f171}\u{1f17e}-\u{1f17f}\u{1f18e}\u{3030}\u{2b50}\u{2b55}\u{2934}-\u{2935}\u{2b05}-\u{2b07}\u{2b1b}-\u{2b1c}\u{3297}\u{3299}\u{303d}\u{00a9}\u{00ae}\u{2122}\u{23f3}\u{24c2}\u{23e9}-\u{23ef}\u{25b6}\u{23f8}-\u{23fa}]/ug;
	const updated = str.replace(rex, match => elementsEmotHexa[`${match.codePointAt(0).toString(16)}`] );
	
	if ( typeof(callback) == 'function')
	{
		callback.apply(this, [updated]);
	}
	return 0;
}

// function convert char Uniquecode to Hexadecimal 
 const generateElementsUnicode = (unicode, callback) => {
	var comp, emot = unicode;
	if (emot.length === 1) {
		comp = emot.charCodeAt(0);
	}
	
	comp = ( (emot.charCodeAt(0) - 0xD800) * 0x400 + (emot.charCodeAt(1) - 0xDC00) + 0x10000 );
	if (comp < 0) {
		comp = emot.charCodeAt(0);
	}
	
	var hexadecimal = comp.toString('16'), htmlchar = elementsEmotHexa[hexadecimal], ret_emot = {
		emot_dexc : unicode,
		emot_hexa : hexadecimal,
		emot_char : htmlchar,
	}
	
	if (typeof callback == 'function')
	{
		callback.apply(this, [ret_emot]);
	}
	
	return 0;
}
	
 const generateElements = (emojiInput, emojiBinding) => {
	
	// handler Onclick List EmojiPicker
	const clickLink = (event) => {
		
		generateElementsUnicode(event.target.innerHTML, (res) => {
			emojiBinding.val( emojiBinding.val() + res.emot_dexc); 
			onBoxTextHtml.val( onBoxTextHtml.val() + res.emot_char );
			
			$(emojiPicker).hide(100,() => {
				emojiBinding.focus();
			}); 
		});
		
		
		// trigger ng-change for angular
		if (typeof angular !== "undefined") 
		{
			angular.element(emojiInput).triggerHandler('change')
		}
	}

	emojiInput.style = "width: 100%"

	const emojiContainer = document.createElement('div')
		emojiContainer.style = "position: relative;"

	const parent = emojiInput.parentNode
		parent.replaceChild(emojiContainer, emojiInput)
		emojiContainer.appendChild(emojiInput)

  
	const emojiPicker = document.createElement('div')
	emojiPicker.setAttribute('class', 'app-chat-emoji')
	emojiPicker.style = "position: absolute;"+
	"right: -40px;"+
	"top: -110px;"+
	"z-index: 999;"+
	"display: none;"+ 
	"padding: 5px 2px 5px 5px;"+
	"margin-top: 5px;"+
	"overflow: hidden;"+
	"background: #fff;"+
	"border: 1px #dfdfdf solid;"+
	"border-radius: 3px;"+
	"box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);";


	const emojiTrigger = $("<a href='javascript:void(0);' class='publisher-btn display-handler'></a>");
	emojiTrigger.html("<i class='fa fa-smile'></i>");
	emojiTrigger.click(() => {
		const wsize = $('.col-md-6').width() - 35;
		if (parseInt(wsize)){
			$(emojiPicker).width(wsize);
		}
		emojiPicker.style.display = emojiPicker.style.display === 'block' ? 'none' : 'block';
	}); 
	 
	$(emojiContainer).append(emojiTrigger);
	const emojiList = $('<ul></ul');
		$(emojiList)
			.attr('style', 'border:none;padding: 0;margin: 0;list-style: none;'); 
			
	// https://www.rapidtables.com/convert/number/decimal-to-hex.html		
	// hexa decimal look at from decimal to hexa 		
	
	elementsEmojis.map( (item) => {
	const emojiLi = $('<li style="display: inline-block;margin: 5px;"></li>');  
	const emojiLink = $('<a href="javascript:void(0);"></a>')
		$(emojiLink)
		.attr('style', 'text-decoration: none;margin: 5px;position: initial; font-size: 24px;')
		.html(String.fromCodePoint(item))
		.click( clickLink );
		
		$(emojiList).append(emojiLink); 
	});

	$(emojiPicker).append(emojiList);
	$(emojiContainer).append( $(emojiPicker) );
}

window.EmojiPicker = {
	// init handleEvent process */
	init: function(){
		const emojiBinding = $('#text-msg');
		const emojiInputs = document.querySelectorAll('[data-emoji="true"]'); 
    
		// find for each 'generateElements'	
		emojiInputs.forEach((element) => {
			generateElements(element, emojiBinding);
		});
	}
}

// detatch Event resize window 
$(window).resize(() => {
	const wsize = ( $('.page-container').find('.col-md-6').width() - 35 );
	if ( parseInt(wsize) ){
		$('.app-chat-emoji').width(wsize);
	}
});