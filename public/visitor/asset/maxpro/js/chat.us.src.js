/*!
 * SCP -- An open source Server Chat Pro
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * \link https://www.freecodecamp.org/news/how-to-secure-your-websocket-connections-d0be0996c556/
 *
 * \link https://sodocumentation.net/webrtc/topic/5641/webrtc-simple-examples 
 *
 */
  
 
/*! \retval Global Variable */ 
 window.onBoxDisplay  = null;
 window.onBoxContent  = null;
 window.onBoxTextMsg  = null;
 window.onBoxConnect  = null;
 window.onBoxRinging  = false;
 window.onBoxDialing  = false;
 window.onBoxLibrary  = false;
 
/*! \remark test load Audio */
 window.onBoxOnError  = 0; 
 window.onBoxOnAudio  = 
{
	message : "//{!APP_SERVER_DOMAIN}/{!APP_URL_MGR}/asset/sound/message-alert.mp3",
	ringing : "//{!APP_SERVER_DOMAIN}/{!APP_URL_MGR}/asset/sound/ringing-alert.mp3"
};
 
 /*! \remark create for visitor session */
 window.onUserSession = 
{
	ws_prog		 : '{!APP_SERVER_TITLE}',  
	ws_versi	 : '{!APP_SERVER_VERSI}', 
	ws_copy		 : '{!APP_SERVER_COPY}' ,
	ws_host 	 : '{!APP_SOCKET_HOST}' ,
	ws_port 	 : '{!APP_SOCKET_PORT}' ,
	ws_domain    : '{!APP_CLIENT_DUID}'	,
	ws_group	 : '{!APP_CLIENT_WVST}',
	ws_uid		 : 0,
	ws_userid 	 : '',
	ws_channel   : '',
	ws_channel2  : '',
	ws_username  : '',
	ws_useremail : '',
	ws_session   : '',
	ws_regtime   : '',
	ws_status    : '',
	ws_state     : '',
	ws_chatboxs  : {}
};

/*! \retval Static Variable */ 
 const EVT_META_USER = 100;
 const EVT_META_SYST = 200;
 const EVT_META_HAND = 300;
 const EVT_META_MESG = 400;
 

/*! \retval sent onWebHooks */
 function onVisitorHookAction(meta, action, callback)
{
	/*! \remark indication of Error */ 
	window.onBoxOnError = onVisitorGetPeerStatus();
	if ( parseInt(window.onBoxOnError) < 0) {
		writeloger(sprintf( "onVisitorHookAction:Status = %s", 
			window.onBoxOnError
		));
		return 0;
	}
	
	var url = window.sprintf('/{!APP_URL_VST}/action/%s.hook', action);
	$.ajax ({
		type :'POST', url: url, data: meta, 
		success : ( res ) => {
			if (typeof callback == 'function') {
				callback.apply(this, [res]);
			}
		}
	});
	/*! \retval hooker */
	return 0;
}

/*! \retval Implements 'onVisitorHookLoger' */
 function onVisitorHookLoger(data, callback)
{
	/*! \remark mengambil data history chat yang sedang dan sudah terjadi di stream socket melalui Hook */
	onVisitorHookAction(data, 'loger', (Loger) => {
		/*! \retval check is valid object */
		if ( !app_func_sizeof(Loger) ) {
			return 0;
		}
		
		/*! if Error to next flow */
		if ( Loger.error ==1 ) 
		{ 
			if( app_func_isfunc(callback) ){
				callback.apply(this, [false]);
			}
			return 0;
		} 
		
		/*! if Error to next flow */
		if ( Loger.error == 2 ) 
		{
			Loger.data.time = app_func_minute(Loger.data.time);
			onVisitorSysMessge(Loger.data, (onText) => {
				onText.show(200, ()=>{
					window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
				});
			}); 
			
			if( app_func_isfunc(callback) ){
				callback.apply(this, [false]);
			}
			return 0;
		} 
		
		/*! \remark for iteration this */
		var retval = 0, msg = false;
		for (var i in Loger.data)
		{
			/*! \remark jika data yang di iterasi bukan Object sebaiknya di skip */
			msg = Loger.data[i];
			if (!app_func_isobj(msg))
			{
				continue;
			}			
			
			/*! \remark untuk chat yang berasal dari Luar atau incoming */
			if( msg.sfm == 1 ){  
				msg.time = app_func_minute(msg.time);
			}
			
			if( msg.sfm == 2 ){  
				msg.time = app_func_minute(msg.time);
			}
			
			msg.audio = false;
			if ( msg.sfm == 1 ){
				onVisitorRcvMessage(msg, (onText) => {
					onText.show(500, () => {
						onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
					});
				});
			}
			
			/*! \remark untuk chat yang di kirim ke luar atau outbound */
			if ( msg.sfm == 2 ){
				onVisitorBoxMessge(msg, (onTextTime) => {
					onTextTime.html(msg.time);
				});
			}
			/*! \remark untuk chat merger jika chat lebih dari satu channel */
			if ( msg.sfm == 3 )
			{
				msg.text = sprintf("Chat Session \"<b>%s</b>\"", msg.msgbox);
				msg.time = app_func_second(msg.time); 
				onVisitorLogMessge(msg, (onTextTime) => {
					console.log("loger devider");
				});
			}
			
			/*! \remark untuk chat chat dengan session Box ada namun belum ada percakapan  */
			if (msg.sfm == 4)
			{
				msg.text = sprintf("Active Session \"<b>%s</b>\"", msg.msgbox);
				msg.time = app_func_second(msg.time); 
				onVisitorLogMessge(msg, (onTextTime) => {					
					/*! \remark if find its then get this show discard */
					var sfv = new onVisitorFetchStorage(), onData = {};
					if ( app_func_isobj(sfv) )
					{
						onData.session   = sfv.get('ws_session');
						onData.channel   = sfv.get('ws_channel');
						onData.chatboxid = sfv.get('ws_chatboxid');
						onData.username  = sfv.get('ws_username');
						onData.regtime   = sfv.get('ws_regtime');
						
						if (onData.chatboxid)
						{
							window.setTimeout(() => { 
								onVisitorHookButton(onData);
							}, 500); 
						} 
					}		 
				});
			}
			
			/*!  \remark for callback method */
			retval = retval + 1; 
		}
		
		/*! \remark sent to callback function */
		if ( app_func_isfunc(callback) )
		{
			callback.apply(this, [retval]);
		}
		
		return 0;
	}); 
 }

/*! \retval Implements 'onVisitorHookRegister' */
 function onVisitorHookWelcome(data, flow)
{
	/*! \remark khussus ketika terjadi register */
	if (typeof flow != 'undefined' ){
		data.flow = flow;
	}
	
	onVisitorHookAction(data, 'welcome', (res) => {
		/*! \retval check is valid object */
		if ( !app_func_sizeof(res) ) {
			return 0;
		}
		
		if ( res.error ==1 ) { 
			return 0;
		}
		
		onVisitorSysMessge(res.data, (onText) => {
			onText.show(1000, () => {
				window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	 
			});
		});
		
		return 0;
	});
 } 
 
 
 /*! \retval Implements 'onVisitorHookClosing' */
 function onVisitorHookClosing(data, flow)
{
	/*! \remark khussus ketika terjadi register */
	if (typeof flow != 'undefined' ){
		data.flow = flow;
	}
	
	onVisitorHookAction(data, 'closing', (res) => {
		/*! \retval check is valid object */
		if ( !app_func_sizeof(res) ) {
			return 0;
		}
		
		if ( res.error ==1 ) { 
			return 0;
		}
		
		onVisitorSysMessge(res.data, (onText) => {
			onText.show(1000,() => {
				window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
				window.setTimeout( () => 
				{
					/*! \remark close socket */
					chatApps.onSetSockClose(1);   
					onVisitorDeleteStorage(() => 
					{
						/*! \remark delete semua attributes yang ada di Global window */
						onVisitorSetSession('ws_uid'	  , '');
						onVisitorSetSession('ws_userid'	  , '');
						onVisitorSetSession('ws_channel'  , '');
						onVisitorSetSession('ws_channel2' , '');
						onVisitorSetSession('ws_username' , '');
						onVisitorSetSession('ws_useremail', '');
						onVisitorSetSession('ws_session'  , '');
						onVisitorSetSession('ws_regtime'  , '');
						onVisitorSetSession('ws_status'	  , '');
						onVisitorSetSession('ws_chatboxid', '');
						onVisitorSetSession('ws_chatboxs' , {});
						
						/*! \remark setelah di clear baru masuk ke process pencarian session */
						onVisitorIniateSession( ( handler, register) => { 
							if( register ) handler.onVisitorStartSession();
							else if( !register ){
								handler.onVisitorFormRegister(); 
							}
						});
						
					});  
					
				}, 3000); 
			
			});
		
		});
		 
		return 0;
		
	});
 }
 
 
 /*! \retval Implements 'onVisitorHookRegister' */
 function onVisitorHookError(data, error)
{
	/*! \remark khussus ketika terjadi register */
	if (typeof error != 'undefined' ){
		data.error = error;
	}
	
	/*! \retval check is valid object */
	onVisitorHookAction(data, 'error', (res) => {
		
		if ( !app_func_sizeof(res) ) {
			return 0;
		}
		
		if ( res.error ==1 ) { 
			return 0;
		}
		
		onVisitorSysMessge(res.data, (onText) => {
			onText.show(200, ()=>{
				window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
			});
		});
		
		return 0;
	}); 
 }
/*! \retval Implements 'onVisitorHookUnregister' */
 function onVisitorHookUnregister() 
{
	/*! \remark starting to search agent on available */
	var client = new onVisitorFetchStorage();
	var data = {
		uid 	 	: client.get('ws_uid'),
		session  	: client.get('ws_session'),
		channel  	: client.get('ws_channel'),
		userid	 	: client.get('ws_userid'),
		username 	: client.get('ws_username'),
		useremail	: client.get('ws_useremail'),
		userdomain 	: client.get('ws_domain')
	};
	
	/*! \remark sent Request to object Hooker */
	onVisitorHookAction(data, 'unregister', (res) => {
		return 0;
	});
}

/*! \remark onVisitorHookRouting */
 function onVisitorHookRouting(channel2)
{
	var user = new onVisitorFetchStorage();	
	var data = {
		channel1 : user.get('ws_channel'), 
		channel2 : channel2
	};
	
	/*! \remark sent Request to object Hooker */
	onVisitorHookAction(data, 'routing', (res) => {
		/*! \retval check is valid object */
		if ( !app_func_sizeof(res) ) {
			return 0;
		}
		
		if ( res.error ==1 ) {
			writeloger(sprintf("Routing '%s' ", res.message));
			return 0;
		}
	});
	return 0;
}

/*! \remark onVisitorHookDiscard / Tolak */
 function onVisitorHookDiscard(chatboxid)
{
	// media-163305436800010
	var sfm  = new onVisitorFetchStorage(), sfc = sfm.data();
	var data = {
		channel   : sfm.get('ws_channel'),
		session	  : sfm.get('ws_session'),
		username  : sfm.get('ws_username')
	}
	
	if (!app_func_isobj(data)){
		return 0;
	}
	
	/*! \remark sent Request to object Hooker */ 
	data.chatboxid = chatboxid;
	onVisitorHookAction(data, 'discard', (res) => { 
		if (!app_func_sizeof(res)) 
		{	
			return 0;
		}
		
		// true process 
		if (res.error == 0 ){
			var onText = $(sprintf('.media-%s', chatboxid));
			onText.hide(500,()=> {
				onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
			}); 
		}			
		
		if ( res.error == 1 )  
		{
			writeloger( sprintf("Discard '%s' ", res.message) );
			return 0;
		}
	});
	return 0;
}

/*! \remark onVisitorHookButton / Tolak */
 function onVisitorHookButton(data, callback) 
 {
	onVisitorHookAction(data, 'button', (Button) => { 
	
		/*! \note if not object */
		if (!app_func_sizeof(Button)) {	
			return 0;
		}
		
		/*! \note Button error */
		if ( Button.error == 1 ) {
			writeloger( sprintf("button '%s' ", res.message) );
			return 0;
		}
		
		if ( Button.error == 0 ) 
		{
			Button.data.time = app_func_minute(Button.data.time);
			onVisitorSysMessge(Button.data, (onText) => {
				onText.show(200, ()=>{
					window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
				});
			}); 
			
			if( app_func_isfunc(callback) ){
				callback.apply(this, [false]);
			}
			return 0;
		}
	});
 }

/*! \remark onVisitorHookPickup */
 function onVisitorHookPickup(data)
{
	var user = new onVisitorFetchStorage();	
	if ( !app_func_isobj(data)){
		return 0;
	}
	
	/*! \remark sent Request to object Hooker */
	delete data.text;
	onVisitorHookAction(data, 'pickup', (res) => {
		/*! \retval check is valid object */
		if ( !app_func_sizeof(res) ) {
			return 0;
		}
		
		if ( res.error ==1 ) {
			writeloger(sprintf("Routing '%s' ", res.message));
			return 0;
		}
	});
	return 0;
}


/*! \retval Implements 'onVisitorHookRegister' */
 function onVisitorHookRegister() 
{
	var data = {
		username   : $('#InputUsername').val(),
		useremail  : $('#InputEmail').val(),
		userdomain : window.onUserSession.ws_domain,	
		sessionid  : ''
	};
	
	/*! \remark sent Request to object Hooker */
	onVisitorHookAction(data, 'register', (res) => {
		/*! \retval check is valid object */
		if ( !app_func_sizeof(res) ) {
			return 0;
		}
		
		if ( res.error ==1 ) {
			alert(res.message);
			return 0;
		}
		
		/*! \retval next if true */
		
		onVisitorSetSession('ws_uid'	  , res.data.uid);
		onVisitorSetSession('ws_channel'  , res.data.channel);
		onVisitorSetSession('ws_userid'	  , res.data.userid);
		onVisitorSetSession('ws_session'  , res.data.session);
		onVisitorSetSession('ws_username' , res.data.username);
		onVisitorSetSession('ws_useremail', res.data.email);
		onVisitorSetSession('ws_regtime'  , res.data.regts); 
		
		/*! \remark session storage */
		onVisitorCreateStorage(window.onUserSession);
		
		/*! \remark on success then start of session */
		onVisitorIniateSession(( handler, register) => {
			if( register ){
				window.onVisitorBoxDisplay(true, () => { 
					handler.onVisitorStartSession(); 
				}); 
			}
		});
		
		return 0;
	});
}
/*! \retval Implements 'onVisitorHookRouting' */
 function onVisitorHookSearch() 
{
	/*! \remark starting to search agent on available */
	var client = new onVisitorFetchStorage();
	var data = {
		uid 	 	: client.get('ws_uid'),
		group 		: client.get('ws_group'),
		session  	: client.get('ws_session'),
		channel  	: client.get('ws_channel'),
		userid	 	: client.get('ws_userid'),
		username 	: client.get('ws_username'),
		useremail	: client.get('ws_useremail'),
		userdomain 	: client.get('ws_domain')
	};
	
	/*! \remark sent Request to object Hooker */
	onVisitorHookAction(data, 'search', (res) => {
		/*! \retval check is valid object */
		if ( !app_func_sizeof(res) ) {
			return 0;
		}
		
		if ( res.error ==1 ) {
			alert(res.message);
			return 0;
		}
		
		// tidak ada user yang Online
		if ( res.error == 2 ) { 
			onVisitorSysMessge(res.data, (onText) => {
				onText.show(200, ()=>{
					window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
				});
			}); 
			return 0;
		}
		
		// Lagi Sibuk Cari Operator 
		if ( res.error == 3 ) { 
			return 0;
		}
		
		onVisitorUsrMessage(res.data, (onText) => {
			onText.show(200, ()=>{
				window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
			});
		}); 
		
		return 0;
	});
}



/*! \retval Implements 'onVisitorSockEvents' get Message from socket */ 
 function onVisitorSockEvents(recv, chat)
{
	/*! \remark if false of object */
	if ( !app_func_isobj(recv) ){
		return 0;
	}
	
	var eventsMeta = recv.meta,
		eventsType = recv.type,
		eventsMake = null,
		eventsText = null;
		
	var eventsTimeout = 0;  
	var eventsDivider = {};
	if (recv.event != 'pong') {
		console.log(recv); 
	}
	
	/*! \retval this event for event "Handler" format */
	if (eventsMeta == EVT_META_HAND && eventsType == 'event')
	{
		eventsEvent = recv.event;
		eventsData  = recv.data;
		
		/*! \retval get message pong */
		if (eventsEvent == 'pong' && eventsData.tick) {
			window.onBoxConnect
				.html(sprintf("Time: %s", eventsData.tick)); 
		}
		
		/*! \retval get message dead your PID */
		if (eventsEvent == 'dead' && eventsData.tick) 
		{
			onVisitorSetPeerStatus(eventsData.status, (onStatus) => 
			{  
				onBoxConnect.html( 'Error' ).css({'color': 'red'});  
				eventsData.text = sprintf("Close ( <b>%s</b> ) Thread of session , Multiple open of task ..!", eventsData.pid );
				onVisitorErrMessage(eventsData, (onText) => {
					onText.show(200, ()=>{
						window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);
						chatApps.onSetSockClose(1);   /*! close socket interupted */	 
					});
				}); 
			}); 
			
			return 0;
		}
		
		/*! \retval get message Error */
		if (eventsEvent == 'error') {
			window.onBoxConnect
				.html( eventsData.text );
		}
		
		/*! \retval get message Error */
		if (eventsEvent == 'dialing') 
		{	
			if ( app_func_isobj(onUserSession.ws_chatboxs) &&( Object.keys(onUserSession.ws_chatboxs).length < 2 )) {
				window.onBoxContent.html('');	
			}
			
			/*! \remark for tagline export this*/
			eventsDivider.cid = eventsData.cid;
			eventsDivider.msgbox = eventsData.chatboxid;
			eventsDivider.time = app_func_second(eventsData.time); 
			if ( eventsDivider.cid )
			{ 
				eventsDivider.text = sprintf("Start Session \"<b>%s</b>\"", eventsDivider.msgbox); 
				onVisitorLogMessge(eventsDivider, (onTextTime) => {
					console.log("loger devider");
				});
			}
			
			
			eventsData.text = sprintf(" %s on <b>Ringing </b>...", eventsData.callerid); 
			onVisitorEvtMessage(eventsData, (onText) => {
				writeloger(sprintf("Ringing On '%s'", eventsData.callerid));
				onText.show(200, () => {
					window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
				});
			});
		}
		
		/*! \retval get message Error */
		if (eventsEvent == 'ringing') 
		{
			/*! \remark Play Audio from 'onVisitorAudioHandler' */
			onVisitorAudioHandler((s) => {
				if (onBoxRinging = s.ringing) {
					onBoxRinging.play();	
				}
				return 0;	
			}); 
			
			/*! \remark harus di check apakah sebelumnya punya box id 
				jika iya box tidak perlu di delete 
			*/
			eventsData.text = sprintf("<b>Invite</b> ... from %s", eventsData.callerid);   
			if ( app_func_isobj(onUserSession.ws_chatboxs) &&( Object.keys(onUserSession.ws_chatboxs).length < 1 )) {
				onBoxContent.html('');	
			}				
			
			onVisitorEvtMessage(eventsData, (onText, eventsData) => { 
				eventsTimeout = (parseInt(eventsData.timeout) *1000)
				writeloger(sprintf("Invite from '%s'", eventsData.callerid)); 
				onText.show(200, () => {
					onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);		
					window.setTimeout(() => {
						onVisitorHookPickup(eventsData);
					}, eventsTimeout);
				});   
			});	
		}
		
		/*! \retval get message Error */
		if (eventsEvent == 'connect') 
		{
			eventsData.text = sprintf("%s on <b>Pickup</b> ...", eventsData.callerid);  
			onVisitorEvtMessage(eventsData, (onText, eventsData) => {
				eventsTimeout = (parseInt(eventsData.timeout) *1000)
				writeloger(sprintf("Pickup on '%s'", eventsData.callerid));
				onText.show(200, () => {
					onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100); 
				});  
			});	
		}
		
		/*! \retval get message Error */
		if (eventsEvent == 'up') 
		{
			eventsData.text = sprintf("<b>Connected</b> ... with %s", eventsData.callerid);  
			onVisitorEvtMessage(eventsData, (onText, eventsData) => {
				eventsTimeout = (parseInt(eventsData.timeout) *1000)
				writeloger(sprintf("Connected on '%s'", eventsData.callerid));
				onText.show(200, () => {
					onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100); 
				});   
			});	
			
			/*! \remark Stop Audio from 'onVisitorAudioHangup' */
			onVisitorAudioHangup((s) => {
				writeloger("Stop Ringing"); 
				return 0;
			});
		}
		
		/*! \retval get message Error "Bye" from channel */
		if (eventsEvent == 'discard') 
		{
			eventsData.time = app_func_minute(eventsData.time);
			if ( eventsData.discard == 1){
				eventsData.text = sprintf( "<b>Reject</b> Chat session <b>%s</b> for <b>%s</b>", 
					eventsData.chatboxid, eventsData.callerid
				);  
			}
			else if ( eventsData.discard == 2){
				eventsData.text = sprintf( "<b>Rejected</b> Chat session <b>%s</b> by <b>%s</b>", 
					eventsData.chatboxid, eventsData.callerid
				);  
			}
			
			onVisitorEvtMessage(eventsData, (onText, eventsData) => {
				eventsTimeout = (parseInt(eventsData.timeout) *1000)
				writeloger(sprintf("'%s' Sent Discard", eventsData.callerid)); 
				onText.show(200, () => {
					onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100); 
				});   
			});	
		}
		
		/*! \retval get message Error "Bye" from channel */
		if (eventsEvent == 'bye') 
		{
			eventsData.time = app_func_minute(eventsData.time);
			eventsData.text = sprintf( "<b>%s</b> Sent <b>Bye</b> Close Chat session '<b>%s</b>'", 
				eventsData.callerid, eventsData.chatboxid
			);  
			
			onVisitorEvtMessage(eventsData, (onText, eventsData) => {
				eventsTimeout = (parseInt(eventsData.timeout) *1000)
				writeloger(sprintf("'%s' Sent Bye", eventsData.callerid)); 
				onText.show(200, () => {
					onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100); 
				});  
			});	
		}
	}
	
	/*! \retval this event for event Message format */
	if (eventsMeta == EVT_META_MESG && eventsType == 'event')
	{
		eventsEvent = recv.event;
		eventsError = recv.error;
		eventsData  = recv.data;
		
		
		/*! \remark  for Outgoing Message */
		if (eventsEvent == 'sent') 
		{
			eventsMake = sprintf('.time-%s', eventsData.cid);
			$(eventsMake).text(eventsData.time);
			
			/*! check pesan yang di kirim untuk di proces di Hooker */
			/*! jika error ==1 tidak ada chat tujuan */
			if (eventsError == 1){
				onVisitorHookError(eventsData, eventsError);
			}
		} 
		
		/*! \remark  for Incoming Message */
		if (eventsEvent == 'recv') 
		{
			eventsMake = sprintf('.time-%s', eventsData.cid);
			$(eventsMake).text(eventsData.time);
			
			/*! check pesan yang di kirim untuk di proces di Hooker */
			/*! jika error ==1 tidak ada chat tujuan */
			if (eventsError == 1){
				onVisitorHookError(eventsData, eventsError);
			}
			
			eventsData.audio = true;
			onVisitorRcvMessage(eventsData, (onText, onData) => { 
				onText.show(200, ()=>{
					window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
				});
			});
		} 
		
		/*! \remark for State Typing from Channel */
		if (eventsEvent == 'typing') 
		{
			if (eventsData.text == 1)
			{ 
				onBoxStatus.show(5, () => {
					$( '.text-typing' )
						.html('is typing ...'); 
				});
			}
			else if (eventsData.text == 0) 
			{
				onBoxStatus.hide(1, () => {
					$( '.text-typing' )
						.html('');
				});
			}
		}
	}
	
	/*! \retval this event for event "User" format */
	if (eventsMeta == EVT_META_USER && eventsType == 'event')
	{
		eventsEvent = recv.event;
		eventsData  = recv.data;
		
		/*! \retval get message pong */
		if (eventsEvent == 'register') 
		{
			/*! \remark set status Before Next process */
			onVisitorSetPeerStatus(eventsData.status, (onStatus) => 
			{
				window.onBoxContent.html('');
				
				/*! \remark jika pernah chating lalu melkukan reload page check jika sudah punya chatboxid maka check dulu apakah pernah chat */
				var sfg = new onVisitorFetchStorage(); 
				if (app_func_sizeof(sfg.get('ws_chatboxid')) < 5)
				{
					onVisitorHookWelcome(eventsData, 1);
					window.setTimeout(() => {
						onVisitorHookWelcome(eventsData, 2);
					},1000); 	
				} 
				else if (app_func_sizeof(sfg.get('ws_chatboxid')) > 5) 
				{
					eventsData.chatboxid = sfg.get('ws_chatboxid');
					if ( eventsData.chatboxid )
					{
						onVisitorHookLoger(eventsData, (cond) => {
							console.log("history");	
						});
					}
				} 
				
			}); 
		}
		
		/*! \retval get message pong */
		if (eventsEvent == 'unregister') 
		{   
			onVisitorSetPeerStatus(eventsData.status, (onStatus) => {
				onVisitorHookClosing(eventsData,1);	
			}); 
		}
	}
	
	return 0;
}

/*! \retval Implements 'onVisitorSockErrors' */
 function onVisitorSockErrors(recv, chat) 
{
	console.log('onErrorMessage');
	console.log(recv);
}

/*! \retval Implements 'onVisitorSentColor' */
 function onVisitorSentColor(cid, callback)
{
	onBoxContent
		.find( sprintf('.tick-%s', cid))
			.css({ 'color' : 'rgb(233, 240, 253)' });	// #a8c1f0 
			
	window.setTimeout( () => {
		onBoxContent
		.find( sprintf('.tick-%s', cid))
			.css({ 'color' : 'rgb(168, 193, 240)' });	// #a8c1f0
	},2000);
	
	return 0;	
 }
 
 
/*! \retval Implements 'onVisitorLogMessge' */
 function onVisitorLogMessge(msg, callback)
{
	var html = sprintf( 
		"<div class=\"%s media media-meta-day loger-divider-%s\">"+
			"<p class='meta'>"+ 
				"<span class='time-%s'>Today %s</span>"+ 
			"</p>"+	
			"<p>%s</p>"+ 
			"</div>",	 
			msg.msgbox, msg.cid, msg.cid, msg.time, msg.text
	);
	
	onBoxContent
		.append( html )
			.animate({ scrollTop: onBoxContent[0].scrollHeight }, 100); 
			
	if ( app_func_isfunc(callback) )
	{
		var onText = $( sprintf('.loger-divider-%s', msg.cid));
		callback.apply(this, [onText]);
	}		
	return 0;	
 }  

/*! \retval Implements 'onVisitorBoxMessge' */
 function onVisitorBoxMessge(msg, callback)
{
	var html = window.sprintf
	( 
		"<div class='%s media media-chat media-chat-reverse %s'>"+
			"<div class='media-body'>"+
				"<p>%s</p>"+
				"<p class='meta'>"+ 
					"<span class='time-%s'></span>"+ 
					"<span class='tick-%s' style='margin-left:10px;font-size:12px;'>"+ 
						"<i class='fas fa-check-double'></i>"+ 
					"</span>"+ 
				"</p>"+ 
			"</div>"+
		"</div>", msg.msgbox, msg.cid, msg.text, msg.cid, msg.cid
	);			
	
	onBoxContent
		.append( html )
			.animate({ scrollTop: onBoxContent[0].scrollHeight }, 100); 
			onVisitorSentColor(msg.cid);
			
	if ( app_func_isfunc(callback) )
	{
		var eventsTime = $( sprintf(".time-%s", msg.cid) );
		if ( app_func_isobj(eventsTime) )
		{ 
			callback.apply(this, [eventsTime]);
		}
	}		
	return 0;	
}

/*! \retval Implements 'onVisitorSysMessge' */
 function onVisitorSysMessge(msg, callback)
{
	var html = sprintf("<div class='media media-chat media-%s'> <img class='avatar avatar-%s' src='/{!APP_URL_VST}/asset/maxpro/image/3861075.png' alt='...'>"+
					"<div class='media-body media-body-%s'>"+
						"<p class='text-%s'>%s</p>"+ 
						"<p class='meta meta-%s'><span class='time-%s'>%s</span></p>"+
					"</div>"+
				"</div>", 
					msg.cid, msg.cid, msg.cid, msg.cid, 
					msg.text, msg.cid, msg.cid, msg.time
				);
				
				
	onBoxContent.append( html );
	var textEvent = $(sprintf('.media-%s',msg.cid));  
	if (app_func_isobj(textEvent)){
		textEvent.hide();
	}
	
	/*! \remark create Animations */
	if ( app_func_isfunc(callback) ) {
		callback.apply(this, [textEvent]);
	}
	return 0;	
}
 
/*! \retval Implements 'onVisitorRcvMessage' */
 function onVisitorRcvMessage(msg, callback)
{
	var html = sprintf
	(
		"<div class='%s media media-chat media-%s'>  <img class='avatar avatar-%s' src='/{!APP_URL_VST}/asset/maxpro/image/3861075.png' alt='...'>"+
			"<div class='media-body media-body-%s'>"+
				"<p class='text-%s'><span style='font-size:12px;font-weight:bold;'>%s</span> <br> %s</p>"+ 
				"<p class='meta meta-%s'>"+ 
					"<span class='time-%s'>%s</span>"+
				"</p>"+
			"</div>"+
		"</div>", 
		msg.msgbox, msg.cid, msg.cid,  
		msg.cid, msg.cid, msg.from, 
		msg.text, msg.cid, msg.cid,  
		msg.time 
	);
	
	onBoxContent.append(html);
	var textEvent = $(sprintf('.media-%s',msg.cid));  
	if (app_func_isobj(textEvent)) {
		textEvent.hide();
	}
	
	/*! \remark Test Play audio */
	if (msg.audio){
		onVisitorAudioHandler((s) => {
			s.message.play();		
		}); 
	}
		
	/*! \remark create Animations */
	if ( app_func_isfunc(callback) )
	{
		callback.apply(this, [textEvent, msg ]);
	}		 
	return 0;	
 } 

 /*! \retval Implements 'onVisitorErrMessage' */
 function onVisitorErrMessage(msg, callback)
{
	var html = sprintf
	(
		"<div class='media media-chat media-%s'><img class='avatar avatar-error' src='/{!APP_URL_AGT}/asset/maxpro/image/3861075.png' alt='...'>"+
			"<div class='media-body media-body-error'>"+
				"<p class='text-error'>%s</p>"+ 
				"<p class='meta meta-error'>"+ 
					"<span class='time-error'></span>"+
				"</p>"+
			"</div>"+
		"</div>", msg.msgid, msg.text
	);
	
	onBoxContent.append(html);
	var textEvent = $(sprintf('.media-%s', msg.msgid));  
	if (app_func_isobj(textEvent)) {
		textEvent.hide();
	}
 
	/*! \remark create Animations */
	if ( app_func_isfunc(callback) )
	{
		callback.apply(this, [textEvent]);
	}		 
	return 0;	
 }
    
 
/*! \retval Implements 'onVisitorUsrMessage' */
 function onVisitorUsrMessage(msg, callback)
{
	msg.text = "";
	for (var i in msg.user) 
	{
		var box = msg.user[i];
		msg.text += sprintf(
			"<div class='media media-chat media-%s'>  <img class='avatar avatar-%s' src='/{!APP_URL_VST}/asset/maxpro/image/3861075.png' alt='...'>"+
				"<div class='media-body media-body-%s'>"+
					"<p class='text-%s'>%s</p>"+ 
					"<p class='meta meta-%s'><span class='time-%s'>%s</span></p>"+
				"</div>"+
			"</div>", 
			msg.cid, msg.cid, msg.cid, msg.cid, box.text, msg.cid, msg.cid, msg.time 
		);
	}
	
	onBoxContent.append(msg.text);
	var textEvent = $(sprintf('.media-%s',msg.cid));  
	if (app_func_isobj(textEvent))
	{
		textEvent.hide();
	}
	
	/*! \remark create Animations */
	if (app_func_isfunc(callback)) 
	{
		callback.apply(this, [textEvent]);
	}		 
	return 0;	 
}

/*! \retval Implements 'onVisitorEvtMessage' */
 function onVisitorEvtMessage(msg, callback)
{
	/** Add New Session User */
	if ( app_func_isobj(msg) )
	{
		/*! \remark tambahkan setiap kali ada box id */
		onUserSession.ws_chatboxs[msg.chatboxid] = msg.chatboxid;
		var datas =  { 
			'ws_chatboxid' : msg.chatboxid,
			'ws_direction' : msg.direction,
			'ws_callerid'  : msg.callerid 
		};
		
		datas.ws_channel2 = (msg.direction == 1 ? msg.channel1 : msg.channel2 );
		onVisitorAddStorage(datas);
	}
	 
	var html = sprintf(
			"<div class='media media-chat media-%s'>  <img class='avatar avatar-%s' src='/{!APP_URL_VST}/asset/maxpro/image/3861075.png' alt='...'>"+
				"<div class='media-body media-body-%s'>"+
					"<p class='text-%s'>%s</p>"+ 
					"<p class='meta meta-%s'><span class='time-%s'>%s</span></p>"+
				"</div>"+
			"</div>", 
				msg.cid, msg.cid, msg.cid, msg.cid, msg.text, msg.cid, msg.cid, msg.time 
	)
	
	onBoxContent.append(html);
	var textEvent = $(sprintf('.media-%s',msg.cid));  
	if (app_func_isobj(textEvent)) {
		textEvent.hide();
	}
	
	/*! \remark create Animations */
	if (app_func_isfunc(callback)) 
	{
		callback.apply(this, [textEvent, msg]);
	}		 
	return 0;	
 }

/*! \retval Implements 'onVisitorProgDisplay' */
 function onVisitorProgDisplay(display, callback) 
{
	var dispatchAttr = $( '.card-title' ),
		dispatchCard = $( '.card' );
		
	if( display )
	{
		/*! \remark Hide Typing Indikator */
		dispatchCard.find('.chat-pro-myname').attr('title', onUserSession.ws_username);
		dispatchCard.find( '.chat-pro-copy' ).hide();
		if (app_func_isobj(onBoxStatus)) {
			onBoxStatus.hide();
		}
	}
	
	if( !display )
	{
		dispatchAttr.find('.chat-pro-avatar').html('');
		dispatchAttr.find('.chat-pro-users').html('');
		dispatchCard.find('.chat-pro-copy').show(); 
	}
	
	if( app_func_isfunc(callback) ){
		callback.apply(this, [ dispatchAttr ]);
	}
} 

/*! \retval Implements 'onVisitorFormRegister' */
function onVisitorBoxDisplay(display, callback) 
{
	window.onBoxDisplay = $( '.display-handler' ); 
	if( display )
	{
		window.onBoxContent.html('');
		window.onBoxConnect.html('');
		window.onBoxTextMsg.val('');
		
		/*! \remark for Animations */
		window.onBoxDisplay.show(1000, () => {
			onBoxContent.attr("style","overflow-y:scroll !important; height: 460px !important");
			onVisitorProgDisplay( display, (s) => {
				s.find('.chat-pro-avatar').html( '<img class="avatar avatar-xs" src="/{!APP_URL_VST}/asset/maxpro/image/3861075.png" width="16px" alt="...">' );
				s.find('.chat-pro-users').html(onUserSession.ws_prog); 
			});
		}); 
		
		/*! \remark call arguments for ready session */
		if ( app_func_isfunc(callback) ){
			callback.apply(this, [display]);
		}
		
		return 0;
		
	} else if ( !display){
		
		onVisitorProgDisplay( display, (s) => {
			s.find('.chat-pro-avatar').html( '<img src="/favicon.ico" width="36px" alt="...">' );
			s.find('.chat-pro-users').html(onUserSession.ws_prog); 
		});
		
		window.onBoxDisplay.hide(300, () => {
			onBoxContent.attr("style","overflow-y:no; height: 480px !important");
			if (typeof callback == 'function' )
			{
				callback.apply(this, [display]);
				return 0;
			}
		}); 
	}
	
	return 0;
}

/*! \retval Implements 'onVisitorClearTextMsg' */
 function onVisitorClearTextMsg() 
{
	onBoxTextMsg.val('').focus();
	onBoxTextHtml.val('').focus();
	return 0;		
}


/*! \retval Implements 'onVisitorCreateMakeId' */
 function onVisitorCreateMakeId(callback) 
{
	var msgValId = makeuid(); // var msgValText = onBoxTextMsg.val();
	var msgValText = onBoxTextHtml.val();  
	if (app_func_sizeof(msgValId) >3)
	{
		if (app_func_isfunc(callback)){
			callback.apply(this, [msgValId, msgValText]);
		}
	}
	return 0;
}


/*! \retval Implements 'onVisitorUserRegister' 
 *
 * 1. onVisitorHookRegister  --	Via Webhook Untuk dapat User & sebagainya 
 * 2. onVisitorUserRegister  -- Untuk Login Ke Websoket dan Memulai Start Session  
 *	
 */ 
 function onVisitorUserRegister(chat, event) 
{
    var fs = new onVisitorFetchStorage();
	
	console.log(fs.data());
	if ( !app_func_isobj(fs) ){
		return 0;
	}
	
	var msgval  = {
		meta: '100', type: 'action', action: 'register',
		data   : 
		{
			uid   	  : fs.get('ws_uid'), 	
			session   : fs.get('ws_session'),
			channel   : fs.get('ws_channel'),	
			userid    :	fs.get('ws_userid'),
			username  : fs.get('ws_username'),
			useremail : fs.get('ws_useremail'),
			regtime   : fs.get('ws_regtime'),
			status    : fs.get('ws_status'),
			domain    : fs.get('ws_domain'),
			group	  : fs.get('ws_group')
		}
	}; 
	
	var buf = JSON.stringify(msgval); 
	window.chatApps.onSend( buf, (chat, str) => {
		writeloger( sprintf( "Sent Size %s byte", 
			app_func_sizeof(str)
		));
	}); 
	
	return 0;
 }
 
/*! \retval Implements 'onVisitorUserLogout' */
 function onVisitorUserLogout()
{
   /** \remark flow process end / close chat 
	* o. pertama kirim ke Hook Untuk Triger Socket Ws 
	* o. Ws client mendapat feedbac hasil bahwa session sduah di delete 
	* o. Info Balikan ini di lanjukan untuk Triger Hook Closing statement 
    * o. Destroy semua session yang ada di client browser
	*
	*/
	
	if (app_func_isfunc(onVisitorHookUnregister))
	{
		onVisitorHookUnregister();
	}
	return 0;
} 
/*! \retval Implements 'onVisitorSentTyping' */
 function onVisitorSentTyping(onTyping)
{
	var fs = new onVisitorFetchStorage();
	if ( !app_func_isobj(fs) ){
		return 0;
	}

	var evtText  = onTyping,
		evtBox   = fs.get('ws_chatboxid'),
		evtChan1 = fs.get('ws_channel'),
		evtchan2 = fs.get('ws_channel2'),
		evtType  = 'event', 
		evtFlag  = 0 
		
	var msgval  = {
		meta: EVT_META_MESG, type: 'action', action: 'typing',
		data   : 
		{
			msgbox : evtBox, 
			chan1  : evtChan1,
			chan2  : evtchan2,
			type   : evtType,
			text   : evtText,	
			flag   : evtFlag
		}
	};
	
	var buf = JSON.stringify(msgval);
	window.chatApps.onSend( buf, (chat, str) => {
		writeloger( sprintf( "Sent Size %s byte", app_func_sizeof(str) ));
	}); 
	
	return ;
}
/*! \retval Implements 'SentMessage' */
 function onVisitorSentMessage()
{
	/*! \remark indication of Error */ 
	window.onBoxOnError = app_func_int(onVisitorGetPeerStatus());
	if ( window.onBoxOnError < 0) {
		return 0;
	}
	
	var fs = new onVisitorFetchStorage();
	if ( !app_func_isobj(fs) ){
		return 0;
	}

	onVisitorCreateMakeId((onTextId, onTextVal) => {
		var msgtext = onTextVal,
			msgcid  = onTextId,
			msgbox  = fs.get('ws_chatboxid'),
			msgchan = fs.get('ws_channel'),
			msgfrom = fs.get('ws_username'),
			msgto   = fs.get('ws_channel2'),
			msgtype = 'text',
			msgid	= 0,
			msgflag = 0;
			msgtime = 0;
			
			
		if ( app_func_sizeof(msgtext)<1 )
		{
			onBoxTextMsg.focus();
			return 0;
		}		
			
		var msgval  = {
			meta: EVT_META_MESG, type: 'action', action: 'sent',
			data   : 
			{
				cid    : msgcid, 	
				msgbox : msgbox,
				msgid  : msgid,
				chan   : msgchan,
				from   : msgfrom,
				to 	   : msgto,
				type   : msgtype,
				text   : msgtext,	
				time   : msgtime,
				flag   : msgflag
			}
		};
			
		var buf = JSON.stringify(msgval);
		if (app_func_isfunc(onVisitorBoxMessge)){
			onVisitorBoxMessge(msgval.data); 
		}
		
		window.chatApps.onSend( buf, (chat, str) => {
			
			if ( !onVisitorClearTextMsg() )
			{
				writeloger( sprintf( "Sent Size %s byte", 
					app_func_sizeof(str)
				));
			}
		}); 
		
	}); 
	
	return 0;
} 

/*! \retval Implements 'onVisitorDeleteStorage' */
 function onVisitorAddStorage(data)
{
	if ( !app_func_isobj(data)){
		return 0;
	}
	
	for( var i in data){
		window.onUserSession[i] = data[i];
	}
	
	onVisitorDeleteStorage( (success) => {
		if ( success ){
			onVisitorCreateStorage(window.onUserSession);
		}
	});
	
	return 0;
}
/*! \retval Implements 'onVisitorSetSession' */
 function onVisitorSetSession(key, val)
{
	window.onUserSession[key] = val;
	return 0;
 }
 
/*! \retval Implements 'onVisitorSetPeerStatus' */
 function onVisitorSetPeerStatus(status, callback) 
{
	onVisitorSetSession('ws_status', status);
	if ( app_func_isfunc(callback) ){
		callback.apply(this, [status]);
	}
	return 0;
 } 
 
 /*! \retval Implements 'onVisitorGetPeerStatus' */
 function onVisitorGetPeerStatus() {
	return window.onUserSession.ws_status;
 } 
 
/*! \retval Implements 'onVisitorDeleteStorage' */
 function onVisitorDeleteStorage(callback)
{
	if( window.localStorage !== null )
	{
		window.localStorage.removeItem('ws_visitor_session');  
		if( app_func_isfunc(callback) ) 
		{
			callback.apply(this, [true]);
		}
	}
	return 0;
}

/*! \retval Implements 'onVisitorCreateStorage' */
 function onVisitorCreateStorage(data)
{
	if( window.localStorage != null ){
		window.localStorage.setItem( 'ws_visitor_session', 
			JSON.stringify(data)
		);
	}
}

/*! \retval Implements 'onVisitorCreateStorage' */
 function onVisitorFetchStorage()
{
	var fetchStorage = false;
	if( window.localStorage.getItem('ws_visitor_session') !== null ){
		fetchStorage = JSON.parse(window.localStorage.getItem('ws_visitor_session')); 
	} 
	
	/*! \retval object class */
	return {
		find: function(key){
			return typeof fetchStorage[key] != 'undefined' ? true : false;
		},
		get : function(key){
			return typeof fetchStorage[key] != 'undefined' ? fetchStorage[key] : '';
		},
		update : function(key, val)
		{
			if ( typeof fetchStorage[key] != 'undefined')
			{
				fetchStorage[key] = val;
				if ( app_func_isobj(fetchStorage)){ 
					onVisitorCreateStorage(fetchStorage);
				}
			}
		},
		
		add : function(key, val){
			fetchStorage[key] = val; 
		},
		
		delete : function(key){
			
		},
		data : function(){
			return fetchStorage;
		}	
	} 
}


/*! \retval Implements 'onVisitorFormRegister' */
 function onVisitorFormRegister()
{
	onVisitorBoxDisplay(false, () => {
		var formRegister  = "<div class='form-register' >" + 
			"<form onsubmit= return false;>"+
			 
			  "<div class='form-group'>"+
				"<label for='InputUsername'>Username</label>"+
				"<input type='text' class='form-control' id='InputUsername' aria-describedby='usernameHelp' placeholder='Username'>"+
				"<small id='usernameHelp' class='form-text text-muted'>username required for nick name on chat.</small>"+
			  "</div>"+ 
			  
			  "<div class='form-group'>"+
				"<label for='InputEmail'>Email address</label>"+
				"<input type='email' class='form-control' id='InputEmail' aria-describedby='emailHelp' placeholder='Enter email'>"+
				"<small id='emailHelp' class='form-text text-muted'>We'll never share your email with anyone else.</small>"+
			  "</div>"+
			  
			  "<div class='row justify-content-center'>"+
				"<button type='submit' class='btn btn-primary' onclick='onVisitorHookRegister();return false;'>Submit</button>"+
			  "</div>"+
			"</form>"+ 
		"</div>";	
	 
		// then will render func
		onBoxContent.html(formRegister);
	});
}

/*! \retval Implements 'onVisitorAudioHangup' */
 function onVisitorAudioHangup(func)
{
	if ( onBoxRinging ) 
	{
		onBoxRinging.pause();
		onBoxRinging.currentTime = 0;
		onBoxRinging = false;
	}
	
	if ( app_func_isfunc(func))
	{
		func.apply(this, [onBoxRinging]);
	}
	
	return 0;
}


/*! \retval Implements 'onVisitorFormRegister' */
 function onVisitorAudioHandler(func)
{
	/*! \remark object every this */
	if (!app_func_isobj(onBoxLibrary)){ 
		onBoxLibrary  = {
			message : new Audio(onBoxOnAudio.message),
			ringing : new Audio(onBoxOnAudio.ringing)
		};
	}
	
	if ( app_func_isobj(onBoxLibrary) ){ 
		if ( app_func_isfunc(func) ){
			func.apply(this, [onBoxLibrary]);
		}
	} 
	return 0;
}

/*! \retval Implements 'onVisitorInputHandler' */
 function onVisitorInputHandler()
{
	/*! \remark Execute a function when the user releases a key on the keyboard */
	onBoxTextMsg.keypress((e) => {
		var code = (e.keyCode ? e.keyCode : e.which);  
		
		clearTimeout(onBoxIntval);
		if (onBoxTyping < 1)  {
			onVisitorSentTyping(1); 
		}
		
		// https://stackoverflow.com/questions/59918250/how-to-replace-all-emoji-in-string-to-unicode-js
		// call expresion fuction from from 'emot.js'
		generateElementsRegexUnicode(onBoxTextMsg.val(), (onUpdateText) => {
			onBoxTextHtml.val(onUpdateText);
		});
		
		/*! \remark increment detach event */	
		onBoxTyping = ((onBoxTyping) + 1); 
		if ( code == 13 ) {
			onBoxTriger.trigger( 'click' );
			return true;
		}   
	})
	.keyup( (e) => {
		
		// https://stackoverflow.com/questions/59918250/how-to-replace-all-emoji-in-string-to-unicode-js
		// call expresion fuction from from 'emot.js' 
		generateElementsRegexUnicode(onBoxTextMsg.val(), (onUpdateText) => {
			onBoxTextHtml.val(onUpdateText);
		});
		
		// prevent errant multiple timeouts from being generated
		clearTimeout(onBoxIntval); 
		onBoxIntval = setTimeout(() => {
			onVisitorSentTyping(0); onBoxTyping = 0;
			
		}, onBoxTimeout );
	});

	return 0;	
}

/*! \retval Implements 'onVisitorStartSession' */
 function onVisitorStartSession()
{	
	/*! \retval rollback screen */	
	/*! \retval overflow-y: scroll !important; height:460px !important; */
	
	onBoxContent.attr("style","overflow-y:scroll !important; height: 460px !important");
	
	/*! \retval rollback screen */	
	onVisitorProgDisplay(true, (s) => {
		s.find('.chat-pro-avatar').html( '<img class="avatar avatar-xs" src="/{!APP_URL_VST}/asset/maxpro/image/3861075.png" width="16px" alt="...">' );
		s.find('.chat-pro-users').html(onUserSession.ws_prog); 
	});
	
	/*! \retval create New Object class */	
	window.chatApps = new chatPro(); 
	if ( app_func_isobj(window.chatApps) )
	{
		window.chatApps.onSetSockPrepare((chat, cfg) =>
		{
			window.chatApps.onSetSockHandler( 
				chatApps.onConnect,  
				chatApps.onError, 
				chatApps.onThread, 
				chatApps.onClose,  
				chatApps.onTimeout
			);
			
			window.chatApps.onSetStartHandler(onVisitorUserRegister);
			window.chatApps.onSetThreadHandler(onVisitorSockEvents,onVisitorSockErrors);
			if ( app_func_isobj(window.onUserSession) )
			{
				chatApps.onSetSockConnect(
					window.onUserSession.ws_host, 
					window.onUserSession.ws_port
				);
			}
		});
	} 
}

/*! \retval Implements 'onVisitorStartSession' */
 function onVisitorIniateSession(callback)
{
	window.onUserSession.ws_uid = 0;
	if( window.localStorage.getItem('ws_visitor_session') !== null ){
		window.onUserSession = JSON.parse(window.localStorage.getItem('ws_visitor_session')); 
	} 
	
	if( app_func_isfunc(callback) )
    {
	   // console.log(window.onUserSession);
	   callback.apply(this, [window, window.onUserSession.ws_uid]); 
	   return 0;
    }
    
	return 0;
	
}
/*! #END */