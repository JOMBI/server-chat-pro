<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>{!APP_SERVER_TITLE}</title> 
	<link type="text/css" rel="stylesheet" href="/{!APP_URL_VST}/asset/maxpro/css/bootstrap-4.0.0.min.css/?domain={!APP_CLIENT_DUID}"/>
	<link type="text/css" rel="stylesheet" href="/{!APP_URL_VST}/asset/maxpro/css/font-awesome-v5.7.2-all.css/?domain={!APP_CLIENT_DUID}"/>
	<link type="text/css" rel="stylesheet" href="/{!APP_URL_VST}/asset/maxpro/css/visitor.css/?domain={!APP_CLIENT_DUID}"/>
	<script type="text/javascript" src="/{!APP_URL_VST}/asset/maxpro/js/jquery-3.2.1.min.js/?domain={!APP_CLIENT_DUID}"></script>
	<script type="text/javascript" src="/{!APP_URL_VST}/asset/maxpro/js/botstrap-4.0.0.min.js/?domain={!APP_CLIENT_DUID}"></script>
	<script type="text/javascript" src="/{!APP_URL_MGR}/asset/socket/chat.io.js/?domain={!APP_CLIENT_DUID}"></script>
	<script type="text/javascript" src="/{!APP_URL_VST}/asset/maxpro/js/chat.us.js/?domain={!APP_CLIENT_DUID}"></script> 
	<script type="text/javascript" src="/{!APP_URL_VST}/asset/maxpro/js/chat.ap.js/?domain={!APP_CLIENT_DUID}"></script> 
	<script type="text/javascript" src="/{!APP_URL_VST}/asset/maxpro/js/chat.ico.js/?domain={!APP_CLIENT_DUID}"></script> 
	
</head>	
<body> 
	<div class="page-content page-container" id="page-content">
    <div class="padding">
        <div class="row container d-flex justify-content-center">
            <div class="col-md-6">
                <div class="card card-bordered">
                    <div class="card-header">
                        
						<h4 class="card-title">
							<span class="chat-pro-avatar" id="chat-avatar"></span>
							<span class="chat-pro-users " id="chat-operator"></span>
							<span class="chat-pro-timer display-handler" id="chat-connect"></span> 
							<span class="chat-pro-timer display-handler" id="chat-state">
								<img class="display-typing" src="/{!APP_URL_VST}/asset/maxpro/image/output-onlinegiftools.gif" height="36px" width="36px" alt="...">
									<span class="text-typing"></span>
							</span>
						</h4> 
						
						
						<span>
							<a class="btn btn-xs btn-primary btn-secondary display-handler" href="javascript:void(0);" onclick="onVisitorHookSearch();" data-abc="true" data-toggle="tooltip" data-placement="top"  title="Start chat with user"><i class="fa fa-users" aria-hidden="true"></i></a>
							<a class="btn btn-xs btn-primary btn-secondary display-handler" href="javascript:void(0);" onclick="onVisitorUserLogout();" data-abc="true" data-toggle="tooltip" data-placement="top"  title="Close from apps"><i class="fa fa-times"></i></a>
						</span> 
                    </div>
					
					<div class="ps-container ps-theme-default ps-active-y" id="chat-content"></div>
					
					<div class="publisher bt-1 border-light display-handler"> 
						<img class="chat-pro-myname avatar avatar-xs display-handler" src="/{!APP_URL_VST}/asset/maxpro/image/administrator-male.png" alt="..."> 
						<input class="publisher-input display-handler" type="text" id="text-msg" placeholder="Write something">  
						<span class="publisher-btn file-group display-handler"> 
							<i class="fa fa-paperclip file-browser"></i> 
							<input type="file"> 
						</span> 
						<span class="display-handler" data-emoji="true"></span>
						
						<!-- <a class="publisher-btn display-handler" href="javascript:void(0);" data-abc="true"> -->
							<!-- <i class="fa fa-smile"></i> -->
						<!-- </a>  -->
									
						<a class="publisher-btn text-info btn-sent display-handler" href="javascript:void(0);" onclick="onVisitorSentMessage();" data-abc="true">
							<i class="fa fa-paper-plane"></i>
						</a> 
					</div>
					
					<div class="chat-pro-copy" style="border:none;font-size:11px;color:#ddd !important;text-align:center;padding:2px 0px 10px 0px;"> 
						<span class="chat-pro-copy">Copyright &copy {!APP_SERVER_LINE}</span>
					</div>
					
					<!-- input handler for html data chat -->
					<input type="text" id="chat-html" value="" style="display:none;"> 
					<script>(() => { window.EmojiPicker.init() })() </script>
					
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>