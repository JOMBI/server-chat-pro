/*!
 * SCP -- An open source Server Chat Pro
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * \link https://www.freecodecamp.org/news/how-to-secure-your-websocket-connections-d0be0996c556/
 *
 * \link https://sodocumentation.net/webrtc/topic/5641/webrtc-simple-examples 
 *
 */
  
 
/*! \retval Global Variable */ 
 window.onBoxDisplay  = null;
 window.onBoxContent  = null;
 window.onBoxTextMsg  = null;
 window.onBoxConnect  = null;
 window.onBoxRinging  = false;
 window.onBoxDialing  = false;
 window.onBoxLibrary  = false;
 
/*! \remark test load Audio */
 window.onBoxOnError = 0; 
 window.onBoxOnAudio = 
{
	message : "//{!APP_SERVER_DOMAIN}/{!APP_URL_MGR}/asset/sound/message-alert.mp3",
	ringing : "//{!APP_SERVER_DOMAIN}/{!APP_URL_MGR}/asset/sound/ringing-alert.mp3"
};
 
 /*! \remark create for visitor session */
 window.onUserSession = 
{
	ws_prog		 : '{!APP_SERVER_TITLE}',  
	ws_versi	 : '{!APP_SERVER_VERSI}', 
	ws_copy		 : '{!APP_SERVER_COPY}' ,
	ws_host 	 : '{!APP_SOCKET_HOST}' ,
	ws_port 	 : '{!APP_SOCKET_PORT}' ,
	ws_domain    : '{!APP_CLIENT_DUID}',
	ws_group     : '{!APP_CLIENT_WAGT}',
	ws_uid		 : 0,
	ws_userid 	 : '',
	ws_channel   : '',
	ws_channel2  : '',
	ws_username  : '',
	ws_useremail : '',
	ws_session   : '',
	ws_regtime   : '',
	ws_status    : '', 
	ws_state     : '',
	ws_chatboxs  : {}
};

/*! \retval Static Variable */ 
 const EVT_META_USER = 100;
 const EVT_META_SYST = 200;
 const EVT_META_HAND = 300;
 const EVT_META_MESG = 400;
 

/*! \retval sent onWebHooks */
 function onAgentHookAction(meta, action, callback)
{
	/*! \remark indication of Error */
	window.onBoxOnError = onAgentGetPeerStatus();
	if ( parseInt(window.onBoxOnError) < 0) {
		writeloger(sprintf( "onVisitorHookAction:Status = %s", 
			window.onBoxOnError
		));
		return 0;
	}
	
	var url = window.sprintf('/{!APP_URL_AGT}/action/%s.hook', action);
	$.ajax ({
		type :'POST', url: url, data: meta, 
		success : ( res ) => {
			if (typeof callback == 'function') {
				callback.apply(this, [res]);
			}
		}
	});
	/*! \retval hooker */
	return 0;
}

/*! \retval Implements 'onAgentHookLoger' */
 function onAgentHookLoger(data, callback)
{
	/*! \remark mengambil data history chat yang sedang dan sudah terjadi di stream socket melalui Hook */
	onAgentHookAction(data, 'loger', (Loger) => {
		/*! \retval check is valid object */
		if ( !app_func_sizeof(Loger) ) {
			return 0;
		}
		
		/*! if Error to next flow */
		if ( Loger.error ==1 ) 
		{ 
			if( app_func_isfunc(callback) ){
				callback.apply(this, [false]);
			}
			return 0;
		} 
		
		/*! if Error to next flow */
		if ( Loger.error == 2 ) 
		{
			Loger.data.time = app_func_minute(Loger.data.time);
			onAgentSysMessge(Loger.data, (onText) => {
				onText.show(200, ()=>{
					window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
				});
			}); 
			
			if( app_func_isfunc(callback) ){
				callback.apply(this, [false]);
			}
			return 0;
		} 
		
		/*! \remark for iteration this */
		var retval = 0, msg = false;
		for (var i in Loger.data)
		{
			/*! \remark jika data yang di iterasi bukan Object sebaiknya di skip */
			msg = Loger.data[i];
			if (!app_func_isobj(msg))
			{
				continue;
			}			
			
			/*! \remark untuk chat yang berasal dari Luar atau incoming */
			if( msg.sfm == 1 ){  
				msg.time = app_func_minute(msg.time);
			}
			
			if( msg.sfm == 2 ){  
				msg.time = app_func_minute(msg.time);
			}
			
			msg.audio = false;
			if ( msg.sfm == 1 ){
				onAgentRcvMessage(msg, (onText) => {
					onText.show(500, () => {
						onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
					});
				});
			}
			
			/*! \remark untuk chat yang di kirim ke luar atau outbound */
			if ( msg.sfm == 2 ){
				onAgentBoxMessge(msg, (onTextTime) => {
					onTextTime.html(msg.time);
				});
			}
			
			/*! \remark untuk chat merger jika chat lebih dari satu channel */
			if ( msg.sfm == 3 )
			{
				msg.text = sprintf("Chat Session \"<b>%s</b>\"", msg.msgbox);
				msg.time = app_func_second(msg.time); 
				onAgentLogMessge(msg, (onTextTime) => {
					console.log("loger devider");
				});
			}
			
			/*! \remark untuk chat chat dengan session Box ada namun belum ada percakapan  */
			if (msg.sfm == 4)
			{
				msg.text = sprintf("Active Session \"<b>%s</b>\"", msg.msgbox);
				msg.time = app_func_second(msg.time); 
				onAgentLogMessge(msg, (onTextTime) => {
					console.log("loger devider");
				});
			}
			
			/*!  \remark for callback method */
			retval = retval + 1; 
		}
		
		/*! \remark sent to callback function */
		if ( app_func_isfunc(callback) )
		{
			callback.apply(this, [retval]);
		}
		
		return 0;
	}); 
 }

/*! \retval Implements 'onAgentHookRegister' */
 function onAgentHookWelcome(data, flow)
{
	/*! \remark khussus ketika terjadi register */
	if (typeof flow != 'undefined' ){
		data.flow = flow;
	}
	
	onAgentHookAction(data, 'welcome', (res) => {
		/*! \retval check is valid object */
		if ( !app_func_sizeof(res) ) {
			return 0;
		}
		
		if ( res.error ==1 ) { 
			return 0;
		}
		
		onAgentSysMessge(res.data, (onText) => {
			onText.show(1000, () => {
				window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	 
			});
		});
		
		return 0;
	});
 } 
 
 
 /*! \retval Implements 'onAgentHookClosing' */
 function onAgentHookClosing(data, flow)
{
	/*! \remark khussus ketika terjadi register */
	if (typeof flow != 'undefined' ){
		data.flow = flow;
	}
	
	onAgentHookAction(data, 'closing', (res) => {
		/*! \retval check is valid object */
		if ( !app_func_sizeof(res) ) {
			return 0;
		}
		
		if ( res.error ==1 ) { 
			return 0;
		}
		
		onAgentSysMessge(res.data, (onText) => {
			onText.show(1000,() => {
				window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
				window.setTimeout( () => 
				{
					/*! \remark close socket */
					chatApps.onSetSockClose(1);   
					onAgentDeleteStorage(() => 
					{
						/*! \remark delete semua attributes yang ada di Global window */
						onAgentSetSession('ws_uid'	  , '');
						onAgentSetSession('ws_userid'	  , '');
						onAgentSetSession('ws_channel'  , '');
						onAgentSetSession('ws_channel2' , '');
						onAgentSetSession('ws_username' , '');
						onAgentSetSession('ws_useremail', '');
						onAgentSetSession('ws_session'  , '');
						onAgentSetSession('ws_regtime'  , '');
						onAgentSetSession('ws_status'	  , '');
						onAgentSetSession('ws_chatboxid', '');
						onAgentSetSession('ws_chatboxs' , {});
						
						/*! \remark setelah di clear baru masuk ke process pencarian session */
						onAgentIniateSession( ( handler, register) => { 
							if( register ) handler.onAgentStartSession();
							else if( !register ){
								handler.onAgentFormRegister(); 
							}
						});
						
					});  
					
				}, 3000); 
			
			});
		
		});
		 
		return 0;
		
	});
 }
 
 
 /*! \retval Implements 'onAgentHookRegister' */
 function onAgentHookError(data, error)
{
	/*! \remark khussus ketika terjadi register */
	if (typeof error != 'undefined' ){
		data.error = error;
	}
	
	/*! \retval check is valid object */
	onAgentHookAction(data, 'error', (res) => {
		
		if ( !app_func_sizeof(res) ) {
			return 0;
		}
		
		if ( res.error ==1 ) { 
			return 0;
		}
		
		onAgentSysMessge(res.data, (onText) => {
			onText.show(200, ()=>{
				window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
			});
		});
		
		return 0;
	}); 
 }
 

/*! \retval Implements 'onAgentCreateMakeId' */
 function onAgentHookMakeId(callback) 
{
	var msgValId = makeuid(); 
	if (app_func_sizeof(msgValId) >3)
	{
		if (app_func_isfunc(callback)){
			callback.apply(this, [msgValId]);
		}
	}
	return 0;
}

/*! \retval Implements 'onAgentHookUnregister' */
 function onAgentHookUnregister() 
{
	/*! \remark starting to search agent on available */
	var client = new onAgentFetchStorage();
	var data = {
		uid 	 	: client.get('ws_uid'),
		session  	: client.get('ws_session'),
		channel  	: client.get('ws_channel'),
		userid	 	: client.get('ws_userid'),
		username 	: client.get('ws_username'),
		useremail	: client.get('ws_useremail'),
		userdomain 	: client.get('ws_domain')
	};
	
	/*! \remark sent Request to object Hooker */
	onAgentHookAction(data, 'unregister', (res) => {
		return 0;
	});
}
 
/*! \retval onAgentHookRouting */
 function onAgentHookRouting(channel2)
{
	var user = new onAgentFetchStorage();	
	var data = {
		channel1 : user.get('ws_channel'), 
		channel2 : channel2
	};
	
	/*! \remark sent Request to object Hooker */
	onAgentHookAction(data, 'routing', (res) => { 
		if ( !app_func_sizeof(res) ) {
			return 0;
		}
		
		if ( res.error ==1 ) {
			writeloger(sprintf("Routing '%s' ", res.message));
			return 0;
		}
	});
	return 0;
}

/*! \remark onAgentHookSending */
 function onAgentHookSending(onTextVal)
{
	window.onBoxOnError = app_func_int(onAgentGetPeerStatus());
	if ( window.onBoxOnError < 0) {
		return 0;
	} 
	var fs = new onAgentFetchStorage(), msgval = {}, buf = '';
	if ( !app_func_isobj(fs) ){
		return 0;
	}

	onAgentHookMakeId((onTextId) => {
		var msgtext = onTextVal,
			msgcid  = onTextId,
			msgbox  = fs.get('ws_chatboxid'),
			msgchan = fs.get('ws_channel'),
			msgfrom = fs.get('ws_username'),
			msgto   = fs.get('ws_channel2'),
			msgtype = 'text',
			msgid	= 0,
			msgflag = 0;
			msgtime = 0;
			
			
		if ( app_func_sizeof(msgtext)<1 )
		{
			onBoxTextMsg.focus();
			return 0;
		}		
			
		msgval  = {
			meta: EVT_META_MESG, type: 'action', action: 'sent',
			data   : 
			{
				cid    : msgcid, 	
				msgbox : msgbox,
				msgid  : msgid,
				chan   : msgchan,
				from   : msgfrom,
				to 	   : msgto,
				type   : msgtype,
				text   : msgtext,	
				time   : msgtime,
				flag   : msgflag
			}
		};
			
		buf = JSON.stringify(msgval);
		if (app_func_isfunc(onAgentBoxMessge)){
			onAgentBoxMessge(msgval.data); 
		}
		
		window.chatApps.onSend( buf, (chat, str) => {
			
			if ( !onAgentClearTextMsg() )
			{
				writeloger( sprintf( "Sent Size %s byte", 
					app_func_sizeof(str)
				));
			}
		}); 
	}); 
}

/*! \remark onAgentHookMessage */
 function onAgentHookMessage(message)
{
	var sfm  = new onAgentFetchStorage(), data = {};
	if ( !app_func_isobj(sfm) ){
		return 0;
	}
	
	if ( typeof message ==  'undefined' ){
		return 0;
	}
	
	data.session  = sfm.get('ws_session');
	data.username = sfm.get('ws_username');
	data.chatbox  = sfm.get('ws_chatboxid');
	data.callerid = sfm.get('ws_callerid');
	data.message  = message; 
	
	onAgentHookAction(data, 'message', (res) => 
	{
		if (!app_func_sizeof(res)) {
			return 0;
		}
		
		if (res.error ==1){
			writeloger(sprintf("Routing '%s' ", res.message));
			return 0;
		}
		
		if (res.error == 0) {
			var data  = res.data;
			if (app_func_isobj(data)) {
				onAgentHookSending(data.text);
			}
		}
	});
	return 0;
}


/*! \remark onAgentHookDiscard / Tolak */
 function onAgentHookDiscard(chatboxid)
{
	
	// media-163305436800010
	var sfm  = new onAgentFetchStorage(), sfc = sfm.data();
	var data = {
		channel   : sfm.get('ws_channel'),
		session	  : sfm.get('ws_session'),
		username  : sfm.get('ws_username')
	}
	console.log(sfm.get('ws_chatboxid'));
	if (!app_func_isobj(data)){
		return 0;
	}
	
	/*! \remark sent Request to object Hooker */ 
	if( typeof chatboxid != 'undefined' ){
		data.chatboxid = chatboxid;
	}
	else if( typeof chatboxid == 'undefined' ){
		data.chatboxid = sfm.get('ws_chatboxid');
	}
	
	onAgentHookAction(data, 'discard', (res) => { 
		if (!app_func_sizeof(res)) 
		{	
			return 0;
		}
		
		// true process 
		if (res.error == 0 ){
			var onText = $(sprintf('.media-%s', chatboxid));
			onText.hide(500,()=> {
				onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
			}); 
		}			
		
		if ( res.error == 1 )  
		{
			writeloger( sprintf("Discard '%s' ", res.message) );
			return 0;
		}
	});
	return 0;
}

/*! \remark onAgentHookPickup */
 function onAgentHookPickup(data)
{
	// direction=1&chatboxid=163338146200004&callerid=VISITOR01&channel1=VISITOR01&channel2=AGENT01&timeout=3&cid=1633381462&time=04%3A04
	var user = new onAgentFetchStorage();	
	console.log(user.data());
	if ( !app_func_isobj(data)){
		return 0;
	}
	
	
	/*! \remark sent Request to object Hooker */
	delete data.text;
	onAgentHookAction(data, 'pickup', (res) => {
		/*! \retval check is valid object */
		if ( !app_func_sizeof(res) ) {
			return 0;
		}
		
		if ( res.error ==1 ) {
			writeloger(sprintf("Routing '%s' ", res.message));
			return 0;
		}
	});
	return 0;
}


/*! \retval Implements 'onAgentHookRegister' */
 function onAgentHookRegister() 
{
	var data = {
		username   : $('#InputUsername').val(),
		useremail  : $('#InputEmail').val(),
		userdomain : window.onUserSession.ws_domain,	
		sessionid  : ''
	};
	
	/*! \remark sent Request to object Hooker */
	onAgentHookAction(data, 'register', (res) => {
		/*! \retval check is valid object */
		if ( !app_func_sizeof(res) ) {
			return 0;
		}
		
		if ( res.error ==1 ) {
			alert(res.message);
			return 0;
		}
		
		/*! \retval next if true */
		
		onAgentSetSession('ws_uid'	  , res.data.uid);
		onAgentSetSession('ws_channel'  , res.data.channel);
		onAgentSetSession('ws_userid'	  , res.data.userid);
		onAgentSetSession('ws_session'  , res.data.session);
		onAgentSetSession('ws_username' , res.data.username);
		onAgentSetSession('ws_useremail', res.data.email);
		onAgentSetSession('ws_regtime'  , res.data.regts); 
		
		/*! \remark session storage */
		onAgentCreateStorage(window.onUserSession);
		
		/*! \remark on success then start of session */
		onAgentIniateSession(( handler, register) => {
			if( register ){
				window.onAgentBoxDisplay(true, () => { 
					handler.onAgentStartSession(); 
				}); 
			}
		});
		
		return 0;
	});
}
/*! \retval Implements 'onAgentHookRouting' */
 function onAgentHookSearch() 
{
	/*! \remark starting to search agent on available */
	var client = new onAgentFetchStorage();
	var data = {
		uid 	 	: client.get('ws_uid'),
		group 		: client.get('ws_group'),
		session  	: client.get('ws_session'),
		channel  	: client.get('ws_channel'),
		userid	 	: client.get('ws_userid'),
		username 	: client.get('ws_username'),
		useremail	: client.get('ws_useremail'),
		userdomain 	: client.get('ws_domain')
	};
	
	/*! \remark sent Request to object Hooker */
	onAgentHookAction(data, 'search', (res) => {
		/*! \retval check is valid object */
		if ( !app_func_sizeof(res) ) {
			return 0;
		}
		
		if ( res.error ==1 ) {
			alert(res.message);
			return 0;
		}
		
		// tidak ada user yang Online
		if ( res.error == 2 ) { 
			onAgentSysMessge(res.data, (onText) => {
				onText.show(200, ()=>{
					window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
				});
			}); 
			return 0;
		}
		
		// Lagi Sibuk Cari Operator 
		if ( res.error == 3 ) { 
			return 0;
		}
		
		onAgentUsrMessage(res.data, (onText) => {
			onText.show(200, ()=>{
				window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
			});
		}); 
		
		return 0;
	});
}

/*! \retval Implements 'onAgentHookHistory' */ 
function onAgentHookHistory(){
	
	var sfg = new onAgentFetchStorage();  
	var data = {
		uid 		: sfg.get('ws_uid'),
		session 	: sfg.get('ws_session'),
		channel 	: sfg.get('ws_channel'),
		userid 		: sfg.get('ws_userid'),
		username 	: sfg.get('ws_username'),
		regtime 	: sfg.get('ws_regtime'),
		status		: sfg.get('ws_status'),
		chatboxid	: sfg.get('ws_chatboxid')  
	}  
	if ( data.chatboxid )
	{
		window.onBoxContent.html('');
		onAgentHookLoger(data, (cond) => {
			console.log("history");	
		});
	}
	return 0;
}


/*! \retval Implements 'onAgentSockEvents' */ 
 function onAgentSockEvents(recv, chat)
{
	/*! \remark if false of object */
	if ( !app_func_isobj(recv) ){
		return 0;
	}
	var eventsMeta = recv.meta,
		eventsType = recv.type,
		eventsMake = null,
		eventsText = null;
		
	var eventsTimeout = 0;  
	var eventsDivider = {};
	
	if (recv.event != 'pong')
	{
		console.log(recv);
	}
		
	/*! \retval this event for event "Handler" format */
	if (eventsMeta == EVT_META_HAND && eventsType == 'event')
	{
		eventsEvent = recv.event;
		eventsData  = recv.data;
		
		/*! \retval get message pong */
		if (eventsEvent == 'pong' && eventsData.tick) {
			window.onBoxConnect
				.html(sprintf("Time: %s", eventsData.tick)); 
		}
		
		/*! \retval get message dead your PID */
		if (eventsEvent == 'dead' && eventsData.tick) 
		{
			onAgentSetPeerStatus(eventsData.status, (onStatus) => 
			{ 
				onBoxConnect.html( 'Error' ).css({'color': 'red' });  
				eventsData.text = sprintf("Close ( <b>%s</b> ) Thread of session , Multiple open of task ..!", eventsData.pid );
				onAgentErrMessage(eventsData, (onText) => {
					onText.show(200, ()=>{
						window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);
						chatApps.onSetSockClose(1);   /*! close socket interupted */	 
					});
				}); 
			});  
			
			return 0;
		}
		
		
		/*! \retval get message Error */
		if (eventsEvent == 'error') {
			window.onBoxConnect
				.html( eventsData.text );
		}
		
		/*! \retval get message Error */
		if (eventsEvent == 'dialing') 
		{	
			if ( app_func_isobj(onUserSession.ws_chatboxs) &&( Object.keys(onUserSession.ws_chatboxs).length < 2 )) {
				window.onBoxContent.html('');	
			}
			
			/*! \remark for tagline export this*/
			eventsDivider.cid = eventsData.cid;
			eventsDivider.msgbox = eventsData.chatboxid;
			eventsDivider.time = app_func_second(eventsData.time); 
			if ( eventsDivider.cid )
			{ 
				eventsDivider.text = sprintf("Start Session \"<b>%s</b>\"", eventsDivider.msgbox); 
				onAgentLogMessge(eventsDivider, (onTextTime) => {
					console.log("loger devider");
				});
			}
			
			eventsData.text = sprintf(" %s on <b>Ringing </b>...", eventsData.callerid); 
			onAgentEvtMessage(eventsData, (onText) => {
				writeloger(sprintf("Ringing On '%s'", eventsData.callerid));
				onText.show(200, () => {
					window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
				});
			});
		}
		
		/*! \retval get message Error */
		if (eventsEvent == 'ringing') 
		{
			/*! \remark Play Audio from 'onAgentAudioHandler' */
			onAgentAudioHandler((s) => {
				if (onBoxRinging = s.ringing) {
					onBoxRinging.play();	
				}
				return 0;	
			}); 
			
			/*! \remark harus di check apakah sebelumnya punya box id 
				jika iya box tidak perlu di delete 
			*/
			eventsData.text = sprintf("<b>Invite</b> ... from %s", eventsData.callerid);   
			if ( app_func_isobj(onUserSession.ws_chatboxs) &&( Object.keys(onUserSession.ws_chatboxs).length < 1 )) {
				onBoxContent.html('');	
			}	
			
			/*! \remark for tagline export this*/
			eventsDivider.cid = eventsData.cid;
			eventsDivider.msgbox = eventsData.chatboxid;
			eventsDivider.time = app_func_second(eventsData.time); 
			if ( eventsDivider.cid )
			{ 
				eventsDivider.text = sprintf("Start Session \"<b>%s</b>\"", eventsDivider.msgbox); 
				onAgentLogMessge(eventsDivider, (onTextTime) => {
					console.log("loger devider");
				});
			}
			
			
			onAgentEvtMessage(eventsData, (onText, eventsData) => { 
				eventsTimeout = (parseInt(eventsData.timeout) *1000)
				writeloger(sprintf("Invite from '%s'", eventsData.callerid)); 
				onText.show(200, () => {
					onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);		
					window.setTimeout(() => {
						onAgentHookPickup(eventsData);
					}, eventsTimeout);
				});   
			});	
		}
		
		/*! \retval get message Error */
		if (eventsEvent == 'connect') 
		{
			eventsData.text = sprintf("%s on <b>Pickup</b> ...", eventsData.callerid);  
			onAgentEvtMessage(eventsData, (onText, eventsData) => {
				eventsTimeout = (parseInt(eventsData.timeout) *1000)
				writeloger(sprintf("Pickup on '%s'", eventsData.callerid));
				onText.show(200, () => {
					onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100); 
					
				});  
			});	
		}
		
		/*! \retval get message Error */
		if (eventsEvent == 'up') 
		{
			eventsData.text = sprintf("<b>Connected</b> ... with %s", eventsData.callerid);  
			onAgentEvtMessage(eventsData, (onText, eventsData) => {
				eventsTimeout = (parseInt(eventsData.timeout) *1000)
				writeloger(sprintf("Connected on '%s'", eventsData.callerid));
				onText.show(200,() => {
					onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100); 
					window.setTimeout(() => {
						onAgentHookMessage(1);
					}, 300);
				});   
			});	
			
			/*! \remark Stop Audio from 'onAgentAudioHangup' */
			onAgentAudioHangup((s) => {
				writeloger("Stop Ringing"); 
				return 0;
			});
		}
		
		/*! \retval get message Error "Bye" from channel */
		if (eventsEvent == 'discard') 
		{
			eventsData.time = app_func_minute(eventsData.time);
			if ( eventsData.discard == 1){
				eventsData.text = sprintf( "<b>Reject</b> Chat session <b>%s</b> for <b>%s</b>", 
					eventsData.chatboxid, eventsData.callerid
				);  
			}
			else if ( eventsData.discard == 2){
				eventsData.text = sprintf( "<b>Rejected</b> Chat session <b>%s</b> by <b>%s</b>", 
					eventsData.chatboxid, eventsData.callerid
				);  
			}
			
			onAgentEvtMessage(eventsData, (onText, eventsData) => {
				eventsTimeout = (parseInt(eventsData.timeout) *1000)
				writeloger(sprintf("'%s' Sent Discard", eventsData.callerid)); 
				onText.show(200, () => {
					onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100); 
				});   
			});	
		}
		
		/*! \retval get message Error "Bye" from channel */
		if (eventsEvent == 'bye') 
		{
			eventsData.time = app_func_minute(eventsData.time);
			eventsData.text = sprintf( "<b>%s</b> Sent <b>Bye</b> Close Chat session '<b>%s</b>'", 
				eventsData.callerid, eventsData.chatboxid
			);  
			
			onAgentEvtMessage(eventsData, (onText, eventsData) => {
				eventsTimeout = (parseInt(eventsData.timeout) *1000)
				writeloger(sprintf("'%s' Sent Bye", eventsData.callerid)); 
				onText.show(200, () => {
					onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100); 
				});  
			});	
		}
	}
	
	/*! \retval this event for event Message format */
	if (eventsMeta == EVT_META_MESG && eventsType == 'event')
	{
		eventsEvent = recv.event;
		eventsError = recv.error;
		eventsData  = recv.data;
		
		
		/*! \remark  for Outgoing Message */
		if (eventsEvent == 'sent') 
		{
			eventsMake = sprintf('.time-%s', eventsData.cid);
			$(eventsMake).text(eventsData.time);
			
			/*! check pesan yang di kirim untuk di proces di Hooker */
			/*! jika error ==1 tidak ada chat tujuan */
			if (eventsError == 1){
				onAgentHookError(eventsData, eventsError);
			}
		} 
		
		/*! \remark  for Incoming Message */
		if (eventsEvent == 'recv') 
		{
			eventsMake = sprintf('.time-%s', eventsData.cid);
			$(eventsMake).text(eventsData.time);
			
			/*! check pesan yang di kirim untuk di proces di Hooker */
			/*! jika error ==1 tidak ada chat tujuan */
			if (eventsError == 1){
				onAgentHookError(eventsData, eventsError);
			}
			
			eventsData.audio = true;
			onAgentRcvMessage(eventsData, (onText, onData) => { 
				onText.show(200, ()=>{
					window.onBoxContent.animate({scrollTop: onBoxContent[0].scrollHeight}, 100);	
				});
			});
		} 
		
		/*! \remark for State Typing from Channel */
		if (eventsEvent == 'typing') 
		{
			if (eventsData.text == 1)
			{ 
				onBoxStatus.show(5, () => {
					$( '.text-typing' )
						.html('is typing ...'); 
				});
			}
			else if (eventsData.text == 0) 
			{
				onBoxStatus.hide(1, () => {
					$( '.text-typing' )
						.html('');
				});
			}
		}
	}
	
	/*! \retval this event for event "User" format */
	if (eventsMeta == EVT_META_USER && eventsType == 'event')
	{
		eventsEvent = recv.event;
		eventsData  = recv.data;
		
		/*! \retval get message pong */
		if (eventsEvent == 'register') 
		{
			onAgentSetPeerStatus(eventsData.status, (onStatus) => 
			{
				window.onBoxContent.html('');	
				/*! jika pernah chating lalu melkukan reload page check jika sudah punya chatboxid maka check dulu apakah pernah chat */
				var sfg = new onAgentFetchStorage(); 
				if (app_func_sizeof(sfg.get('ws_chatboxid')) < 5)
				{
					onAgentHookWelcome(eventsData, 1);
					window.setTimeout(() => {
						onAgentHookWelcome(eventsData, 2);
					},1000); 	
				} 
				else if (app_func_sizeof(sfg.get('ws_chatboxid')) > 5) 
				{
					eventsData.chatboxid = sfg.get('ws_chatboxid');
					if ( eventsData.chatboxid )
					{
						onAgentHookLoger(eventsData, (cond) => {
							console.log("history");	
						});
					}
				} 
			});
		}
		
		/*! \retval get message pong */
		if (eventsEvent == 'unregister') 
		{
			onAgentHookClosing(eventsData, 1);	
		}
	}
	
	return 0;
}

/*! \retval Implements 'onAgentSockErrors' */
 function onAgentSockErrors(recv, chat) 
{
	console.log('onAgentSockErrors');
	console.log(recv);
}

/*! \retval Implements 'onAgentSentColor' */
 function onAgentSentColor(cid, callback)
{
	onBoxContent
		.find( sprintf('.tick-%s', cid))
			.css({ 'color' : 'rgb(233, 240, 253)' });	// #a8c1f0 
			
	window.setTimeout( () => {
		onBoxContent
		.find( sprintf('.tick-%s', cid))
			.css({ 'color' : 'rgb(168, 193, 240)' });	// #a8c1f0
	},2000);
	
	return 0;	
 }

 /*! \retval Implements 'onAgentLogMessge' */
 function onAgentLogMessge(msg, callback)
{

	var html = sprintf( 
		"<div class=\"%s media media-meta-day loger-divider-%s\">"+
			"<p class='meta'>"+ 
				"<span class='time-%s'>Today %s</span>"+ 
			"</p>"+	
			"<p>%s</p>"+ 
			"</div>",	 
			msg.msgbox, msg.cid, msg.cid, msg.time, msg.text
	);
	
	onBoxContent
		.append( html )
			.animate({ scrollTop: onBoxContent[0].scrollHeight }, 100); 
	return 0;	
 } 
 
/*! \retval Implements 'onAgentBoxMessge' */
 function onAgentBoxMessge(msg, callback)
{
	var html = window.sprintf
	( 
		"<div class='%s media media-chat media-chat-reverse %s'>"+
			"<div class='media-body'>"+
				"<p>%s</p>"+
				"<p class='meta'>"+ 
					"<span class='time-%s'></span>"+ 
					"<span class='tick-%s' style='margin-left:10px;font-size:12px;'>"+ 
						"<i class='fas fa-check-double'></i>"+ 
					"</span>"+ 
				"</p>"+ 
			"</div>"+
		"</div>", msg.msgbox, msg.cid, msg.text, msg.cid, msg.cid
	);			
	
	onBoxContent
		.append( html )
			.animate({ scrollTop: onBoxContent[0].scrollHeight }, 100); 
			onAgentSentColor(msg.cid);
			
	if ( app_func_isfunc(callback) )
	{
		var eventsTime = $( sprintf(".time-%s", msg.cid) );
		if ( app_func_isobj(eventsTime) )
		{ 
			callback.apply(this, [eventsTime]);
		}
	}		
	return 0;	
}

/*! \retval Implements 'onAgentSysMessge' */
 function onAgentSysMessge(msg, callback)
{
	var html = sprintf("<div class='media media-chat media-%s'> <img class='avatar avatar-%s' src='/{!APP_URL_AGT}/asset/maxpro/image/3861075.png' alt='...'>"+
					"<div class='media-body media-body-%s'>"+
						"<p class='text-%s'>%s</p>"+ 
						"<p class='meta meta-%s'><span class='time-%s'>%s</span></p>"+
					"</div>"+
				"</div>", 
					msg.cid, msg.cid, msg.cid, msg.cid, 
					msg.text, msg.cid, msg.cid, msg.time
				);
				
				
	onBoxContent.append( html );
	var textEvent = $(sprintf('.media-%s',msg.cid));  
	if (app_func_isobj(textEvent)){
		textEvent.hide();
	}
	
	/*! \remark create Animations */
	if ( app_func_isfunc(callback) ) {
		callback.apply(this, [textEvent]);
	}
	return 0;	
}
 
/*! \retval Implements 'onAgentRcvMessage' */
 function onAgentRcvMessage(msg, callback)
{
	var html = sprintf
	(
		"<div class='%s media media-chat media-%s'>  <img class='avatar avatar-%s' src='/{!APP_URL_AGT}/asset/maxpro/image/3861075.png' alt='...'>"+
			"<div class='media-body media-body-%s'>"+
				"<p class='text-%s'><span style='font-size:12px;font-weight:bold;'>%s</span> <br> %s</p>"+ 
				"<p class='meta meta-%s'>"+ 
					"<span class='time-%s'>%s</span>"+
				"</p>"+
			"</div>"+
		"</div>", 
		msg.msgbox, msg.cid, msg.cid,  msg.cid, 
		msg.cid, msg.from, msg.text,
		msg.cid, msg.cid,  msg.time 
	);
	
	onBoxContent.append(html);
	var textEvent = $(sprintf('.media-%s',msg.cid));  
	if (app_func_isobj(textEvent)) {
		textEvent.hide();
	}
	
	/*! \remark Test Play audio */
	if (msg.audio){
		onAgentAudioHandler((s) => {
			s.message.play();		
		}); 
	}
		
	/*! \remark create Animations */
	if ( app_func_isfunc(callback) )
	{
		callback.apply(this, [textEvent, msg ]);
	}		 
	return 0;	
 } 
 

 
 
 
 /*! \retval Implements 'onAgentRcvMessage' */
 function onAgentErrMessage(msg, callback)
{
	var html = sprintf
	(
		"<div class='media media-chat media-%s'><img class='avatar avatar-error' src='/{!APP_URL_AGT}/asset/maxpro/image/3861075.png' alt='...'>"+
			"<div class='media-body media-body-error'>"+
				"<p class='text-error'>%s</p>"+ 
				"<p class='meta meta-error'>"+ 
					"<span class='time-error'></span>"+
				"</p>"+
			"</div>"+
		"</div>", msg.msgid, msg.text
	);
	
	onBoxContent.append(html);
	var textEvent = $(sprintf('.media-%s', msg.msgid));  
	if (app_func_isobj(textEvent)) {
		textEvent.hide();
	}
 
	/*! \remark create Animations */
	if ( app_func_isfunc(callback) )
	{
		callback.apply(this, [textEvent]);
	}		 
	return 0;	
 }
  
 
/*! \retval Implements 'onAgentUsrMessage' */
 function onAgentUsrMessage(msg, callback)
{
	msg.text = "";
	for (var i in msg.user) 
	{
		var box = msg.user[i];
		msg.text += sprintf(
			"<div class='media media-chat media-%s'>  <img class='avatar avatar-%s' src='/{!APP_URL_AGT}/asset/maxpro/image/3861075.png' alt='...'>"+
				"<div class='media-body media-body-%s'>"+
					"<p class='text-%s'>%s</p>"+ 
					"<p class='meta meta-%s'><span class='time-%s'>%s</span></p>"+
				"</div>"+
			"</div>", 
			msg.cid, msg.cid, msg.cid, msg.cid, box.text, msg.cid, msg.cid, msg.time 
		);
	}
	
	onBoxContent.append(msg.text);
	var textEvent = $(sprintf('.media-%s',msg.cid));  
	if (app_func_isobj(textEvent))
	{
		textEvent.hide();
	}
	
	/*! \remark create Animations */
	if (app_func_isfunc(callback)) 
	{
		callback.apply(this, [textEvent]);
	}		 
	return 0;	 
}

/*! \retval Implements 'onAgentEvtMessage' */
 function onAgentEvtMessage(msg, callback)
{
	/** Add New Session User */
	if ( app_func_isobj(msg) )
	{
		/*! \remark tambahkan setiap kali ada box id */
		onUserSession.ws_chatboxs[msg.chatboxid] = msg.chatboxid;
		var datas =  { 
			'ws_chatboxid' : msg.chatboxid,
			'ws_direction' : msg.direction,
			'ws_callerid'  : msg.callerid 
		};
		
		datas.ws_channel2 = (msg.direction == 1 ? msg.channel1 : msg.channel2 );
		onAgentAddStorage(datas);
	}
	 
	var html = sprintf(
			"<div class='%s media media-chat media-%s'>  <img class='avatar avatar-%s' src='/{!APP_URL_AGT}/asset/maxpro/image/3861075.png' alt='...'>"+
				"<div class='media-body media-body-%s'>"+
					"<p class='text-%s'>%s</p>"+ 
					"<p class='meta meta-%s'><span class='time-%s'>%s</span></p>"+
				"</div>"+
			"</div>", 
				msg.chatboxid, msg.cid, msg.cid, msg.cid, msg.cid, msg.text, msg.cid, msg.cid, msg.time 
	)
	
	onBoxContent.append(html);
	var textEvent = $(sprintf('.media-%s',msg.cid));  
	if (app_func_isobj(textEvent)) {
		textEvent.hide();
	}
	
	/*! \remark create Animations */
	if (app_func_isfunc(callback)) 
	{
		callback.apply(this, [textEvent, msg]);
	}		 
	return 0;	
 }

/*! \retval Implements 'onAgentProgDisplay' */
 function onAgentProgDisplay(display, callback) 
{
	var dispatchAttr = $( '.card-title' ),
		dispatchCard = $( '.card' );
		
	if( display )
	{
		dispatchCard.find('.chat-pro-myname').attr('title', onUserSession.ws_username);
		dispatchCard.find('.chat-pro-copy').hide();
		if (app_func_isobj(onBoxStatus)) {
			onBoxStatus.hide();
		}
	}
	
	if( !display )
	{
		dispatchAttr.find('.chat-pro-avatar').html('');
		dispatchAttr.find('.chat-pro-users').html('');
		dispatchCard.find('.chat-pro-copy').show(); 
	}
	
	if( app_func_isfunc(callback) ){
		callback.apply(this, [ dispatchAttr ]);
	}
} 

/*! \retval Implements 'onAgentFormRegister' */
function onAgentBoxDisplay(display, callback) 
{
	window.onBoxDisplay = $( '.display-handler' ); 
	if( display )
	{
		window.onBoxContent.html('');
		window.onBoxConnect.html('');
		window.onBoxTextMsg.val('');
		
		/*! \remark for Animations */ 
		 
		window.onBoxDisplay.show(1000, () => {
			onBoxContent.attr("style","overflow-y:scroll !important; height: 460px !important");
			onAgentProgDisplay( display, (s) => {
				s.find('.chat-pro-avatar').html( '<img class="avatar avatar-xs" src="/{!APP_URL_AGT}/asset/maxpro/image/3861075.png" width="16px" alt="...">' );
				s.find('.chat-pro-users').html(onUserSession.ws_prog); 
			}); 
		}); 
		
		/*! \remark call arguments for ready session */
		if ( app_func_isfunc(callback) ){
			callback.apply(this, [display]);
		}
		
		return 0;
		
	} else if ( !display) { 	
		onAgentProgDisplay( display, (s) => {
			s.find('.chat-pro-avatar').html( '<img src="/favicon.ico" width="36px" alt="...">' );
			s.find('.chat-pro-users').html(onUserSession.ws_prog); 
		});
		
		window.onBoxDisplay.hide(300, () => {
			onBoxContent.attr("style","overflow-y:no; height: 480px !important");
			if (typeof callback == 'function' )
			{
				callback.apply(this, [display]);
				return 0;
			}
		}); 
	}
	
	return 0;
}

/*! \retval Implements 'onAgentClearTextMsg' */
 function onAgentClearTextMsg() 
{
	onBoxTextMsg.val('').focus();
	onBoxTextHtml.val('').focus();
	return 0;		
}


/*! \retval Implements 'onAgentCreateMakeId' */
 function onAgentCreateMakeId(callback) 
{
	var msgValId = makeuid();
	// var msgValText = onBoxTextMsg.val();
	var msgValText = onBoxTextHtml.val();  
	
	if (app_func_sizeof(msgValId) >3)
	{
		if (app_func_isfunc(callback)){
			callback.apply(this, [msgValId, msgValText]);
		}
	}
	return 0;
}


/*! \retval Implements 'onAgentUserRegister' 
 *
 * 1. onAgentHookRegister  --	Via Webhook Untuk dapat User & sebagainya 
 * 2. onAgentUserRegister  -- Untuk Login Ke Websoket dan Memulai Start Session  
 *	
 */ 
 function onAgentUserRegister(chat, event) 
{
    var fs = new onAgentFetchStorage();
	if ( !app_func_isobj(fs) ){
		return 0;
	}
	
	var msgval  = {
		meta: '100', type: 'action', action: 'register',
		data   : 
		{
			uid   	  : fs.get('ws_uid'), 	
			session   : fs.get('ws_session'),
			channel   : fs.get('ws_channel'),	
			userid    :	fs.get('ws_userid'),
			username  : fs.get('ws_username'),
			useremail : fs.get('ws_useremail'),
			regtime   : fs.get('ws_regtime'),
			status    : fs.get('ws_status'),
			domain    : fs.get('ws_domain'),
			group	  : fs.get('ws_group')
		}
	}; 
	
	var buf = JSON.stringify(msgval); 
	window.chatApps.onSend( buf, (chat, str) => {
		writeloger( sprintf( "Sent Size %s byte", 
			app_func_sizeof(str)
		));
	}); 
	
	return 0;
 }
 
/*! \retval Implements 'onAgentUserLogout' */
 function onAgentUserLogout()
{
   /** \remark flow process end / close chat 
	* o. pertama kirim ke Hook Untuk Triger Socket Ws 
	* o. Ws client mendapat feedbac hasil bahwa session sduah di delete 
	* o. Info Balikan ini di lanjukan untuk Triger Hook Closing statement 
    * o. Destroy semua session yang ada di client browser
	*
	*/
	
	if (app_func_isfunc(onAgentHookUnregister))
	{
		onAgentHookUnregister();
	}
	return 0;
} 
/*! \retval Implements 'onAgentSentTyping' */
 function onAgentSentTyping(onTyping)
{
	var fs = new onAgentFetchStorage();
	if ( !app_func_isobj(fs) ){
		return 0;
	}

	var evtText  = onTyping,
		evtBox   = fs.get('ws_chatboxid'),
		evtChan1 = fs.get('ws_channel'),
		evtchan2 = fs.get('ws_channel2'),
		evtType  = 'event', 
		evtFlag  = 0 
		
	var msgval  = {
		meta: EVT_META_MESG, type: 'action', action: 'typing',
		data   : 
		{
			msgbox : evtBox, 
			chan1  : evtChan1,
			chan2  : evtchan2,
			type   : evtType,
			text   : evtText,	
			flag   : evtFlag
		}
	};
	
	var buf = JSON.stringify(msgval);
	window.chatApps.onSend( buf, (chat, str) => {
		writeloger( sprintf( "Sent Size %s byte", app_func_sizeof(str) ));
	}); 
	
	return ;
}
/*! \retval Implements 'SentMessage' */
 function onAgentSentMessage()
{
	/*! \remark indication of Error */
	window.onBoxOnError = app_func_int(onAgentGetPeerStatus());
	if ( window.onBoxOnError < 0) {
		return 0;
	}
	
	/*! \remark next of process */
	var fs = new onAgentFetchStorage();
	if ( !app_func_isobj(fs) ){
		return 0;
	}

	onAgentCreateMakeId((onTextId, onTextVal) => {
		var msgtext = onTextVal,
			msgcid  = onTextId,
			msgbox  = fs.get('ws_chatboxid'),
			msgchan = fs.get('ws_channel'),
			msgfrom = fs.get('ws_username'),
			msgto   = fs.get('ws_channel2'),
			msgtype = 'text',
			msgid	= 0,
			msgflag = 0;
			msgtime = 0;
			
			
		if ( app_func_sizeof(msgtext)<1 )
		{
			onBoxTextMsg.focus();
			return 0;
		}		
			
		var msgval  = {
			meta: EVT_META_MESG, type: 'action', action: 'sent',
			data   : 
			{
				cid    : msgcid, 	
				msgbox : msgbox,
				msgid  : msgid,
				chan   : msgchan,
				from   : msgfrom,
				to 	   : msgto,
				type   : msgtype,
				text   : msgtext,	
				time   : msgtime,
				flag   : msgflag
			}
		};
			
		var buf = JSON.stringify(msgval);
		if (app_func_isfunc(onAgentBoxMessge)){
			onAgentBoxMessge(msgval.data); 
		}
		
		window.chatApps.onSend( buf, (chat, str) => {
			
			if ( !onAgentClearTextMsg() )
			{
				writeloger( sprintf( "Sent Size %s byte", 
					app_func_sizeof(str)
				));
			}
		}); 
		
	}); 
	
	return 0;
} 

/*! \retval Implements 'onAgentDeleteStorage' */
 function onAgentAddStorage(data)
{
	if ( !app_func_isobj(data)){
		return 0;
	}
	
	for( var i in data){
		window.onUserSession[i] = data[i];
	}
	
	onAgentDeleteStorage( (success) => {
		if ( success ){
			onAgentCreateStorage(window.onUserSession);
		}
	});
	
	return 0;
}
/*! \retval Implements 'onAgentSetSession' */
 function onAgentSetSession(key, val)
{
	window.onUserSession[key] = val;
	return 0;
 }
 
/*! \retval Implements 'onVisitorSetPeerStatus' */
 function onAgentSetPeerStatus(status, callback) 
{
	onAgentSetSession('ws_status', status);
	if ( app_func_isfunc(callback) ){
		callback.apply(this, [status]);
	}
	return 0;
 } 
 
 /*! \retval Implements 'onVisitorGetPeerStatus' */
 function onAgentGetPeerStatus() 
 {
	return window.onUserSession.ws_status;
 } 
 
 
/*! \retval Implements 'onAgentDeleteStorage' */
 function onAgentDeleteStorage(callback)
{
	if( window.localStorage !== null )
	{
		window.localStorage.removeItem('ws_visitor_session');  
		if( app_func_isfunc(callback) ) 
		{
			callback.apply(this, [true]);
		}
	}
	return 0;
}

/*! \retval Implements 'onAgentCreateStorage' */
 function onAgentCreateStorage(data)
{
	if ( window.localStorage != null )
	{
		window.localStorage.setItem( 'ws_visitor_session', 
			JSON.stringify(data)
		);
	}
}

/*! \retval Implements 'onAgentCreateStorage' */
 function onAgentFetchStorage()
{
	var fetchStorage = false;
	if( window.localStorage.getItem('ws_visitor_session') !== null ){
		fetchStorage = JSON.parse(window.localStorage.getItem('ws_visitor_session')); 
	} 
	
	/*! \retval object class */
	return {
		find: function(key){
			return typeof fetchStorage[key] != 'undefined' ? true : false;
		},
		get : function(key){
			return typeof fetchStorage[key] != 'undefined' ? fetchStorage[key] : '';
		},
		update : function(key, val)
		{
			if ( typeof fetchStorage[key] != 'undefined')
			{
				fetchStorage[key] = val;
				if ( app_func_isobj(fetchStorage)){ 
					onAgentCreateStorage(fetchStorage);
				}
			}
		},
		
		add : function(key, val){
			fetchStorage[key] = val; 
		},
		
		delete : function(key){
			
		},
		data : function(){
			return fetchStorage;
		}	
	} 
}


/*! \retval Implements 'onAgentFormRegister' */
 function onAgentFormRegister()
{
	onAgentBoxDisplay(false, () => {
		var formRegister  = "<div class='form-register' >" + 
			"<form onsubmit= return false;>"+
			 
			  "<div class='form-group'>"+
				"<label for='InputUsername'>Username</label>"+
				"<input type='text' class='form-control' id='InputUsername' aria-describedby='usernameHelp' placeholder='Username'>"+
				"<small id='usernameHelp' class='form-text text-muted'>username required for nick name on chat.</small>"+
			  "</div>"+ 
			  
			  "<div class='form-group'>"+
				"<label for='InputEmail'>Email address</label>"+
				"<input type='email' class='form-control' id='InputEmail' aria-describedby='emailHelp' placeholder='Enter email'>"+
				"<small id='emailHelp' class='form-text text-muted'>We'll never share your email with anyone else.</small>"+
			  "</div>"+
			  
			  "<div class='row justify-content-center'>"+
				"<button type='submit' class='btn btn-primary' onclick='onAgentHookRegister();return false;'>Submit</button>"+
			  "</div>"+
			"</form>"+ 
		"</div>";	
	 
		// then will render func
		onBoxContent.html(formRegister);
	});
}

/*! \retval Implements 'onAgentAudioHangup' */
 function onAgentAudioHangup(func)
{
	if ( onBoxRinging ) 
	{
		onBoxRinging.pause();
		onBoxRinging.currentTime = 0;
		onBoxRinging = false;
	}
	
	if ( app_func_isfunc(func))
	{
		func.apply(this, [onBoxRinging]);
	}
	
	return 0;
}


/*! \retval Implements 'onAgentFormRegister' */
 function onAgentAudioHandler(func)
{
	/*! \remark object every this */
	if (!app_func_isobj(onBoxLibrary)){ 
		onBoxLibrary  = {
			message : new Audio(onBoxOnAudio.message),
			ringing : new Audio(onBoxOnAudio.ringing)
		};
	}
	
	if ( app_func_isobj(onBoxLibrary) ){ 
		if ( app_func_isfunc(func) ){
			func.apply(this, [onBoxLibrary]);
		}
	} 
	return 0;
}
 
/*! \retval Implements 'onAgentInputHandler' */
 function onAgentInputHandler()
{
	/*! \remark Hide Typing Indikator */
	if (app_func_isobj(onBoxStatus)) 
	{
		onBoxStatus.hide();
	}
	
	/*! \remark Execute a function when the user releases a key on the keyboard */
	onBoxTextMsg.keypress((e) => {
		var code = (e.keyCode ? e.keyCode : e.which);  
		clearTimeout(onBoxIntval);
		if (onBoxTyping < 1)  {
			onAgentSentTyping(1); 
		}
		
		// https://stackoverflow.com/questions/59918250/how-to-replace-all-emoji-in-string-to-unicode-js
		// call expresion fuction from from 'emot.js'
		generateElementsRegexUnicode(onBoxTextMsg.val(), (onUpdateText) => {
			onBoxTextHtml.val(onUpdateText);
		});
		
		/*! \remark increment detach event */	
		onBoxTyping = ((onBoxTyping) + 1); 
		if ( code == 13 ) {
			onBoxTriger.trigger( 'click' );
			return true;
		}   
	})
	.keyup( (e) => {
		
		// https://stackoverflow.com/questions/59918250/how-to-replace-all-emoji-in-string-to-unicode-js
		// call expresion fuction from from 'emot.js' 
		generateElementsRegexUnicode(onBoxTextMsg.val(), (onUpdateText) => {
			onBoxTextHtml.val(onUpdateText);
		});
		
		// prevent errant multiple timeouts from being generated
		clearTimeout(onBoxIntval); 
		onBoxIntval = setTimeout(() => {
			onAgentSentTyping(0); onBoxTyping = 0;
			
		}, onBoxTimeout );
	});

	return 0;	
}

/*! \retval Implements 'onAgentStartSession' */
 function onAgentStartSession()
{	
	/*! \retval rollback screen */	
	/*! \retval overflow-y: scroll !important; height:460px !important; */
	
	onBoxContent.attr("style","overflow-y:scroll !important; height: 460px !important");
	
	/*! \retval rollback screen */	
	onAgentProgDisplay(true, (s) => {
		s.find('.chat-pro-avatar').html( '<img class="avatar avatar-xs" src="/{!APP_URL_AGT}/asset/maxpro/image/3861075.png" width="16px" alt="...">' );
		s.find('.chat-pro-users').html(onUserSession.ws_prog); 
	});
	
	/*! \retval create New Object class */	
	window.chatApps = new chatPro(); 
	if ( app_func_isobj(window.chatApps) )
	{
		window.chatApps.onSetSockPrepare((chat, cfg) =>
		{
			window.chatApps.onSetSockHandler( 
				chatApps.onConnect,  
				chatApps.onError, 
				chatApps.onThread, 
				chatApps.onClose,  
				chatApps.onTimeout
			);
			
			window.chatApps.onSetStartHandler(onAgentUserRegister);
			window.chatApps.onSetThreadHandler(onAgentSockEvents,onAgentSockErrors);
			if ( app_func_isobj(window.onUserSession) )
			{
				chatApps.onSetSockConnect(
					window.onUserSession.ws_host, 
					window.onUserSession.ws_port
				);
			}
		});
	} 
}

/*! \retval Implements 'onAgentIniateSession' */
 function onAgentIniateSession(callback)
{
	window.onUserSession.ws_uid = 0;
	if( window.localStorage.getItem('ws_visitor_session') !== null ){
		window.onUserSession = JSON.parse(window.localStorage.getItem('ws_visitor_session')); 
	} 
	
	if( app_func_isfunc(callback) )
    {
	   // console.log(window.onUserSession);
	   callback.apply(this, [window, window.onUserSession.ws_uid]); 
	   return 0;
    }
    
	return 0;
	
}
/*! #END */