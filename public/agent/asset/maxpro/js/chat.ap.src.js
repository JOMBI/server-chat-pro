/*!
 * SCP -- An open source Server Chat Pro
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/*!
 * NOTICE 
 * new object 'chatPro' 
 * for easy usage jquery frame work recomended 
 * nothing else selector more then jquery
 *
 */ 
 
 window.chatVisitor = null;
 $(document).ready(() => {
	 
	$('[data-toggle="tooltip"]').tooltip();
	
	/*! \remark register All Component */
	
	window.onBoxTextHtml = $('#chat-html');
	window.onBoxConnect  = $('#chat-connect');
	window.onBoxStatus   = $('#chat-state');
	window.onBoxContent  = $('#chat-content');
	window.onBoxTextMsg  = $('#text-msg');
	window.onBoxDisplay  = $('.display-handler');
	window.onBoxTriger   = $('.btn-sent');
	window.onBoxTimeout  = 1000;// timeut
	window.onBoxTyping   = 0;// on typeof
	window.onBoxIntval   = 0;// on interval
	
	window.onBoxContent.animate({ 
		scrollTop: window.onBoxContent[0].scrollHeight
	}, 100);
	
	
	/*! \remark Handler if Not Have Session cannt Chat */
	onAgentIniateSession((handler, register) => {
		if( register ) handler.onAgentStartSession(); 
		else if( !register ) {
			handler.onAgentFormRegister();  
		} 
		
		/*! \remark default register */
		handler.onAgentInputHandler();
	});
 }); 
 
