<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/**
 * \brief __scp_main_handler 
 *  All file Iteration to put variable for dynamic process 
 * 	Load if have new file process to Next time 
 *
 * \param 	Global Object Application	
 * \retval  Global Object Variable 
 *
 */
 function __scp_main_handler(&$scp)
{
   /*! 
    * \remark < __scp_main_handler >
	* Untuk Loader Pertama kali Tidak Bisa Menggukan fungsi yang tersedia 
	* pada loader process , di karenakan ini load utama sebelum load 
	* laod process yang lainnya sehingga harus di definisikan secara 
	* manual dan independent 
	*
	*/
	
	$scp->AppProgEnviroment = array
	(
		'__APP__' => SCP_APP . '/main'					 ,
		'__CFG__' => SCP_APP . '/config'				 ,  
		'__LIB__' => SCP_APP . '/libraries/app_res_func' ,
		'__SCK__' => SCP_APP . '/libraries/app_res_sock' , 	
		'__CLI__' => SCP_APP . '/modul/app_res_console'  ,
		'__MGR__' => SCP_APP . '/modul/app_res_manager'  , 
		'__WHK__' => SCP_APP . '/modul/app_res_webhook'	 ,
		'__WSK__' => SCP_APP . '/modul/app_res_websock'	 , 
		'__WKR__' => SCP_APP . '/modul/app_res_worker'   , 
		'__SES__' => SCP_APP . '/modul/app_res_session'  , 
		'__CHN__' => SCP_APP . '/modul/app_res_channel'  , 
		'__VST__' => SCP_APP . '/modul/app_res_visitor'  ,
		'__PSK__' => SCP_APP . '/modul/app_res_peer'	 ,
		'__ASK__' => SCP_APP . '/modul/app_res_apps'	 , 
		'__DBS__' => SCP_APP . '/driver'				 ,
		'__LOG__' => SCP_APP . '/loger'				 	 ,
		'__PUB__' => SCP_APP . '/public'				 ,
		'__EXP__' => SCP_APP . '/exports'			
	);
	
	/*! \retval for handler Load file $.dir /Main */
	if (isset($scp->AppProgLoader))
	{	
		
		$scp->AppProgLoader = array();
	}
	 
	/*! \remark scan dan include semua file yang ada pada dir "./main"  */
	$cfgMainFiles = scandir( $scp->AppProgEnviroment[ '__APP__' ] );
	if ( !is_array($cfgMainFiles) )
	{
		exit( sprintf( "Fail Loader from '%s'\n", $scp->AppProgEnviroment[ '__APP__' ] ));
	}
	
	/*! \remark Next Iterator process */
	foreach ($cfgMainFiles as $j => $f)
	{
		
	   /*! \remark 
	    * 1. jika ketika process iterasi yang di temukan bukan file melainkan 
		* 	 directory maka lanjutkan tidak perlu di process
		*
		* 2. Jika di temukan file namun bukan ber extensi .c maka abaikan tidak perlu 
		* 	 di process  
		*/	
		
		if ( is_dir($f) )
		{
			continue;
		}
		
		/*! \remark check apakah file tersebut megandung '.c' */
		if ( !stristr($f, '.c') )
		{
			continue;
		}
		
		/*! \remark check apakah file tersebut tersedia */
		
		$scp->ProgFile = $scp->AppProgEnviroment[ '__APP__' ] . '/'. $f;
		if ( !file_exists($scp->ProgFile) )
		{
			continue;	
		}
		
		/*! \remark Goto process bro */
		if ( !isset($scp->AppProgLoader[ '__APP__' ][$f]) )
		{ 
			$scp->AppProgLoader[ '__APP__' ][$f] = $scp->ProgFile;
			include_once ($scp->ProgFile);
		}
	}
	
	/*! \retval loader Process */
	unset($scp->ProgFile);
	return $scp->AppProgLoader;
}


/*! \remark for Handler Loader from Config Master / Main Application */
__scp_main_handler($scp);
