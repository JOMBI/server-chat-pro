<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
 class evaluate 
{
	var $param = array();
	
	/*! \retval 'default' set params */
	 function __construct($data)
	{
		$this->param = $data;
	}
	/*! \retval 'find' of key from params */
	 function add($key = NULL, $val = '' ) 
	{
		if ( !is_array($key) )
		{
			$key = array( $key => $val );
		}	
		
		 foreach($key as $k => $v )
		{
			$this->param[$k] = $v;
		}
		
		return $this;
	}
	
	/*! \retval 'find' of key from params */
	 function find($key = '' ) 
	{
		if ( array_key_exists($key, $this->param) ) {
			return $key;
		}
		return 0;
	}
	
	/*! \retval 'get' of data from params */
	 function get($key = '' )
	{
		if ( $this->find($key) )
		{
			return 	$this->param[$key];
		}
		return '';
	}
	
	/*! \retval detach */
	function detach($key = NULL, $find = '')
	{
		$val = $this->get($key);	
		if ( !strcmp($val, $find) )
		{
			return $val;
		}			
		return 0;
	}
	
	/*! \retval detach */
	function dispatchEvent($event = NULL, $key = '')
	{
		$val = $this->get($key);
		if ( function_exists ('app_func_event') )
		{
			return app_func_event( $event, array($val), 
				__FILE__, __LINE__ );
		}
		return false;
	}
	
}
?>