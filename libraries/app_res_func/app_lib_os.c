<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SCP project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */ 
 
 
 /*! \retval <app_os_hostname> for get hostname */
 function app_os_hostname($value, &$hostname = "")
{
	/*! create local struct */
	
	app_func_struct($s);
	
	/*! declare parameter */
	
	$s->hosts =& $hostname;
	$s->value = $value;
	
	/*! \remark trim all sections */ 
	app_func_trim($s->value);
	if (app_func_sizeof($s->value) > 2)
	{
		app_func_copy( $s->hosts, $s->value );
		return $s->hosts;
	}
	
	if( !function_exists('exec' ) )
	{
		return 0;
	}
	
	/*! get hostname from machine  by default */
	if ( @exec( 'hostname',$s->h) )
	{
		if	( app_func_trim($s->h) )
		{
			app_func_copy($s->hosts , isset($s->h[0]) ? $s->h[0] : 'chat.localhost.com' );
		}
	}
	
	return $s->hosts;
}	