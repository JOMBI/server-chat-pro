<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */ 
 
/**
 * \retval example Request 
 * http://192.168.10.236:60002/visitor/chat/?uid=18918891&host=www.localhost.com 
 *
 */
 /*! \def app_webhook_get_config <m* master > */ 
 function app_webhook_get_config(&$m = NULL)
{
	// scp_log_core("app_webhook_get_config-dump.AppProgVar", $m->AppProgVar);  
	// scp_log_core("app_webhook_get_config-dump.WebHooks", $m->WebHooks);   
	
	// print_r($s);
}
 
 
/*! \def app_webhook_get_type <s* master > */ 
 function app_webhook_get_type(&$s)
{
	app_func_copy($r,"");
	app_func_copy($f,app_webhook_get_param($s, 'Remote-Path') );
	if ($o = @pathinfo($f) ) 
	{
		if (isset($o['extension']))
		{
			app_func_copy($r, app_func_upper($o['extension'])); 
		}
	} 
	 
	return app_func_str($r);
}  

/*! \def app_webhook_get_router <s* master > */ 
 function app_webhook_get_router(&$s)
{
	app_func_copy($r,"");
	app_func_copy($f,app_webhook_get_param($s, 'Remote-Path') );
	if ($o = @pathinfo($f) ) 
	{
		if( !isset($o['extension'])) {
			$o['extension'] = "";
		}
		if (isset($o['basename']))
		{
			$o['basename'] = str_replace( array( '.', $o['extension']), array( '', ''), $o['basename']);
			app_func_copy($r, $o['basename']); 
		}
	} 
	
	return app_func_str($r);
} 

/*! \def app_webhook_get_path <s* master > */ 
 function app_webhook_get_path(&$s)
{
	app_func_free($path);
	app_func_output($path, $s->AppProgEnviroment['__PUB__']);
	if ( app_func_output($path, app_webhook_get_param($s, 'Remote-Path')) )
	{
		return app_func_str($path);
	}
	return 0;
} 

/*! \def app_webhook_get_method <s* master > */
 function app_webhook_get_method(&$s)
{
	return app_webhook_get_param($s, 'Remote-Method');
}   
 
/*! \def app_webhook_get_events <s* master > */
 function app_webhook_get_events(&$s)
{
	return app_webhook_get_param($s, 'Remote-Event');
}  

/*! \def app_webhook_get_request_domain <s* master > */
 function app_webhook_get_param_domain(&$s)
{
	return app_webhook_get_request($s, 'domain');
} 

/*! \def app_webhook_get_server_host <s* master > */
 function app_webhook_get_server_host(&$s)
{
	return app_webhook_get_param($s, 'Server-Host');
}  

/*! \def app_webhook_get_server_port <s* master > */
 function app_webhook_get_server_port(&$s)
{
	return app_webhook_get_param($s, 'Server-Port');
}  


/*! \def app_webhook_parse_param <s* master , code>*/
 function app_webhook_get_param(&$s, $key = '')
{
	if( isset($s->WebHooks->head) && isset($s->WebHooks->head[$key]) )
	{
		return $s->WebHooks->head[$key];
	}
	return '';
} 

/*! \def app_webhook_parse_param <s* master , key>*/
 function app_webhook_get_request(&$s, $key = '')
 {
	$hook = new evaluate($s->WebHooks->param);
	if (!is_object($hook))
	{	
		return false;
	}
	return $hook->get($key);
} 

/*! \def app_webhook_parse_request < m* master, s* client >	*/ 
 function app_webhook_get_valid(&$m)
{
	if ( !isset($m->WebHooks))
	{
		return 0;
	}
	
	/*! \retval is true number */
	return $m->WebHooks->prog;
}

 
/*! \def app_webhook_parse_request < m* master, s* client >	*/ 
 function app_webhook_parse_request($m, $s)
{
	
	/*! \remark default for parsing process */
	app_func_copy($m->WebHooks->reqs , array());
	app_func_copy($m->WebHooks->buff , array());
	app_func_copy($m->WebHooks->head , array());
	app_func_copy($m->WebHooks->param, array());
	app_func_copy($m->WebHooks->field, NULL);
	app_func_copy($m->WebHooks->path , NULL);
	app_func_copy($m->WebHooks->url  , NULL); 
	app_func_copy($m->WebHooks->prog , 0);
	
	
	
	
	/*! \note starting iterations */
	$m->WebHooks->reqs = array_map('app_func_trim', explode("\n", $s->nread));
	foreach ($m->WebHooks->reqs as $i => $h ) 
	{
		app_func_trim($h); 
		if ( app_func_sizeof($h) )
		{
			$m->WebHooks->buff[] = $h; 	
		}
	}
	
	/*! \note starting iterations */
	$sizeof = count($m->WebHooks->buff); $ptr = 0;
	foreach ($m->WebHooks->buff as $i => $t)
	{
		/*! \remark clean all process from here */
		app_func_trim($t); 
		if( !app_func_sizeof($t) )
		{
			continue;
		}
		
		/*! \remark dump for Method */
		if ( !$ptr &&($meta_method = $m->WebHooks->buff[$ptr]) )
		{
			app_func_copy($m->WebHooks, 'path', app_func_trim($meta_method));
			app_func_next($ptr);
			continue;
			
		}
		
		/*! \remark for get field */
		if ($ptr == (($sizeof) -1) && ($meta_field = $m->WebHooks->buff[$ptr]))
		{
			app_func_copy($m->WebHooks->field, $meta_field);
			app_func_next($ptr);
			continue;
		}
		
		/*! \remark jika mengandung char ":" */
		if ( app_func_strpos($t, chr(58)) )
		{
			$retval = array_map('trim', explode(chr(58), $t));
			if ($retval)
			{
				app_func_copy( $m->WebHooks->head, $retval[0], 
					app_func_trim( app_func_substr($t, 58) ) 
				); 
			} 
		}
		
		/*! \next current index */
		app_func_next($ptr);
	}
	
	
	
	 
	/*! \remark default for URL */
	app_func_output($m->WebHooks->url, "http://");
	app_func_output($m->WebHooks->url, $m->WebHooks->head['Host']);
	
	/*! \remark  for server host & server port */
	if ( app_func_explode($e, $m->WebHooks->head['Host'], chr(58)) )
	{ 
		app_func_copy($m->WebHooks->head, 'Server-Host', $e[0]);
		app_func_copy($m->WebHooks->head, 'Server-Port', $e[1]);
		app_func_free($e);
	}
	
	/*! \remark for remote Addr & port */
	if( $s->id )
	{
		app_func_copy($m->WebHooks->head, 'Remote-Host', $s->host);
		app_func_copy($m->WebHooks->head, 'Remote-Port', $s->port);
	}
	
	/*! \remark cari type method yang di request */
	while (1)
	{
		if ( preg_match('/(^POST)/', $m->WebHooks->path, $match) )
		{
			app_func_copy($m->WebHooks->head, 'Remote-Method', $match[0]);  
			app_func_replace($match[0], $m->WebHooks->path); 
			
			if ($ret = preg_split('/[\s]+/', app_func_trim($m->WebHooks->path)))
			{
				app_func_output($m->WebHooks->url, $ret[0]);
				app_func_copy($m->WebHooks->path, $ret[0]); 
				
				if ($m->WebHooks->field)
				{
					app_func_copy($m->WebHooks->head, 'Remote-Uri', $m->WebHooks->field );
				}
				
				app_func_copy($m->WebHooks->head, 'Remote-Path', rtrim(app_func_trim($m->WebHooks->path), '/') );
				// app_func_copy($m->WebHooks->head, 'Remote-Uri', app_func_trim($cath[1]) );
				
				/* \remark for URL */
				app_func_copy( $m->WebHooks->head, 'Remote-Url', 
					$m->WebHooks->url
				); 
				
				parse_str($m->WebHooks->head['Remote-Uri'], $output);
				if (is_array($output)) foreach ($output as $j => $o) {
					$m->WebHooks->param[$j] = $o;	
				}
			}
			
			break;
		}
		
		
		if ( preg_match('/(^GET)/', $m->WebHooks->path, $match) )
		{
			app_func_copy($m->WebHooks->head, 'Remote-Method', $match[0]);  
			app_func_replace($match[0], $m->WebHooks->path); 
			
			if ( $ret = preg_split('/[\s]+/', app_func_trim($m->WebHooks->path)) )
			{
				app_func_output($m->WebHooks->url, $ret[0]);
				app_func_copy($m->WebHooks->path, $ret[0]);  
				if ( $cath = preg_split('/[?]+/', $m->WebHooks->path) )
				{
					
					app_func_copy($m->WebHooks->head, 'Remote-Path', rtrim( app_func_trim($cath[0]), '/') );
					app_func_copy($m->WebHooks->head, 'Remote-Uri', "");
					if (isset($cath[1])) {
						app_func_copy($m->WebHooks->head, 'Remote-Uri', app_func_trim($cath[1]) );
					}
					
					/* \remark for URL */
					app_func_copy( $m->WebHooks->head, 'Remote-Url', 
						$m->WebHooks->url
					); 
				} 
			}

			/*! \remark get & set paramters */
			app_func_copy($output, false);
			app_func_copy($weburl, @parse_url($m->WebHooks->head['Remote-Url']));
			if ( is_array($weburl) &&( @array_key_exists( 'query', $weburl)) )
			{
				parse_str($weburl['query'], $output);
				if (is_array($output)) foreach ($output as $j => $o){
					$m->WebHooks->param[$j] = $o;
				}	
			}	
			break;
			
		}
		
		// goto end :
		break;
	}
	
	/*! \remark for get default path */
	
	// print_r($m->WebHooks);
	/*! \remark for path will replace and Customize */
	/*! \remark loop data for next */
	
	app_func_next($m->WebHooks->prog);
	
    /*! \retval clean up after process is success */
	app_func_delete($m->WebHooks, 'reqs');
	app_func_delete($m->WebHooks, 'buff');
	
	/*! \retval next process if true */
	return $m->WebHooks->prog;
}