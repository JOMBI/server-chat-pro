<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
 if(!defined( 'SCP_VAR_LOG' )) define( 'SCP_VAR_LOG', '/var/log/scp');

/*!
 * \brief 	smt_log_main
 * \note 	[description]
 * \retval  [description]
 *
 */
 function scp_log_main($app = NULL, $str = NULL)
{
	Global $scp; 
	
	/*! \retval make void Time for application */
	
	app_func_struct($log); 
	app_func_copy($log->app, $app);
	app_func_copy($log->str, $str); 
	
	/*! \retval if succes proces then sent signal break */
	if (app_func_copy( $log->time, date('H:i:s')))
	{
		/*! \remark add color for style console screen */
		$log->out = sprintf("%s %s[%s]: %s", $log->time, $log->app, $scp->AppProgPid, $log->str);
		
		scp_log_stream($scp, $log->out); 
		if ($scp->AppProgScr &&(app_func_color($log->out, 32)))
		{
			printf("%s\n", $log->out);
		}
	}
	
	/*! \retval clean up memory usage */
	app_func_free($log);
	return 0;
}

/*!
 * \brief 	scp_log_main
 * \note 	[description]
 * \retval  [description]
 *
 */
 function scp_log_stream(&$scp = NULL, $str = "", &$ret = 0)
{
	/*! \remark one touch process */
	app_func_copy($scp->LogIsDir , 0);
	app_func_copy($scp->LogIsFile, 0);
	
	/*! \retval check basedir of log */
	if (!isset($scp->LogPathCfg) )
	{
		app_func_copy($scp->LogPathCfg, SCP_VAR_LOG);
	}
	
	/*! \retval create base of Log */
	if ($scp->AppProgUid &&(!isset($scp->LogPathUid)))
	{
		app_func_copy($scp->LogPathUid, 
			strtolower($scp->AppProgUid)
		);	
	} 
	
	/*! \remark dir one check dont check every time 
	because exhause of CPU consume */ 
	
	if (!$scp->LogIsDir && !is_dir($scp->LogPathCfg))
	{
		system (sprintf("mkdir -p %s && chmod 0777", $scp->LogPathCfg) );
		if (!is_dir($scp->LogPathCfg))
		{
			printf("error 'scp_log_stream(0)' failed created log directory '%s' .\n", $scp->LogPathCfg);
			return 0;
		}
		
		/*! \retval add new isdir */
		app_func_next($scp->LogIsDir);
	} 
	
	
	/*! \retval will rotate every day */
	
	$scp->LogPathNow = date('Ymd');
	if (!strcmp($scp->LogPathNow, $scp->LogPathDts))
	{
		app_func_copy($scp->LogPathRoot, 
			sprintf( "%s/%s-%s.log", $scp->LogPathCfg, $scp->LogPathUid, $scp->LogPathNow)
		);
		
		if (!is_resource($scp->LogPathLink))
		{
			$scp->LogPathLink = fopen($scp->LogPathRoot, 'a');
			if (!$scp->LogPathLink)
			{
				printf("error 'scp_log_stream(3)' failed created log stream '%s' .\n", $scp->LogPathRoot);
				return 0;
			}
			
			/*! \retval input file to this process */
			system(sprintf("chmod 0777 %s", $scp->LogPathRoot));
			app_func_copy($ret, $scp->LogPathLink);
		}
	}
	
	/*! \retval diffrent day */
	else if (strcmp($scp->LogPathNow, $scp->LogPathDts))
	{
		/*! \remark close stream file */
		@fclose($scp->LogPathLink);
		
		/*! \remark update config Rotate to Now */
		app_func_copy($scp->LogPathLink, -1);
		app_func_copy($scp->LogPathDts , $scp->LogPathNow);
		app_func_copy($scp->LogPathRoot, 
			sprintf( "%s/%s-%s.log", $scp->LogPathCfg, $scp->LogPathUid, $scp->LogPathNow)
		);
		
		if (!is_resource($scp->LogPathLink))
		{
			$scp->LogPathLink = fopen($scp->LogPathRoot, 'a');
			if (!$scp->LogPathLink)
			{
				printf("error 'scp_log_stream(3)' failed created log stream '%s' .\n", $scp->LogPathRoot);
				return 0;
			}
			
			/*! \retval input file to this process */
			system(sprintf("chmod 0777 %s", $scp->LogPathRoot));
			app_func_copy($ret, $scp->LogPathLink);
		}
	}
	

	/*! \retval If Succcess then write log */	
	if (is_resource($scp->LogPathLink))
	{
		@fwrite( $scp->LogPathLink, 
			sprintf("%s\n", $str)
		);
	}
	
	return $ret;
	
 }
/*!
 * \brief 	scp_log_error
 * \note 	[description]
 * \retval  [description]
 *
 */
 function scp_log_error($str = NULL, $file= NULL, $line = 0)
{
	Global $scp;  
	
	/*! make void Time */
	if ( function_exists('scp_log_main') )
	{
		if (file_exists($file))
		{ 
			scp_log_main('SCP', sprintf("-- ERROR: %s:%s %s", basename($file), $line, $str));
		}
	} 
	return 0;
}

/*!
 * \brief 	scp_log_warn
 * \note 	[description]
 * \retval  [description]
 *
 */
 function scp_log_warn($str = NULL, $file= NULL, $line = 0)
{
	Global $scp;  
	
	/*! make void Time */
	if ( function_exists('scp_log_main') )
	{
		if (file_exists($file))
		{ 
			scp_log_main('SCP', sprintf("-- WARNING: %s:%s %s", basename($file), $line, $str));
		}
	} 
	return 0;
}

/*!
 * \brief 	scp_log_info
 * \note 	[description]
 * \retval  [description]
 *
 */
 function scp_log_info($str = NULL, $file= NULL, $line = 0)
{
	Global $scp;  
	
	/*! make void Time */
	if ( function_exists('scp_log_main') )
	{
		if (file_exists($file))
		{ 
			scp_log_main('SCP', sprintf("-- INFO: %s:%s %s", basename($file), $line, $str));
		}
	} 
	return 0;
}

/*!
 * \brief 	scp_log_notice
 * \note 	[description]
 * \retval  [description]
 *
 */
 function scp_log_notice($str = NULL, $file= NULL, $line = 0)
{
	Global $scp;  
	
	/*! make void Time */
	if ( function_exists('scp_log_main') )
	{
		if (file_exists($file))
		{ 
			scp_log_main('SCP', sprintf("-- NOTICE: %s:%s %s", basename($file), $line, $str));
		}
	} 
	return 0;
}

/*!
 * \brief 	scp_log_app
 * \note 	[description]
 * \retval  [description]
 *
 */
 function scp_log_app($str = NULL)
{
	Global $scp;  
	
	/*! make void Time */
	if ( function_exists('scp_log_main') )
	{
		// scp_log_main('SCP', $str);
		scp_log_main('SCP', sprintf("%s:%s %s", basename(__FILE__), __LINE__, $str));
		
	} 
	return 0;
}

/*!
 * \brief 	scp_log_core
 * \note 	[description]
 * \retval  [description]
 *
 */
 function scp_log_core( $name = "", $log = NULL )
{
	Global $scp;  
	if ( $name == '' )
	{
		scp_log_error(" dump 'scp_log_core(1)' Fail.", __FILE__, __LINE__);
		return 0;
	}
	
	if ( !file_put_contents( $scp->LogPathCore ."/$name.$scp->AppProgPid.core", print_r($log, true)) )
	{
		scp_log_error(" dump 'scp_log_core(2)' Fail.", __FILE__, __LINE__);
		return 0;
	} 
	
	return 0;
}

/*!
 * \brief 	scp_log_baner
 * \note 	[description]
 * \retval  [description]
 *
 */
 function scp_log_scr(&$scp = NULL, $log = "" )
{
	/*! \retval sent to log stream */ 
	scp_log_stream($scp, $log); 
	
	/*! \retval set color log for screen */
	if ($scp->AppProgScr &&(app_func_color($log, 32))) 
	{
		printf("%s\n", $log);
	} 
	
	app_func_free($log);
	return 0;
 }
 
/*!
 * \brief 	scp_log_baner
 * \note 	[description]
 * \retval  [description]
 *
 */
 function scp_log_close(&$scp = NULL)
{
	if( is_resource($scp->LogPathLink) ){
		fclose($scp->LogPathLink);
	}
	return 0;
 }
 

