<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */ 
 
 class udp
{
	var $host = '127.0.0.1';
	var $port = '60001'; 
	var $sock = -1;
	var $recv = "";
	var $buf  = "";
	
	/*! \remark construct from global object */
	function __construct()
   {
		Global $scp;
		
		/* \remark set UDP config 'app_func_copy' */
		app_func_copy($this->host, $scp->MgrUdpHost);
		app_func_copy($this->port, $scp->MgrUdpPort);
		
		if (sockUdpClientCreate($this->sock)<0)
		{
			sockUdpClose($this->sock);
		}
		
		return $this;
	}
	
	/*! \remark construct from global 'sent' */
	function sent(&$recv = '')
	{
		app_func_copy($recv, '');
		if ($this->get($buf)) 
		{	
			$this->encrypt($buf);
			sockUdpWrite($this->sock, $buf, $this->host, $this->port);
			if ( sockUdpRecv($this->sock, $this->recv) ) 
			{
				$this->decrypt($this->recv);
				if ( app_func_copy($recv, $this->recv) ){
					app_func_free($this->recv);
				}
				sockUdpClose($this->sock);
			}
		}
		
		return $recv;
	}
	
	function set($key = NULL, $val = '') 
	{
		if (!is_array($key)) {
			$key = array( $key => $val );
		}
		
		foreach ($key as $k => $v){
			app_func_stream($this->buf, sprintf("%s:%s", $k, $v));
		}
		
		return $this;
	}
	
	/*! \remark construct from global 'get' */
	function get(&$buf = '')
	{
		if (app_func_enter($this->buf)) 
		{
			if( app_func_copy($buf, $this->buf)){
				app_func_free($this->buf);
			}
		}
		return $buf;
	} 
	
	/*! \remark encrypt  UDP 'sent' */
	function ontrace($buf = NULL, &$ret = 0 ){
		app_func_copy($ret, 0);
		if (!onTraceUdpReceive($buf, $ret) ){
			return 0;
		}
		return $ret;
	}
	
	/*! \remark encrypt  UDP 'sent' */
	function encrypt(&$buf = NULL)
    {
	   return app_func_encrypt($buf);
	}
	
	/*! \remark decrypt UDP 'recv' */
	function decrypt(&$buf = NULL)
    {
	   return app_func_decrypt($buf);
	}
	
}

?>