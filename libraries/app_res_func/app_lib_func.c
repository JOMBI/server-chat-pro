<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/*!
 * \brief  {fnc} <\ app_func_memcopy>
 *
 * \param  {out} <\ description>
 * \retval {ret} <\ description>
 *
 */	 
 function app_func_memcopy(&$s, &$q = NULL)
{
	$s = $q;
	return $s; 
} 

/*!
 * \brief  {fnc} <\ app_func_microstr>
 *
 * \param  {out} <\ description>
 * \retval {ret} <\ description>
 *
 */	
 function app_func_microstr()
{
	Global $scp;
	$micro_time = sprintf("%s%s", strtotime('now'), $scp->AppProgId);
	return $micro_time;
}

/*!
 * \brief  {fnc} <\ app_func_createid>
 *
 * \param  {out} <\ description>
 * \retval {ret} <\ description>
 *
 */	
 function app_func_createid()
{
	Global $scp;
	
	$CreateId = ( $scp->CreateId>99999 ?($scp->CreateId=1) : ++ $scp->CreateId );
	return sprintf("%s%05d", app_func_microstr(), $CreateId);
}

/*!
 * \brief  {fnc} <\ app_func_createboxid>
 *
 * \param  {out} <\ description>
 * \retval {ret} <\ description>
 *
 */	
 function app_func_createboxid()
{
	Global $scp;
	$CreateId = ($scp->AppBoxId>99999?($scp->AppBoxId=1):++$scp->AppBoxId);
	return sprintf("%s%05d", app_func_microstr(), $CreateId);
}

/*!
 * \brief  {fnc} <\ app_func_msgid>
 *
 * \param  {out} <\ description>
 * \retval {ret} <\ description>
 *
 */
 function app_func_msgid()
{
	Global $scp;
	$CreateId = ($scp->AppMsgId>99999?($scp->AppMsgId=1):++$scp->AppMsgId);
	return sprintf("%s%05d", app_func_microstr(), $CreateId);
}


/*!
 * \brief  {fnc} <\ app_func_memcopy>
 *
 * \param  {out} <\ description>
 * \retval {ret} <\ description>
 *
 */	
 function app_func_beetween($string, $start, $end, &$retval ='' )
{
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) 
	{
		$retval = '';
		return '';
	}
	
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    $retval = substr($string, $ini, $len);
	return $retval;
}

/*!
 * \brief  {fnc} <\ app_func_memcopy>
 *
 * \param  {out} <\ description>
 * \retval {ret} <\ description>
 *
 */	 
 function app_func_file(&$f = NULL)
{
	if ( file_exists($f) )
	{
		return $f;
	}
	return 0;
} 
 
 
/*!
 * \brief  {fnc} <\ app_func_color>
 *
 * \param  {out} <\ description>
 * \retval {ret} <\ description>
 *
 */	
 function app_func_color(&$out = NULL, $code= 32)
{
   /*! 
    * \code Simple Text Color 
    *
	* \param {red}	  < \033[31m{text} \033[0m\n >;
	* \param {green}  < \033[32m{text} \033[0m\n >;
	* \param {yellow} < \033[33m{text} \033[0m\n >;
	* \param {blue}   < \033[36m{text} \033[0m\n >;
	*
	*/
	
	/*! \retval default */
	$outcolor = "%s";  
	if ($code == 31){
		$outcolor = "\033[31m%s \033[0m";
	}
	else if ($code == 32){
		$outcolor = "\033[32m%s \033[0m";
	}
	else if ($code == 33) {
		$outcolor = "\033[33m%s \033[0m";
	}
	else if ($code == 34) {
		$outcolor = "\033[34m%s \033[0m";
	}
	else if ($code == 35) {
		$outcolor = "\033[35m%s \033[0m";
	}
	else if ($code == 36) {
		$outcolor = "\033[36m%s \033[0m";
	}
	
	/*! \retval text color */
	$out = sprintf($outcolor, $out);
	return strlen($out);
 }
 
/*!
 *\brief  app_func_lower 
 *\param  @param description 
 *\retval @return description
 *
 */
 function app_func_lower(&$str= '')
{ 
	$str = strtolower($str);
	return $str;
}

/*! \remark app_func_pathinfo */
 function app_func_pathinfo($file= '', &$ret= false)
{ 
	$retval =& $ret;
	$retval = @pathinfo($file);
	if ( sizeof($retval)<1 )
	{
		return 0;
	}
	
	/*! \remark return object */
	$retval = app_func_retval($retval);
	return $retval;
}

/*!
 *\brief  app_func_upper 
 *\param  @param description 
 *\retval @return description
 *
 */
 function app_func_upper(&$str= '')
{ 
	$str = strtoupper($str);
	return $str;
}
 /*!
 *\brief  func_copy 
 *\param  @param description 
 *\retval @return description
 *
 */
 function app_func_search($key= NULL, $arr= NULL )
{ 
	if ( !is_array($arr) ) {
		return 0;
	}
	
	if ( in_array( $key, $arr) )
	{
		return $key;
	}		
	
	return 0;
}

/*!
 *\brief  app_func_replace 
 *\param  @param description 
 *\retval @return description
 *
 */
 function app_func_replace($src = '', &$target= NULL, $content = NULL)
{ 
	$target = str_replace( array($src), array($content), $target);
	if ( $target ){
		return $src;
	}
	return 0;
	
}

/*!
 *\brief  app_func_output 
 *\param  @param description 
 *\retval @return description
 *
 */
 function app_func_output(&$out= NULL, $buf = NULL, &$ret = 0 )
{ 
	if (strlen($buf)<1 ){
		return 0;
	}
	
	/*! \def all to object data process */
	$out .= $buf;
	if ( app_func_sizeof($buf, $ret) ){
		return $ret;
	}
	return 0;
}

/*!
 *\brief  app_func_stream 
 *\param  @param description 
 *\retval @return description
 *
 */
 function app_func_stream(&$out= NULL, $buf = NULL, &$ret = 0 )
{ 
	if (strlen($buf)<1 ){
		return 0;
	}
	
	/*! \def all to object data process */
	$out .= sprintf("%s".chr(124), $buf);
	if ( app_func_sizeof($out, $ret) ){
		return $ret;
	}
	return 0;
}
/*!
 * \brief  app_func_isbase64
 *
 * \param  {buf} data array  
 * \param  {ret} cursor or pointer 	
 *
 * \retval  mixed 
 *
 */	

 function app_func_isbase64($s)
{
	if ((bool) preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $s) === false) {
        return false;
    }
	
	$decoded = base64_decode($s, true);
	if ($decoded === false) {
		return false;
    }
	
	$encoding = mb_detect_encoding($decoded);
	if (! in_array($encoding, array('UTF-8', 'ASCII'), true)) {
		return false;
    }
	return $decoded !== false && base64_encode($decoded) === $s;
}

/*!
 * \brief  app_func_decode
 *
 * \param  {buf} data array  
 * \param  {ret} cursor or pointer 	
 *
 * \retval  mixed 
 *
 */	
  function app_func_decode($buf= NULL, &$ret = false)
 {
	app_func_free($ret); 
	if ($base64_decode = base64_decode($buf)){
		app_func_copy($ret, 
			json_decode( $base64_decode)
		);
	}
	return $ret;
	
 }
 /*!
 * \brief  app_func_eval
 *
 * \param  {buf} data array  
 * \param  {ret} cursor or pointer 	
 *
 * \retval  mixed 
 *
 */	
 function app_func_eval(&$o =NULL, $key)
{
	if ( !is_object($o) ){
		return false;
	}		
	
	if ( !isset($o->$key) ){
		return false;
	}
	
	return $o->$key;
 }
 
/*!
 * \brief  app_func_encoder
 *
 * \param  {buf} data array  
 * \param  {ret} cursor or pointer 	
 *
 * \retval  mixed 
 *
 */	
  function app_func_encode($buf= NULL, &$ret = false)
 {
	app_func_copy($eval, 0); 
	app_func_maloc( $ret, 
		array( 'event' => NULL, 'data' => NULL )
	);
	
	/*! for Event data */
	if (app_func_typdef($buf, $out))
	{
		if ( isset($out->event) ){
			app_func_copy($ret->event, $out->event);
			app_func_next($eval);
		}
		
		if (isset($out->data)){
			app_func_copy($ret->data, 
				base64_encode(	
					json_encode($buf['data'])
				)
			);
			app_func_next($eval);
		}
		
		app_func_free($out);
		app_func_free($buf);
	} 
	
	/*! retval is Object */
	if ( $eval){ 
		return $ret;
	}
	return 0;
	
 }
/*!
 * \brief  app_func_typdef
 *
 * \param  {buf} data array  
 * \param  {ret} cursor or pointer 	
 *
 * \retval  mixed 
 *
 */	
 function app_func_typdef($buf= NULL, &$ret = 0 )
{
	if ( !is_array($buf) ){
		return 0;
	}	 
	
	// The we convert the json string to a stdClass()
	if ($buf= json_encode($buf)) {
		$buf = json_decode($buf);
	}
	
	if ( app_func_copy($ret, $buf) ){
		return $ret;
	}
	return 0;
 }
 
 /* \retval app_func_retval */
  function app_func_retval($buf= NULL, &$ret = 0 )
{
	app_func_free($ret);
	if ( !is_array($buf) ){
		return 0;
	}	 
	
	$buf = (object)$buf;	
	if (app_func_copy($ret, $buf)){
		return $ret;
	}
	return 0;
 }
 

/*!
 * [app_func_enter]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 function app_func_enter(&$buf= "" )
 {
	if ( app_func_sizeof($buf)<1 ){
		return 0;
	}
	
	// check for Enter end string 
	$buf = trim($buf);
	if (!strcmp(substr($buf, -1,1), chr(124)))
	{
		$buf = rtrim($buf , chr(124));
	}
	
	/*! \def all to object data process */
	$buf .= SOK_EINTR;
	if ( app_func_sizeof($buf, $ret) ){
		return $ret;
	}
	return 0;
 }
 
/*!
 *\brief  func_copy 
 *\param  @param description 
 *\retval @return description
 *
 */
 function app_func_copy(&$out= NULL, $key= NULL, $val= NULL, $file = NULL, $line= 0 )
{ 
	while (1)
	{
		/*! for Asumption object with key */  
		if ( is_object($out) &&( !is_null($key) ))
		{
			if ( !is_array($key) )
			{
				if (!is_bool($key) && !is_object($key) )
				{
					$out->$key = $val;
					break;
				}
				
				if (!is_bool($key) && is_object($key) )
				{
					echo "ERROR\n";
					print_r($key);
				}
			}
			
			break;
		}
		
		/*! for Asumption array with key */ 
		if ( is_array($out) &&( !is_null($key) ))
		{
			/*! \remark cleanup process non array */
			if ( !is_array($key) )
			{
				$out[$key] = $val; 	
			
			}			
			break;
		}
		
		/*! by default is object will 
		retval this */
		
		$out = $key;
		break; 
	}	 
	/*! finaly return ===  0 */
	return $out;
}

/**
 *\brief  app_func_strpos 
 *\param  description 
 *\retval description
 */
 function app_func_strpos($str= "", $chr = "")
{
	return strpos($str, $chr);
}

/** 
 *\brief  app_func_match 
 *\param  description 
 *\retval description
 */
 function app_func_substr($str= "", $chr = "", $num = 1)
{
	return substr( strchr($str, chr($chr) ), 1);
}

/*! 
 *\brief  app_func_trim 
 *\param  description 
 *\retval description
 */
function app_func_explode(&$ret = 0, $out = NULL, $chr = '')
{
	$c = explode($chr, $out);
	if( is_array($c) && app_func_trim($c) ){
		if (app_func_copy($ret, $c)){
			app_func_free($c);
		}
	}
	return $ret;
}

/*! 
 *\brief  app_func_encrypt 
 *\param  description 
 *\retval description
 */
 function app_func_encrypt(&$str = NULL) 
{
	return encrypt($str);
}

/*! 
 *\brief  app_func_decrypt 
 *\param  description 
 *\retval description
 */
 function app_func_decrypt(&$str = NULL) 
{
	return decrypt($str);
}


/*! 
 *\brief  app_func_trim 
 *\param  description 
 *\retval description
 */
function app_func_trim($out = NULL)
{
	if (is_array($out))
	{
		$out = array_map('trim', $out);
		return $out;
	}
	
	$out = trim($out);
	return $out;	
}

/**
 *\brief  cti_func_class  
 *\param  <out> description  
 *\retval description
 */
 function cti_func_class($o = NULL, &$ret = false)
{
	/*! \retval PHP if non object no detecte class or Null */
	/*! \retval PHP Bugs is called without carification process */ 
	if ( !is_object($o) ) 
	{
		return 0;
	} 
	/*! \retval return <get_class> data string */
	$ret = get_class($o); 
	return $ret;
 }
 
/**
 *\brief  app_func_delete  
 *\param  <out> description  
 *\retval description
 */
 
 function app_func_delete(&$out, $key = '' )
{
	app_func_copy($ret, 0);
	
	if ( is_array($out) )
	{
		if ( isset($out[$key]) ) 
		{
			unset($out[$key]);
			app_func_next($ret);
		}
		
		return $ret;
	}
	
	if ( cti_func_class($out, $name) )
	{
		if ( ($name == 'stdClass') && isset($out->$key) )
		{
			unset($out->$key);
			app_func_next($ret);
		}
		
		return $ret;
	}
	
	return 0;
}

/**
 *\brief 	func_free 
 *\param 	@param description 
 *\retval  @return description
 */
function app_func_free(&$out = NULL)
{
	$out = NULL;
	return $out;	
}


/*! 
 *\brief 	app_func_maloc  
 *\param 	@param description  
 *\retval  @return description 
 */
function app_func_maloc(&$out = NULL, $item = NULL )
{
	
	/*! \retval Created Object 'stdClass' */
	app_func_struct($out);
	if (!$out){
		return 0;
	}
	
	/*! \retval next if object created success */
	if (false === is_null($item) )
	{
		return 0;
	}
	
	/*! \retval If Not Array & Array Condition */
	if (!is_array($item))
		app_func_copy($out, $item, NULL); 
	else if (is_array($item))
	{
		foreach ($item as $i => $j ){
			app_func_copy($out, $i, $j);
		}
	}
	return $out;
}

/*! 
 *\brief 	func_struct  
 *\param 	@param description  
 *\retval  @return description 
 */
function app_func_struct(&$out = NULL)
{ 
  $out = NULL;
  if (!is_object($out) ){
	$out = new stdClass();  
  }
  return $out;
}

/*! 
 *\brief 	func_struct  
 *\param 	@param description  
 *\retval  @return description 
 */
function app_func_call($func = NULL , $argc = NULL )
{	
	if (function_exists($func) ){
		/*! if function exist with arguments */
		if (is_array($argc)){
			return call_user_func_array($func, $argc );
		} 
		/*! function not exist false argc */
		if (!is_array($argc)){
			return call_user_func_array( $func, 
				array()
			);
		}
	}
	return 0;
}

/*! 
 *\brief 	func_alloc 
 *\param 	@param description 
 *\retval  @return description
 */
function app_func_alloc(&$out = NULL)
{ 
	if ( !is_array($out) ){
		$out = array();
	}
	return $out;
}

/*! 
 *\brief 	func_alloc 
 *\param 	@param description 
 *\retval  @return description
 */
function app_func_nowtime(&$out = NULL)
{ 
	$out = date('H:s', strtotime('now'));
}

/*! 
 *\brief 	app_func_sizeof 
 *\param 	@param description 
 *\retval  @return description
 */
function app_func_sizeof($out = NULL, &$ret= 0)
{
	if (!is_array($out)){
		$ret = strlen($out);
		return (int)$ret;
	}
	/*! is this array sections */
	$ret = sizeof($out);
	return (int)$ret; 
}


/*!
 * \brief 	func_str  
 *\param 	@param description  
 *\retval  @return description 
 */
function app_func_str($out = NULL, &$ret = NULL)
{ 
	$ret = (string)$out;
	return $ret;
	
}

/*!
 *\brief 	app_func_sint  
 *\param 	@param description 
 *\retval  @return description 
 */
function app_func_sint($out = NULL, &$ret = NULL)
{ 
	$ret = sprintf('%d', $out);
	return $ret;
}

/*!
 *\brief 	func_int  
 *\param 	@param description 
 *\retval  @return description 
 */
function app_func_int(&$out = NULL, &$ret = NULL)
{ 
	$ret = (int)$out;
	return $ret;
}

/*!
 *\brief   app_func_time  
 *\param   @param description 
 *\retval  @return description 
 */
function app_func_time()
{ 
	return strtotime('now');
}

/*!
 *\brief  app_func_interval  <time * unixtimestamp>
 *\param  @param description 
 *\retval @return description 
 */
function app_func_interval($time = 0, $now = 0)
{ 
	if( !$time )
	{
		return 0;	
	}
	
	if( !$now){
		$now = app_func_time();
	}
	
	$intval = ( $now - $time);
	return $intval; 
}

/*!
 *\brief  duration  <now * unixtimestamp>
 *\param  @param description 
 *\retval @return description 
 */
 function app_func_duration($seconds = 0 )
{
	app_func_struct($sec);
	app_func_copy($sec->sec , 0);  
	app_func_copy($sec->min , 0); 
	app_func_copy($sec->hour, 0); 
    app_func_copy($sec->sec , $seconds%60); 
	app_func_copy($seconds  , floor($seconds/60));
	
	if ( $seconds )
	{
		app_func_copy($sec->min  ,$seconds%60);
        app_func_copy($sec->hour ,floor($seconds/60));
    }
	
	// get data process :
	app_func_copy($sec->retval, NULL);
	if ($seconds == 0 &&($sec->sec == 0)) 
	{
		$sec->retval = sprintf("");
	}
    else{
		$sec->retval = sprintf("%02d:%02d:%02d", $sec->hour, $sec->min, $sec->sec);
	} 
	// return data :
	return $sec->retval;
 } 
 
/*!
 *\brief  app_func_timer  
 *\param  description 
 *\retval description 
 */
function app_func_tick($tick = 0, &$ret = 0)
{ 
	if (!$tick)
	{
		return 0;
	}
		
		
	$ret = (time() - $tick);
	return $ret;
	
}

/*! 
 *\brief 	func_float  
 *\param 	@param description 
 *\retval  @return description
 *
 */
function app_func_float(&$out = NULL, &$ret = NULL)
{ 
	$ret = (float)$out;
	return $ret;
}

/** 
 *\brief for increment integer loopback like ( ++i ) 
 * data value by user reference 
 * 
 *\param integer from user reference if empty / null 
 * by default int = 0 
 *
 *\retval return is integer 
 *
 */
function app_func_next(&$out = 0)
{
   /*! \note will increment process if have number choice  
	*  resent back to user request .  
	*/
	$out = app_func_int($out); ++$out;
	return $out;
}

/** 
 *\brief 	func_wait 
 *
 *\param 	@param description 
 *\retval  @return description
 *
 */
function app_func_wait( $wt= 1, $fn= NULL, $gc = NULL )
{ 
  /*! must be true */
  if (!app_func_struct($o)){
	return 0;  
  }
  
  /** copy to list object 
  "app_func_copy" */
  
  app_func_copy($o->wt, $wt);
  app_func_copy($o->fn, $fn);
  app_func_copy($o->gc, $gc);
  
  /*! next process 
  Iteration */
  
  app_func_copy($o->ts, 1000000);
  app_func_copy($o->tj, 0);
  
  while ($o->wt){
	if ($o->tj >= $o->wt){
		app_func_call( $o->fn, 
			$o->gc
		);
		break;
	}
	
    /** next sleep Timeout process */
	if (app_func_next($o->tj)){
		usleep($o->ts);
	}
  }
  /** break if false process */
  app_func_free($o);
  return 0;
  
}

/** 
 *\brief 	app_func_sock 
 *
 *\param 	@param description 
 *\retval  @return description
 *
 */
function app_func_sock($out = NULL, &$ret = NULL)
{ 
	if ( !is_resource($out) ){
		$ret = 0;
		return $ret;
	}
	
	$ret = $out;
	return $ret;	
}

/** 
 *\brief 	app_func_sock 
 *
 *\param 	@param description 
 *\retval  @return description
 *
 */
 function app_func_event($fn = '', $var = NULL, $file = NULL, $line = NULL )
{
	if ( !function_exists($fn))
	{
		scp_log_error( sprintf( "dispatchEvent: '%s()' Error. ", $fn), $file, $line);
		return 0;
	}
	
    if (is_array($var) )
	{ 
		return call_user_func_array($fn, $var);
	}
	
	if (!is_array($var) )
	{ 
		return call_user_func_array($fn, 
			array($var)
		);
	} 
	return 0;
}