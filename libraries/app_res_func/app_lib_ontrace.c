<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */ 
 function onTraceUdpReceive(&$s= NULL, &$ret= false)
{
	app_func_struct($out);
	if (!app_func_explode($out->f, $s, SOK_EINTR))
	{
		app_func_free($s); /*! \retval failed procss */
		return 0;
	}
	
	/*! \remark alloc_memory */
	app_func_alloc($out->p);
	foreach ($out->f as $r)
	{
		app_func_sizeof($r, $out->n);
		if (!$out->n){ 
			app_func_free($out->n);
			continue;
		}
		
		app_func_explode($out->c, $r, chr(124));
		foreach ($out->c as $c)
		{
			if (app_func_explode($out->q, $c, chr(58)))
			{
				if ( strcmp($out->q[0], 'Data') )
					app_func_copy($out->p, $out->q[0], $out->q[1]); 
				else if ( !strcmp($out->q[0],'Data') )
				{
					if( false != app_func_decode($out->q[1], $out->a) ){
						app_func_copy($out->p, $out->q[0], $out->a);	
					}				
					else if( false == app_func_decode($out->q[1], $out->a) ){
						app_func_copy($out->p, $out->q[0], $out->q[1]);	
					}
				}
			}
			
			/*! \retval clear Memory usage */
			app_func_free($out->q);
		}
		
		/*! \retval clear Memory usage */
		app_func_free($out->c);
	}
	
	if ( app_func_copy($ret, app_func_typdef($out->p)) ){
		app_func_free($out);
	} 
	 
	return $ret; 
}

/*! \retval "onTraceUdpAction" */
 function onTraceUdpConsole(&$s= NULL, &$ret= false)
{
	app_func_copy($ret, 0);
	if( isset($s->Data) && !is_object($s->Data) ) 
	{
		app_func_next($ret);
	}
	
	return $ret; 
}


/*! \retval onTraceWssReceive <buf* bufer, ret array object > */
 function onTraceWssReceive(&$buf= NULL, &$ret= false)
{
	app_func_struct($out);
	
	/*! \retval failed procss */
	if ( !app_func_explode($out->f, $buf, SOK_EINTR) )
	{
		app_func_free($buf); 
		return 0;
	}
	
	$out->ptr = 0;
	$out->buf = array();
	
	if (is_array($out->f)) 
	foreach( $out->f as $ptr => $data )
	{
		if( $out->buf[] = json_decode($data, true) )
		{
			app_func_next($out->ptr);
		}
	}
	
	if (app_func_copy($ret, $out->buf))
	{
		return $out->ptr;
	}
	
	return 0;
}


/*! \retval onTraceAppsReceive <buf* bufer, ret array object > */
 function onTraceAppsReceive(&$buf= NULL, &$ret= false)
{
	app_func_struct($out);
	
	/*! \retval failed procss */
	if ( !app_func_explode($out->f, $buf, SOK_EINTR) )
	{
		app_func_free($buf); 
		return 0;
	}
	
	$out->ptr = 0;
	$out->buf = array();
	
	if (is_array($out->f)) 
	foreach( $out->f as $ptr => $data )
	{
		if( $out->buf[] = json_decode($data, true) )
		{
			app_func_next($out->ptr);
		}
	}
	
	if (app_func_copy($ret, $out->buf))
	{
		return $out->ptr;
	}
	
	return 0;
 }
 
 /*! \retval onTracePeerReceive <buf* bufer, ret array object > */
 function onTracePeerReceive(&$buf= NULL, &$ret= false)
{
	return 0; 
 }