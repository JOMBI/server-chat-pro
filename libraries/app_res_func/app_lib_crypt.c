<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
 
 /*! \remark encrypt for stream */
 function encrypt(&$str = NULL)
{
	app_func_copy($str, str_split($str));
	app_func_copy($chr, '\x');
	app_func_copy($ret, '');
	
	foreach ($str as $c) 
	{
		app_func_output($ret, sprintf("%s\x", ord($c) ));
	} 
	
	app_func_free($str);
	if (stristr($ret, '\x')) 
	{
		app_func_copy($str, rtrim($ret, '\x'));
		app_func_free($ret);
	} 
	
	return $str;
 }
  
 /*! \remark decrypt for stream */
 function decrypt(&$str = '')
{
	app_func_copy($res, NULL);
	app_func_copy($ret, NULL); 
	
	if (app_func_explode($ret, $str, '\x'))
	{
		foreach ($ret as $i => $c){
			app_func_output($res, sprintf("%s", chr($c) ));
		} 
		app_func_free($ret);
	} 
	
	app_func_copy($str, NULL);
	if (app_func_copy($str, $res))
	{
		app_func_free($res);
	}
	
	return $str;
 }
  