<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
/*!
 ASCI - Table Number 
 -------------------
 ord {!} chr {33}
 ord {#} chr {35}
 ord {$} chr {36}
 ord {%} chr {37}
 ord {&} chr {38}
 ord {(} chr {40}
 ord {)} chr {41}
 ord {*} chr {42}
 ord {+} chr {43}
 ord {,} chr {44}
 ord {-} chr {45}
 ord {.} chr {46}
 ord {/} chr {47}
 ord {:} chr {58}
 ord {;} chr {59}
 ord {<} chr {60}
 ord {=} chr {61}
 ord {>} chr {62}
 ord {?} chr {63}
 ord {@} chr {64}
 ord {[} chr {91}
 ord {\} chr {92}
 ord {]} chr {93}
 ord {^} chr {94}
 ord {_} chr {95}
 ord {`} chr {96}
 ord {{} chr {123}
 ord {|} chr {124}
 ord {}} chr {125}
 ord {~} chr {126}
 
 space 		\x20
 tab	 	\t 
 new line 	\n 
 enter 		\r\n 
 */ 
  
/*! 
 * \brief  app_cfg_parser 
 *
 * \param <\scp > Global Object Reference  
 *
 * [0] => /opt/kawani/bin/chatpro/scp.c
 * [1] => --pid=/var/run/scp/scp.pid
 * [2] => --vvv=1
 * [3] => --cfg=scp2.conf
 
 *
 * \retval Void(0)
 *
 */
 function app_cfg_main(&$scp = NULL)
{
	Global $argc, $argv;  
	
	
   /*! 
    * \remark Untuk read Config Tidak di baca dari argumen melainkan langsung aksess ke object lokasi 
	* Dengan Tujuan tidak lagi config di simpan di header karena terlalu panjang dan applikasi 
	* tidak bisa di buat paralel layaknya yang sudah - sudah 
	*
	* Applikasi Ini sifatnya tunggal dan tidak bisa multibase 
	*
	*/
	
	// /opt/kawani/bin/chatpro/scp.c --pid=/var/run/scp/scp.pid --cfg=scp2.conf --verbose=1 
	if ($argc < 3)
	{
		scp_log_error("Invalid Arguments Configuration.", __FILE__, __LINE__ );
		exit(0);
	}
	
	app_func_struct($out);
	app_func_copy($out->app, $argv[0]);		/*!> Run of applikasi */
	app_func_copy($out->run, $argv[1]);		/*!> Pid Of Run 	  */
	app_func_copy($out->scr, $argv[3]);		/*!> scren debuger 	  */
	
	
	
	/*! for customize config */
	app_func_copy($out->rem, false);
	if ( isset($argv[2]) )
	{
		app_func_copy($out->rem, $argv[2]);		/*!> config customize */
	} 
	
   /*! \remark 
	* Untuk Mendapatkan file config bisa dengan cara check run app dir kemudian 
	* set by default saja dengan asumsi file config tersebut memang ada 
	*
	*/
	
	if ($out->rem)
	{
		$cfg = substr($out->rem, strpos($out->rem, chr(61))+1, strlen($out->rem));
		
		/*! \remark jika file "full path langsung baca aja"*/
		if ( file_exists($cfg) ){
			app_func_copy($out->cfg, $cfg); 
		}
		else if ( !file_exists($cfg) )
		{
			app_func_copy($out->cfg, dirname($out->app) .'/config/'. $cfg); 
		} 
	}
	
	else if (!$out->rem)
	{
		app_func_copy($out->cfg, dirname($out->app) .'/config'); 
		
		/*! \remark validation of directory */
		if (!is_dir($out->cfg))
		{
			scp_log_error("Invalid Path Configuration.", __FILE__, __LINE__ );
			exit(0);
		}
		
		/*! \remark Adding To Hash output */
		app_func_output($out->cfg, '/');
		if (app_func_lower($scp->AppProgUid))
		{
			app_func_output($out->cfg, sprintf('%s.conf', $scp->AppProgUid));
		}
		
	} 
	
	/*! \remark check file pakah ada atau kosong */
	/*! \remark Rollback UID progname */
	
	app_func_upper($scp->AppProgUid);
	if (!app_func_file($out->cfg))
	{
		scp_log_error(sprintf("Config File '%s' .", $out->cfg), __FILE__, __LINE__ );
		exit(0);
	}
	
	/*! \remark for scren hide by default show */
	app_func_copy($scp->AppProgScr, 1); 
	if ( isset($argv[3]) )
	{	
		/*! \remark < scr :  0 is hide console 1:show console > */
		app_func_copy($scp->AppProgScr, 
			(int)substr($out->scr, strpos($out->scr, chr(61))+1, strlen($out->scr)) 
		);
	}
	
	// check data for validation 
	if ($out->run) {
		app_func_copy( $out->run, 
			substr($out->run, strpos($out->run, chr(61))+1, strlen($out->run))
		);
	}
	
	// next Iterator process  
	if( !$scp->AppProgCfg )
	{
		app_func_copy($scp->AppProgCfg, $out->cfg);
	}
	
	// running config 
	if ( !$scp->AppProgLck )
	{
		app_func_copy($scp->AppProgLck, $out->run);
	} 
	 
	/*! \remark free file config */
	app_func_free($out);
	
	/*! \remark grep All Config File */
	return $scp->AppProgPid;
}


/**!
 * \brief 	app_cfg_parser
 * \note 	[description]
 * \retval  [description]
 *
 */
 function app_cfg_parser($buf = NULL, &$ret= 0)
{
	app_func_struct($cfg);
	app_func_copy($ret, 0);
	
	if (!$buf)
	{
		return 0;	
	}
	
	/*! \retval if read config fail is false */
	if (false === ($cfg->sof = file_get_contents($buf)) )
	{
		return 0;
	}		
	
	/*! \retval if read config fail is false */
	if( false == ($cfg->buf = array_map('trim', explode("\n", $cfg->sof)) ))
	{
		return 0;
	}
	
	/*! \retval if Next Iterator successfuly */ 
	app_func_copy( $cfg->size, count($cfg->buf));
	app_func_copy( $cfg->next, 0);
	
	/*! \remark Create Allocation Array Populated */ 
	
	app_func_alloc($cfg->app); 
	while ( $cfg->next < $cfg->size )
	{
		/*! \remark copy link from app */
		app_func_copy($cfg->ptr, 
			trim($cfg->buf[$cfg->next])
		);
		
		/*! \remark validation length */
		if (!strlen($cfg->ptr))
		{
			app_func_next($cfg->next);
			continue;
		}   
		
		/*! \remark check istring not valid */
		if (substr($cfg->ptr, 0, 1) == chr(59))
		{
			app_func_next($cfg->next);
			continue;
		}
		
		/*! \remark validation of data process */
		if (false == $cfg->ptr){
			app_func_next($cfg->next);
			continue;
		}
		
		/*! \remark validation of data process */
		app_func_copy($cfg->has, stristr($cfg->ptr, chr(91)));
		
		if (false != $cfg->has)
		{
			app_func_copy($cfg->hex, strpos($cfg->ptr, chr(91)));
			
			if ($cfg->idx = substr($cfg->ptr, $cfg->hex+1, strlen($cfg->ptr)-2))
			{
				app_func_alloc($cfg->app[$cfg->idx]); 
			}
		}
		
		/*! \remark for body context here then will exclude */
		
		if (false == $cfg->has)
		{
			if (list($i, $j) = array_map( 'trim', explode( chr(61), $cfg->ptr )))
		    {
				/*! \retval for checking hash */ 
				if ( false !== ($n = strpos( $j, chr(59) ) )){
					$j = trim(substr($j, 0, $n));
				}					
				
				/*! \retval push data to here */ 
				$cfg->app[$cfg->idx][$i] = $j; 
			}
		} 
		
		/*! \def next loop process */
		app_func_next($cfg->next);
	}
	
	/*! \retval if successfuly then clear Allocation Size */  
	if (app_func_copy($ret, $cfg->app))
	{
		app_func_free($cfg);
	}  
	
	return $ret;
} 

/*!
 * \brief 	app_cfg_param
 * \note 	[description]
 * \retval  [description]
 *
 */ 
 function app_cfg_param($cfg = NULL, $key = NULL, &$ret = '')
{
	Global $scp;
	
	/*! jika di temukan keynya */
	if( !isset($scp->AppProgVar[$cfg]))
	{
		return '';
	}
	
	/*! jika di temukan keynya */
	if ( !isset($scp->AppProgVar[$cfg][$key]))
	{
		return '';
	}
	
	/*! copy process on program */
	app_func_copy( $ret, 
		$scp->AppProgVar[$cfg][$key]
	);
	
	/*! \retval final is OK */
	return $ret; 
}

/*!
 * \brief 	app_cfg_header
 * \note 	[description]
 * \retval  [description]
 *
 */ 
 function app_cfg_header($cfg = NULL, &$ret = '')
{
	Global $scp;
	
	/*! jika di temukan keynya */
	if( !isset($scp->AppProgVar[$cfg]))
	{
		return '';
	}
	
	/*! copy process on program */
	app_func_copy( $ret, 
		$scp->AppProgVar[$cfg]
	);
	
	/*! \retval final is OK */
	return $ret; 
}

/*!
 * \brief 	app_cfg_alerting
 * \note 	[description]
 * \retval  [description]
 *
 */ 
 function app_cfg_alerting($str = NULL, &$ret = false )
{
	if ( !strstr($str, 'EventAlerting') )
	{
		return 0;
	}
	
	if ( !strpos($str, chr(44)))
	{
		return 0;
	}
	
	$str = str_replace( array('EventAlerting', chr(40), chr(41)),
		array('', '', ''),  $str);
		
	$ret = array_map('trim', explode(chr(44), $str));
	return $ret;
}

/*!
 * \brief 	app_cfg_handler
 * \note 	[description]
 * \retval  [description]
 *
 */ 
 function app_cfg_handler($str = NULL, &$ret = false )
{
	if ( !strstr($str, 'EventHandler') )
	{
		return 0;
	}
	
	if ( !strpos($str, chr(44)))
	{
		return 0;
	}
	
	$str = str_replace( array('EventHandler', chr(40), chr(41)),
		array('', '', ''),  $str);
		
	$ret = array_map('trim', explode(chr(44), $str));
	return $ret;
}

/*!
 * \brief 	app_cfg_handler
 * \note 	[description]
 * \retval  [description]
 *
 */ 
 function app_cfg_stream($cfg = NULL, $key = NULL, &$ret = '')
{
	Global $scp;
	printf("cfg = %s key = %s\n", $cfg, $key);
	print_r($scp->$cfg);
	if (!isset($scp->$cfg))
	{
		return 0;
	}
	
	/*! copy process on program */
	app_func_copy($ret, $scp->$cfg); 
	if ( !is_null($key) )
	{
		$ret = ( isset($ret[$key]) ? $ret[$key] : false);
	}
	
	return $ret;
}

/*!
 * \brief 	app_cfg_handler
 * \note 	[description]
 * \retval  [description]
 *
 */ 
 
 function app_cfg_partner($cfg = NULL, $key = NULL, &$ret = false) 
{
	Global $scp; 
	
	app_func_copy($peers, false);
	if ( app_cfg_param($cfg, $key, $peers) )
	{
		if( app_func_explode($ret, $peers, chr(44) ) )
		{
			return $ret;
		} 
	}
	
	return $ret;
 }
 
 /*!
 * \brief 	app_cfg_peers
 * \note 	[description]
 * \retval  [description]
 *
 */ 
 
 function app_cfg_peers($peer = NULL, &$ret = false) 
{
	Global $scp; 
	if ( !isset($scp->AppProgVar[$peer] )) 
	{
		app_func_copy($ret, false);
		return $ret;
	}
	
	$retval_cfg = $scp->AppProgVar[$peer];
	if ( !app_func_sizeof($retval_cfg) )
	{
		app_func_copy($ret, false);
		return $ret;
	}		
	
	if (app_func_retval($retval_cfg, $ret) )
	{
		return $ret;
	}
	
	return 0;
 }