<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SMT -- An open source Server Monitoring Toolkit < SMT >
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */
 
/*! \retval < \sockWebSocketRegister > */
 function sockWebSocketRegister(&$s, &$ret = 0)
{
	app_func_struct($obj);
	app_func_copy($obj->retval,0);
	if (preg_match("/Sec-WebSocket-Version: (.*)\r\n/", $s->buf, $match)) 
	{
		app_func_next($obj->retval);
	}
	
	if (app_func_copy($ret, $obj->retval))
	{
		app_func_free($obj);
	}
	
	/*! \retval copy source */
	return $ret;
}

/*! \retval < \sockWebSocketResponse > */
 function sockWebSocketResponse(&$s)
{
	
}

/*! \retval < \sockWebSocketUpgrade > */
 function sockWebSocketUpgrade(&$m= NULL, &$s= NULL)
{
	/*! \remark check apakah yang connect ini adalah websocket ? */
	if (!sockWebSocketRegister($s, $retval))
	{
		return 0;
	}
	
	/*! \figure next process */
	app_func_struct($obj); 
	app_func_copy($obj->id		 ,$s->id);
	app_func_copy($obj->sock 	 ,$s->sock);
	app_func_copy($obj->pid  	 ,$s->pid);
	app_func_copy($obj->header   ,$s->buf);
	app_func_copy($obj->appversi ,$m->AppProgVer); 
	app_func_copy($obj->appcopy  ,$m->AppProgCopy);
	app_func_copy($obj->appbuild ,$m->AppProgBuild);
	app_func_copy($obj->appvendor,$m->AppProgUid);
	app_func_copy($obj->apptitle ,$m->AppProgTitle);
	app_func_copy($obj->appname	 ,$m->AppProgName);
	app_func_copy($obj->match 	 ,NULL);
	app_func_copy($obj->root 	 ,NULL);
	app_func_copy($obj->host 	 ,NULL);
	app_func_copy($obj->origin   ,NULL);
	app_func_copy($obj->key      ,NULL);
	app_func_copy($obj->retval   ,0);
	app_func_copy($obj->version  ,0);
	
	/*! \retval check version on websocket browser */
	if ( @preg_match("/Sec-WebSocket-Version: (.*)\r\n/", $obj->header, $obj->match) ) 
		$obj->version = $obj->match[1];
	else
	{
		scp_log_app("\tSERVER >> WSR: The client doesn't support WebSocket");
		scp_log_app("\t"); 
		return 0;
	}
	
	/*! \retval  jika bukan Versi: 13 data return:[0] */
	if ($obj->version != 13) 
	{
		scp_log_app("\tSERVER >> WSR: The client doesn't support WebSocket");
		return 0;
	}
	
	/*! \retval Extract header variables */
	
	if (preg_match("/GET (.*) HTTP/", $obj->header, $obj->match))
		app_func_copy($obj->root, $obj->match[1]);
	if (preg_match("/Host: (.*)\r\n/", $obj->header, $obj->match))
		app_func_copy($obj->host, $obj->match[1]);
	if (preg_match("/Origin: (.*)\r\n/", $obj->header, $obj->match))
		app_func_copy($obj->origin, $obj->match[1]);
	if (preg_match("/Sec-WebSocket-Key: (.*)\r\n/", $obj->header, $obj->match))
		app_func_copy($obj->key, $obj->match[1]);
	
	
	/*! \retval ugraded socket header from 
	webbrowser statements: */
	
	app_func_copy($obj->acceptKey, $obj->key .'258EAFA5-E914-47DA-95CA-C5AB0DC85B11');
	app_func_copy($obj->acceptKey, base64_encode(sha1($obj->acceptKey,true)));
	
	app_func_copy($obj->upgrade, sprintf(
			"HTTP/1.1 101 Switching Protocols\r\n".
			"Access-Control-Allow-Origin: *\r\n".
			"Upgrade: websocket\r\n".
			"Sec-WebSocket-Application: %s\r\n".
			"Sec-WebSocket-Version: %s/%s\r\n".
			"Sec-WebSocket-Copyright: %s\r\n".
			"Sec-WebSocket-Client: %s\r\n".
			"Sec-WebSocket-Build: %s\r\n".
			"Connection: Upgrade\r\n".
			"Sec-WebSocket-Accept: %s".
			"\r\n\r\n", 
			$obj->apptitle,
			$obj->appname,
			$obj->appversi,
			$obj->appcopy,
			$obj->appvendor,
			$obj->appbuild,
			$obj->acceptKey
		)
	);
	
	/*! \retval handler sent to client: */
	if ( sockTcpWrite($s->sock, $obj->upgrade) )
	{
		if (app_func_next($s->shak))
		{
			$m->AppProgClient[$s->id]['shak'] = $s->shak;
		}
	}
	
	/*! \retval return data if success 
	Upgrade header to websocket: */
	
	return $s->shak;
}
 
/*! \retval < \sockWebSocketEncoder > */
 function sockWebSocketEncoder($buf = '', $type = 'text')
{
	/*! \retval set default object */ 
	
	app_func_struct($obj); 
	app_func_copy($obj->buff,app_func_trim($buf));
	app_func_copy($obj->type,app_func_trim($type));
	
	/*! \retval case type type data on Binnary 
	string precision */
	
	$obj->b1 = 0; $obj->b2 = 0;
	switch($obj->type) 
	{
		case 'continuous':
			$obj->b1 = 0;
			break;
		case 'text':
			$obj->b1 = 1;
			break;
		case 'binary':
			$obj->b1 = 2;
			break;
		case 'close':
			$obj->b1 = 8;
			break;
		case 'ping':
			$obj->b1 = 9;
			break;
		case 'pong':
			$obj->b1 = 10;
			break;
	}
 
	/*! \retval next of process : */
	
	$obj->b1 += 128; // increement 128 bit 
	$obj->length = strlen($obj->buff); // next process 
	$obj->lengthField = ''; $obj->hexLength = NULL; 
	 
	/*! \remark sent to byte compress for websocket process: */
	
	if ($obj->length<126) $obj->b2 = $obj->length;
	else if (($obj->length>=126) &&($obj->length<=65536)) 
	{
		$obj->b2 = 126;
		$obj->hexLength = dechex($obj->length);
		
		if (strlen($obj->hexLength)%2 == 1) 
		{
			$obj->hexLength = '0' . $obj->hexLength;
		}
		
		$n = strlen($obj->hexLength) - 2;
		for ($i = $n; $i >= 0; $i=$i-2) 
		{
			$obj->lengthField = chr(hexdec(substr($obj->hexLength, $i, 2))) . $obj->lengthField;
		}
		
		while((strlen($obj->lengthField)<2)) 
		{
			$obj->lengthField = chr(0) . $obj->lengthField;
		}
		
	} else { 
	
		/*! \remark Other data type not defined: */
		
		$obj->b2 = 127;
		$obj->hexLength = dechex($obj->length);
		
		if (strlen($obj->hexLength)%2 == 1) 
		{
			$obj->hexLength = '0' . $obj->hexLength;
		}
		
		$n = strlen($obj->hexLength) - 2;
		for ($i = $n; $i>= 0; $i = $i - 2) 
		{
			$obj->lengthField = chr(hexdec(substr($obj->hexLength, $i, 2))) . $obj->lengthField;
		}
		
		while((strlen($obj->lengthField)<8))
		{
		   $obj->lengthField = chr(0) . $obj->lengthField;
		}
    }
	
	/*! \retval return substring process: */
	
	$obj->rets = chr($obj->b1) . chr($obj->b2) . $obj->lengthField . $obj->buff;
	$obj->buff = NULL; 
	return $obj->rets;  
}

/*! \retval < \sockWebSocketDecoder > */
 function sockWebSocketDecoder($buf)
{
	app_func_struct($obj);
	
	$obj->buff = $buf;
	$obj->length = @ord($obj->buff[1]) & 127;
	if ($obj->length == 126) 
	{
		$obj->masks = substr($obj->buff, 4, 4);
		$obj->data = substr($obj->buff, 8);
	}
	else if ($obj->length == 127) 
	{
		$obj->masks = substr($obj->buff, 10, 4);
		$obj->data = substr($obj->buff, 14);
	}
	else {
		$obj->masks = substr($obj->buff, 2, 4);
		$obj->data = substr($obj->buff, 6);
	}

	/*! \retval new string toback data on stream data: */	
	app_func_copy( $obj->buff, '');
	for ($i = 0; $i < strlen($obj->data); ++$i) 
	{
		$obj->buff .= $obj->data[$i] ^ $obj->masks[$i%4];
	}
	 
	if ( strlen($obj->buff)>0 )
	{
		$obj->buff = $obj->buff;
	}
	
	return $obj->buff;	
}
 
/*! \retval < \sockWebSocketOutput > */
 function sockWebSocketOutput(&$s, &$str= NULL, &$byte= 0)
{
	if (app_func_copy($s->out, sockWebSocketEncoder($str)))
	{
		if ($byte = sockTcpClientWrite($s->sock, $s->out)) 
		{
			app_func_free($s->out);
		}
	} 
	
	return $s->id;
} 