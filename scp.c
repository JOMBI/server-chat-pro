#!/usr/bin/php
<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/*! \remark running
 * php -q /opt/kawani/bin/chatpro/scp.c --pid=/var/run/scp/scp.pid --cfg=/opt/kawani/bin/chatpro/config/vos.conf --verbose=1 
 * php -q /opt/kawani/bin/chatpro/scp.c --pid=/var/run/scp/scp.pid --cfg=/opt/kawani/bin/chatpro/config/dev.conf --verbose=1 
 */
 
/*! create Global Object */
 $scp = new stdClass();


 /*! debug Track Error */
 if (function_exists('error_reporting')){
  ini_set("display_errors",1);
  error_reporting(E_ALL);	
}
 
 /*! create signal fork handler */ 
 if (function_exists( 'pcntl_async_signals')) 
	pcntl_async_signals(true);
 else {
	declare(ticks=1);
 }
 
/*! Unlimited process */
 if ( function_exists('set_time_limit') ){
   set_time_limit(0);
   ob_implicit_flush();
   ignore_user_abort(0);
}
 
/*! declare All php Ini Modul */
 if (function_exists('date_default_timezone_set')){
	date_default_timezone_set( 'Asia/Jakarta' );
 }

 
/*! 
 * include Application Header 
 *
 */
 
 include SCP_APP .'/scp.h';
 
/*!
 * HandlerMainApplictaion
 *
 * \note Main Program for Server Monitoring Toolkit 
 * base program Under CLI console .
 *
 * \retval void(0) running KepAlive(0)
 *
 */
 function HandlerMainApplictaion(&$scp)
{
	/*! \remark get config from Handler 'CLI' */
	scpAppMainHandler($scp);
	
	/*! \remark get Main Config Application */
	if ( scpAppMainConfig($scp) )
	{
		/*! \remark Main Loader Application */
		scpAppMainLoader($scp);
		
		/*! \remark Init All Config Pus to Global Object */
		if ( !scpAppInitConfig($scp) )
		{
			scpAppUnregHandler($scp);
			exit(0);
		}
		
		/*! \def Iniated Banner & Signal*/
		scpAppMainBanner($scp);  
		if ( !scpAppMainSignal($scp) ) 
		{
			scp_log_app("Create Signal Fail");
			scp_log_app("\t");
		}
		
		/*! \remark dump of process if true */
		scp_log_app(sprintf("Starting Application(%d)", $scp->AppProgPid));
		if ( scpAppMainSocket($scp) )
		{
			scp_log_app(sprintf("Starting Created Socket(%d)", $scp->RunSock)); 
		} 
	}
	// scp_log_core("scp-dump", $scp);  
	while (1)
	{
		
		/*! \retval process on ready */
		
		app_func_copy($scp->AppProgSocket, 0, $scp->AppTcpLink);
		app_func_copy($scp->AppProgSocket, 1, $scp->MgrUdpLink);
		app_func_copy($scp->AppProgSocket, 2, $scp->PubTcpLink);
		app_func_copy($scp->AppProgSocket, 3, $scp->VstTcpLink);  
		
		/*! \remark Iterasi untuk semua jenis peer yang connect ke server lain */
		if ( function_exists( 'scpAppClientIterator'))
		{
			scpAppPeerIterator($scp);
		}
		
		
		/*! \remark Iterasi untuk semua jenis client yang connect ke socket server */
		if ( function_exists( 'scpAppClientIterator'))
		{
			scpAppClientIterator($scp);
		}
		
		/*! \retval Iterupted Socket Sent Event */ 
		$scp->AppExcept = $scp->AppWrite = array(); $scp->AppRead = $scp->AppProgSocket;
		@socket_select($scp->AppRead, $scp->AppWrite, $except, $scp->AppSec, ($scp->AppSec * 0)); 
		if ( @socket_select($scp->AppProgSocket, $scp->AppWrite, $scp->AppExcept, $scp->AppSec, ($scp->AppSec * 0))>0) 
		{
			
			/*! \remark for App Server Port */ 
			if (app_func_sock($scp->AppTcpLink) &&(app_func_search($scp->AppTcpLink, $scp->AppProgSocket)))
			{
				/*! \remark ketika Ada yang Connect dari Port Public Lamgsung di iterasi untuk 
				dimasukan ke list socket */
				
				sockTcpAccept($scp->AppTcpLink, $sock); 
				if (app_func_sock($sock) &&(sockTcpPeer($sock, $s)) )
				{
					$scp->AppProgClient[$s->from_id]['id']    = $s->from_id;
					$scp->AppProgClient[$s->from_id]['sock']  = $s->from_sock;
					$scp->AppProgClient[$s->from_id]['host']  = $s->from_ip;
					$scp->AppProgClient[$s->from_id]['port']  = $s->from_port;
					$scp->AppProgClient[$s->from_id]['time']  = $s->from_time;
					$scp->AppProgClient[$s->from_id]['pid']   = $s->from_pid; 
					$scp->AppProgClient[$s->from_id]['type']  = 'tcp_app'; 
					$scp->AppProgClient[$s->from_id]['uid']   = 0; 
					
					/*! \remark for Iformasi debug */
					scp_log_app(sprintf("%s:%06d - Peer Session #%d Client Connected.", $s->from_ip, $s->from_port, $s->from_id));
				}
			} 
			
			
			/*! \remark for Public Server Port */
			if (app_func_sock($scp->PubTcpLink) &&(app_func_search($scp->PubTcpLink, $scp->AppProgSocket)))
			{
				/*! \remark ketika Ada yang Connect dari Port Public Lamgsung di iterasi untuk 
				dimasukan ke list socket */
				
				sockTcpAccept($scp->PubTcpLink, $sock); 
				if (app_func_sock($sock) &&(sockTcpPeer($sock, $s)) )
				{
					$scp->AppProgClient[$s->from_id]['id']    = $s->from_id;
					$scp->AppProgClient[$s->from_id]['sock']  = $s->from_sock;
					$scp->AppProgClient[$s->from_id]['host']  = $s->from_ip;
					$scp->AppProgClient[$s->from_id]['port']  = $s->from_port;
					$scp->AppProgClient[$s->from_id]['time']  = $s->from_time;
					$scp->AppProgClient[$s->from_id]['pid']   = $s->from_pid;
					$scp->AppProgClient[$s->from_id]['type']  = 'tcp_pub'; 
					$scp->AppProgClient[$s->from_id]['uid']   = 0; 
					
					/*! \remark for Iformasi debug */
					scp_log_app(sprintf("%s:%06d - Session #%d Client Connected.", $s->from_ip, $s->from_port, $s->from_id));
				}
			} 
			
			
			/*! \remark for Visitor Server Port */
			if (app_func_sock($scp->VstTcpLink) &&(app_func_search($scp->VstTcpLink, $scp->AppProgSocket)))
			{
				/*! \remark ketika Ada yang Connect dari Port Public Lamgsung di iterasi untuk 
				dimasukan ke list socket */
				
				sockTcpAccept($scp->VstTcpLink, $sock); 
				if (app_func_sock($sock) &&(sockTcpPeer($sock, $s)) )
				{
					$scp->AppProgClient[$s->from_id]['id']    = $s->from_id;
					$scp->AppProgClient[$s->from_id]['sock']  = $s->from_sock;
					$scp->AppProgClient[$s->from_id]['host']  = $s->from_ip;
					$scp->AppProgClient[$s->from_id]['port']  = $s->from_port;
					$scp->AppProgClient[$s->from_id]['time']  = $s->from_time;
					$scp->AppProgClient[$s->from_id]['pid']   = $s->from_pid;
					$scp->AppProgClient[$s->from_id]['type']  = 'wss_vst'; 
					$scp->AppProgClient[$s->from_id]['shak']  = 0;
					$scp->AppProgClient[$s->from_id]['uid']   = 0; 
					
					/*! \remark for Iformasi debug */
					scp_log_app(sprintf("%s:%06d - Visitor #%d Client Connected.", $s->from_ip, $s->from_port, $s->from_id));
				}
			}
			
			/*! \remark for Manager UDP Server Port  */
			if (app_func_sock($scp->MgrUdpLink) &&(app_func_search($scp->MgrUdpLink, $scp->AppProgSocket)))
			{
				if( function_exists('scpAppManagerHandler' )){
					scpAppManagerHandler($scp, $scp->MgrUdpLink);
				}
			}
			
			/*! \remark Cari Semua Peer yang connnect ke server lain buat dapet Feedback */
			if ( function_exists('scpAppPeerHandler')){
				scpAppPeerHandler($scp);
			}
			
			
			
			/*! \remark Cari Semua Client yang connnect ke Sini */
			if ( function_exists('scpAppClientHandler')){
				scpAppClientHandler($scp);
			}
		}	
		
		// scp_log_core("scp-dump", $scp);  
	}
	
	
	/*! \retval Void(0) */
	scpAppUnregHandler($scp);
	return 0;	
}

 /*! \retval __ ToolkitMainApplictaion ___*/
HandlerMainApplictaion($scp);
 