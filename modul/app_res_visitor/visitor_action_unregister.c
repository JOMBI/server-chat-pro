<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 function visitor_action_unregister(&$o= NULL, &$s= NULL, &$m= NULL, &$replay = NULL)
{
	// for default response 
	$replay = array( 'error' => 1,  'event' => 'register', 'message' => 'error', 
		'data' => array()
	);
	
	app_func_struct($req);
	while ($s->id)
	{
		/*! \remark data to process on unregister from webhook */ 
		// uid=1&session=163255841600001&channel=omens%40yahoo.com&userid=omens
		// &username=omens&useremail=omens%40yahoo.com
		// &userdomain=chat.dev.local
		
		app_func_copy($req->uid    	   , app_webhook_get_request($m,'uid'));
		app_func_copy($req->userid     , app_webhook_get_request($m,'userid'));
		app_func_copy($req->channel    , app_webhook_get_request($m,'channel'));
		app_func_copy($req->session    , app_webhook_get_request($m,'session'));
		app_func_copy($req->username   , app_webhook_get_request($m,'username'));
		app_func_copy($req->useremail  , app_webhook_get_request($m,'useremail'));
		app_func_copy($req->userdomain , app_webhook_get_request($m,'userdomain'));
		
		
		/*! \remark if session is empty bro */
		if ( !app_user_session_find($req->useremail, $retval) )
		{
			app_func_copy($replay, 'error', 2); 
			app_func_copy($replay, 'message', 'session expired'); 
			
			/*! \retval set data for callback process OR DB session */ 
			app_func_alloc($req->log); 
			app_func_copy($req->log, 'uid'		, $req->uid);
			app_func_copy($req->log, 'channel'	, $req->channel);
			app_func_copy($req->log, 'session'	, $req->session);
			app_func_copy($req->log, 'userid'	, $req->userid);
			app_func_copy($req->log, 'username'	, $req->username);
			app_func_copy($req->log, 'domain'	, $req->userdomain);
			app_func_copy($req->log, 'email'	, $req->useremail); 
			
			if( $req->log && app_func_copy($replay, 'data', $req->log)) 
			{
				app_func_free($req->log);	
			}
				
			break;
		}
		
		/*! \retval if exist sent to Manager to kick this session */
		if ($req->channel && app_func_struct($udp) )
		{
			$udp->sock = new udp(); 
			$udp->sock->set('msg'	  , 'mgr');
			$udp->sock->set('type'	  , 'action');
			$udp->sock->set('action'  , 'unregister');
			$udp->sock->set('ctype'   , 'tcp_pub');
			$udp->sock->set('uid'	  , $retval->get('uid'));
			$udp->sock->set('channel' , $retval->get('channel'));
			$udp->sock->set('session' , $retval->get('session'));
			$udp->sock->set('userid'  , $retval->get('userid'));
			$udp->sock->set('username', $retval->get('username'));
			$udp->sock->set('domain'  , $retval->get('domain'));
			$udp->sock->set('email'	  , $retval->get('email'));
			
			/*! \retval default NULL */
			app_func_copy($udp->retval, "");
			if ( $udp->sock->sent($udp->retval) ) 
			{
				app_web_hook_loger($s, sprintf("Success Unregister '%s'", $req->channel));
			}
		}
		
		/*! \retval sent relay to client */
		
		app_func_copy($replay, 'error', 0); 
		app_func_copy($replay, 'message', true);
			
		app_func_alloc($req->log); 
		app_func_copy($req->log, 'uid'		, $req->uid);
		app_func_copy($req->log, 'channel'	, $req->channel);
		app_func_copy($req->log, 'session'	, $req->session);
		app_func_copy($req->log, 'userid'	, $req->userid);
		app_func_copy($req->log, 'username'	, $req->username);
		app_func_copy($req->log, 'domain'	, $req->userdomain);
		app_func_copy($req->log, 'email'	, $req->useremail); 
		
		if( $req->log && app_func_copy($replay, 'data', $req->log)) 
		{
			app_func_free($req->log);	
		}
		
		break;
	}
	
	/*! \retval cleanup process */  
	app_func_free($req);
	return 0;	
	
}