<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 function visitor_action_register(&$o= NULL, &$s= NULL, &$m= NULL, &$replay = NULL)
{
	// for default response 
	$replay = array( 'error' => 1,  'event' => 'register', 'message' => 'error', 
		'data' => array()
	);
	
	app_func_struct($req);
	while ($s->id)
	{
		/*! \remark data to process on register from webhook */
		
		app_func_copy($req->username  , app_webhook_get_request($m,'username'));
		app_func_copy($req->useremail , app_webhook_get_request($m,'useremail'));
		app_func_copy($req->userdomain, app_webhook_get_request($m,'userdomain'));
		app_func_copy($req->sessionid , app_webhook_get_request($m,'sessionid'));
		app_func_copy($req->channel   , NULL);
		
		
		/*! \remark next for checking session if not exist will created  */
		if ( app_user_session_eval('email', $req->useremail) )
		{
			app_func_copy($replay, 'error', 2); 
			app_func_copy($replay, 'message', 'session still exist'); 
			if (app_user_session_find($req->useremail, $ret) )
			{
				/*! \retval set data for callback process OR DB session */ 
				app_func_alloc($req->log); 
				app_func_copy($req->log, 'uid'		, $ret->get('uid'));
				app_func_copy($req->log, 'channel'	, $ret->get('channel'));
				app_func_copy($req->log, 'session'	, $ret->get('session'));
				app_func_copy($req->log, 'userid'	, $ret->get('userid'));
				app_func_copy($req->log, 'username'	, $ret->get('name'));
				app_func_copy($req->log, 'domain'	, $ret->get('domain'));
				app_func_copy($req->log, 'email'	, $ret->get('email'));
				app_func_copy($req->log, 'regts'	, $ret->get('regts'));
				
				if( $req->log && app_func_copy($replay, 'data', $req->log)) 
				{
					app_func_free($req->log);	
				}
			}
			
			break;
		}
		
		if ( !app_user_session_eval('email', $req->useremail) )
		{
			if ( !app_user_session_create($req->useremail, $email) )
			{
				app_func_copy($replay, 'message', 'error' ); 
				break;
			}
			
			// if true dont have session 
			app_func_copy($req->channel, $email);
			app_func_copy($req->regts, strtotime('now'));
			if (app_user_session_unique($chan))
			{
				app_func_copy($req->uid , $chan->uid);
				app_func_copy($req->session, $chan->session);
				
				app_user_session_set($email, 'uid', $chan->uid);
				app_user_session_set($email, 'session', $chan->session);
			}
			
			app_user_session_set($email, 'userid'  , $req->username);
			app_user_session_set($email, 'username', $req->username);
			app_user_session_set($email, 'domain'  , $req->userdomain);
			app_user_session_set($email, 'email'   , $req->useremail); 
			
		   /*! \remark ketika process cretae dari sisi Webhook maka data tersebut harus 
			* di kirim juga ke master applikasi karean pada PHP tidak ada process sharing object 
			* maka di perlukan transport untuk mengirim data tersebut 
			* 
			* ini masih nanti dulu 
			*/
			
			if ( app_func_struct($udp) 
			&& ( app_user_session_find($req->useremail, $udp->chan) ))
			{
				$udp->sock = new udp(); 
				$udp->sock->set('msg'	  , 'mgr');
				$udp->sock->set('type'	  , 'action');
				$udp->sock->set('action'  , 'register');
				$udp->sock->set('ctype'   , 'tcp_pub');
				$udp->sock->set('uid'	  , $udp->chan->get('uid'));
				$udp->sock->set('channel' , $udp->chan->get('channel'));
				$udp->sock->set('session' , $udp->chan->get('session'));
				$udp->sock->set('userid'  , $udp->chan->get('userid'));
				$udp->sock->set('username', $udp->chan->get('username'));
				$udp->sock->set('domain'  , $udp->chan->get('domain'));
				$udp->sock->set('email'	  , $udp->chan->get('email'));
				$udp->sock->set('regts'	  , $udp->chan->get('regts'));
				
				/*! \retval default NULL */
				app_func_copy($udp->Retval, "");
				if ( $udp->sock->sent($udp->Retval) ) 
				{
					app_web_hook_loger($s, "Success syncronize data to server");
				}
			}
			
			
			/*! \retval set for response */
			
			app_func_copy($replay, 'error', 0); 
			app_func_copy($replay, 'message', true);
			
			/*! \retval set data for callback process OR DB session */ 
			app_func_alloc($req->log); 
			app_func_copy($req->log, 'uid'		, $req->uid);
			app_func_copy($req->log, 'channel'	, $req->channel);
			app_func_copy($req->log, 'session'	, $req->session);
			app_func_copy($req->log, 'userid'	, $req->username);
			app_func_copy($req->log, 'username'	, $req->username);
			app_func_copy($req->log, 'domain'	, $req->userdomain);
			app_func_copy($req->log, 'email'	, $req->useremail);
			app_func_copy($req->log, 'regts'	, $req->regts);
			
			if( $req->log && app_func_copy($replay, 'data', $req->log)) 
			{
				app_func_free($req->log);	
			}
			
			break; 
		}
		
		// default is break;
		break;
	}
	
	scp_log_core("scp-hook-register", $replay);  
	/*! \retval cleanup process */
	app_func_free($req);
	return 0;	
 }
  