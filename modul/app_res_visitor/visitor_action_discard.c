<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
 function visitor_action_discard(&$o= NULL, &$s= NULL, &$m= NULL, &$replay = NULL)
{
	app_func_free($replay);
	app_func_alloc($replay);
	app_func_copy($replay, 'error', 1);
	app_func_copy($replay, 'event', 'discard'); 
	app_func_copy($replay, 'message', 'uknown');
	
	
	/*! \remark for Body Message */
	app_func_struct($req);
	app_func_struct($app);
	
	while ($s->id)
	{
		// channel=ADMIN1&session=163304251000001&username=ADMIN1&chatboxid=163304262800001
		app_func_copy($req->channel , app_webhook_get_request($m,'channel'));
		app_func_copy($req->session  , app_webhook_get_request($m,'session'));
		app_func_copy($req->username  , app_webhook_get_request($m,'username'));
		app_func_copy($req->chatboxid , app_webhook_get_request($m,'chatboxid'));
		app_func_copy($req->chaterror , 0);
		
		
		
		/*! \remark if session is empty bro */
		app_func_copy($app->fail, 0);
		if (!app_user_session_find($req->channel, $app->channel))
		{
			app_func_free($app->channel);
			app_func_next($app->fail); 
		}
		
		/*! \remark check for fail process */
		if ($app->fail)
		{
			app_func_copy($replay, 'message', 'invalid channel');  
			break;
		}
		
		/*! \retval if exist sent to Manager to kick this session */
		if ( !$app->fail && app_func_struct($udp) )
		{
			$udp->sock = new udp(); 
			$udp->sock->set('msg'	    , 'mgr');
			$udp->sock->set('type'	    , 'action');
			$udp->sock->set('action'    , 'discard');
			$udp->sock->set('ctype'     , 'tcp_pub');
			$udp->sock->set('channel'   , $req->channel);
			$udp->sock->set('session'   , $req->session);
			$udp->sock->set('username'  , $req->username);
			$udp->sock->set('chatboxid' , $req->chatboxid);
			
			/*! \retval default NULL */
			app_func_copy($udp->buf, "");
			if ( $udp->sock->sent($udp->buf) ) 
			{
				app_func_copy($udp->recv, 0);
				if ($udp->sock->ontrace($udp->buf, $udp->recv) ) 
				{
					app_func_copy($req->chatboxid , $udp->recv->chatboxid);  
					app_func_copy($req->chaterror , $udp->recv->error);
					app_func_free($udp->recv);
					
				}
				
				/*! \retval error == 0 this success Pickup process */
				if ( !$req->chaterror ) {
					
					app_func_copy($replay, 'message', 'success discard');  
					app_web_hook_loger($s, sprintf( "Success to Discard '%s' with chat BoxId= %s ", 
						$req->channel, $req->chatboxid
					));
					
				} 
				/*! \retval error > 0 this Error Pickup process */
				else if ( $req->chaterror == 1 ){
					
					app_func_copy($replay, 'message', 'No Discard');  
					app_web_hook_loger($s, sprintf( "No Channel to Discard '%s' with chat BoxId= %s ", 
						$req->channel, $req->chatboxid
					));
				}
			}
			
			app_func_free($udp->buf);
			break;
		}
		
		/*! \note stop And Go */
		break; 
	}	 
	
	/*! \remark sent response for Hook client */
	app_func_alloc($app->log);
	app_func_copy($app->log, 'channel' 	, $req->channel);
	app_func_copy($app->log, 'session'  , $req->session);
	app_func_copy($app->log, 'username' , $req->username); 
	app_func_copy($app->log, 'chatboxid', $req->chatboxid);
	app_func_copy($app->log, 'response' , $req->chaterror); 
	
	if ( app_func_copy($replay, 'data',$app->log) )
	{
		app_func_free($app->log);
	}
	
	
	/*! \retval cleanup process */  
	app_func_free($req);
	return 0;	
}