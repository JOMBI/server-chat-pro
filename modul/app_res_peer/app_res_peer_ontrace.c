<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 * 
 *  flow: scpAppMainPeerSock -> onTrace > onEvent
 */
 
/*! \retval app_res_peer_ontrace < m* master, s* client > */
 function app_res_peer_ontrace(&$m, &$s) 
{ 
	app_func_copy($retval, 0);
	app_func_next($retval);
	
	return $retval;
}