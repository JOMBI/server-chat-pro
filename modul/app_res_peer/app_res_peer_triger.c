<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 * 
 * Goto: Meta_Event
 */
 
 
 
/*! \retval < app_res_peer_triger_action_status < m* scp , c* channel, p * meta >> */
 function app_res_peer_triger_action_status(&$m= NULL, &$c= NULL, $p= NULL)
{
	app_func_struct($chan);
	app_func_copy($chan->type	 , 'tcp_peer'); 
	app_func_copy($chan->uid	 , $c->get('uid')); 
	app_func_copy($chan->appagent, $c->get('appagent')); 
	app_func_copy($chan->session , $c->get('session')); 
	app_func_copy($chan->channel , $c->get('channel')); 
	app_func_copy($chan->domain  , $c->get('domain'));  
	app_func_copy($chan->userid	 , $c->get('userid'));
	app_func_copy($chan->name	 , $c->get('name'));
	app_func_copy($chan->email	 , $c->get('email'));
	app_func_copy($chan->domain  , $c->get('domain'));
	app_func_copy($chan->group	 , $c->get('group'));
	app_func_copy($chan->status  , $c->get('status'));
	app_func_copy($chan->state	 , $c->get('state'));
	app_func_copy($chan->pid	 , $c->get('pid'));
	app_func_copy($chan->host	 , $c->get('host'));
	app_func_copy($chan->port	 , $c->get('port'));
	app_func_copy($chan->time	 , $c->get('time'));
	app_func_copy($chan->regts	 , $c->get('regts'));
	app_func_copy($chan->alive	 , $c->get('alive'));
	
	/*! \remark get client Peer */
	app_func_copy($retval, 0);
	app_func_copy($s, false);  
	if (res_peer_utils_findby_domain($m, $chan->domain, $s))
	{
		/*! \note iteration by socket_ID */
		while ($s->id)
		{
			/*! \note if not found then break */
			app_func_copy($s->output, "");
			if (!app_func_sock($s->sock))
			{
				app_func_free($s); 	
				break;
			}	
			
			/*! \remark create example 'plus'  */
			app_func_struct($chan->m); 
			app_func_copy($chan->m->meta   , PRIV_META_HAND);
			app_func_copy($chan->m->type   , 'action');
			app_func_copy($chan->m->action , 'status');
			app_func_copy($chan->m->error  , 0);   
			
			/*! for data exceptionaly */
			app_func_alloc($chan->m->data);
			app_func_copy($chan->m->data , 'channel', $chan->channel);
			app_func_copy($chan->m->data , 'status' , $chan->status);
			app_func_copy($chan->m->data , 'state'  , $chan->state);
			
			/*! \remark create Meta Body */
			app_func_copy($meta, 
				array(
					'meta' 	 => $chan->m->meta,
					'type' 	 => $chan->m->type,	
					'action' => $chan->m->action,	
					'error'	 => $chan->m->error,
					'data'	 => $chan->m->data
				)
			); 
			
			/*! \sent message to partner client peers */
			app_func_output($s->output, json_encode($meta));  
			if (app_func_enter($s->output)) 
			{
				if ( sockTcpClientWrite($s->sock, $s->output))  {
					app_func_free($s->output); 		
					app_func_next($retval);
				}
			}
			
			/*! \note break and stop */
			break;
		}
	}
	
	/*! \remark overide data from this */
	app_func_free($chan); 
	return $retval; 
}


/*! \retval < app_res_peer_triger_action_register < m* scp , c* channel, p * meta >> */
 function app_res_peer_triger_action_register(&$m= NULL, &$c= NULL, &$p= NULL)
{
	/*! \remark create Local Object */
	/*! \remark Untuk Register antar server Pertama kali memang agak Besar */
	
	app_func_struct($chan);
	app_func_copy($chan->type	 , 'tcp_peer'); 
	app_func_copy($chan->uid	 , $c->get('uid')); 
	app_func_copy($chan->appagent, $c->get('appagent')); 
	app_func_copy($chan->session , $c->get('session')); 
	app_func_copy($chan->channel , $c->get('channel')); 
	app_func_copy($chan->domain  , $c->get('domain'));  
	app_func_copy($chan->userid	 , $c->get('userid'));
	app_func_copy($chan->name	 , $c->get('name'));
	app_func_copy($chan->email	 , $c->get('email'));
	app_func_copy($chan->domain  , $c->get('domain'));
	app_func_copy($chan->group	 , $c->get('group'));
	app_func_copy($chan->status  , $c->get('status'));
	app_func_copy($chan->state	 , $c->get('state'));
	app_func_copy($chan->pid	 , $c->get('pid'));
	app_func_copy($chan->host	 , $c->get('host'));
	app_func_copy($chan->port	 , $c->get('port'));
	app_func_copy($chan->time	 , $c->get('time'));
	app_func_copy($chan->regts	 , $c->get('regts'));
	app_func_copy($chan->alive	 , $c->get('alive'));
	
	/*! \remark get client Peer */
	app_func_copy($retval, 0);
	app_func_copy($s, false);
	if (res_peer_utils_findby_domain($m, $chan->domain, $s))
	{
		/*! \note iteration by socket_ID */
		while ($s->id)
		{
			/*! \note if not found then break */
			app_func_copy($s->output, "");
			if (!app_func_sock($s->sock))
			{
				app_func_free($s); 	
				break;
			}	
			
			/*! \remark create example 'plus'  */
			app_func_struct($chan->m); 
			app_func_copy($chan->m->meta   , PRIV_META_USER);
			app_func_copy($chan->m->type   , 'action');
			app_func_copy($chan->m->action , 'register');
			app_func_copy($chan->m->error  , 0);  
			
			
			/*! \remark create Meta Body */
			app_func_alloc($chan->m->data);
			app_func_copy($chan->m->data, 'uid'	 	 , $chan->uid); 
			app_func_copy($chan->m->data, 'type'	 , $chan->type);  
			app_func_copy($chan->m->data, 'appagent' , $chan->appagent); 
			app_func_copy($chan->m->data, 'session'  , $chan->session); 
			app_func_copy($chan->m->data, 'channel'  , $chan->channel); 
			app_func_copy($chan->m->data, 'domain'   , $chan->domain);  
			app_func_copy($chan->m->data, 'userid'	 , $chan->userid);
			app_func_copy($chan->m->data, 'name'	 , $chan->name);
			app_func_copy($chan->m->data, 'email'    , $chan->email);
			app_func_copy($chan->m->data, 'domain'   , $chan->domain);
			app_func_copy($chan->m->data, 'group'    , $chan->group);
			app_func_copy($chan->m->data, 'status'   , $chan->status);
			app_func_copy($chan->m->data, 'state'    , $chan->state);
			app_func_copy($chan->m->data, 'pid'	   	 , $chan->pid);
			app_func_copy($chan->m->data, 'host'	 , $chan->host);
			app_func_copy($chan->m->data, 'port'	 , $chan->port);
			app_func_copy($chan->m->data, 'time'	 , $chan->time);
			app_func_copy($chan->m->data, 'regts'	 , $chan->regts);
			app_func_copy($chan->m->data, 'alive'	 , $chan->alive);
			
			
			/*! \remark build meta data */
			app_func_copy($meta, 
				array(
					'meta' 	 => $chan->m->meta,
					'type' 	 => $chan->m->type,	
					'action' => $chan->m->action,	
					'error'	 => $chan->m->error,
					'data'	 => $chan->m->data
				)
			); 
			
			/*! \sent message to partner client peers */
			app_func_output($s->output, json_encode($meta)); 
			if (app_func_enter($s->output)) 
			{
				if (sockTcpClientWrite($s->sock, $s->output))  {
					app_func_free($s->output); 		
					app_func_next($retval);
				}
			}
			
			/*! \note break and stop */
			break;
		}
	}
	
	/*! \remark overide data from this */
	app_func_free($chan); 
	return $retval;
}


/*! \remark < app_res_peer_triger_action_ringing < m* scp , c* channel, p * meta > > */
 function app_res_peer_triger_action_ringing(&$m= NULL, &$c= NULL, &$p= NULL)
{
	app_func_struct($chan);
	app_func_copy($chan->type	 , 'tcp_peer'); 
	app_func_copy($chan->uid	 , $c->get('uid')); 
	app_func_copy($chan->appagent, $c->get('appagent')); 
	app_func_copy($chan->session , $c->get('session')); 
	app_func_copy($chan->channel , $c->get('channel')); 
	app_func_copy($chan->domain  , $c->get('domain'));  
	app_func_copy($chan->userid	 , $c->get('userid'));
	app_func_copy($chan->name	 , $c->get('name'));
	app_func_copy($chan->email	 , $c->get('email'));
	app_func_copy($chan->domain  , $c->get('domain'));
	app_func_copy($chan->group	 , $c->get('group'));
	app_func_copy($chan->status  , $c->get('status'));
	app_func_copy($chan->state	 , $c->get('state'));
	app_func_copy($chan->pid	 , $c->get('pid'));
	app_func_copy($chan->host	 , $c->get('host'));
	app_func_copy($chan->port	 , $c->get('port'));
	app_func_copy($chan->time	 , $c->get('time'));
	app_func_copy($chan->regts	 , $c->get('regts'));
	app_func_copy($chan->alive	 , $c->get('alive'));
	
	/*! \remark get client Peer */
	app_func_copy($retval, 0);
	app_func_copy($s, false);  
	if (res_peer_utils_findby_domain($m, $chan->domain, $s))
	{
		/*! \note iteration by socket_ID */
		while ($s->id)
		{
			/*! \note if not found then break */
			app_func_copy($s->output, "");
			if (!app_func_sock($s->sock))
			{
				app_func_free($s); 	
				break;
			}	
			
			/*! \remark create example 'plus'  */
			app_func_struct($chan->m); 
			app_func_copy($chan->m->meta   , PRIV_META_HAND);
			app_func_copy($chan->m->type   , 'action');
			app_func_copy($chan->m->action , 'ringing');
			app_func_copy($chan->m->error  , 0);  
			app_func_copy($chan->m->data   , $p->data);
			
			/*! \remark create Meta Body */
			app_func_copy($meta, 
				array(
					'meta' 	 => $chan->m->meta,
					'type' 	 => $chan->m->type,	
					'action' => $chan->m->action,	
					'error'	 => $chan->m->error,
					'data'	 => $chan->m->data
				)
			); 
			
			/*! \sent message to partner client peers */
			app_func_output($s->output, json_encode($meta)); 
			if (app_func_enter($s->output)) 
			{
				if ( sockTcpClientWrite($s->sock, $s->output))  {
					app_func_free($s->output); 		
					app_func_next($retval);
				}
			}
			
			/*! \note break and stop */
			break;
		}
	}
	
	/*! \remark overide data from this */
	app_func_free($chan); 
	return $retval;
}

/*! \remark <app_res_peer_triger_action_connect < m* scp , c* channel, p * meta >> */
 function app_res_peer_triger_action_connect(&$m= NULL, &$c= NULL, &$p= NULL)
{
	app_func_struct($chan);
	app_func_copy($chan->type	 , 'tcp_peer'); 
	app_func_copy($chan->uid	 , $c->get('uid')); 
	app_func_copy($chan->appagent, $c->get('appagent')); 
	app_func_copy($chan->session , $c->get('session')); 
	app_func_copy($chan->channel , $c->get('channel')); 
	app_func_copy($chan->domain  , $c->get('domain'));  
	app_func_copy($chan->userid	 , $c->get('userid'));
	app_func_copy($chan->name	 , $c->get('name'));
	app_func_copy($chan->email	 , $c->get('email'));
	app_func_copy($chan->domain  , $c->get('domain'));
	app_func_copy($chan->group	 , $c->get('group'));
	app_func_copy($chan->status  , $c->get('status'));
	app_func_copy($chan->state	 , $c->get('state'));
	app_func_copy($chan->pid	 , $c->get('pid'));
	app_func_copy($chan->host	 , $c->get('host'));
	app_func_copy($chan->port	 , $c->get('port'));
	app_func_copy($chan->time	 , $c->get('time'));
	app_func_copy($chan->regts	 , $c->get('regts'));
	app_func_copy($chan->alive	 , $c->get('alive'));
	
	/*! \remark get client Peer */
	app_func_copy($retval, 0);
	app_func_copy($s, false);  
	if (res_peer_utils_findby_domain($m, $chan->domain, $s))
	{
		/*! \note iteration by socket_ID */
		while ($s->id)
		{
			/*! \note if not found then break */
			app_func_copy($s->output, "");
			if (!app_func_sock($s->sock))
			{
				app_func_free($s); 	
				break;
			}	
			
			/*! \remark create example 'plus'  */
			app_func_struct($chan->m); 
			app_func_copy($chan->m->meta   , PRIV_META_HAND);
			app_func_copy($chan->m->type   , 'action');
			app_func_copy($chan->m->action , 'connect');
			app_func_copy($chan->m->error  , 0);  
			app_func_copy($chan->m->data   , $p->data);
			
			/*! \remark create Meta Body */
			app_func_copy($meta, 
				array(
					'meta' 	 => $chan->m->meta,
					'type' 	 => $chan->m->type,	
					'action' => $chan->m->action,	
					'error'	 => $chan->m->error,
					'data'	 => $chan->m->data
				)
			); 
			
			/*! \sent message to partner client peers */
			app_func_output($s->output, json_encode($meta)); 
			if (app_func_enter($s->output)) 
			{
				if ( sockTcpClientWrite($s->sock, $s->output))  {
					app_func_free($s->output); 		
					app_func_next($retval);
				}
			}
			
			/*! \note break and stop */
			break;
		}
	}
	
	/*! \remark overide data from this */
	app_func_free($chan); 
	return $retval;
}


/*! \remark <app_res_peer_triger_action_receive < m* scp , c* channel, p * meta >> */
 function app_res_peer_triger_action_receive(&$m= NULL, &$c= NULL, &$p= NULL)
{
	app_func_struct($chan);
	app_func_copy($chan->type	 , 'tcp_peer'); 
	app_func_copy($chan->uid	 , $c->get('uid')); 
	app_func_copy($chan->session , $c->get('session')); 
	app_func_copy($chan->channel , $c->get('channel')); 
	app_func_copy($chan->domain  , $c->get('domain'));
	
	/*! \remark get client Peer */
	app_func_copy($retval, 0);
	app_func_copy($s, false);  
	if (res_peer_utils_findby_domain($m, $chan->domain, $s))
	{
		/*! \note iteration by socket_ID */
		while ($s->id)
		{
			/*! \note if not found then break */
			app_func_copy($s->output, "");
			if (!app_func_sock($s->sock))
			{
				app_func_free($s); 	
				break;
			}	
			 
			/*! \remark create example 'plus'  */
			app_func_struct($chan->m); 
			app_func_copy($chan->m->meta   , PRIV_META_MESG);
			app_func_copy($chan->m->type   , 'action');
			app_func_copy($chan->m->action , 'recv');
			app_func_copy($chan->m->error  , 0);  
			app_func_copy($chan->m->data   , $p->data);
			
			/*! \remark create Meta Body */
			app_func_copy($meta, 
				array(
					'meta' 	 => $chan->m->meta,
					'type' 	 => $chan->m->type,	
					'action' => $chan->m->action,	
					'error'	 => $chan->m->error,
					'data'	 => $chan->m->data
				)
			); 
			
			/*! \sent message to partner client peers */
			app_func_output($s->output, json_encode($meta)); 
			if (app_func_enter($s->output)) 
			{
				if ( sockTcpClientWrite($s->sock, $s->output))  {
					app_func_free($s->output); 		
					app_func_next($retval);
				}
			}
			
			/*! \note break and stop */
			break;
		}
	}
	
	/*! \remark overide data from this */
	app_func_free($chan); 
	return $retval;
}

/*! \remark <app_res_peer_triger_action_discard < m* scp , c* channel, p * meta >> */
 function app_res_peer_triger_action_discard(&$m= NULL, &$c= NULL, &$p= NULL)
{
	app_func_struct($chan);
	app_func_copy($chan->type	 , 'tcp_peer'); 
	app_func_copy($chan->uid	 , $c->get('uid')); 
	app_func_copy($chan->session , $c->get('session')); 
	app_func_copy($chan->channel , $c->get('channel')); 
	app_func_copy($chan->domain  , $c->get('domain'));
	
	/*! \remark get client Peer */
	app_func_copy($retval, 0);
	app_func_copy($s, false);  
	if (res_peer_utils_findby_domain($m, $chan->domain, $s))
	{
		/*! \note iteration by socket_ID */
		while ($s->id)
		{
			/*! \note if not found then break */
			app_func_copy($s->output, "");
			if (!app_func_sock($s->sock))
			{
				app_func_free($s); 	
				break;
			}	
			 
			/*! \remark create example 'plus'  */
			app_func_struct($chan->m); 
			app_func_copy($chan->m->meta   , PRIV_META_HAND);
			app_func_copy($chan->m->type   , 'action');
			app_func_copy($chan->m->action , 'discard');
			app_func_copy($chan->m->error  , 0);  
			
			
			/*! \remark create Meta Body */
			app_func_copy($chan->m->data, $p->data);
			app_func_copy($meta, 
				array(
					'meta' 	 => $chan->m->meta,
					'type' 	 => $chan->m->type,	
					'action' => $chan->m->action,	
					'error'	 => $chan->m->error,
					'data'	 => $chan->m->data
				)
			);  
			/*! \sent message to partner client peers */
			app_func_output($s->output, json_encode($meta)); 
			if (app_func_enter($s->output)) 
			{
				if ( sockTcpClientWrite($s->sock, $s->output))  {
					app_func_free($s->output); 		
					app_func_next($retval);
				}
			}
			
			/*! \note break and stop */
			break;
		}
	}
	
	/*! \remark overide data from this */
	app_func_free($chan); 
	return $retval;
}

/*! \remark <app_res_peer_triger_action_typing < m* scp , c* channel, p * meta >> */
 function app_res_peer_triger_action_typing(&$m= NULL, &$c= NULL, &$p= NULL)
{
	app_func_struct($chan);
	app_func_copy($chan->type	 , 'tcp_peer'); 
	app_func_copy($chan->uid	 , $c->get('uid')); 
	app_func_copy($chan->session , $c->get('session')); 
	app_func_copy($chan->channel , $c->get('channel')); 
	app_func_copy($chan->domain  , $c->get('domain'));
	
	/*! \remark get client Peer */
	app_func_copy($retval, 0);
	app_func_copy($s, false);  
	if (res_peer_utils_findby_domain($m, $chan->domain, $s))
	{
		/*! \note iteration by socket_ID */
		while ($s->id)
		{
			/*! \note if not found then break */
			app_func_copy($s->output, "");
			if (!app_func_sock($s->sock))
			{
				app_func_free($s); 	
				break;
			}	
			 
			/*! \remark create example 'plus'  */
			app_func_struct($chan->m); 
			app_func_copy($chan->m->meta   , PRIV_META_MESG);
			app_func_copy($chan->m->type   , 'action');
			app_func_copy($chan->m->action , 'typing');
			app_func_copy($chan->m->error  , 0);  
			
			
			/*! \remark create Meta Body */
			app_func_copy($chan->m->data, $p->data);
			app_func_copy($meta, 
				array(
					'meta' 	 => $chan->m->meta,
					'type' 	 => $chan->m->type,	
					'action' => $chan->m->action,	
					'error'	 => $chan->m->error,
					'data'	 => $chan->m->data
				)
			);  
			/*! \sent message to partner client peers */
			app_func_output($s->output, json_encode($meta)); 
			if (app_func_enter($s->output)) 
			{
				if ( sockTcpClientWrite($s->sock, $s->output))  {
					app_func_free($s->output); 		
					app_func_next($retval);
				}
			}
			
			/*! \note break and stop */
			break;
		}
	}
	
	/*! \remark overide data from this */
	app_func_free($chan); 
	return $retval;
}

