<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 * 
 *
 */

/*! \retval res_peer_utils_findby_domain < *m Global Parameter, domain, *ret> */
 function res_peer_utils_findby_domain(&$m= NULL, &$domain = NULL, &$ret = 0)
{ 
	app_func_copy($ret, 0);
	if (!app_func_sizeof($m->AppPeerClients))
	{
		return $ret;
	}
	
	/*! \remark cari data berdasarkan domain */
	foreach ($m->AppPeerClients as $i => $r)
	{
		app_func_copy($o, false);
		if (app_func_retval($r, $o)) 
		{
			if (!app_func_eval($o, 'sid' )) {
				app_func_free($o);		
				continue;
			}
			
			
			/*! if client still exist compare with domain */
			if (!strcmp($o->sid, $domain))
			{
				if (app_func_copy($ret, $o) ){
					app_func_free($o);		
				}
				break;	
			}
			
			/*! if client still exist compare with domain */
			if (!strcmp($o->cid, $domain))
			{
				if (app_func_copy($ret, $o) ){
					app_func_free($o);		
				}
				break;	
			}
			
			app_func_free($o);		
		}
	}
	
	return $ret;
}

/*! \retval res_peer_utils_findby_host */
 function res_peer_utils_findby_host(&$m= NULL, &$host = NULL, &$ret = 0)
{
	return $ret;	
}
