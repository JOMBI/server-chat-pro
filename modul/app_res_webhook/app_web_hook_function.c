<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/*!
 * Custom function for User from client state 
 */
 
/*! \brief app_webhook_chat_channel_session */ 
 function app_webhook_chat_channel_session(&$m = NULL, &$chan = NULL, &$retval= false)
{	
	app_func_free($retval);
	if (is_array($m->AppChanSession) )
	{
		if (isset($m->AppChanSession[$chan]))
		{
			app_func_copy($retval, $m->AppChanSession[$chan]);
			
		}
	}	
	return $retval;
}

/*! \brief app_webhook_chat_channel_loger <app_webhook_chat_channel_session> 
 *
 * \remark get All chating on my channel with display divider or session 
 * history log chat 
 */
 function app_webhook_chat_channel_loger(&$m = NULL, &$app_webhook_chat_channel_session = NULL, &$retval= false)
{	
	Global $scp;
	
	app_func_free($retval);
	app_func_copy($retval, false);
	
	app_func_alloc($chats);
	if ( app_func_sizeof($app_webhook_chat_channel_session) > 0 )
	{
		foreach( $app_webhook_chat_channel_session as $chatboxid => $chat )
		{
			/*! \remark dump of chats */
			$boxtime = $m->AppBoxTime[$chatboxid];
			
			/*! \remark session ada tapi belum ada conversation */
			if (app_func_sizeof($chat) <1 ){
				$chats[] = array
				(
					'cid' 	 => strtotime('now'),
					'msgid'  => strtotime('now'),
					'msgbox' => $chatboxid,
					'time' 	 => $boxtime,
					'sfm' 	 => 4,
				);
			}
			/*! \remark session ada tapi sudah ada conversation */
			else if( app_func_sizeof($chat) >0 ){
					$chats[] = array
				(
					'cid' 	 => strtotime('now'),
					'msgid'  => strtotime('now'),
					'msgbox' => $chatboxid,
					'time' 	 => $boxtime,
					'sfm' 	 => 3,
				);
				
				/*! \remark push data to this content */ 
				foreach( $chat as $j => $text ) {
					$chats[] = $text;
				}	
			} 
		} 
	}	 
	
	if ( app_func_copy($retval, $chats) )
	{
		app_func_free($chats);	
	}
	
	return $retval;
}
 
 

/*! \brief app_webhook_chat_channel_chatboxid <app_webhook_chat_channel_session> */
 function app_webhook_chat_channel_chatboxid(&$app_webhook_chat_channel_session = NULL, &$boxid = NULL, &$retval= false)
{	
	app_func_free($retval);
	if ( app_func_sizeof($app_webhook_chat_channel_session) > 0 )
	{
		if ( isset($app_webhook_chat_channel_session[$boxid]) )
		{
			app_func_copy($retval, $app_webhook_chat_channel_session[$boxid]);
			
		}
	}	
	return $retval;
}
 

/*! \brief app_webhook_chat_box_session <m* master, sessionid, retval* mixed > */
 function app_webhook_chat_box_session(&$m = NULL, $chatboxid = NULL, &$retval = false)
{
	app_func_struct($p);
	app_func_copy($p->chatboxid, $chatboxid);
	
	if (isset($m->AppBoxSesion[$p->chatboxid]))
	{
		$p->chatbox = $m->AppBoxSesion[$p->chatboxid]; $p->chatvar = array();
		foreach ($p->chatbox as $j => $chan)
		{
			/*! \remark ini artinya channels yang melakukan INVITE */
			app_func_copy($s, false);
			if (!$j &&( app_user_session_find($chan, $s) ))
			{
				app_func_struct($o);
				app_func_copy($o->type, 2);
				app_func_copy($o->name, $s->get('name'));
				app_func_copy($o->session, $s->get('session'));
				app_func_copy($o->chatboxid, $p->chatboxid);
				app_func_copy($o->channel, $chan);
				
				/** \remark add new object array */
				$p->chatvar[$j] = array (
					'type' 	  	=> $o->type,
					'name' 	  	=> $o->name,
					'channel'   => $o->channel,
					'session' 	=> $o->session,
					'chatboxid' => $o->chatboxid
				);
				
				app_func_free($s);
				app_func_free($o);
			} 
			 
			/*! \remark channels yang di udang atau yang di INVITE */
			if ($j &&(app_user_session_find($chan, $s)))
			{
				app_func_struct($o);
				app_func_copy($o->type, 1);
				app_func_copy($o->name, $s->get('name'));
				app_func_copy($o->session, $s->get('session'));
				app_func_copy($o->chatboxid, $p->chatboxid);
				app_func_copy($o->channel, $chan);
				
				
				/** \remark add new object array */
				$p->chatvar[$j] = array (
					'type' 	  	=> $o->type,
					'name' 	  	=> $o->name,
					'channel'   => $o->channel,
					'session' 	=> $o->session,
					'chatboxid' => $o->chatboxid
				);
				
				app_func_free($s);
				app_func_free($o);
			}
		}
	}
	
	/*! \remark copy sebelum di destroy */
	if ( app_func_copy($retval, $p->chatvar) )
	{
		app_func_free($p);
	}
	
	return $retval;
}

/*! \brief app_webhook_chat_box_retval <m* master, sessionid, retval* mixed > */
 function app_webhook_chat_box_retval(&$m = NULL, $boxid = 0, &$retval = 0)
{
	
   /* cari jumlah channel pada suatu box sesion berdasarkkan boxid 
	* ada kemungkinan dalam satu box hanya satu channel karena channel yang lain 
	* sudah meninggalkan box percakapan 
	*/
	app_func_copy($retval, 0);
	$chatboxs = ( isset($m->AppBoxSesion[$boxid]) ? $m->AppBoxSesion[$boxid] : false);
	
	if ( !$chatboxs )
	{
		return 0;
	}
	
	if( app_func_sizeof($chatboxs, $restval) ){
		app_func_copy($retval, $restval);
	}
	
	return $retval;
}


/*! \brief app_webhook_user_find_session <m* master, sessionid, retval* mixed > */
 function app_webhook_user_find_session(&$m = NULL, $sessionid = NULL, &$retval = false)
{	
	app_func_free($retval);
	if (is_array($m->AppUserSesion)) foreach ($m->AppUserSesion as $k => $val){
		
		if ( !is_array($val) ){
			continue;
		}
		
		/*! cari nama dan sessionnya lalu bandingakan */
		if ( isset($val['session']) &&( !strcmp($val['session'], $sessionid))) 
		{
			if ( isset( $m->AppUserSesion[$k] ) )
			{
				app_func_copy($retval, $m->AppUserSesion[$k]);
				break;
			}
		}
	}
	 
	return $retval;
}