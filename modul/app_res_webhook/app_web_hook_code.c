<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
if( !defined( 'DEFAULT_RESPONSE') ) define( 'DEFAULT_RESPONSE', 'UKNOWN' );
 
/*! \def app_webhook_init_code < p* scp > */
 function app_webhook_init_code(&$p = NULL)
{
  
  $p->AppHookHeaderResponse = array
  (
    // Informational 1xx
    100 => 'Continue',
    101 => 'Switching Protocols',
    // Success 2xx
    200 => 'OK',
    201 => 'Created',
    202 => 'Accepted',
    203 => 'Non-Authoritative Information',
    204 => 'No Content',
    205 => 'Reset Content',
    206 => 'Partial Content',

    // Redirection 3xx
    300 => 'Multiple Choices',
    301 => 'Moved Permanently',
    302 => 'Found', // 1.1
    303 => 'See Other',
    304 => 'Not Modified',
    305 => 'Use Proxy',
    306 => 'is deprecated but reserved',
    307 => 'Temporary Redirect',

    // Client Error 4xx
    400 => 'Bad Request',
    401 => 'Unauthorized',
    402 => 'Payment Required',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    406 => 'Not Acceptable',
    407 => 'Proxy Authentication Required',
    408 => 'Request Timeout',
    409 => 'Conflict',
    410 => 'Gone',
    411 => 'Length Required',
    412 => 'Precondition Failed',
    413 => 'Request Entity Too Large',
    414 => 'Request-URI Too Long',
    415 => 'Unsupported Media Type',
    416 => 'Requested Range Not Satisfiable',
    417 => 'Expectation Failed',

    // Server Error 5xx
    500 => 'Internal Server Error',
    501 => 'Not Implemented',
    502 => 'Bad Gateway',
    503 => 'Service Unavailable',
    504 => 'Gateway Timeout',
    505 => 'HTTP Version Not Supported',
    509 => 'Bandwidth Limit Exceeded'
 );
} 

/*! \def app_webhook_code_header <o * Output , code > */
 function app_webhook_set_code(&$o= NULL, $code= NULL)
{
	if ( is_object($o) )
	{
		/*! \remark set default */
		app_func_copy($o->WebResCode,0);
		app_func_copy($o->WebResText,'UKNOWN');
		
		if ( app_webhook_get_code($code, $f) )
		{
			app_func_copy($o->WebResCode, $f->Code);
			app_func_copy($o->WebResText, $f->Text); 
		}
		
		app_func_free($f);
	}	
	
	return $o;	
}

/*! \def app_webhook_get_code < code , retval> */
 function app_webhook_get_code($code= 200, &$ret= false)
{
	Global $scp;
	
	/*! \remark crate Local Object */
	
	app_func_struct($out);
	app_func_copy($out->Code , $code);
	app_func_copy($out->Text , DEFAULT_RESPONSE);  
	if (isset($scp->AppHookHeaderResponse))
	{
		if (isset($scp->AppHookHeaderResponse[$out->Code]))
		{
			/*! \retval set response Handler */	
			app_func_copy( $out->Text, 
				$scp->AppHookHeaderResponse[$out->Code]
			);
		}
	}
	/*! \retval finaly text code */
	if ( app_func_copy($ret, $out) ) 
	{
		return $out->Text;
	}
	return 0;
}