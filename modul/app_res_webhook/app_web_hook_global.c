<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/*! \remark Binding Data */
/*!
{
	{APP_SERVER_TITLE}
	{APP_SERVER_NAME}
	{APP_SERVER_VERSI}
	{APP_SERVER_COPY}
	{APP_SERVER_AUTH}
	{APP_SERVER_HOST}
	{APP_SERVER_PORT}
	{APP_CLIENT_HOST}
	{APP_CLIENT_PORT}
	{APP_CLIENT_TIME}
} 
*/
	
/*! \retval app_webhook_global < s* client , output>*/
 function app_webhook_global_variable(&$s = NULL, &$output = NULL)
{
	Global $scp;	
	/*! \remark Default Parameter */
	app_webhook_global_server($scp, $s, $output);
	app_webhook_global_client($scp, $s, $output);
	
	/*! \return on client_id */
	return $s->id;
 }

/*! \retval app_webhook_global_server */
 function app_webhook_global_server(&$m, &$s = NULL, &$output = "")
{
	
	/*! \remark repalace content */
	app_func_replace('{!APP_SERVER_TITLE}', $output, $m->AppProgBranch);
	app_func_replace('{!APP_SERVER_NAME}' , $output, $m->AppProgUid);
	app_func_replace('{!APP_SERVER_VERSI}', $output, $m->AppProgVer);
	app_func_replace('{!APP_SERVER_COPY}' , $output, $m->AppProgCopy);
	app_func_replace('{!APP_SERVER_LINE}' , $output, $m->AppProgLine); 
	app_func_replace('{!APP_SERVER_AUTH}' , $output, $m->AppProgAuth);
	
	
	/*! \remark  for visitor socket */
	if (app_webhook_get_valid($m))
	{
		app_func_replace('{!APP_SOCKET_HOST}' , $output, app_webhook_get_server_host($m));
		app_func_replace('{!APP_SOCKET_PORT}' , $output, $m->VstTcpPort);
	}
	 
	/*! \remark Untuk port Ambil Ke real time */
	if (app_webhook_get_valid($m))
	{
		app_func_replace('{!APP_SERVER_HOST}', $output, app_webhook_get_server_host($m));
		app_func_replace('{!APP_SERVER_PORT}', $output, app_webhook_get_server_port($m));
		
	}
	
	/*! \retval force for server port URL Binding */ 
	app_func_replace('{!APP_SERVER_DOMAIN}', $output, 
		sprintf("%s:%s", app_webhook_get_server_host($m), app_webhook_get_server_port($m))
	);
	
	
	// for ALl dir 
	app_func_replace('{!APP_URL_MGR}', $output, 'manager'); 
	app_func_replace('{!APP_URL_VST}', $output, 'visitor');
	app_func_replace('{!APP_URL_AGT}', $output, 'agent'); 
	return 0;
 }
 
 /*! \retval app_webhook_global_client */
 function app_webhook_global_client(&$m, &$s = NULL, &$output = "")
{
   /*! \remark repalace content */
   
    app_func_replace('{!APP_CLIENT_SCID}' , $output, $s->id);
	app_func_replace('{!APP_CLIENT_HOST}' , $output, $s->host);
	app_func_replace('{!APP_CLIENT_PORT}' , $output, $s->port);
	app_func_replace('{!APP_CLIENT_TIME}' , $output, $s->time);
	
	/*! \retval for convert date time */
	app_func_replace('{!APP_CLIENT_DTFL}' , $output, 
		date('Y-m-d H:i:s', $s->time)
	);
	
	/*! \retval today */
	app_func_replace('{!APP_CLIENT_DTFD}' , $output, 
		date('d M Y H:i:s', $s->time)
	); 
	
	/*! \retval today */
	app_func_replace('{!APP_CLIENT_DTTD}' , $output, 
		date('d M Y H:i:s', $s->time)
	); 
	
	/*! \retval for get domain from parameter if not exist will default get from server name */
	if ( app_os_hostname(app_webhook_get_param_domain($m), $hostname) )
	{
		app_func_replace( '{!APP_CLIENT_DUID}', $output, addslashes($hostname) ); 
	}
	
	app_func_replace('{!APP_CLIENT_WVST}' , $output, addslashes('visitor'));
	app_func_replace('{!APP_CLIENT_WAGT}' , $output, addslashes('agent')); 
	
	return 0;
 }
 
/*! \retval app_webhook_global_hook < m* Master , s* client , o * output, content >*/
 function app_webhook_global_hook(&$m, &$s, &$o, &$output = NULL)
{
    /*! \remark repalace content */ 
	if ( !class_exists ('http') )
	{
	   return 0;
	}
	
	/*! \retval buat class object untuk Aksess client */
	app_func_struct($Webhook);
	
	/*! \remark agar file php bisa di baca maka perlu di lakukan manipulasi data */
	$Webhook->global   =& $m;
	$Webhook->clients  = $m->AppUserSesion;
	$Webhook->server   = new evaluate((array)$m);
	$Webhook->request  = new evaluate($m->WebHooks->param);
	$Webhook->session  = new evaluate((array)$s); 
	$Webhook->response = array();
	
	
	/*! \remark dispatch data ti new objec */
	if ( !app_webhook_events($o, $s, $m, $Webhook->response) ) 
	{
		app_func_free($Webhook->response);
	} 
	
	/*! \remark read buffer */
	if (function_exists('ob_start'))
	{ 
		ob_start(); 
		$length = include($o->WebResPath); 
		$output = ob_get_clean();
	}
	
	/*! \retval cleanup process */
	app_func_free($Webhook->request);
	app_func_free($Webhook->session);
	
	/*! return is size of content*/
	if ( app_func_sizeof($output, $retval))
	{
		return $retval;
	}
	
	return 0;
 }
 
 /*! \retval app_webhook_global_method < m* Master , s* client , o * output, content >*/
 function app_webhook_global_method(&$m, &$s, &$o, &$output = NULL)
{
    /*! \remark repalace content */ 
	if ( !class_exists ('http') )
	{
	   return 0;
	}
	
	/*! \retval buat class object untuk Aksess client */
	app_func_struct($Webhook);
	
	/*! \remark agar file php bisa di baca maka perlu di lakukan manipulasi data */
	
	$Webhook->request = new http($m->WebHooks->param);
	$Webhook->session = new http((array)$s); 
	
	/*! \remark read buffer */
	if (function_exists('ob_start'))
	{ 
		ob_start(); 
		$length = include($o->WebResPath); 
		$output = ob_get_clean();
	}
	
	/*! \retval cleanup process */
	app_func_free($Webhook->request);
	app_func_free($Webhook->session);
	
	/*! return is size of content*/
	if ( app_func_sizeof($output, $retval))
	{
		return $retval;
	}
	return 0;
	
 }
 