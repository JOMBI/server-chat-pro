<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/*! \def app_webhook_events_agent */ 
 function app_webhook_events_agent(&$o= NULL, &$s= NULL, &$m= NULL, &$events_replay = NULL)
{
	
}

/*! \def app_webhook_events_visitor */
 function app_webhook_events_visitor(&$o= NULL, &$s= NULL, &$m= NULL, &$replay = NULL)
{
	
	/*! \remark set default replay for action stream */
	app_func_copy($retval, 0);
	$replay = array('error'=> 1,  'event'=> 'none', 'message'=> 'fail', 'data'=> array());
	
	/*! \remark for visitor_event "register" */
	if (!strcmp($o->WebResRoute, 'register')) 
	{
		app_func_event('visitor_action_register', array(&$o, &$s, &$m, &$replay), 
		__FILE__, __LINE__ ); 
	}
	
	/*! \remark for visitor_event "unregister" */
	if (!strcmp($o->WebResRoute, 'unregister')) 
	{
		app_func_event('visitor_action_unregister', array(&$o, &$s, &$m, &$replay), 
		__FILE__, __LINE__ ); 
	} 
	
	/*! \remark for visitor_event "routing" */
	if (!strcmp($o->WebResRoute, 'routing')) 
	{
		app_func_event('visitor_action_routing', array(&$o, &$s, &$m, &$replay), 
		__FILE__, __LINE__ ); 
	} 
	
	
	/*! \remark for visitor_event "pickup" */
	if (!strcmp($o->WebResRoute, 'pickup')) 
	{
		app_func_event('visitor_action_pickup', array(&$o, &$s, &$m, &$replay), 
		__FILE__, __LINE__ ); 
	} 
	
	/*! \remark for visitor_event "discard" */
	if (!strcmp($o->WebResRoute, 'discard')) 
	{
		app_func_event('visitor_action_discard', array(&$o, &$s, &$m, &$replay), 
		__FILE__, __LINE__ ); 
	} 
	
	/*! \remark for visitor_event "delete" */
	else if (!strcmp($o->WebResRoute, 'delete')) 
	{
		app_func_event('visitor_action_delete', array(&$o, &$s, &$m, &$replay), 
		__FILE__, __LINE__ ); 
	}
	
	/*! \remark for visitor_event "welcome" */
	else if (!strcmp($o->WebResRoute, 'welcome')) 
	{
		app_func_event('visitor_action_welcome', array(&$o, &$s, &$m, &$replay), 
		__FILE__, __LINE__ ); 
	}
	
	/*! \retaval finaly is true / false */ 
	app_func_next($retval);
	return $retval;
}


/*! \def app_webhook_events */
 function app_webhook_events(&$o= NULL, &$s= NULL, &$m= NULL, &$events_replay = NULL)
{
	app_func_copy($retval, 0);
	if ( app_webhook_events_visitor($o, $s, $m, $events_replay) )
	{
		app_func_next($retval);
	}		 
	return $retval;
} 