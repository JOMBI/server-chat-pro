<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 // https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types

/*! \def app_webhook_init_mime < p* scp > */ 
 function app_webhook_init_mime(&$p = NULL)
{
   $p->AppHookMimeResponse = array
	(
		'text'  => 'text/plain',
		'html'  => 'text/html charset=utf-8',
		'envc'  => 'application/x-www-form-urlencoded',
		'json'  => 'application/json; charset=UTF-8',
		'hook'	=> 'application/json;application/hook; charset=UTF-8',
		'icon'  => 'image/x-icon; charset=UTF-8',
		'ico'	=> 'image/vnd.microsoft.icon',
		'svg'	=> 'image/svg+xml',
		'woff2' => 'font/woff2',
		'woff'  => 'font/woff',
		'ttf'   => 'font/ttf',
		'mp3'	=> 'audio/mpeg',
		'mp4'	=> 'video/mp4',
		'mpeg'	=> 'video/mpeg',
		'png'	=> 'image/png',
		'gif'	=> 'image/gif',
		'jpg'	=> 'image/jpg;image/jpeg',
		'css'   => 'text/css',
		'app' 	=> 'text/html;text/app charset=utf-8',
		'js'    => 'text/x-javascript',
		'on'    => 'application/json; charset=UTF-8' 
	);
	
	return 0;
}

/*! \def app_webhook_cfg_mime< type, retval > */
 function app_webhook_cfg_mime(&$ret = NULL)
{
	/*! default NULL */
	app_func_copy($ret, NULL);
	
	/*! local object */
	app_func_struct($mime);
	app_func_copy($mime->video	, array('MP4', 'MPEG'));
	app_func_copy($mime->image	, array('JPG', 'JPEG', 'PNG', 'GIF'));
	app_func_copy($mime->audio	, array('MIDI', 'MID', 'MP3', 'WAV'));
	app_func_copy($mime->font 	, array('WOFF', 'WOFF2', 'TTF'));
	app_func_copy($mime->icon 	, array('ICO', 'ICON', 'SVG'));
	app_func_copy($mime->style 	, array('CSS'));
	app_func_copy($mime->script , array('JS'));
	app_func_copy($mime->json 	, array('JSON'));
	app_func_copy($mime->html 	, array('HTML', 'APP'));
	app_func_copy($mime->hook 	, array('HOOK'));
	app_func_copy($mime->text 	, array('TXT','CSV'));
	
	if (app_func_copy($ret, $mime) )
	{
		return $ret;
	}
 }

/*! \def app_webhook_set_mime < o* output, type > */
 function app_webhook_set_mime(&$o = NULL, &$type = "")
{
	app_func_copy($o->WebResContentType,'text/plain'); 
	app_func_copy($type, app_func_lower($type));
	if ( app_webhook_get_mime($type, $retval) )
	{
		app_func_copy($o->WebResContentType, $retval);
		app_func_free($retval);
	}
	
	return $o->WebResContentType;
} 

/*! \def app_webhook_get_mime < type, retval > */
 function app_webhook_get_mime($type = NULL, &$ret = NULL)
{
	Global $scp;
	
	if ( !is_array($scp->AppHookMimeResponse) )
	{
		return 0;
	}
	
	if ( isset($scp->AppHookMimeResponse[$type]) )
	{
		app_func_copy($ret,$scp->AppHookMimeResponse[$type]);
	} 
	
	return $ret;
}


