<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/*! \remark app_webhook_client_delete < p* scp, s* client > */
 function app_webhook_client_delete(&$p, &$s = NULL )
{
	if ( !sockTcpClose($s->sock) )
	{
		scpAppClientDeleted($p, $s);
	}
	return $s->id;
 }
  
 
/*! \remark app_webhook_client_sighup < p* scp, s* client > */
 function app_webhook_client_sighup(&$p, &$s = NULL )
{
	if (app_webhook_client_delete($p, $s))
	{
		unset($p->AppProgSocket[$s->id]); 
	}
	
	return $s->id;
}
 
/*! \remark app_webhook_client_collected <scp, s* client> */
 function app_webhook_client_collected(&$p = NULL, &$s= NULL)
{
	/*! \def finaly return ==== false */
	if ( function_exists( 'scpAppClientCollected' ) ) {
		return 0;
	}
	
	if ( scpAppClientCollected($p, $s) )
	{
		return $s->id;
	}
	
	return 0;
}

 
/*! \remark app_webhook_client_thread < p* scp, s* client, pid > */
 function app_webhook_client_thread(&$p = NULL, &$s= NULL)
{
	if (scpAppCreateThread($p,'app_webhook_client_handler', array(&$p, &$s), $s->pid))
	{
		return $s->pid;		
	}		
	
	return 0;
}
 
/*! \remark app_webhook_client_handler < p* scp, s* client> */
 function app_webhook_client_handler(&$p, &$s= NULL )
{
	/*! \remark iniated for object Global 'WebHooks' */
	if( isset($p->WebHooks) )
	{
		app_func_struct($p->WebHooks);
	}
	
	/*! \remark  next to generate response */
	app_func_struct($o); 
	app_func_copy( $o->sizeTxByte, 0);
	if ( app_webhook_response($p, $s, $o) )
	{ 
		$o->sizeTxByte = sockTcpWrite($s->sock, $o->output);
		if ( $o->sizeTxByte ){
			scp_log_app(sprintf( "Sent Size Byte = %s", $o->sizeTxByte));
		} 
	}
	
   /*! \remark karena ini sifatnya state less maka socket langsug 
	*  di destroy atau close saja 
	*/ 
	if ($o->sizeTxByte) {
		/*! \remark dont usage  usleep(1000000); */
	}
	
	sockTcpClose($s->sock);
	return $s->id;	 
} 