<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
  /*! \remark app_web_hook_header_caching */
 function app_web_hook_header_caching(&$o = NULL, $mime = "css")
{
	app_func_copy($CacheControl, 0); 
	if ( $mime == "css" ){
		app_func_copy($o, 'WebResCacheResponse', true); 
		app_func_copy($o, 'WebResCacheControl', "max-age=3600;"); 
		app_func_next($CacheControl);
	}
	else if ( $mime == "ico" ){
		app_func_copy($o, 'WebResCacheResponse', true);
		app_func_copy($o, 'WebResCacheControl', "max-age=3600;"); 
		app_func_next($CacheControl);
	} 
	return $CacheControl;
 }
 
 /*! \def app_web_hook_header < s* client, o* output, code, mime > */
 function app_webhook_header_default(&$o = NULL)
 {
	app_func_copy($o, 'WebResTimeout'	  , 120); 	
	app_func_copy($o, 'WebResDateTime'	  , date('d M Y H:i:s'));
	app_func_copy($o, 'WebResConnection'  , 'close');
	app_func_copy($o, 'WebResAllowOrigin' , '*');
	/*! \remark return is header.response.timeout */
	return $o->WebResTimeout;
 } 
 
/*! \def app_webhook_header < s* client, o* output, code, mime > */
 function app_webhook_header(&$s = NULL, &$o = NULL, $code = 200, $mime = 'html')
{	
	if (app_webhook_header_default($o))
	{
		/*! \remark set header Code Response */ 
		/*! \remark set header Mime Response */ 
		
		app_webhook_set_code($o, $code);
		if (app_webhook_set_mime($o, $mime))
		{
			app_func_output($o->output, sprintf("HTTP/1.0 %d %s\r\n", $o->WebResCode, $o->WebResText));
			app_func_output($o->output, sprintf("Date: %s\r\n", $o->WebResDateTime));
			app_func_output($o->output, sprintf("Server: %s/%s\r\n", $o->WebResName, $o->WebResVersion));
			app_func_output($o->output, sprintf("Copyright: %s\r\n", $o->WebResCopy));
			app_func_output($o->output, sprintf("Connection: %s\r\n", $o->WebResConnection)); 
			app_func_output($o->output, sprintf("Keep-Alive: timeout=%d, max=%d\r\n", $o->WebResTimeout, $o->WebResTimeout * 2)); 
			app_func_output($o->output, sprintf("Access-Control-Allow-Origin: %s\r\n", $o->WebResAllowOrigin));
			app_func_output($o->output, sprintf("Content-Type: %s\r\n", $o->WebResContentType));
			
			/*! \remark app_web_hook_header_caching */
			if (app_web_hook_header_caching($o, $mime))
			{
				app_func_output($o->output, sprintf("Cache-Control: %s\r\n", $o->WebResCacheControl)); 
			}
			
			app_func_output($o->output, "\r\n"); 
		}
	}
	
	/*! \remark return is header.response.code */
	return $o->WebResCode;
} 