<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/*! app_webhook_config_path $m <scp> , out*/ 
 function app_webhook_config_path(&$m = NULL, &$o= NULL,  &$ret = 0)
{
	while ($o->WebResPath)
	{
		
		/*! \remark jika file extension sudah  ada maka tidak perlu di cari lagi */
		app_func_copy($s, false);
		if ( app_func_sizeof($o->WebResType) )
		{
			break;
		}
		
		/*! \def get Default from config Page "Agent" */
		if (stristr($o->WebResPath, "agent") )
		{	
			/*! \remark Not an configuration file */
			if (!app_func_search('agent', array_keys($m->AppProgVar)) )
			{
				break; 
			}
			
			/*! \def Ok is process */
			if (isset($m->AppProgVar['agent']['default_page']))
			{
				app_func_output($o->WebResPath, sprintf("/%s", $m->AppProgVar['agent']['default_page']));
				if ( file_exists($o->WebResPath) && (app_func_pathinfo($o->WebResPath, $s)))
				{
					app_func_copy($o->WebResType , app_func_upper($s->extension));
					app_func_next($o->WebResPage);
					app_func_free($s);
				}	
			}
			
			break;
		}
		
		/*! \def get Default from config Page "visitor" */
		if (stristr($o->WebResPath, "visitor") )
		{	
			/*! \remark Not an configuration file */
			if ( !app_func_search('visitor', array_keys($m->AppProgVar)) )
			{
				break;
			}
			
			if ( isset($m->AppProgVar['visitor']['default_page']) ) 
			{
				app_func_output($o->WebResPath, sprintf("/%s", $m->AppProgVar['visitor']['default_page']));
				if ( file_exists($o->WebResPath) && (app_func_pathinfo($o->WebResPath, $s)))
				{
					app_func_copy($o->WebResType , app_func_upper($s->extension));
					app_func_next($o->WebResPage);
					app_func_free($s);
				}	
			}
		}	 
		
		break;
	}
	
	/*! \remark finaly is true or false */
	return $o->WebResPage;
	
} 

/*! app_webhook_header_path */
 function app_webhook_header_path(&$o, &$ret = 0)
{
	app_func_copy($ret, HEADER_RESPONSE_404);  
	if ( app_func_file($o->WebResPath) )
	{
		app_func_copy($ret, HEADER_RESPONSE_200);
	}	
	
	return $ret;
} 