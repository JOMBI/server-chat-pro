<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/*! \def app_webhook_icon < o* output> */
 function app_webhook_content_icon(&$o, &$s, &$m)
{
	/*! \remark return indication */ 
	app_func_copy($o->ret, 0);
	$content = @file_get_contents($o->WebResPath);  
	if (app_func_output($o->output, $content))
	{
		app_func_free($content);
	} 
	
	/*! \remark for return indication */
	app_func_next($o->ret);
	return $o->ret;
}

/*! \def app_webhook_content_html < o* output> */
 function app_webhook_content_html(&$o, &$s, &$m)
{
	app_func_copy($o->ret, 0);
	while (!$o->ret)
	{
		/*! \remark jika file yang dimaksud tidak ada */
		if ( !app_func_file($o->WebResPath) ) 
		{
			if ( app_webhook_content_error($o, $s, $m) )
			{
				app_func_next($o->ret);
			}
			break;
		}
		
		/*! \remark return indication */ 
		if ( app_webhook_global_method($m, $s, $o, $content) )
		{
			if (app_func_output($o->output, $content))
			{
				app_func_free($content); 
			} 
		} 
		
		/*! \remark for return indication */
		app_func_next($o->ret);
		break;
	}
	
	return $o->ret;
}


/*! \def app_webhook_content_json < o* output> */
 function app_webhook_content_hook(&$o, &$s, &$m)
{
	/*! \remark return indication */ 
	app_func_copy($o->ret, 0);
	if ( app_webhook_global_hook($m, $s, $o, $content) )
	{
		if (app_func_output($o->output, $content))
		{
			app_func_free($content); 
		} 
	} 
	
	/*! \remark for return indication */
	app_func_next($o->ret);
	return $o->ret; 
}

/*! \def app_webhook_content_json < o* output> */
 function app_webhook_content_json(&$o, &$s, &$m)
{
	app_func_copy($o->ret, 0);
	$content = @file_get_contents($o->WebResPath); 	
	if (app_func_output($o->output, $content))
	{
		if (app_webhook_events($o, $s, $m, $eventsReplay))
		{
			$dispatchEvents = @json_encode($eventsReplay);
			if (!app_func_replace( '{!on_web_response}', $o->output, $dispatchEvents))
			{
				app_func_free($dispatchEvents);
			}  
		} 
		
		app_func_free($content);
	} 
	
	app_func_next($o->ret);
	return $o->ret;
}


/*! \def app_webhook_content_script < o* output> */
 function app_webhook_content_script(&$o, &$s, &$m)
{
	/*! \remark return indication */ 
	app_func_copy($o->ret, 0);
	ob_start(); 
	$size = @readfile($o->WebResPath);  
	$content = ob_get_clean();
	if ( app_func_output($o->output, $content) )
	{
		app_func_free($content);
	} 
	
	/*! \remark for return indication */
	app_func_next($o->ret);
	return $o->ret;
}

/*! \def app_webhook_content_style < o* output> */
 function app_webhook_content_style(&$o, &$s, &$m)
{
	app_func_copy($o->ret, 0);
	while ( !$o->ret )
	{
		
		/*! \remark jika file yang dimaksud tidak ada */
		if ( !app_func_file($o->WebResPath) ) 
		{
			app_func_next($o->ret);
			break;
			
		}
		
		/*! \remark return indication */  
		$content = @file_get_contents($o->WebResPath);  
		if (app_func_output($o->output, $content))
		{
			app_func_free($content);
		} 
		
		/*! \remark for return indication */
		app_func_next($o->ret); 
	}
	
	return $o->ret;
}

/*! \def app_webhook_content_font < o* output> */
 function app_webhook_content_font(&$o, &$s, &$m)
{
	/*! \remark return indication */ 
	app_func_copy($o->ret, 0);
	$content = @file_get_contents($o->WebResPath);  
	if (app_func_output($o->output, $content))
	{
		app_func_free($content);
	} 
	
	/*! \remark for return indication */
	app_func_next($o->ret);
	return $o->ret;
}

/*! \def app_webhook_content_audio < o* output> */
 function app_webhook_content_audio(&$o, &$s, &$m)
{
	/*! \remark return indication */ 
	app_func_copy($o->ret, 0);
	$content = @file_get_contents($o->WebResPath);  
	if (app_func_output($o->output, $content))
	{
		app_func_free($content);
	} 
	
	/*! \remark for return indication */
	app_func_next($o->ret);
	return $o->ret;
}


/*! \def app_webhook_content_image < o* output> */
 function app_webhook_content_image(&$o, &$s, &$m)
{
	/*! \remark return indication */ 
	app_func_copy($o->ret, 0);
	$content = @file_get_contents($o->WebResPath);  
	if (app_func_output($o->output, $content))
	{
		app_func_free($content);
	} 
	
	/*! \remark for return indication */
	app_func_next($o->ret);
	return $o->ret;
}


/*! \def app_webhook_error < o* output> */
 function app_webhook_content_error(&$o, &$s, &$m)
{
	/*! \remark return indication */ 
	app_func_copy($o->retval, 0);
	
	/*! \remark body make Atrribute JS OR order  */
	app_func_output( $o->output, "");
	app_func_output( $o->output, 
		sprintf( 
			"<!doctype html>\n".
			"<html>\n".
			"<head>\n".
			"\t<title>%s</title>\n".
			"\t<style>\n".
			"\t\thtml, body {".
			"font-family: sans-serif;".
			"font-size:13px;".
			"color: #444; ".
			"margin:10%%; ".
			"}\n".
			"\t</style>\n".
			"</head>\n",
			$o->WebResTitle		
		)
	);
	
	/*! \remark Build Body Content-Type */
	app_func_output( $o->output, 
		sprintf("<body>\n\t<center>%s Version %s</center>\n\t<center><h3>%d - %s</h3></center>\n", 
			$o->WebResTitle, 
			$o->WebResVersion, 
			$o->WebResCode, 
			$o->WebResText
		)
	);
	
	app_func_output($o->output, "</body>\n"); 
	app_func_output($o->output, "</html>"); 
	
	/*! \remark for return indication */
	app_func_next($o->retval);
	return $o->retval;
}  