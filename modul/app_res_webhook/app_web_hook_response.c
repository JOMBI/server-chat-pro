<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
 if ( !defined( 'HEADER_RESPONSE_200' ) ) define( 'HEADER_RESPONSE_200', 200 );
 if ( !defined( 'HEADER_RESPONSE_403' ) ) define( 'HEADER_RESPONSE_403', 403 );
 if ( !defined( 'HEADER_RESPONSE_404' ) ) define( 'HEADER_RESPONSE_404', 404 );
 
 
/*! \def app_webhook_response <scp, s* client, o* output >*/ 
  function app_webhook_response_config(&$o= NULL, &$p = NULL)
{
	app_func_copy($o->WebResVersion, $p->AppProgVer); 
	app_func_copy($o->WebResTitle, $p->AppProgTitle);  
	app_func_copy($o->WebResBuild, $p->AppProgBuild); 
	app_func_copy($o->WebResName, $p->AppProgName);  
	app_func_copy($o->WebResCopy, $p->AppProgCopy);  
	app_func_copy($o->WebResLink, $p->AppProgLink); 
	app_func_copy($o->WebResAuth, $p->AppProgAuth); 
	app_func_copy($o->WebResUid, $p->AppProgUid);   
	return $o;
}
 
/*! \def app_webhook_response <scp, s* client, o* output >*/ 
 function app_webhook_response(&$scp= NULL, &$s = NULL, &$o = NULL)
{
   /*!
	* \remark Urutan Process Ketika Kirim Response ke web client 
	* 1. parsing request from client
	* 3. untuk mime simpan di aray saja di kelompokan berdasarkan jenis 
	* 2. set Init default code 
	* 4. Response Code 
	* 5. Global Parameter 
	*
	*/
	
	if (function_exists('app_webhook_parse_request'))
	{
		app_webhook_parse_request($scp, $s);
	}
	
	app_webhook_cfg_mime($mime);
	app_webhook_init_mime($scp);
	app_webhook_init_code($scp); 
	
	if ( function_exists('app_webhook_set_code') )
	{ 
		app_webhook_set_code($o, HEADER_RESPONSE_200);
	} 
	
	app_webhook_response_config($o, $scp); 
	while ($s->id)
	{
		app_func_copy($o->output, ""); 
		app_func_copy($o->WebResPage , 0);
		app_func_copy($o->WebResRoute, app_webhook_get_router($scp));
		app_func_copy($o->WebResType , app_webhook_get_type($scp));
		app_func_copy($o->WebResPath , app_webhook_get_path($scp));
		app_func_copy($o->WebResCode , HEADER_RESPONSE_404);
		
	   /*! 
		* \file app_web_hook_path.c	
	    * \note Check apakah untuk dir ini di allow atau tidak dan apakah 
		*  memiliki nilai default page 
		*/
		app_webhook_config_path($scp, $o);
		 
		/*! \retval Response Content If Empty 'NULL' */
		if (!$o->WebResPage &&( !app_func_sizeof($o->WebResType) ))
		{	
			app_func_copy($o->WebResType, 'html');
			if (app_webhook_header($s, $o, $o->WebResCode, $o->WebResType))
			{
			   /*! \remark for Response data html By default set All Argument requested from clients session ,
				* If requested Fail return fail. And Close Session 
				*/ 
				if (app_webhook_content_error($o, $s, $scp)) {
					app_func_output($o->output, "\n");	
				}
			}  
			break; 
		}
		
		
		/*! \retval Response Content for 'ICON' */ 
		if (in_array($o->WebResType, $mime->icon))
		{
			/*! \retval check file content by path */
			if (app_webhook_header_path($o, $response)){
				app_func_copy($o->WebResCode, $response);
			}
			
			if (app_webhook_header($s, $o, $o->WebResCode, $o->WebResType))
			{
				app_web_hook_loger($s, sprintf( "Open File Type= %s Code= %s Name= %s", 
					$o->WebResType, $o->WebResCode, basename($o->WebResPath)
				));
				
				if (app_webhook_content_icon($o, $s, $scp)) {
					app_func_output($o->output, "\n");	
				}
			}  
			
			break;
		}
		
		
		/*! \retval Response Content for 'HTML' */
		if (in_array($o->WebResType, $mime->html))
		{
			/*! \retval check file content by path */
			if (app_webhook_header_path($o, $response)){
				app_func_copy($o->WebResCode, $response);
			}
			
			if (app_webhook_header($s, $o, $o->WebResCode, $o->WebResType))
			{
				app_web_hook_loger($s, sprintf( "Open File Type= %s Code= %s Name= %s", 
					$o->WebResType, $o->WebResCode, basename($o->WebResPath)
				));
				
				if (app_webhook_content_html($o, $s, $scp)) 
				{
					if (app_webhook_global_variable($s, $o->output))
					{
						app_func_output($o->output, "\n");	
					}
				}
			}   
			
			break;
		}
		
		
		/*! \retval Response Content for 'HOOK' */
		if (in_array($o->WebResType, $mime->hook))
		{
			/*! \retval check file content by path */
			if (app_webhook_header_path($o, $response)){
				app_func_copy($o->WebResCode, $response);
			}
			
			if (app_webhook_header($s, $o, $o->WebResCode, $o->WebResType))
			{
				app_web_hook_loger($s, sprintf( "Open File Type= %s Code= %s Name= %s", 
					$o->WebResType, $o->WebResCode, basename($o->WebResPath)
				));
				
				if (app_webhook_content_hook($o, $s, $scp)) 
				{
					if (app_webhook_global_variable($s, $o->output))
					{
						app_func_output($o->output, "\n");	
					}
				}
			}   
			
			break;
		}
		
		
		/*! \retval Response Content for 'JSON' */
		if (in_array($o->WebResType, $mime->json))
		{
			/*! \retval check file content by path */
			if (app_webhook_header_path($o, $response)){
				app_func_copy($o->WebResCode, $response);
			} 
			
			if (app_webhook_header($s, $o, $o->WebResCode, $o->WebResType))
			{
				app_web_hook_loger($s, sprintf( "Open File Route=%s type= %s Code = %s Name= %s", 
					$o->WebResRoute, $o->WebResType, $o->WebResCode, basename($o->WebResPath)
				));
				
			    if (app_webhook_content_json($o, $s, $scp))
				{
					app_func_output($o->output, "\n");	
				}
			} 
			
			break;
		}
		
		
	    /*! \retval Response Content for 'JS' */
		if (in_array($o->WebResType, $mime->script))
		{
			/*! \retval check file content by path */
			if (app_webhook_header_path($o, $response)){
				app_func_copy($o->WebResCode, $response);
			} 
			
			if (app_webhook_header($s, $o, $o->WebResCode, $o->WebResType))
			{
				app_web_hook_loger($s, sprintf( "Open File Type= %s Code= %s Name= %s", 
					$o->WebResType, $o->WebResCode, basename($o->WebResPath)
				));
				
			    if (app_webhook_content_script($o, $s, $scp))
				{
					app_webhook_global_variable($s, $o->output);
					app_func_output($o->output, "\n");
				}
			} 
			
			break;
		}
		
		
		/*! \retval Response Content for 'STYLE' */
		if (in_array($o->WebResType, $mime->style))
		{
			/*! \retval check file content by path */
			if (app_webhook_header_path($o, $response)){
				app_func_copy($o->WebResCode, $response);
			} 
			
			if (app_webhook_header($s, $o, $o->WebResCode, $o->WebResType))
			{
				app_web_hook_loger($s, sprintf( "Open file Type= %s Code= %s Name= %s",
					$o->WebResType, $o->WebResCode, basename($o->WebResPath)
				));
				
			    if (app_webhook_content_style($o, $s, $scp))
				{
					
					app_func_output($o->output, "\n");	
					if ( app_webhook_global_variable($s, $o->output) )
					{
						app_func_output($o->output, "\n");	
					}
				}
			} 
			
			break;
		}
		
		
		/*! \retval Response Content for 'Font' */
		if (in_array($o->WebResType, $mime->font))
		{
			/*! \retval check file content by path */
			if (app_webhook_header_path($o, $response)){
				app_func_copy($o->WebResCode, $response);
			} 
			
			if (app_webhook_header($s, $o, $o->WebResCode, $o->WebResType))
			{
				app_web_hook_loger($s, sprintf( "Open File Type= %s Code= %s Name= %s",
					$o->WebResType, $o->WebResCode, basename($o->WebResPath)
				));
				
			    if (app_webhook_content_font($o, $s, $scp)){
					app_func_output($o->output, "\n");	
				}
			} 
			
			break;
		}
		
		
		/*! \retval Response Content for 'Image' */
		if (in_array($o->WebResType, $mime->image))
		{
			/*! \retval check file content by path */
			if (app_webhook_header_path($o, $response)){
				app_func_copy($o->WebResCode, $response);
			} 
			
			if (app_webhook_header($s, $o, $o->WebResCode, $o->WebResType))
			{
				app_web_hook_loger($s, sprintf( "Open File Type= %s Code= %s Name= %s",
					$o->WebResType, $o->WebResCode, basename($o->WebResPath)
				));
				
			    if (app_webhook_content_image($o, $s, $scp)){
					app_func_output($o->output, "\n");	
				}
			}  
			break;
		}
		
		
		/*! \retval Response Content for 'AUDIO' */
		if (in_array($o->WebResType, $mime->audio))
		{
			/*! \retval check file content by path */
			if (app_webhook_header_path($o, $response)){
				app_func_copy($o->WebResCode, $response);
			} 
			
			if (app_webhook_header($s, $o, $o->WebResCode, $o->WebResType))
			{
				app_web_hook_loger($s, sprintf( "Open File Type= %s Code= %s Name= %s",
					$o->WebResType, $o->WebResCode, basename($o->WebResPath)
				));
				
			    if (app_webhook_content_audio($o, $s, $scp)){
					app_func_output($o->output, "\n");	
				}
			} 
			
			break;
		}
		
		
		/*! \retval Response Content for 'VIDEO' */
		if (in_array($o->WebResType, $mime->video))
		{
			break;
		}
		
		
		/*! \retval Response Content for 'TEXT' */
		if (in_array($o->WebResType, $mime->text))
		{
			break;
		}
		
		/*! \remark break And stop */
		app_web_hook_loger($s, sprintf("Redirect file '%s:%s' failed. ", $o->WebResType,$o->WebResPath));
		app_func_copy($o->WebResType, 'html');
		if (app_webhook_header($s, $o, $o->WebResCode, $o->WebResType))
		{
		   /*! \remark for Response data html By default set All Argument requested from clients session ,
			* If requested Fail return fail. And Close Session 
			*/ 
			if (app_webhook_content_error($o, $s, $scp)) {
				app_func_output($o->output, "\n");	
			}
		}  
		break;
	}
	
	/*! \retval is socket_client_id */
	return $s->id;
}