<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */ 
 
 /**
 
 chan1 Routing  => `
 chan1 Dialing  =>  	chan2 => Ringing
 chan1 Ring     =>  	chan2 => ringing
 chan1 Ring     =>  	chan2 => up 
 chan1 connect  =>  	chan2 => 
 
 
 /**
	status 
	
	Logout 		0
	Register	3 
	Ready 		1
	AUX  		2
	Reserv		6
	Busy		4
	
	
	Routing		1 
	dialing		2  
	ringing 	3  
	pickup		4 
	connected 	5
	close by 	6  
  */
  
 if ( !defined('STATE_LOGOUT')) define('STATE_LOGOUT', 0);
 if ( !defined('STATE_REGISTER')) define('STATE_REGISTER', 3);
 if ( !defined('STATE_READY')) define('STATE_READY', 1);
 if ( !defined('STATE_AUX')) define('STATE_AUX', 2);
 if ( !defined('STATE_RESERVED')) define('STATE_RESERVED', 6);
 if ( !defined('STATE_BUSY')) define('STATE_BUSY',4);
 
 
 /*! state for channel */ 
 if ( !defined('CHAN_ROUTING')) define('CHAN_ROUTING', 1);
 if ( !defined('CHAN_DIALING')) define('CHAN_DIALING', 2);
 if ( !defined('CHAN_RINGING')) define('CHAN_RINGING', 3);
 if ( !defined('CHAN_PICKUP')) define('CHAN_PICKUP', 4);
 if ( !defined('CHAN_CONNECTED')) define('CHAN_CONNECTED', 5);
 if ( !defined('CHAN_DROPED')) define('CHAN_DROPED',6);
 if ( !defined('CHAN_DISCARD')) define('CHAN_DISCARD',7);
 if ( !defined('CHAN_RESERVED')) define('CHAN_RESERVED',0);
 
 
/*! \remark  app_set_channel_status */
 function app_set_channel_status(&$m= NULL, &$channel = NULL, $state = 0)
{
	/** state */
	app_func_copy($retval, false);
	app_func_copy($enum, 0);
	if ( app_user_session_find($channel, $retval) )
	{
		if ($retval)
		{
			
			/*! \remark set state untuk channel */
			$m->AppUserSesion[$channel]['state'] = $state; 
			
			/*! \remark untuk state sessio di set di sini saja */
			
			
			/** CHAN_ROUTING */
			if (!strcmp($state, CHAN_ROUTING))
			{
				app_set_session_status($m, $channel, STATE_RESERVED);
				app_func_next($enum);
				
			} 
			/** CHAN_DIALING */
			else if (!strcmp($state, CHAN_DIALING)){
				
				app_set_session_status($m, $channel, STATE_BUSY);
				app_func_next($enum);
				
			} 
			/** CHAN_RINGING */
			else if (!strcmp($state, CHAN_RINGING)){
				
				app_set_session_status($m, $channel, STATE_BUSY);
				app_func_next($enum);
				
			} 
			/** CHAN_PICKUP */
			else if (!strcmp($state, CHAN_PICKUP)){
				
				app_set_session_status($m, $channel, STATE_BUSY);
				app_func_next($enum);
				
			} 
			/** CHAN_CONNECTED */
			else if (!strcmp($state, CHAN_CONNECTED)) {
				
				app_set_session_status($m, $channel, STATE_BUSY);
				app_func_next($enum);
				
			} 
			/** CHAN_DROPED */
			else if (!strcmp($state, CHAN_DROPED)) {
				
				app_set_session_status($m, $channel, STATE_READY);
				app_func_next($enum);
				
			} 
			/** CHAN_DISCARD */
			else if (!strcmp($state, CHAN_DISCARD)) {
				
				app_set_session_status($m, $channel, STATE_READY);
				app_func_next($enum);
			}
		}
	}
	
	return $enum;
 }
 
/*! \remark  app_set_session_status */
 function app_set_session_status(&$m= NULL, &$channel = NULL, $state = 0)
{
	/** status */
	app_func_copy($retval, false);
	app_func_copy($enum, 0);
	
	if ( app_user_session_find($channel, $retval) )
	{
		/** Attach Sent Syncronize to My server Client */
		if ($retval)
		{
			$m->AppUserSesion[$channel]['status'] = $state;  
			$m->AppUserSesion[$channel]['time'] = strtotime('now');
			app_func_next($enum);
		}
		
		/*! \note update session to this channel on other partner */
		if (function_exists('app_res_peer_triger_action_status')) 
		{
			if (app_user_session_find($channel, $session))
			{
				app_res_peer_triger_action_status($m, $session);
				app_func_free($session);
			}
		}
	}
	
	return $enum;
 } 