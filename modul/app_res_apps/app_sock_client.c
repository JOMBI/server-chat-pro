<?php 

/*! \remark app_sock_client_disconnect */
 function app_appsock_client_disconnect(&$m, &$s)
{
	if ( !sockTcpClose($s->sock) )
	{
		return $s->id; 
	}			
	return $s->id;
}

/*! \remark app_sock_client_deleted */
 function app_appsock_client_deleted(&$m, &$s)
{
	if (scpAppClientDeleted($m, $s))
	{
		return $s->id;
	}
	return $s->id;
}

/*! \remark app_sock_client_collected */
function app_appsock_client_collected(){}

function app_appsock_client_sighup(){}

