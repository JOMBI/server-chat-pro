<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * FLOW: onTrace->Meta->Action->Event 
 */

/*! \retval app_appsock_meta_quit */ 
 function app_appsock_meta_quit(&$m, &$s, &$meta)
{ 
	app_appsock_meta_action_quit($m, $s, $meta);
	return 0; 
}
 
/*! \retval app_appsock_meta_user */ 
 function app_appsock_meta_user(&$m, &$s, &$meta)
{
	
	if (!strcmp($meta->type, 'action') && $meta->action == 'register' )
	{
		$eval = new evaluate($meta->data);
		if ($eval)
		{
			app_appsock_meta_action_register($m, $s, $meta, $eval);
			app_func_free($eval);
		} 
	}
	
	if (!strcmp($meta->type, 'action') && $meta->action == 'unregister' )
	{
		$eval = new evaluate($meta->data);
		if($eval)
		{
			app_appsock_meta_action_unregister($m, $s, $meta, $eval);
			app_func_free($eval);
		}  
	} 
	
	return 0; 
}

/*! \retval app_appsock_meta_system */ 
 function app_appsock_meta_system(&$m, &$s, &$meta)
{
	return 0; 
}

/*! \retval app_appsock_meta_handler */ 
 function app_appsock_meta_handler(&$m, &$s, &$meta)
{
	if ( !strcmp($meta->type, 'action') && $meta->action == 'status' )
	{
		$eval = new evaluate($meta->data);
		if ($eval)
		{
			app_appsock_meta_action_status($m, $s, $meta, $eval);
			app_func_free($eval);
		}
	} 
	
	if ( !strcmp($meta->type, 'action') && $meta->action == 'ringing' )
	{
		$eval = new evaluate($meta->data);
		if ($eval) {
			app_appsock_meta_action_ringing($m, $s, $meta, $eval);
			app_func_free($eval);
		}
	} 
	
	if ( !strcmp($meta->type, 'action') && $meta->action == 'connect' )
	{
		$eval = new evaluate($meta->data);
		if ($eval)
		{
			app_appsock_meta_action_connect($m, $s, $meta, $eval);
			app_func_free($eval);
		}
	} 
	
	if ( !strcmp($meta->type, 'action') && $meta->action == 'discard' )
	{
		$eval = new evaluate($meta->data);
		if ($eval)
		{
			app_appsock_meta_action_discard($m, $s, $meta, $eval);
			app_func_free($eval);
		}
	} 
	
	return 0;  
}

/*! \retval app_appsock_meta_message */ 
 function app_appsock_meta_message(&$m, &$s, &$meta)
{
	if (!strcmp($meta->type, 'action') && $meta->action == 'recv' )
	{
		$eval = new evaluate($meta->data);
		if ($eval)
		{
			app_appsock_meta_action_receive($m, $s, $meta, $eval);
			return 0;
		}
	}
	
	if (!strcmp($meta->type, 'action') && $meta->action == 'sent' )
	{
		$eval = new evaluate($meta->data);
		if ($eval)
		{
			app_appsock_meta_action_sent($m, $s, $meta, $eval);
			return 0;
		}
	}
	
	/*! \remark even user typing on message */
	if (!strcmp($meta->type, 'action') && $meta->action == 'typing' )
	{
		
		$eval = new evaluate($meta->data);
		if ($eval)
		{
			app_appsock_meta_action_typing($m, $s, $meta, $eval);
			return 0;
		}
	}
	
	return 0;
}