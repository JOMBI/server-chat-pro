<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 * 
 * FLOW: onTrace->Meta->Action->Event 
 */

/*! \remark app_appsock_ontrace */
 function app_appsock_ontrace(&$m = NULL, &$s = NULL)
{
	app_func_copy($s->ret, 0);
	app_func_trim($s->buf);
	
	while ($s->id)
	{
		/*! \remark set default event */
		if (!strcmp($s->buf, 'quit')) 
		{
			if (app_func_copy($event, 'quit')) {
				app_func_event('app_appsock_meta_quit', array(&$m, &$s, $event), __FILE__, __LINE__ );  
			}
			
			app_func_next($s->ret);
			break;
		}	
		
		// echo $s->buf ."\n\n";
		
		/*! \remark if Encoder */
		app_func_copy($recv, false);
		if ( !onTraceAppsReceive($s->buf, $recv) ) 
		{
			app_func_next($s->ret);
			break;
		}
		
		 
		
		/*! \remark Next for Iteration process */
		app_func_copy($s->ptr, 0); 
		foreach ($recv as $j => $f )
		{
			if ( app_func_retval($f, $e) )
			{
				/*! \retval for 'PRIV_META_USER' */
				if ($e->meta && (!strcmp($e->meta, PRIV_META_USER)))
				{
					app_func_event('app_appsock_meta_user', array(&$m, &$s, &$e), 
					__FILE__, __LINE__ );  
					
					app_func_next($s->ptr);
				}
				
				/*! \retval for 'PRIV_META_SYST' */
				else if ($e->meta && (!strcmp($e->meta, PRIV_META_SYST)))
				{
					app_func_event('app_appsock_meta_system', array(&$m, &$s, &$e), 
					__FILE__, __LINE__ );  
					
					app_func_next($s->ptr);
				} 
				
				/*! \retval for 'PRIV_META_HAND' */
				else if ($e->meta && (!strcmp($e->meta, PRIV_META_HAND)))
				{
					app_func_event('app_appsock_meta_handler', array(&$m, &$s, &$e), 
					__FILE__, __LINE__ ); 
					
					app_func_next($s->ptr);
				} 
				
				/*! \retval for 'PRIV_META_MESG' */
				else if ($e->meta && (!strcmp($e->meta, PRIV_META_MESG)))
				{
					app_func_event('app_appsock_meta_message', array(&$m, &$s, &$e), 
					__FILE__, __LINE__ );  
					
					app_func_next($s->ptr);
				} 
			} 
			
			/*! \retval Clean All Data */
			app_func_free($e);
			app_func_free($f);
		} 
		
		app_func_copy($s->ret, $s->ret + $s->ptr);
		app_func_next($s->ret);
		
		break;
	}
	
	return $s->ret;
}
