<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 * 
 * FLOW: onTrace->Meta->Action->Event 
 */

/*! \remark app_appsock_meta_event_quit */
 function app_appsock_meta_event_quit(&$m, &$s, &$o= NULL) 
{
	app_sock_client_disconnect($m, $s);
	return 0;
}

/*! \remark app_appsock_meta_event_user */
 function app_appsock_meta_event_user(&$m= NULL, &$s = NULL, &$o= NULL)
{ 
   return 0;
}

/*! \remark app_appsock_meta_event_message */
 function app_appsock_meta_event_message(&$m= NULL, &$s = NULL, &$o= NULL)
{ 
   return 0;
}

/*! \remark app_appsock_meta_event_system */
 function app_appsock_meta_event_system(&$m= NULL, &$s = NULL, &$o= NULL)
{ 
   return 0;
}
