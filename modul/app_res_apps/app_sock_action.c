<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 * 
 * FLOW: onTrace->Meta->Action->Event 
 */
 
 
/*! \retval <app_appsock_meta_action_quit > */
 function app_appsock_meta_action_quit(&$m, &$s= NULL, &$meta=NULL, $eval = NULL)
{
	if ( app_sock_client_disconnect($m, $s) )
	{
		ap_func_free($meta);
	}
	return $s->id;
}

/*! \retval <app_appsock_meta_action_status > */
 function app_appsock_meta_action_status(&$m, &$s= NULL, &$meta=NULL, $eval = NULL)
{
	/*! \note create Box first */
	app_func_struct($o);
	app_func_copy($o->channel, $eval->get('channel'));
	app_func_copy($o->status , $eval->get('status')); 
	app_func_copy($o->state  , $eval->get('state'));
	
	/*! check session if still exist Update */
	app_func_copy($o->retval, 0);
	if ( app_user_session_find($o->channel, $retval) )
	{
		$m->AppUserSesion[$o->channel]['status'] = $o->status;  
		$m->AppUserSesion[$o->channel]['state']  = $o->state;  
		$m->AppUserSesion[$o->channel]['time']   = strtotime('now');
		app_func_next($o->retval);
	}  
	
	app_func_free($retval);
	return $o->retval;
}

/*! \retval <app_appsock_meta_action_register> */
 function app_appsock_meta_action_register(&$m, &$s= NULL, &$meta=NULL, $eval = NULL)
{
	
	if ( !$s->id ) {
		return 0;
	}
	
	/*! \remark created header for Fedback client */
	app_func_copy($meta->type 	,'event');
	app_func_copy($meta->event 	,'register'); 
	app_func_copy($meta->error 	,0);
	 
	/*! \remark for Utilize to User Session */ 
	app_func_struct($out);
	app_func_copy($out->uid		 , $eval->get('uid'));
	app_func_copy($out->channel	 , $eval->get('channel'));
	app_func_copy($out->domain	 , $eval->get('domain'));
	app_func_copy($out->session  , $eval->get('session'));
	app_func_copy($out->userid   , $eval->get('userid'));
	app_func_copy($out->name 	 , $eval->get('name'));
	app_func_copy($out->email	 , $eval->get('email'));
	app_func_copy($out->regtime	 , $eval->get('regts'));
	app_func_copy($out->status	 , $eval->get('status'));
	app_func_copy($out->group	 , $eval->get('group'));
	app_func_copy($out->appagent , $eval->get('appagent'));
	app_func_copy($out->state	 , $eval->get('state'));
	app_func_copy($out->pid   	 , $eval->get('pid'));
	app_func_copy($out->type 	 , $eval->get('type'));  
	app_func_copy($out->host 	 , $eval->get('host'));
	app_func_copy($out->port 	 , $eval->get('port')); 
	app_func_copy($out->time 	 , $eval->get('time'));
	app_func_copy($out->regts 	 , $eval->get('regts'));
	app_func_copy($out->alive 	 , $eval->get('alive'));  
	app_func_copy($out->sock 	 , $s->sock);
	app_func_copy($out->boxes 	 , array());
	
	
	/*! \remark untuk referensi socket di ambil dari client */   
	
	
	while ($out->uid)
	{
		/*! \remark cari dengan menggunakan ref channels / email sebagai UniqUid Key */ 
		app_func_copy($retval, 0);
		if ( !app_user_session_find($out->channel, $retval) )
		{
			
			app_user_session_set($out->channel, 'uid'	  , $out->uid); 
			app_user_session_set($out->channel, 'appagent', $out->appagent);
			app_user_session_set($out->channel, 'session' , $out->session);
			app_user_session_set($out->channel, 'channel' , $out->channel);
			app_user_session_set($out->channel, 'userid'  , $out->userid);
			app_user_session_set($out->channel, 'type'	  , $out->type);
			app_user_session_set($out->channel, 'domain'  , $out->domain);
			app_user_session_set($out->channel, 'group'   , $out->group);
			app_user_session_set($out->channel, 'name'    , $out->name);
			app_user_session_set($out->channel, 'email'   , $out->email);
			app_user_session_set($out->channel, 'status'  , $out->status); 
			app_user_session_set($out->channel, 'state'   , sprintf('%d', $out->state));  
			app_user_session_set($out->channel, 'pid'     , $out->pid);
			app_user_session_set($out->channel, 'sock'    , $out->sock);
			app_user_session_set($out->channel, 'host'    , $out->host);
			app_user_session_set($out->channel, 'port'    , $out->port);
			app_user_session_set($out->channel, 'boxes'   , $out->boxes);
			app_user_session_set($out->channel, 'time'    , $out->time);
			app_user_session_set($out->channel, 'regts'   , $out->regts);
			app_user_session_set($out->channel, 'alive'   , $out->alive); 
			break;
		}
		
		/*! jika data sudah ada sebelumnya */
		if( $retval->find('session') )
		{
			app_user_session_set($out->channel, 'group', $out->group);
		}
		 
	   /** 
	    * \remark 
		* o. Jika session sudah ada dan yang meregis berasal dari hook artinya ini perlu di Konfirmasi 
		*
	    * o. Check Periode of Syncronize session , 
	    * 	 Jika suatu sesion waktunya tidak syncron anatar yang di register oleh webclient dengan yang ada 
		* 	 pada server maka ini akan di anggap sebagai session yang kadaluwarsa / expired dan akan langsung di destroy saja 
		*/
		
		if ( $retval->detach('type', 'tcp_peer') )
		{
			app_user_session_set($out->channel, 'appagent', $out->appagent);
			app_user_session_set($out->channel, 'type'	  , $out->type);
			app_user_session_set($out->channel, 'status'  , $out->status); 
			app_user_session_set($out->channel, 'pid'     , $out->pid);
			app_user_session_set($out->channel, 'sock'    , $out->sock);
			app_user_session_set($out->channel, 'host'    , $out->host);
			app_user_session_set($out->channel, 'port'    , $out->port);
			app_user_session_set($out->channel, 'boxes'   , $out->boxes);
			app_user_session_set($out->channel, 'time'    , $out->time);
			app_user_session_set($out->channel, 'regts'   , $out->regts);
			app_user_session_set($out->channel, 'alive'   , $out->alive);
			
			/*! \remark then rollback session */
			app_user_session_roolback($out->channel, $retval);
		}
		
	   /*! 
		* \remark jika session Masih ada ketika Register Biasanya yang berubah hanya 
		* data dari referensi client saja yang lain akan tetap sama 
		*/
		
		app_func_copy($out->uid		 , $retval->get('uid'));
		app_func_copy($out->channel	 , $retval->get('channel'));
		app_func_copy($out->session  , $retval->get('session'));
		app_func_copy($out->userid   , $retval->get('userid'));
		app_func_copy($out->name 	 , $retval->get('name'));
		app_func_copy($out->email	 , $retval->get('email'));
		app_func_copy($out->regtime	 , $retval->get('regts'));
		
		/*! \remark update data client */
		app_user_session_set($out->channel, 'pid'  , $out->pid);
		app_user_session_set($out->channel, 'sock' , $out->sock);
		app_user_session_set($out->channel, 'host' , $out->host);
		app_user_session_set($out->channel, 'port' , $out->port);
		app_user_session_set($out->channel, 'boxes', $out->boxes);
		app_user_session_set($out->channel, 'time' , $out->time);
		 
		break;	
	} 
 
	/*! \retval to this method */
	app_func_free($out);
	return $s->id; 
	
	return $s->id;
}
 
/*! \retval <app_appsock_meta_action_unregister> */
 function app_appsock_meta_action_unregister(&$m, &$s= NULL, &$p=NULL, $eval = NULL)
{
	return $s->id;
}

/*! \remark <app_appsock_meta_action_ringing> get handler ringing */
 function app_appsock_meta_action_ringing(&$m, &$s= NULL, &$p=NULL, $eval = NULL)
{
	/*! \note create Box first */
	app_func_struct($o);
	app_func_copy($o->channel1 , $eval->get('channel1'));
	app_func_copy($o->channel2 , $eval->get('channel2')); 
	app_func_copy($o->chatboxid, $eval->get('chatboxid'));
	
	/*! \remark create Box Id If false will return == 0 */
	
	if (!app_create_box_session($m, $o, $o->chatboxid))
	{
		return 0;
	}
	
	/*! \remark create channel_session */
	app_func_copy($ret_chan1, false);
	app_set_channel_status($m, $o->channel1, CHAN_RINGING);
	app_set_channel_status($m, $o->channel2, CHAN_RINGING);
	
	if ( app_create_chan_session($m, $o->channel1, $o->chatboxid, $ret_chan1) ) 
	{	
		/*! \remark get client_session from this by channel state */
		if ( app_user_session_global($m, $o->channel2, $chan2, $g))
		{
			/*! \remark for body context */
			app_func_struct($meta);
			app_func_copy($meta->meta , PRIV_META_HAND);
			app_func_copy($meta->type , 'event');
			app_func_copy($meta->event, 'ringing'); // for channel2 
			app_func_copy($meta->error, 0);
			
			
			app_func_alloc($meta->data);
			app_func_copy($meta->data, 'direction', app_func_str($eval->get('direction')));
			app_func_copy($meta->data, 'chatboxid', app_func_str($eval->get('chatboxid')));
			app_func_copy($meta->data, 'callerid' , app_func_str($eval->get('callerid')));
			app_func_copy($meta->data, 'channel1' , app_func_str($eval->get('channel1')));
			app_func_copy($meta->data, 'channel2' , app_func_str($eval->get('channel2')));
			app_func_copy($meta->data, 'timeout'  , app_func_str($eval->get('timeout')));
			app_func_copy($meta->data, 'cid'  	  , app_func_str($eval->get('cid')));
			app_func_copy($meta->data, 'time'  	  , app_func_str($eval->get('time')));
			
			if ( app_websock_visitor_events_meta_user($g, $meta) )
			{
				app_set_channel_status($m, $o->channel1, CHAN_DIALING);
				app_set_channel_status($m, $o->channel2, CHAN_RINGING);
				app_func_free($meta); 
			}
		}
	}
	 
	return 0; 
}

/*! \remark <app_appsock_meta_action_connect > get handler ringing */
 function app_appsock_meta_action_connect(&$m, &$s= NULL, &$p=NULL, $eval = NULL)
{
	/*! \note create Box first */
	app_func_struct($o);
	app_func_copy($o->channel1 , $eval->get('channel1'));
	app_func_copy($o->channel2 , $eval->get('channel2')); 
	app_func_copy($o->chatboxid, $eval->get('chatboxid'));
	
	/*! \remark create Box Id If false will return == 0 */
	app_mgr_loger($s, sprintf("Channel '%s' Pickup Channel '%s' on BoxId= %s ", $o->channel2, $o->channel1, $o->chatboxid));
	if (app_confridge_box_session($m, $o->channel1, $o->channel2, $o->chatboxid) )
	{
		app_mgr_loger($s, sprintf("Got channel on Bridge BoxId = %s", $o->chatboxid));	
		if (app_create_chan_session($m, $o->channel2, $o->chatboxid, $chan2) ) 
		{
			/*! \remark kirim pesan ke channel Yang dial Kalau Pesan sudah di terima */
			if (app_user_session_global($m, $o->channel1, $chan1, $g)) 
			{
				app_func_struct($meta);
				app_func_copy($meta->meta , PRIV_META_HAND);
				app_func_copy($meta->type , 'event');
				app_func_copy($meta->event, 'connect'); // for channel2 
				app_func_copy($meta->error, 0);
				
				app_func_alloc($meta->data);
				app_func_copy($meta->data, 'direction', app_func_str($eval->get('direction')));
				app_func_copy($meta->data, 'chatboxid', app_func_str($eval->get('chatboxid')));
				app_func_copy($meta->data, 'callerid' , app_func_str($eval->get('callerid')));
				app_func_copy($meta->data, 'channel1' , app_func_str($eval->get('channel1')));
				app_func_copy($meta->data, 'channel2' , app_func_str($eval->get('channel2')));
				app_func_copy($meta->data, 'timeout'  , app_func_str($eval->get('timeout')));
				app_func_copy($meta->data, 'cid'  	  , app_func_str($eval->get('cid')));
				app_func_copy($meta->data, 'time'  	  , app_func_str($eval->get('time')));
				
				if ( app_websock_visitor_events_meta_user($g, $meta) )
				{
					app_set_channel_status($m, $o->channel1, CHAN_CONNECTED);
					app_set_channel_status($m, $o->channel2, CHAN_PICKUP);
					app_func_free($meta); 
				}	
			}
		}
	}  
	
	return 0; 
}

/*! \remark <app_appsock_meta_action_receive > get handler ringing */
 function app_appsock_meta_action_receive(&$m, &$s= NULL, &$p=NULL, $eval = NULL)
{
	/*! \note create Box first */
	app_func_struct($o);
	app_func_copy($o->cid 	, $eval->get('cid'));
	app_func_copy($o->chan 	, $eval->get('chan'));
	app_func_copy($o->chan1 , $eval->get('chan1'));
	app_func_copy($o->chan2 , $eval->get('chan2'));
	app_func_copy($o->msgbox, $eval->get('msgbox'));
	app_func_copy($o->msgid , $eval->get('msgid'));
	app_func_copy($o->from  , $eval->get('from'));
	app_func_copy($o->to 	, $eval->get('to'));
	app_func_copy($o->type 	, $eval->get('type'));
	app_func_copy($o->text 	, $eval->get('text'));
	app_func_copy($o->time 	, $eval->get('time'));
	app_func_copy($o->flag 	, $eval->get('flag'));
	
	if ( app_user_session_global($m, $o->chan, $chan, $g) ) 
	{
		app_func_struct($meta);
		app_func_copy($meta->meta  , PRIV_META_MESG);
		app_func_copy($meta->type  , 'event');
		app_func_copy($meta->event , 'recv');  
		app_func_copy($meta->error , 0);
		
		app_func_alloc($meta->data);
		app_func_copy($meta->data, 'cid'   , app_func_str($o->cid));
		app_func_copy($meta->data, 'msgbox', app_func_str($o->msgbox));
		app_func_copy($meta->data, 'msgid' , app_func_str($o->msgid));
		app_func_copy($meta->data, 'chan'  , app_func_str($o->chan));
		app_func_copy($meta->data, 'from'  , app_func_str($o->from));
		app_func_copy($meta->data, 'to'	   , app_func_str($o->to));
		app_func_copy($meta->data, 'type'  , app_func_str($o->type));
		app_func_copy($meta->data, 'text'  , app_func_str($o->text));
		app_func_copy($meta->data, 'time'  , app_func_str($o->time));
		app_func_copy($meta->data, 'flag'  , app_func_str($o->flag));
		
		if ( app_websock_visitor_events_meta_message($g, $meta) )  
		{
			app_func_free($meta->data); 
		} 
		
		/*! \remark add New Data To Loger session */
		app_func_copy($o->date, strtotime('now'));
		app_loger_box_session($m, $o->msgbox, 
			array(
				'cid'    => $o->cid,
				'msgid'  => $o->msgid, 
				'msgbox' => $o->msgbox,
				'chan1'  => $o->chan1,
				'chan2'  => $o->chan2,
				'from'   => $o->from ,	 
				'to'     => $o->to,  
				'type'   => $o->type,  
				'text'   => $o->text,  
				'time'   => $o->date 	
			) 
		);
	}
	
	return 0;
	
}

/*! \remark <app_appsock_meta_action_discard > get handler ringing */
function app_appsock_meta_action_discard(&$m, &$s= NULL, &$p=NULL, $eval = NULL)
{
	/*! \note define All Parameters */
	
	app_func_struct($o); 
	app_func_copy($o->channel1 	, $eval->get('channel1'));
	app_func_copy($o->channel2 	, $eval->get('channel2'));
	app_func_copy($o->chatboxid , $eval->get('chatboxid'));
	app_func_copy($o->direction , $eval->get('direction'));
	app_func_copy($o->discard 	, $eval->get('discard'));
	app_func_copy($o->callerid 	, $eval->get('callerid')); 
	app_func_copy($o->timeout  	, $eval->get('timeout'));  
	app_func_copy($o->time 		, $eval->get('time'));  
	app_func_copy($o->cid 		, $eval->get('cid')); 
	
	
	// masukan ke log untuk data analisys *
	app_mgr_loger($s, sprintf("Channel '%s' Discard ChatBoxId '%s'", $o->channel1, $o->chatboxid));
	if (app_find_box_session($m, $o->chatboxid, $room) ) 
	{
		// find channel on Room 
		foreach ($room as $i => $chan) 
		{
			// delete masing converstion di channel jika belum ada data 
			app_unreg_chan_session($m, $chan, false);
			
			// cari channel local saja , remote sudah discard 
			app_user_session_global($m, $chan, $u, $g);   
			if ( !$g->local )
			{
				// update status for Remote channel 
				app_set_channel_status($m, $chan, CHAN_DROPED); 
				
				// cleanup data 
				app_func_free($u); 
				app_func_free($g); 
				continue;
			}	
			
			// kirim pesan ke channel local  
			app_func_struct($meta);
			app_func_copy($meta->meta 	,PRIV_META_HAND);
			app_func_copy($meta->type 	,'event');
			app_func_copy($meta->event 	,'discard'); // for channel1 
			app_func_copy($meta->error 	,0); 
			
			// Buat body data untuk di kirim ke clinet ws *
			app_func_alloc($meta->data);
			app_func_copy($meta->data, 'direction' , app_func_str($o->direction));
			app_func_copy($meta->data, 'chatboxid' , app_func_str($o->chatboxid));
			app_func_copy($meta->data, 'callerid'  , app_func_str($o->callerid));
			app_func_copy($meta->data, 'channel1'  , app_func_str($o->channel1));
			app_func_copy($meta->data, 'channel2'  , app_func_str($o->channel2));
			app_func_copy($meta->data, 'discard'   , app_func_str($o->discard));
			app_func_copy($meta->data, 'timeout'   , app_func_str($o->timeout));
			app_func_copy($meta->data, 'cid'  	   , app_func_str($o->cid));
			app_func_copy($meta->data, 'time'  	   , app_func_str($o->time)); 
			
			if (app_websock_visitor_events_meta_user($g, $meta)) 
			{
				app_set_channel_status($m, $chan, CHAN_DROPED);
				app_func_free($meta->data);  
			} 
			
			// setiap kali process selesai di clean biar tidak tumpang tindih 
			app_func_free($g); 
			app_func_free($u); 
		}
		
		if (app_unregister_box_session($m, $o->chatboxid, $response))
		{
			app_mgr_loger($s, sprintf( "Unregister ChatBox Session = %s success", $o->chatboxid));
		}
	}
	return 0;
}

/*! \remark <app_appsock_meta_action_typing > get handler typing */ 
 function app_appsock_meta_action_typing(&$m, &$s= NULL, &$p=NULL, $eval = NULL)
{
	/*! \note define All Parameters */ 
	app_func_struct($o); 
	app_func_copy($o->chan1 	, $eval->get('chan1'));
	app_func_copy($o->chan2 	, $eval->get('chan2'));
	app_func_copy($o->msgbox 	, $eval->get('msgbox'));
	app_func_copy($o->from 		, $eval->get('from'));
	app_func_copy($o->to 		, $eval->get('to'));
	app_func_copy($o->type 		, $eval->get('type')); 
	app_func_copy($o->text  	, $eval->get('text'));  
	app_func_copy($o->flag 		, $eval->get('flag'));   
	 
	if (app_user_session_global($m, $o->chan2, $u, $g) ) 
	{
		app_func_struct($meta);
		app_func_copy($meta->meta  , PRIV_META_MESG);
		app_func_copy($meta->type  , 'event');
		app_func_copy($meta->event , 'typing');  
		app_func_copy($meta->error , 0);
		
		app_func_alloc($meta->data);
		app_func_copy($meta->data, 'msgbox', app_func_str($o->msgbox));
		app_func_copy($meta->data, 'chan1' , app_func_str($o->chan1));
		app_func_copy($meta->data, 'chan2' , app_func_str($o->chan2));
		app_func_copy($meta->data, 'from'  , app_func_str($o->from));
		app_func_copy($meta->data, 'to'	   , app_func_str($o->to));
		app_func_copy($meta->data, 'type'  , app_func_str($o->type));
		app_func_copy($meta->data, 'text'  , app_func_str($o->text)); 
		app_func_copy($meta->data, 'flag'  , app_func_str($o->flag));
		
		if ($g->local &&( app_websock_visitor_events_meta_message($g, $meta)))
		{
			app_func_free($meta);  
		} 
		
		app_func_free($u);  
		app_func_free($g);  
			
	}
	
	return 0;	
}