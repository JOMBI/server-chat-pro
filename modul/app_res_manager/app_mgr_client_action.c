<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
 if( !defined('RING_TIMEOUT') ) define('RING_TIMEOUT', 3);
 if( !defined('RING_INBOUND') ) define('RING_INBOUND', 1);
 if( !defined('RING_OUTBOUND') ) define( 'RING_OUTBOUND', 2);
 
/** 
 * \brief <app_mgr_client_triger_action_thread>
 *
 * \remark 
 * Process check data yang berasal dari Thread Socket client yang di lakukan berdasarkan 
 * periode waktu tertentu .
 *
 * untuk Process thread harus di check type 'client' 
 * oleh sebab itu data client type harus di kirim dari yang merequest 
 * yaitu 'ctype' visitor atau agent 
 *
 * \param  <s * client , o* object param >
 * \retval int 
 * 
 */
 function app_mgr_client_triger_action_thread(&$m, &$s, &$o)
{
	app_func_copy($s->retval, 0);
	if (!app_func_eval($o,'ctype'))
	{
		app_mgr_loger("Client Type Reuired for Thread");
		return 0;
	} 
	
	/** from visitor thread request */ 
	app_func_copy($o->error, 0);
	if ( $o->ctype == 'wss_vst' )
	{	
		/* evaluate data from session */
		app_func_copy($eval, false);
		
		if (app_user_session_eval('pid', $o->pid, $eval))
		{
			// jika pid sama dengan session */
			app_func_copy($o->uid	  , $eval->get('uid'));
			app_func_copy($o->appagent, $eval->get('appagent'));
			app_func_copy($o->session , $eval->get('session'));
			app_func_copy($o->channel , $eval->get('channel'));
			app_func_copy($o->userid  , $eval->get('userid'));
			app_func_copy($o->domain  , $eval->get('domain'));
			app_func_copy($o->group   , $eval->get('group'));
			app_func_copy($o->name	  , $eval->get('name'));
			app_func_copy($o->email	  , $eval->get('email'));
			app_func_copy($o->status  , $eval->get('status')); 
			app_func_copy($o->state   , $eval->get('state')); 
			app_func_copy($o->regtime , $eval->get('regts')); 
		}
		
		/*! \remark jika PID yang ada pada session user tidak benar */
		if ( !$eval ){
			app_func_copy($o->error, 1);
		}
		
		/*! \retval sent response to client on request */
		if ( app_mgr_client_triger_events_thread($m, $s, $o) ){
			app_func_next($s->retval);
		} 
		
		app_func_next($s->retval);	
	} 
	
	/** if success will data integer > 0 */
	return $s->retval;
}


/** 
 * \brief <app_mgr_client_triger_action_register>
 *
 * \remark 
 * process yang terjadi antara Hook client dengan socket Master , dimana user register  
 * via browser dengan post methode ke Hook, Karena Hook sifatnya statles maka perlu syncronize 
 * data / share data dengan Socket master , Ketika Process Ini Berhasil kemudian wensocket akan 
 * melakukan register via wensocket Untuk Konfirmasi Kebenaran dari Register Yang sebelumnya 
 * Maka data tersebut harus di kirim ke server di simpan sebagai User Session yang belum terkonfirmasi  
 *
 * \param  <s * client , o* object param >
 * \retval int 
 * 
 */
 function app_mgr_client_triger_action_register(&$m, &$s, &$o)
{
	app_func_copy($s->retval, 0);
	while ($o->session) 
	{
	    app_func_copy($o->status, 0);
		app_func_copy($o->error , 0);
		
		if (!app_func_eval($o,'ctype'))
		{
			app_mgr_loger("Client Type Reuired for register");
			app_func_next($o->error);
			break;
		}
		
	   /**
	    * o. Check apakah channel yang di minta sudah ada ?, Jika belum ada masukan di list session dengan status belum  
		* 	 terkonfirmasi / status = 0  
		*
		* o. Jika Channel tersebut sudah ada Pada session maka pastikan apakah session tersebut masih Aktive 
		* 	 atau bahkan sudah expired jika masih active maka skip process registrasi tersebut . 
		*/
		
		if (!app_user_session_find($o->channel, $retval)) 
		{
			app_func_next($s->retval);
			app_user_session_set($o->channel, 'uid'	 	, app_func_eval($o,'uid')); 
			app_user_session_set($o->channel, 'type'    , app_func_eval($o,'ctype'));
			app_user_session_set($o->channel, 'session' , app_func_eval($o,'session'));
			app_user_session_set($o->channel, 'channel' , app_func_eval($o,'channel'));
			app_user_session_set($o->channel, 'userid'  , app_func_eval($o,'userid'));
			app_user_session_set($o->channel, 'name'    , app_func_eval($o,'username'));
			app_user_session_set($o->channel, 'domain'  , app_func_eval($o,'domain'));
			app_user_session_set($o->channel, 'email'   , app_func_eval($o,'channel'));
			app_user_session_set($o->channel, 'regts'   , app_func_eval($o,'regts')); 
			app_user_session_set($o->channel, 'status'  , app_func_eval($o,'status'));
			
			/*! add New state Register */
			$m->CreateId = $m->CreateId + 1;
			break;
		}
		
		/*! \remark session still exist copy status data */
		if (app_user_session_find($o->channel, $retval)) 
		{
			app_func_next($s->retval);
			
			app_func_copy($o->uid, $retval->get('uid')); 
			app_func_copy($o->session, $retval->get('session'));
			app_func_copy($o->status, $retval->get('status'));
		}
		
		/*! \retval break and stop */
		break;
	}
	
	/*! \retval sent response to client on request */
	if (app_mgr_client_triger_events_register($m, $s, $o)){
		app_func_next($s->retval);
	} 
	
	app_func_next($s->retval);	
	return $s->retval;
}



/*! \retval app_mgr_client_triger_action_unregister <s * client , o* object param >*/
 function app_mgr_client_triger_action_unregister(&$m, &$s, &$o)
{
	app_func_struct($f);
	app_func_struct($p);
	
	/*! \remark default fallback process */
	app_func_copy($s->retval, 0);
	while ($o->session) 
	{
	    app_func_copy($o->status, 0);
		app_func_copy($o->error , 0);
		
		if (!app_func_eval($o,'ctype'))
		{
			app_mgr_loger($s, "Client Type Reuired for register");
			app_func_next($o->error);
			break;
		}
		
	    /* \remark set status before close */
		app_set_session_status($m, $o->channel, STATE_LOGOUT); 
		
		
	   /*! \remark Catatan Untuk Process Unregister 
		*
		* o. Pertama Kirim ke client untuk kirim notice jika process unregister berhasil 
		* o. Kedua kirim notic ke hook jika process sudah selesai 
		* o. Delete Session User Agar tidak nyangkut 
		*
		*/ 
		app_func_copy($f->drop, false);
		if (app_user_session_find($o->channel, $f->drop)) 
		{
			app_func_copy($f->pid, $f->drop->get('pid'));
			app_func_copy($f->channel, $f->drop->get('channel'));
			app_func_copy($f->session, $f->drop->get('session'));
			
			/*! set status before close */ 
			if (app_websock_global_client_bypid($m, $f->pid, $c) ) 
			{
				app_func_struct($meta);
				
				/*! \remark created header for Fedback client */
				app_func_copy($meta->meta 	,'100');
				app_func_copy($meta->type 	,'event');
				app_func_copy($meta->event 	,'unregister'); 
				app_func_copy($meta->error 	,0);
				
				/*! \remark body Meta */ 
				
				app_func_alloc($meta->data);
				app_func_copy($meta->data, 'uid'  	  , app_func_str($f->drop->get('uid')));
				app_func_copy($meta->data, 'session'  , app_func_str($f->drop->get('session')));
				app_func_copy($meta->data, 'channel'  , app_func_str($f->drop->get('channel')));
				app_func_copy($meta->data, 'domain'   , app_func_str($f->drop->get('domain')));
				app_func_copy($meta->data, 'userid'	  , app_func_str($f->drop->get('userid')));
				app_func_copy($meta->data, 'username' , app_func_str($f->drop->get('name')));
				app_func_copy($meta->data, 'useremail', app_func_str($f->drop->get('email')));
				app_func_copy($meta->data, 'regtime'  , app_func_str($f->drop->get('regts')));
				app_func_copy($meta->data, 'status'   , app_func_str($f->drop->get('status')));
				app_func_copy($meta->data, 'time'  	  , app_func_str($f->drop->get('time')));
				 
				if (app_websock_visitor_events_meta_user($c, $meta))
				{
					app_func_free($meta);
					app_func_next($s->retval);
				}
				
				/*! \remark after success sent data to ws then delete this session */
				if ($s->retval &&(app_user_session_delete($o->channel)))
				{
					app_func_next($s->retval);
					app_mgr_loger($s,sprintf( 'Delete Session= %s channel= %s', 
							$f->session, $f->channel 
						)
					);
				}
			} 
			
			/*! \remark caribox id berdasarkan channel */
			if ( app_eval_box_session($m, $f->channel, $box_cahnnels) )
			{
				foreach ($box_cahnnels as $i => $chatboxid ) 
				{
					app_func_copy($p->chatboxid, $chatboxid);
					$conf_channels = isset($m->AppBoxSesion[$p->chatboxid]) ? $m->AppBoxSesion[$p->chatboxid] : false;
					if ( !$conf_channels ) 
				    {
						continue;
					}
					
					/*! \remark jika datanya berisi array */
					app_func_copy($f->recv, false);
					 if (app_func_sizeof($conf_channels) > 1 )
					{
						$next_channels = array_diff($conf_channels, array($f->channel)); 
						
						/*! \remark app_user_session_global < m* Global, channel, u* user_sessin , c* client session */
						
						$next_keys = array_keys($next_channels); $next_channels = end($next_channels);   
						if ( app_user_session_global($m, $next_channels, $f->recv, $c) )
						{
							/*! \remark Untuk Dapat direction dari suatu channel */ 
							$p->next_dispostion = end( $next_keys );
							if ($p->next_dispostion == 0){
								app_func_copy($p->direction, RING_OUTBOUND);
							}
							
							if ($p->next_dispostion == 1){
								app_func_copy($p->direction, RING_INBOUND);
							}
							
							
							app_func_copy($p->callerid , $f->drop->get('name')); 	// yang melakukan Drop 
							app_func_copy($p->channel1 , $f->drop->get('channel')); // yang melakukan Drop 
							app_func_copy($p->channel2 , $f->recv->get('channel')); // yang menerima  Drop 
							app_func_copy($p->timeout  , RING_TIMEOUT); 			// Timeout process 
							app_func_copy($p->time     , strtotime('now')); 		// NULL
							app_func_copy($p->cid  	   , strtotime('now')); 		// NULL  


							
						   /*! 
							* \remark kirim data <channel->drop> meta ke user client Untuk confirmasi kalau saya meninggalkan 
							* process peracakapan dengan channel tersebut atau kirim signa jika saya keluar 
							*
							*/ 
							
							app_func_copy($meta->meta , '300');
							app_func_copy($meta->type , 'event');
							app_func_copy($meta->event, 'bye'); 
							app_func_copy($meta->error, 0);
							
							/*! \remark for Body context */
							app_func_alloc($meta->data); 
							app_func_copy($meta->data , 'direction', app_func_str($p->direction));
							app_func_copy($meta->data , 'chatboxid', app_func_str($p->chatboxid));
							app_func_copy($meta->data , 'callerid' , app_func_str($p->callerid));
							app_func_copy($meta->data , 'channel1' , app_func_str($p->channel1));
							app_func_copy($meta->data , 'channel2' , app_func_str($p->channel2));
							app_func_copy($meta->data , 'timeout'  , app_func_str($p->timeout));
							app_func_copy($meta->data , 'cid'  	   , app_func_str($p->cid));
							app_func_copy($meta->data , 'time'     , app_func_str($p->time));
							
							if (app_websock_visitor_events_meta_user($c, $meta))
							{
								app_func_free($meta);
								app_func_next($s->retval);
							}

							// for channel 'CHAN_DROPED'
							app_set_channel_status($m, $p->channel1, CHAN_DROPED); 
							app_set_channel_status($m, $p->channel2, CHAN_DROPED); 
						}
					}
				}
			}
			
			/*! \remark export hasil percakapan di channel ini */
			if( app_export_chan_conversation($m, $f->channel) )
			{
				app_mgr_loger($s, sprintf( 'Export Conversation from channel= %s', $f->channel ));
				
				/*! \remark delete for conversation */
				if ( app_delete_chan_session($m, $f->channel, $enum) )
				{
					app_mgr_loger($s, sprintf( "Delete Conversation ChatBox(n) = %d ", $enum));
				}
				
				/*! \remark delete channel confbridge */
				app_func_free($enum);
				if ( app_delete_box_session($m, $f->channel, $enum) )
				{
					app_mgr_loger($s, sprintf( "Delete ChatBox Session (n) = %d ", $enum));
				}
			}
		}
		
		/*! break and stop */
		break;
	}
	 
	/*! \retval sent response to client on request */
	if ( app_mgr_client_triger_events_unregister($m, $s, $o) )
	{
		app_func_next($s->retval);
	} 
	
	app_func_next($s->retval);	
	return $s->retval;
}

/*! 
 * \brief app_mgr_client_triger_action_routing <s * client , o* object param >
 *
 * \remark  
 * Untuk Process Routing Atau Dial dari channel 1 ke channel 2 Tujuannya Untuk confirm ke channel tujuan, 
 * sebelum benar -benar terjadi percakpan jika dalam waktu tertentu tidak ada jawaban atau di anggap timeout 
 * maka akan di anggap chat berakhir atau drop process 
 *
 * \param  	 <m* master, s * client , o* object param >
 *
 * \retval 	integer 
 *
 */ 
 function app_mgr_client_triger_action_routing(&$m, &$s, &$o)
{
	app_func_struct($f);
	app_func_copy($s->retval, 0);
	while (!$s->retval) 
	{
		app_func_copy($o->route_type, 0);
		app_func_copy($o->status	, 0);
		app_func_copy($o->error		, 0);
		app_func_copy($o->valid		, 0);
		app_func_copy($o->chatboxid	, 0);
		app_func_copy($o->callerid	, 0);
		app_func_copy($o->cid		, strtotime('now'));	
		app_func_copy($o->time		, date('H:i', $o->cid));	
		app_func_copy($o->timeout	, RING_TIMEOUT); // second php 10 detik,  JS = 1 *1000 detik , 
		
		if ( !app_func_eval($o,'ctype') )
		{
			app_mgr_loger($s, "Client Type Required for session");
			app_func_next($o->error);
			break;
		}
		
	   /*! remark 
		* o. Chat Antar server yang mana agent hanya connect ke Local Server  
		* o. Chat Local Artinya antara visitor dan Agent Berada pada server yang sama 
		*
		* \note 
		* Untuk Chat antar server dimana visitor dan agent tidak Berada dalam server yang sama maka 
		* process dan perlakuannya akan di Buat Beda .
		*
		* o. Pertama Buat Route dulu ke Server Yang di maksud berdasarkan domain
		* o. setelah success maka baru buat processnya 
		* 	
		*/
		
		app_user_session_type($o->channel2, $o->route_type);
		
		/* \remark Set Channel Status */
		app_set_channel_status($m, $o->channel1, CHAN_ROUTING);
		app_set_channel_status($m, $o->channel2, CHAN_ROUTING);
		
		/*! \remark create BoxId */
		app_mgr_loger($s, sprintf("Starting Dialing from channel '%s' to channel '%s'",  $o->channel1, $o->channel2 ));
		if ( app_confridge_box_session($m, $o->channel1, $o->channel2, $o->chatboxid) )
		{
			app_set_channel_status($m, $o->channel1, CHAN_CONNECTED);
			app_set_channel_status($m, $o->channel2, CHAN_PICKUP);
			app_mgr_loger($s, sprintf("Channel on Bridge BoxId = %s", $o->chatboxid));
			app_func_next($o->error);
			break;	
		}
		
		/*! \remark Buat Box Channel Session Untuk Conversation Anatara Channel1 & channel2  */ 
		if ( app_create_box_session($m, $o, $o->chatboxid) )
		{
			if (app_create_chan_session($m, $o->channel1, $o->chatboxid, $chan1))
			{
				
				/*! 
				* \remark 
				* Kirim Pesan ke Yang Melakukan Iniated sebagai On Dialing 
				* Update Status menjadi On dial Bukan Dialing(2)  
				*/
				
				app_func_copy($o->pid, $chan1->get('pid'));  
				app_func_copy($o->callerid1, $chan1->get('name'));
				app_func_copy($o->direction1, RING_OUTBOUND);
				
				if (app_user_session_global($m, $o->channel2, $chan2, $s2))
				{
					/*! \remark for this data then */
					app_func_copy($o->direction2, RING_INBOUND);	
					app_func_copy($o->callerid2, $chan2->get('name'));
					
					/*! \remark for body context */
					app_func_struct($meta);
					app_func_copy($meta->meta , '300');
					app_func_copy($meta->type , 'event');
					app_func_copy($meta->event, 'ringing'); // for channel2 
					app_func_copy($meta->error, 0);
					
					app_func_alloc($meta->data);
					app_func_copy($meta->data, 'direction', app_func_str($o->direction2));
					app_func_copy($meta->data, 'chatboxid', app_func_str($o->chatboxid));
					app_func_copy($meta->data, 'callerid' , app_func_str($o->callerid1));
					app_func_copy($meta->data, 'channel1' , app_func_str($o->channel1));
					app_func_copy($meta->data, 'channel2' , app_func_str($o->channel2));
					app_func_copy($meta->data, 'timeout'  , app_func_str($o->timeout));
					app_func_copy($meta->data, 'cid'  	  , app_func_str($o->cid));
					app_func_copy($meta->data, 'time'  	  , app_func_str($o->time));
					 
					// \remark sent notice to channel2 If Local Server
					if ($s2->local){
						if (app_websock_visitor_events_meta_user($s2, $meta))
						{
							app_func_free($meta); 
							app_func_next($o->valid);
						}
						
					} else if (!$s2->local){
						if (app_res_peer_triger_action_ringing($m, $chan2, $meta))
						{
							app_func_free($meta); 
							app_func_next($o->valid);
						}
					} 
					
					// for channel 'CHAN_RINGING'
					app_set_channel_status($m, $o->channel2, CHAN_RINGING); 
				}
				
				/*! \remark then sent message to channel1 is dial */
				while ($o->valid &&(app_websock_global_client_bypid($m, $o->pid, $c1)))
				{
					app_func_struct($meta);
					app_func_copy($meta->meta , '300');
					app_func_copy($meta->type , 'event');
					app_func_copy($meta->event, 'dialing'); // for channel1 
					app_func_copy($meta->error, 0);
							
					app_func_alloc($meta->data);
					app_func_copy($meta->data, 'direction', $o->direction1);
					app_func_copy($meta->data, 'chatboxid', $o->chatboxid);
					app_func_copy($meta->data, 'callerid' , $o->callerid2);
					app_func_copy($meta->data, 'channel1' , $o->channel1);
					app_func_copy($meta->data, 'channel2' , $o->channel2);
					app_func_copy($meta->data, 'timeout'  , $o->timeout);
					app_func_copy($meta->data, 'cid'  	  , $o->cid);
					app_func_copy($meta->data, 'time'  	  , $o->time);
					
					if ( app_websock_visitor_events_meta_user($c1, $meta) ) 
					{
						app_func_free($meta); 
						app_func_next($o->valid);
					} 
					
					// for channel 'CHAN_DIALING'
					app_set_channel_status($m, $o->channel2, CHAN_DIALING);
					break;
				}	
			} 
		}
		
		app_func_next($s->retval); 
		
		/* \renmark break and stop */
		break;
	}
	
	/*! \retval sent response to client on request */
	if ( app_mgr_client_triger_events_routing($m, $s, $o) )
	{
		app_func_next($s->retval);
	} 
	
	app_func_next($s->retval);	
	return $s->retval;
}


/*! 
 * \brief app_mgr_client_triger_action_pickup <s * client , o* object param >
 *
 * \remark  
 * Pickup Konfirmasi Yang di lakukan oleh yang menerima Invite jika Invite di Terima Maka Kirim Ke channel 1 
 * Menjadi Connected dan channel 2 Menjadi Up Jika Process ini selesai Masing2 channel baru dapat melakukan 
 * conversation satu dengan yang lainnya .
 * 
 * Karena Bisa saja Ketika di INVITE , Namun Yang di INVITE Menolak Atau Reject , Bukan Acepted  
 * Dan Ini Bisa membuat Channel Menjadi Drop . 
 *
 * \param  	 <m* master, s * client , o* object param >
 *
 * \retval 	integer 
 *
 */  
 function app_mgr_client_triger_action_pickup(&$m, &$s, &$o)
{
	app_func_struct($f);
	app_func_copy($s->retval, 0);
	
	while (!$s->retval) 
	{
		app_func_copy($o->status 	, 0);
		app_func_copy($o->error 	, 0);
		app_func_copy($o->valid 	, 0); 
		app_func_copy($o->callerid 	, 0);
		app_func_copy($o->cid 		, strtotime('now'));	
		app_func_copy($o->time 		, date('H:i', $o->cid));	
		app_func_copy($o->timeout	, RING_TIMEOUT); // second php 10 detik,  JS = 1 *1000 detik ,
		
		
		if ( !app_func_eval($o,'ctype') )
		{
			app_mgr_loger($s, "Client Type Required for pickup session");
			app_func_next($o->error);
			break;
		}
		 
		app_mgr_loger($s, sprintf("Channel '%s' Pickup Channel '%s' on BoxId= %s ", $o->channel2, $o->channel1, $o->chatboxid));
		if (app_confridge_box_session($m, $o->channel1, $o->channel2, $o->chatboxid) )
		{
			app_mgr_loger($s, sprintf("Got channel on Bridge BoxId = %s", $o->chatboxid));
			
			/*! \remark create box session Untuk channel2 atau penerima Menandai Cahnnel 2 Bersedia Untuk 
				conversation 
			*/
			if ( app_create_chan_session($m, $o->channel2, $o->chatboxid, $chan2) )
			{
				app_func_copy($f->pid2		, $chan2->get('pid'));	
				app_func_copy($o->callerid2	, $chan2->get('name')); 
				
				/*! \remark Kirim Pesan ke channel1 bahwa Invite di terima */
				if ( app_user_session_global($m, $o->channel1, $chan1, $s1) )
			    {
					app_func_copy($o->callerid1, $chan1->get('name'));
					app_func_copy($o->direction1, 2);
					
					
					/*! \remark build Meta data */
					app_func_struct($meta);
					app_func_copy($meta->meta 	,'300');
					app_func_copy($meta->type 	,'event');
					app_func_copy($meta->event 	,'connect'); // for channel1 
					app_func_copy($meta->error 	,0);
					
					app_func_alloc($meta->data);
					app_func_copy($meta->data, 'direction' , app_func_str($o->direction1));
					app_func_copy($meta->data, 'chatboxid' , app_func_str($o->chatboxid));
					app_func_copy($meta->data, 'callerid'  , app_func_str($o->callerid2));
					app_func_copy($meta->data, 'channel1'  , app_func_str($o->channel1));
					app_func_copy($meta->data, 'channel2'  , app_func_str($o->channel2));
					app_func_copy($meta->data, 'timeout'   , app_func_str($o->timeout));
					app_func_copy($meta->data, 'cid'  	   , app_func_str($o->cid));
					app_func_copy($meta->data, 'time'  	   , app_func_str($o->time));
					
					if ($s1->local){
						if (app_websock_visitor_events_meta_user($s1, $meta) )
						{
							app_func_free($meta); 
							app_func_next($o->valid);
						}
						
					} else if (!$s1->local){
						if (app_res_peer_triger_action_connect($m, $chan2, $meta))
						{
							app_func_free($meta); 
							app_func_next($o->valid);
						}
					}
					// for channel 'CHAN_CONNECTED'
					app_set_channel_status($m, $o->channel1, CHAN_CONNECTED);
				}
				
				/*! \remark Kirim Pesan ke channel2 bahwa Invite sudah di terima */
				if ($o->valid &&( app_websock_global_client_bypid($m, $f->pid2, $c2))) 
				{
					app_func_struct($meta);
					app_func_copy($meta->meta  ,'300');
					app_func_copy($meta->type  ,'event');
					app_func_copy($meta->event ,'up'); // for channel1 
					app_func_copy($meta->error ,0);
						
					app_func_alloc($meta->data);
					app_func_copy($meta->data, 'direction', app_func_str($o->direction));
					app_func_copy($meta->data, 'chatboxid', app_func_str($o->chatboxid));
					app_func_copy($meta->data, 'callerid' , app_func_str($o->callerid1));
					app_func_copy($meta->data, 'channel1' , app_func_str($o->channel1));
					app_func_copy($meta->data, 'channel2' , app_func_str($o->channel2));
					app_func_copy($meta->data, 'timeout'  , app_func_str($o->timeout));
					app_func_copy($meta->data, 'cid'  	  , app_func_str($o->cid));
					app_func_copy($meta->data, 'time'  	  , app_func_str($o->time));
					
					// next Recon If <wss_agt | wss_vst> is diffrent 
					if (app_websock_visitor_events_meta_user($c2, $meta)) 
					{
						app_func_free($meta); 
						app_func_next($o->valid);
					}
					// for channel 'CHAN_PICKUP'
					app_set_channel_status($m, $o->channel2, CHAN_PICKUP);
				}
				
			}
			
			/*! \remark Break and stop */
			break;	
		}
	}
	
	/*! \retval sent response to client on request */
	if ( app_mgr_client_triger_events_pickup($m, $s, $o) )
	{
		app_func_next($s->retval);
	} 
	
	app_func_next($s->retval);	
	return $s->retval;
}

/** 
 * \brief 	app_mgr_client_triger_action_discard 
 *
 * \remark 	discard / reject yaitu ketika salah satu channel mendapat Invite / dial 
 * kemudian meresponse dengan menolak / membatalkan ajakan / mengajak berkomunikasi 
 * dengan demikian Box channel akan di destroy dari semua channel dengan Asumsi belum terjadi percakapan 
 * atau process komunikasi 
 *
 * \param 	<m* > Global Parameter 
 * \param 	<s* > Client Session 
 * \param 	<o* > Object Output / Reuqest Parameter  
 *
 * \retval 	mixed
 *
 */
 
 function app_mgr_client_triger_action_discard(&$m, &$s, &$o)
{
	app_func_struct($f);
	app_func_copy($s->retval, 0);
	
	while (!$s->retval) 
	{
		app_func_copy($o->status 	, 0);
		app_func_copy($o->error 	, 0);
		app_func_copy($o->valid 	, 0); 
		app_func_copy($o->callerid 	, 0);
		app_func_copy($o->dispos 	, 0);
		app_func_copy($o->cid 		, strtotime('now'));	
		app_func_copy($o->time 		, date('H:i', $o->cid));	
		app_func_copy($o->timeout	, RING_TIMEOUT); // second php 10 detik,  JS = 1 *1000 detik ,
		
		/*! \remark must valid request */
		if (!app_func_eval($o,'ctype'))
		{
			app_mgr_loger($s, "Client Type Required for pickup session");
			app_func_next($o->error);
			break;
		}
		
		 
		app_mgr_loger($s, sprintf("Channel '%s' Discard ChatBoxId '%s'", $o->channel, $o->chatboxid));
		if ( app_find_box_session($m, $o->chatboxid, $room))
		{
		   /*! \remark Jika box terisi hanya satu channel saja artiya channel lawan sudah pergi 
			* meninggalkan room / chatbox 
			* 
			*/
			
			if (app_func_sizeof($room)< 2)
			{
				app_func_free($f->client);	 
				app_func_free($f->user); 
				
				foreach ($room as $i => $chan)
				{
					if ( !strcmp($o->channel, $chan) )
					{
						// for get direction channel
						app_func_copy($o->iniator, $chan);
						app_func_copy($o->discard, 1);
						
						if (app_direction_box_session($m, $o->chatboxid, $o->iniator, $o->direction))
						{
							if (!strcmp($o->direction, RING_INBOUND)) {
								app_func_copy($o->channel2, $room[0]);
							} else if (!strcmp($o->direction, RING_OUTBOUND)) {
								app_func_copy($o->channel2, $room[1]);
							} else {
								app_func_copy($o->channel2, $o->channel);
							}
						}
						
						// for get callerid channel
						if (app_user_session_find($o->channel2, $e)) 
						{
							app_func_copy($o->callerid, $e->get('name'));
							app_func_free($e); 
						}
						
						if (app_user_session_global($m, $o->iniator, $f->user, $f->client) ) 
						{
							/*! \remark build Meta data */
							app_func_struct($meta);
							app_func_copy($meta->meta 	,'300');
							app_func_copy($meta->type 	,'event');
							app_func_copy($meta->event 	,'discard'); // for channel1 
							app_func_copy($meta->error 	,0);
							
							app_func_alloc($meta->data);
							app_func_copy($meta->data, 'direction' , app_func_str($o->direction));
							app_func_copy($meta->data, 'chatboxid' , app_func_str($o->chatboxid));
							app_func_copy($meta->data, 'callerid'  , app_func_str($o->callerid));
							app_func_copy($meta->data, 'channel1'  , app_func_str($o->iniator));
							app_func_copy($meta->data, 'channel2'  , app_func_str($o->channel2));
							app_func_copy($meta->data, 'discard'   , app_func_str($o->discard));
							app_func_copy($meta->data, 'timeout'   , app_func_str($o->timeout));
							app_func_copy($meta->data, 'cid'  	   , app_func_str($o->cid));
							app_func_copy($meta->data, 'time'  	   , app_func_str($o->time));
							 
							 
							if (app_websock_visitor_events_meta_user($f->client, $meta)) 
							{
								app_func_free($meta); 
								app_func_next($o->valid);
							}
							
							/*! \remark unreg data session */
							app_unreg_chan_session($m, $o->iniator, false);
							
							/*! \remark for channel 'CHAN_DROPED' */
							app_set_channel_status($m, $o->iniator, CHAN_DROPED);  
						} 
					} 
				} 
				
				/*! \unregister chat room from this session */
				if ( app_unregister_box_session($m, $o->chatboxid, $response) ) {
					app_mgr_loger($s, sprintf( "Unregister ChatBox Session = %s success", $o->chatboxid));
				}
				
				break;	
			}
			
			
			/*! jika kedua channel masih active */
			foreach ($room as $i => $chan)
			{
				app_func_free($f->client);	 
				app_func_free($f->user); 
				 
				/*! \note cari siapa yang duluan melakukan discard */
				if ( !strcmp($o->channel, $chan) )
				{
					// for get direction channel
					app_func_copy($o->iniator, $chan);
					app_func_copy($o->discard, 1);
					
					if (app_direction_box_session($m, $o->chatboxid, $o->iniator, $o->direction))
					{
						if (!strcmp($o->direction, RING_INBOUND)) {
							app_func_copy($o->channel2, $room[0]);
						} else if (!strcmp($o->direction, RING_OUTBOUND)) {
							app_func_copy($o->channel2, $room[1]);
						} else {
							app_func_copy($o->channel2, $o->channel);
						}
					}
					
					// for get callerid channel
					if (app_user_session_find($o->channel2, $e)) {
						app_func_copy($o->callerid, $e->get('name'));
						app_func_free($e); 
					}
					
					if ( app_user_session_global($m, $o->iniator, $f->user, $f->client) ) 
					{
						/*! \remark build Meta data */
						app_func_struct($meta);
						app_func_copy($meta->meta 	,'300');
						app_func_copy($meta->type 	,'event');
						app_func_copy($meta->event 	,'discard'); // for channel1 
						app_func_copy($meta->error 	,0);
						
						app_func_alloc($meta->data);
						app_func_copy($meta->data, 'direction' , app_func_str($o->direction));
						app_func_copy($meta->data, 'chatboxid' , app_func_str($o->chatboxid));
						app_func_copy($meta->data, 'callerid'  , app_func_str($o->callerid));
						app_func_copy($meta->data, 'channel1'  , app_func_str($o->iniator));
						app_func_copy($meta->data, 'channel2'  , app_func_str($o->channel2));
						app_func_copy($meta->data, 'discard'   , app_func_str($o->discard));
						app_func_copy($meta->data, 'timeout'   , app_func_str($o->timeout));
						app_func_copy($meta->data, 'cid'  	   , app_func_str($o->cid));
						app_func_copy($meta->data, 'time'  	   , app_func_str($o->time));
						 
						 
						if (app_websock_visitor_events_meta_user($f->client, $meta)) 
						{
							app_func_free($meta); 
							app_func_next($o->valid);
						}
						
						/*! \remark unreg data session */
						app_unreg_chan_session($m, $o->iniator, false);
						
						/*! \remark for channel 'CHAN_DROPED' */
						app_set_channel_status($m, $o->iniator, CHAN_DROPED); 
							
					}
					
					app_func_next($s->retval);  
					continue;
				}
			   
			   /*! \remark kirim pesan ke lawan bicara jika session ini di tolak dan destroy session chat box 
				*  di sisi client, Kemudian update state dan status di sisi session  		
				*/
				
				if ( strcmp($o->channel, $chan) )
				{
					// for get direction channel
					app_func_copy($o->partner, $chan);
					app_func_copy($o->discard, 2);
					if (app_direction_box_session($m, $o->chatboxid, $o->partner, $o->direction))
					{
						if (!strcmp($o->direction, RING_INBOUND)) {
							app_func_copy($o->channel2, $room[0]);
						} else if (!strcmp($o->direction, RING_OUTBOUND)) {
							app_func_copy($o->channel2, $room[1]);
						} else {
							app_func_copy($o->channel2, $o->partner);
						}
					}
					
					// for get callerid channel
					if (app_user_session_find($o->channel2, $e)) {
						app_func_copy($o->callerid, $e->get('name'));
						app_func_free($e); 
					}
					
					if ( app_user_session_global($m, $o->partner, $f->user, $f->client) ) 
					{
						
						/*! \remark build Meta data */
						app_func_struct($meta);
						app_func_copy($meta->meta 	,'300');
						app_func_copy($meta->type 	,'event');
						app_func_copy($meta->event 	,'discard'); // for channel1 
						app_func_copy($meta->error 	,0);
						
						app_func_alloc($meta->data);
						app_func_copy($meta->data, 'direction' , $o->direction);
						app_func_copy($meta->data, 'chatboxid' , $o->chatboxid);
						app_func_copy($meta->data, 'callerid'  , $o->callerid);
						app_func_copy($meta->data, 'channel1'  , $o->partner);
						app_func_copy($meta->data, 'channel2'  , $o->channel2);
						app_func_copy($meta->data, 'discard'   , $o->discard);
						app_func_copy($meta->data, 'timeout'   , $o->timeout);
						app_func_copy($meta->data, 'cid'  	   , $o->cid);
						app_func_copy($meta->data, 'time'  	   , $o->time);
						
						
						if ($f->client->local){
							if (app_websock_visitor_events_meta_user($f->client, $meta)) 
							{
								app_func_free($meta); 
								app_func_next($o->valid);
							} 
							
						} else if (!$f->client->local){
							if (app_res_peer_triger_action_discard($m, $f->user, $meta)) 
							{
								app_func_free($meta); 
								app_func_next($o->valid);
							} 
						}
						
						/*! \remark unreg data session */
						app_unreg_chan_session($m, $o->partner, false);
						
						/*! \remark for channel 'CHAN_DROPED' */
						app_set_channel_status($m, $o->partner, CHAN_DROPED); 
						
					}
					
					app_func_next($s->retval);
					continue;
				}
			} 
		}
		
		/*! \unregister chat room from this session */
		if ( app_unregister_box_session($m, $o->chatboxid, $response) ) {
			app_mgr_loger($s, sprintf( "Unregister ChatBox Session = %s success", $o->chatboxid));
		}
		
		break;
	}	
	
	
	/*! \retval sent response to client on request */
	if ( app_mgr_client_triger_events_pickup($m, $s, $o) )
	{
		app_func_free($o);
		app_func_next($s->retval);
	} 
	
	app_func_next($s->retval);	
	return $s->retval;
}