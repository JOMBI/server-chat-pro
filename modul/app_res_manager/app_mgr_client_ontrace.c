<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * 
 format data 
 
	action {
		msg    : mgr|cli,
		type   : action 
		action : thread 
		data   : {
			// customize 	
			
		} 
	}
	
 */

/*! app_mgr_client_ontrace <m* master, s* client , buf * stream > */
 function app_mgr_client_ontrace(&$m = NULL, &$s = NULL, &$buf= NULL)
{
	/*! \remark decrypt data from client */
	app_func_decrypt($buf);
	
   /*! \remark untuk Udp dari manager akan ada dua session 
	* 1. Receive Message yang berasal dari Client Internal 
	* 2. Receive Message yang berasal dari CLI / console base 
	*	
	*/ 
	app_func_struct($out);
	app_func_copy($out->error, 0);
	app_func_copy($out->write, "");
	while (true)
	{
		app_func_copy($o, NULL);
		app_func_copy($eval, NULL);
		
		/*! parsing receive message for trace to object .events */
		
		if ( !onTraceUdpReceive($buf, $o) )
		{
			app_func_output($out->write, "Invalid Message(1)"); 
			app_func_next($out->error);
			break;
		}
		
		/*! \type of mode Internal */ 
		if ( app_func_eval($o,'msg') && !strcmp($o->msg, 'mgr') )
		{
			app_mgr_client_triger_socket($m, $s, $o);
			break;
		} 
		
		/*! \type of mode CLI */
		if ( app_func_eval($o,'msg') && !strcmp($o->msg, 'cli') )
		{	
			app_mgr_client_triger_console($m, $s, $o);
			break;
		}
		
		
		/*! \type empty data process */ 
		app_func_output($out->write, "Invalid Message(2)");  
		app_func_next($out->error);
		
		
		/*! \remark stop process */
		break;
	}
	
	/*! \retval jika Error maka tampilkan data */
	if ( $out->error )
	{
		if (app_func_enter($out->write)) 
		{
			if (sockUdpWrite($s->sock, $out->write, $s->host, $s->port)) {
				app_func_free($out);  
			}
		}
	}
	
	/*! \retval finaly return port */
	return $s->port;
} 
 