<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 *  
 */ 
 
 
/*! \brief app_mgr_client_triger_socket <s* client , o* object param > */
 function app_mgr_client_triger_socket(&$m, &$s, &$o)
{
	/*! \retval Action save first register sesesion from webhook */
	if ( !strcmp($o->action, 'register') ){
		app_mgr_client_triger_action_register($m,$s, $o);
	}
	
	/*! \retval Action request unregister sesesion from webhook */
	if (!strcmp($o->action, 'unregister')){
		app_mgr_client_triger_action_unregister($m,$s, $o);
	}
	
	/*! \retval action on request data from thread websocket */
	if ( !strcmp($o->action, 'thread') ){
		app_mgr_client_triger_action_thread($m,$s, $o);
	}
	 
	/*! \retval  client from public  reuest chat to domain of vendor */
	if ( !strcmp($o->action, 'routing') ){
		app_mgr_client_triger_action_routing($m,$s, $o);
	}
	
	/*! \retval  client from public  reuest chat to domain of vendor */
	if ( !strcmp($o->action, 'pickup') ){
		app_mgr_client_triger_action_pickup($m,$s, $o);
	}
	
	/*! \retval  client from public  reuest chat to domain of vendor */
	if ( !strcmp($o->action, 'discard') ){
		app_mgr_client_triger_action_discard($m,$s, $o);
	}
	
	return 0;
} 


/*! \brief app_mgr_client_triger_console <s* client , o* object param > */
 function app_mgr_client_triger_console(&$s)
{
	
} 
 
 