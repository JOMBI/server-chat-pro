<?php if( !defined('SCP_APP') ) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
 
 /*! \brief app_mgr_client_triger_events_thread <s* client , o* object > */
 function app_mgr_client_triger_events_thread(&$m, &$s, &$o)
{
	/* default response */
	app_func_copy($s->output, NULL);
	
	/* revers type of the event */
	app_func_copy($o->type , 'event');
	app_func_copy($o->event, $o->action);
		
	/* write customize messgae to thread for response */
	app_func_stream($s->output, sprintf("msg:%s"	  , app_func_eval($o,'msg')));
	app_func_stream($s->output, sprintf("type:%s"	  , app_func_eval($o,'type')));
	app_func_stream($s->output, sprintf("event:%s"	  , app_func_eval($o,'event')));
	app_func_stream($s->output, sprintf("id:%s"	  	  , app_func_eval($o,'id')));
	app_func_stream($s->output, sprintf("pid:%s"	  , app_func_eval($o,'pid')));
	app_func_stream($s->output, sprintf("tick:%s"	  , app_func_eval($o,'tick')));
	app_func_stream($s->output, sprintf("uid:%s"	  , app_func_eval($o,'uid')));
	app_func_stream($s->output, sprintf("appagent:%s" , app_func_eval($o,'appagent')));
	app_func_stream($s->output, sprintf("session:%s"  , app_func_eval($o,'session')));
	app_func_stream($s->output, sprintf("channel:%s"  , app_func_eval($o,'channel')));
	app_func_stream($s->output, sprintf("userid:%s"   , app_func_eval($o,'userid')));
	app_func_stream($s->output, sprintf("domain:%s"   , app_func_eval($o,'domain')));
	app_func_stream($s->output, sprintf("group:%s"    , app_func_eval($o,'domain')));
	app_func_stream($s->output, sprintf("name:%s"	  , app_func_eval($o,'name')));
	app_func_stream($s->output, sprintf("email:%s"	  , app_func_eval($o,'email')));
	app_func_stream($s->output, sprintf("status:%d"   , app_func_eval($o,'status')));   
	app_func_stream($s->output, sprintf("state:%d"    , app_func_eval($o,'state')));   
	app_func_stream($s->output, sprintf("regtime:%s"  , app_func_eval($o,'regtime')));    
	app_func_stream($s->output, sprintf("error:%d"    , app_func_eval($o,'error'))); 
	
	if (app_func_enter($s->output) &&(app_func_encrypt($s->output))){
		if (sockUdpWrite($s->sock, $s->output, $s->host, $s->port)) {
			app_func_free($s->output); 
			app_func_next($s->retval);
		}
	}
	/*! \retval by ref data .<s> */
	return $s->retval;
}

/** 
 * \brief 	app_mgr_client_triger_events_register
 *
 * \remark  Feedback / Response / Tanggapan Untuk di kirim ke client dari Socket manager dengan asumsi 
 * yang meminta process tersebut berasal dari web server client /w_hook
 *
 * \param  	<s * client , o* object param >
 * \retval 	int 
 * 
 */
 function app_mgr_client_triger_events_register(&$m, &$s, &$o)
{
	/* default response */
	app_func_copy($s->output, NULL);
	
	/* reverse type of the event */
	app_func_copy($o->type , 'event');
	app_func_copy($o->event, $o->action);
	
	/* write customize messgae to thread for response */
	app_func_stream($s->output, sprintf("msg:%s"	  , app_func_eval($o,'msg')));
	app_func_stream($s->output, sprintf("type:%s"	  , app_func_eval($o,'type')));
	app_func_stream($s->output, sprintf("event:%s"	  , app_func_eval($o,'event'))); 
	app_func_stream($s->output, sprintf("uid:%s"	  , app_func_eval($o,'uid'))); 
	app_func_stream($s->output, sprintf("session:%s"  , app_func_eval($o,'session')));
	app_func_stream($s->output, sprintf("channel:%s"  , app_func_eval($o,'channel')));
	app_func_stream($s->output, sprintf("userid:%s"   , app_func_eval($o,'userid')));
	app_func_stream($s->output, sprintf("domain:%s"   , app_func_eval($o,'domain')));
	app_func_stream($s->output, sprintf("name:%s"	  , app_func_eval($o,'username')));
	app_func_stream($s->output, sprintf("email:%s"	  , app_func_eval($o,'email')));
	app_func_stream($s->output, sprintf("status:%s"   , app_func_eval($o,'status'))); 
	app_func_stream($s->output, sprintf("error:%s"    , app_func_eval($o,'error')));  
	
	if (app_func_enter($s->output) &&(app_func_encrypt($s->output))) {
		if (sockUdpWrite($s->sock, $s->output, $s->host, $s->port)) {
			app_func_free($s->output); 
			app_func_next($s->retval);
		}
	}
	/*! \retval by ref data .<s> */
	return $s->retval;
}


/** 
 * \brief 	app_mgr_client_triger_events_unregister
 *
 * \remark  Feedback / Response / Tanggapan Untuk di kirim ke client dari Socket manager dengan asumsi 
 * yang meminta process tersebut berasal dari web server client /w_hook
 *
 * \param  	<s * client , o* object param >
 * \retval 	int 
 * 
 */
 function app_mgr_client_triger_events_unregister(&$m, &$s, &$o)
{
	/* default response */
	app_func_copy($s->output, NULL);
	
	/* reverse type of the event */
	app_func_copy($o->type , 'event');
	app_func_copy($o->event, $o->action);
	
	/* write customize messgae to thread for response */
	app_func_stream($s->output, sprintf("msg:%s"	  , app_func_eval($o,'msg')));
	app_func_stream($s->output, sprintf("type:%s"	  , app_func_eval($o,'type')));
	app_func_stream($s->output, sprintf("event:%s"	  , app_func_eval($o,'event'))); 
	app_func_stream($s->output, sprintf("uid:%s"	  , app_func_eval($o,'uid'))); 
	app_func_stream($s->output, sprintf("session:%s"  , app_func_eval($o,'session')));
	app_func_stream($s->output, sprintf("channel:%s"  , app_func_eval($o,'channel')));
	app_func_stream($s->output, sprintf("userid:%s"   , app_func_eval($o,'userid')));
	app_func_stream($s->output, sprintf("domain:%s"   , app_func_eval($o,'domain')));
	app_func_stream($s->output, sprintf("name:%s"	  , app_func_eval($o,'username')));
	app_func_stream($s->output, sprintf("email:%s"	  , app_func_eval($o,'email')));
	app_func_stream($s->output, sprintf("status:%s"   , app_func_eval($o,'status'))); 
	app_func_stream($s->output, sprintf("error:%s"    , app_func_eval($o,'error')));  
	 
	if (app_func_enter($s->output) &&(app_func_encrypt($s->output))) 
	{ 
		if (sockUdpWrite($s->sock, $s->output, $s->host, $s->port))
		{
			app_func_free($s->output); 
			app_func_next($s->retval);
		}
	}
	
	/*! \retval by ref data .<s> */
	return $s->retval;
}

/** 
 * \brief 	app_mgr_client_triger_events_routing
 *
 * \remark  Feedback / Response / Tanggapan Untuk di kirim ke client dari Socket manager dengan asumsi 
 * yang meminta process tersebut berasal dari web server client /w_hook
 *
 * \param  	<s * client , o* object param >
 * \retval 	int 
 * 
 */
 function app_mgr_client_triger_events_routing(&$m, &$s, &$o)
{
	/* default response */
	app_func_copy($s->output, NULL);
	
	/* reverse type of the event */
	app_func_copy($o->type , 'event');
	app_func_copy($o->event, $o->action);
	
	/* write customize messgae to thread for response */
	app_func_stream($s->output, sprintf("msg:%s"	   , app_func_eval($o,'msg')));
	app_func_stream($s->output, sprintf("type:%s"	   , app_func_eval($o,'type')));
	app_func_stream($s->output, sprintf("event:%s"	   , app_func_eval($o,'event'))); 
	app_func_stream($s->output, sprintf("direction:%s" , app_func_eval($o,'direction'))); 
	app_func_stream($s->output, sprintf("channel1:%s"  , app_func_eval($o,'channel1'))); 
	app_func_stream($s->output, sprintf("channel2:%s"  , app_func_eval($o,'channel2'))); 
	app_func_stream($s->output, sprintf("chatboxid:%s" , app_func_eval($o,'chatboxid'))); 
	app_func_stream($s->output, sprintf("error:%s"     , app_func_eval($o,'error')));  
	 
	if (app_func_enter($s->output) &&(app_func_encrypt($s->output))) 
	{ 
		if (sockUdpWrite($s->sock, $s->output, $s->host, $s->port))
		{
			app_func_free($s->output); 
			app_func_next($s->retval);
		}
	}
	
	/*! \retval by ref data .<s> */
	return $s->retval;
}

/** 
 * \brief 	app_mgr_client_triger_events_pickup
 *
 * \remark  Feedback / Response / Tanggapan Untuk di kirim ke client dari Socket manager dengan asumsi 
 * yang meminta process tersebut berasal dari web server client /w_hook
 *
 * \param  	<s * client , o* object param >
 * \retval 	int 
 * 
 */
 function app_mgr_client_triger_events_pickup(&$m, &$s, &$o)
{
	/* default response */
	app_func_copy($s->output, NULL);
	
	/* reverse type of the event */
	app_func_copy($o->type , 'event');
	app_func_copy($o->event, $o->action);
	
	/* write customize messgae to thread for response */
	app_func_stream($s->output, sprintf("msg:%s"	  , app_func_eval($o,'msg')));
	app_func_stream($s->output, sprintf("type:%s"	  , app_func_eval($o,'type')));
	app_func_stream($s->output, sprintf("event:%s"	  , app_func_eval($o,'event'))); 
	app_func_stream($s->output, sprintf("direction:%s", app_func_eval($o,'direction'))); 
	app_func_stream($s->output, sprintf("channel1:%s" , app_func_eval($o,'channel1'))); 
	app_func_stream($s->output, sprintf("channel2:%s" , app_func_eval($o,'channel2'))); 
	app_func_stream($s->output, sprintf("chatboxid:%s", app_func_eval($o,'chatboxid'))); 
	app_func_stream($s->output, sprintf("error:%s"    , app_func_eval($o,'error')));  
	 
	if (app_func_enter($s->output) &&(app_func_encrypt($s->output))) 
	{ 
		if (sockUdpWrite($s->sock, $s->output, $s->host, $s->port))
		{
			app_func_free($s->output); 
			app_func_next($s->retval);
		}
	}
	
	/*! \retval by ref data .<s> */
	return $s->retval;
}