<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 * 
 * Goto: Meta_Event
 */
 if( !defined('USER_NAVIGATOR') ) define( 'USER_NAVIGATOR', 'DF');
 
 /*! \retval app_websock_visitor_action_user_register <> */
 function app_websock_visitor_action_user_register(&$m, &$s= NULL, &$meta=NULL, &$eval = NULL)
{
	if ( !$s->id ) {
		return 0;
	}
	
	/*! \remark created header for Fedback client */
	app_func_copy($meta->type 	,'event');
	app_func_copy($meta->event 	,'register'); 
	app_func_copy($meta->error 	,0);
	 
	/*! \remark for Utilize to User Session */ 
	app_func_struct($out);
	app_func_copy($out->uid		 , $eval->get('uid'));
	app_func_copy($out->channel	 , $eval->get('channel'));
	app_func_copy($out->domain	 , $eval->get('domain'));
	app_func_copy($out->session  , $eval->get('session'));
	app_func_copy($out->userid   , $eval->get('userid'));
	app_func_copy($out->username , $eval->get('username'));
	app_func_copy($out->useremail, $eval->get('useremail'));
	app_func_copy($out->regtime	 , $eval->get('regtime'));
	app_func_copy($out->status	 , $eval->get('status'));
	app_func_copy($out->group	 , $eval->get('group'));
	
		
	/*! \remark untuk referensi socket di ambil dari client */ 
	app_func_copy($out->pid   	 , $s->pid);
	app_func_copy($out->type 	 , $s->type); /*! ws_vst*/
	app_func_copy($out->sock 	 , $s->sock);
	app_func_copy($out->host 	 , $s->host);
	app_func_copy($out->port 	 , $s->port); 
	app_func_copy($out->time 	 , $s->time);
	app_func_copy($out->regts 	 , $s->time);
	app_func_copy($out->alive 	 , $s->time);
	app_func_copy($out->boxes 	 , array());
	app_func_copy($out->appagent , USER_NAVIGATOR);
	app_func_copy($out->state	 , CHAN_RESERVED);
	app_func_copy($out->status	 , STATE_REGISTER);
	
	while ($out->uid)
	{
		/*! \remark cari dengan menggunakan ref channels / email sebagai UniqUid Key */ 
		app_func_copy($retval, 0);
		if ( !app_user_session_find($out->channel, $retval) )
		{
			
			app_user_session_set($out->channel, 'uid'	  , $out->uid); 
			app_user_session_set($out->channel, 'appagent', $out->appagent);
			app_user_session_set($out->channel, 'session' , $out->session);
			app_user_session_set($out->channel, 'channel' , $out->channel);
			app_user_session_set($out->channel, 'userid'  , $out->userid);
			app_user_session_set($out->channel, 'type'	  , $out->type);
			app_user_session_set($out->channel, 'domain'  , $out->domain);
			app_user_session_set($out->channel, 'group'   , $out->group);
			app_user_session_set($out->channel, 'name'    , $out->username);
			app_user_session_set($out->channel, 'email'   , $out->useremail);
			app_user_session_set($out->channel, 'status'  , $out->status); 
			app_user_session_set($out->channel, 'state'   , $out->state);  
			app_user_session_set($out->channel, 'pid'     , $out->pid);
			app_user_session_set($out->channel, 'sock'    , $out->sock);
			app_user_session_set($out->channel, 'host'    , $out->host);
			app_user_session_set($out->channel, 'port'    , $out->port);
			app_user_session_set($out->channel, 'boxes'   , $out->boxes);
			app_user_session_set($out->channel, 'time'    , $out->time);
			app_user_session_set($out->channel, 'regts'   , $out->regts);
			app_user_session_set($out->channel, 'alive'   , $out->alive+900); 
			break;
		}
		
		/*! jika data sudah ada sebelumnya */
		if( $retval->find('session') )
		{
			app_user_session_set($out->channel, 'group', $out->group);
		}
		 
	   /** 
	    * \remark 
		* o. Jika session sudah ada dan yang meregis berasal dari hook artinya ini perlu di Konfirmasi 
		*
	    * o. Check Periode of Syncronize session , 
	    * 	 Jika suatu sesion waktunya tidak syncron anatar yang di register oleh webclient dengan yang ada 
		* 	 pada server maka ini akan di anggap sebagai session yang kadaluwarsa / expired dan akan langsung di destroy saja 
		*/
		
		if ( $retval->detach('type', 'tcp_pub') )
		{
			if ( $retval->dispatchEvent( 'app_func_interval', 'regts') > 5) 
			{
				app_visitor_loger($s, sprintf("Session Register from '%s' expired", $retval->get('session') ));
				app_func_next($meta->error);
				break;
			}
			
			app_user_session_set($out->channel, 'appagent', $out->appagent);
			app_user_session_set($out->channel, 'type'	  , $out->type);
			app_user_session_set($out->channel, 'status'  , $out->status); 
			app_user_session_set($out->channel, 'pid'     , $out->pid);
			app_user_session_set($out->channel, 'sock'    , $out->sock);
			app_user_session_set($out->channel, 'host'    , $out->host);
			app_user_session_set($out->channel, 'port'    , $out->port);
			app_user_session_set($out->channel, 'boxes'   , $out->boxes);
			app_user_session_set($out->channel, 'time'    , $out->time);
			app_user_session_set($out->channel, 'regts'   , $out->regts);
			app_user_session_set($out->channel, 'alive'   , $out->alive+900); 
			
			/*! \remark then rollback session */
			app_user_session_roolback($out->channel, $retval);
		}
		
	   /*! 
		* \remark jika session Masih ada ketika Register Biasanya yang berubah hanya 
		* data dari referensi client saja yang lain akan tetap sama 
		*/
		
		app_func_copy($out->uid		 , $retval->get('uid'));
		app_func_copy($out->channel	 , $retval->get('channel'));
		app_func_copy($out->session  , $retval->get('session'));
		app_func_copy($out->userid   , $retval->get('userid'));
		app_func_copy($out->username , $retval->get('name'));
		app_func_copy($out->useremail, $retval->get('email'));
		app_func_copy($out->regtime	 , $retval->get('regts'));
		
		
		/*! \remark update data client */
		app_user_session_set($out->channel, 'pid'  , $out->pid);
		app_user_session_set($out->channel, 'sock' , $out->sock);
		app_user_session_set($out->channel, 'host' , $out->host);
		app_user_session_set($out->channel, 'port' , $out->port);
		app_user_session_set($out->channel, 'boxes', $out->boxes);
		app_user_session_set($out->channel, 'time' , $out->time);
		 
		break;	
	} 

	/*! \retval  for sent back to this  */
	app_func_copy($o, false); 
	if (app_user_session_find($out->channel, $o))
	{
		app_func_free($meta->data);
		app_func_alloc($meta->data);
		app_func_copy($meta->data, 'uid'  	  , app_func_str($o->get('uid')));
		app_func_copy($meta->data, 'session'  , app_func_str($o->get('session')));
		app_func_copy($meta->data, 'channel'  , app_func_str($o->get('channel')));
		app_func_copy($meta->data, 'userid'	  , app_func_str($o->get('userid')));
		app_func_copy($meta->data, 'username' , app_func_str($o->get('name')));
		app_func_copy($meta->data, 'useremail', app_func_str($o->get('email')));
		app_func_copy($meta->data, 'regtime'  , app_func_str($o->get('time')));
		app_func_copy($meta->data, 'status'   , app_func_str($o->get('status')));
		
		
		/*! \retval sent event repsonse to client */
		if (app_websock_visitor_events_meta_user($s, $meta))
		{
		   /*! \remark syncron data to partner connected with me */
			if (function_exists('app_res_peer_triger_action_register'))
			{
			   app_res_peer_triger_action_register($m, $o, $meta);
			}
		}
		
		app_func_free($meta);
	}
	
	/*! \retval to this method */
	app_func_free($out);
	app_func_free($eval);
	return $s->id; 
 }
 
 /*! \retval app_websock_visitor_action_sent_message <> */ 
 function app_websock_visitor_action_sent_message(&$m, &$s= NULL, &$meta=NULL, &$eval = NULL)
{
	app_func_struct($out);
	app_func_struct($app);
	
	while ($s->id)
	{	
		/*! \def all parameter */
		app_func_copy($out->cid   , $eval->get('cid'));
		app_func_copy($out->boxid , $eval->get('msgbox'));
		app_func_copy($out->msgid , $eval->get('msgid'));
		app_func_copy($out->chan1 , $eval->get('chan'));
		app_func_copy($out->from  , $eval->get('from'));
		app_func_copy($out->chan2 , $eval->get('to')); 
		app_func_copy($out->type  , $eval->get('type')); 
		app_func_copy($out->text  , $eval->get('text'));
		app_func_copy($out->time  , $eval->get('time'));
		app_func_copy($out->flag  , $eval->get('flag'));
		app_func_copy($out->to    , NULL);
		
		/*! \remark replace time  for default */
		
		app_func_copy($out->time , date('H:i', strtotime('now')));
		app_func_copy($out->msgid, strtotime('now'));
		app_func_copy($out->flag , 1);
		
		
		/*! check destination */
		app_func_copy($meta->error,0);
		if ( !$eval->get('to') OR ( !$out->boxid ))
		{
			app_func_copy($meta->type 	, 'event');
			app_func_copy($meta->event 	, 'sent');  
			app_func_copy($meta->error	, 1);
			
			/*! \remark write If failue sent Message */
			app_visitor_loger($s,
				sprintf("Sent Message from '%s' channel '%s' Error", $out->from, $out->chan1
			));
			
			break;
		}
		
		/*! \remark validasi BoxId jika masih tersedia Boleh di lanjut tapi kalau sudah ksosong exit bro */
		if ( !app_confridge_box_session($m, $out->chan1, $out->chan2 ) )
		{
			app_func_copy($meta->type 	, 'event');
			app_func_copy($meta->event 	, 'sent');  
			app_func_copy($meta->error	, 1);
			
			/*! \remark write If failue sent Message */
			app_visitor_loger($s,
				sprintf("Sent Message from '%s' channel '%s' Error", $out->from, $out->chan1
			));
			break;
		}
		
		
		app_func_copy($out->retval, 0);
		app_func_copy($out->msgid , app_func_msgid());
		if ( $out->msgid ) 
		{
			/*! Kirim Pesan ke chanel Tujuan 'receiver' */
			if ( app_user_session_find($out->chan2, $chan2) )
			{
				app_func_copy($meta->type  , 'event');
				app_func_copy($meta->event , 'recv');  
				app_func_copy($meta->error , 0);
				
				
				/*! \remark get from session */
				app_func_copy($out->to, $chan2->get('name'));
				app_func_copy($app->pid2, $chan2->get('pid'));
				app_func_copy($app->domain2, $chan2->get('domain'));
				
				/*! \remark Normal Session */
				if ( app_websock_global_client_bypid($m, $app->pid2, $app->c2) )
				{
					app_func_free($meta->data);
					app_func_alloc($meta->data);
					app_func_copy($meta->data, 'cid'   , app_func_str($out->cid));
					app_func_copy($meta->data, 'msgbox', app_func_str($out->boxid));
					app_func_copy($meta->data, 'msgid' , app_func_str($out->msgid));
					app_func_copy($meta->data, 'chan'  , app_func_str($out->chan2));
					app_func_copy($meta->data, 'from'  , app_func_str($out->from));
					app_func_copy($meta->data, 'to'	   , app_func_str($out->chan2));
					app_func_copy($meta->data, 'type'  , app_func_str($out->type));
					app_func_copy($meta->data, 'text'  , app_func_str($out->text));
					app_func_copy($meta->data, 'time'  , app_func_str($out->time));
					app_func_copy($meta->data, 'flag'  , app_func_str($out->flag));
					
					if ( app_websock_visitor_events_meta_message($app->c2, $meta) )  
					{
						app_func_free($meta->data);
						app_func_next($out->retval);
					} 
					
					/*! \remark add New Data To Loger session */
					app_func_copy($out->date, strtotime('now'));
					app_loger_box_session($m, $out->boxid, 
						array(
							'cid'    => $out->cid,
							'msgid'  => $out->msgid, 
							'msgbox' => $out->boxid,
							'chan1'  => $out->chan1,
							'chan2'  => $out->chan2,
							'from'   => $out->from ,	 
							'to'     => $out->to,  
							'type'   => $out->type,  
							'text'   => $out->text,  
							'time'   => $out->date 	
						) 
					);
				}
				
				/*! \remark Diffren server */
				else if (res_peer_utils_findby_domain($m, $app->domain2, $app->c2) )
				{
					app_func_free($meta->data);
					app_func_alloc($meta->data);
					app_func_copy($meta->data, 'cid'   , app_func_str($out->cid));
					app_func_copy($meta->data, 'msgbox', app_func_str($out->boxid));
					app_func_copy($meta->data, 'msgid' , app_func_str($out->msgid));
					app_func_copy($meta->data, 'chan1' , app_func_str($out->chan1));
					app_func_copy($meta->data, 'chan2' , app_func_str($out->chan2));
					app_func_copy($meta->data, 'chan'  , app_func_str($out->chan2));
					app_func_copy($meta->data, 'from'  , app_func_str($out->from));
					app_func_copy($meta->data, 'to'	   , app_func_str($out->chan2));
					app_func_copy($meta->data, 'type'  , app_func_str($out->type));
					app_func_copy($meta->data, 'text'  , app_func_str($out->text));
					app_func_copy($meta->data, 'time'  , app_func_str($out->time));
					app_func_copy($meta->data, 'flag'  , app_func_str($out->flag));
					
					if ( app_res_peer_triger_action_receive($m, $chan2, $meta))
					{
						app_func_free($meta->data);
						app_func_next($out->retval);
					}
					
					/*! \remark add New Data To Loger session */
					app_func_copy($out->date, strtotime('now'));
					app_loger_box_session($m, $out->boxid, 
						array(
							'cid'    => $out->cid,
							'msgid'  => $out->msgid, 
							'msgbox' => $out->boxid,
							'chan1'  => $out->chan1,
							'chan2'  => $out->chan2,
							'from'   => $out->from ,	 
							'to'     => $out->to,  
							'type'   => $out->type,  
							'text'   => $out->text,  
							'time'   => $out->date 	
						) 
					);
				} 
			} 
			
			/*! Untuk chanel Pengirim channel1 'sender' */
			if ( $out->retval && app_user_session_find($out->chan1, $chan1) )
			{
				app_func_copy($meta->type  , 'event');
				app_func_copy($meta->event , 'sent');  
				app_func_copy($meta->error , 0); 
				
				app_func_copy($app->pid1, $chan1->get('pid'));
				if (app_websock_global_client_bypid($m, $app->pid1, $app->c1 )) 
				{
					app_func_free($meta->data);
					app_func_alloc($meta->data);
					app_func_copy($meta->data, 'cid'   , app_func_str($out->cid));
					app_func_copy($meta->data, 'msgbox', app_func_str($out->boxid));
					app_func_copy($meta->data, 'msgid' , app_func_str($out->msgid));
					app_func_copy($meta->data, 'chan'  , app_func_str($out->chan1));
					app_func_copy($meta->data, 'from'  , app_func_str($out->from));
					app_func_copy($meta->data, 'to'	   , app_func_str($out->chan2));
					app_func_copy($meta->data, 'type'  , app_func_str($out->type));
					app_func_copy($meta->data, 'text'  , app_func_str($out->text));
					app_func_copy($meta->data, 'time'  , app_func_str($out->time));
					app_func_copy($meta->data, 'flag'  , app_func_str($out->flag));
					
					if ( app_websock_visitor_events_meta_message($app->c1, $meta) )  
					{
						app_func_free($meta->data);
						app_func_next($out->retval);
					}
				}  
			}
		}
		
		/*! \remark break And stop */
		break;
	} 
	
	
	if ( $meta->error )
	{
		app_func_free($meta->data);
		app_func_alloc($meta->data);
		app_func_copy($meta->data, 'cid'   , app_func_eval($out,'cid'));
		app_func_copy($meta->data, 'msgbox', app_func_eval($out,'msgbox'));
		app_func_copy($meta->data, 'msgid' , app_func_eval($out,'msgid'));
		app_func_copy($meta->data, 'chan'  , app_func_eval($out,'chan1'));
		app_func_copy($meta->data, 'from'  , app_func_eval($out,'from'));
		app_func_copy($meta->data, 'to'	   , app_func_eval($out,'chan2'));
		app_func_copy($meta->data, 'type'  , app_func_eval($out,'type'));
		app_func_copy($meta->data, 'text'  , app_func_eval($out,'text'));
		app_func_copy($meta->data, 'time'  , app_func_eval($out,'time'));
		app_func_copy($meta->data, 'flag'  , app_func_eval($out,'flag'));
		
		if ( app_websock_visitor_events_meta_message($s, $meta) )  
		{
			app_func_free($meta);
		} 
	}
	
	/*! \retval to this method */
	app_func_free($eval);
	return $s->id; 
 }
 
/*! \retval app_websock_visitor_action_sent_typing <> */ 
 function app_websock_visitor_action_sent_typing(&$m, &$s= NULL, &$meta=NULL, &$eval = NULL)
{
	app_func_struct($out);
	app_func_struct($app);
	 
	
	/*! \events trap process */
	app_func_copy($out->retval , 0);
	while ($s->id)
	{	
		app_func_copy($out->boxid , $eval->get('msgbox'));
		app_func_copy($out->chan1 , $eval->get('chan1'));
		app_func_copy($out->chan2 , $eval->get('chan2'));
		app_func_copy($out->type  , $eval->get('type'));
		app_func_copy($out->text  , $eval->get('text')); 
		app_func_copy($out->flag  , $eval->get('flag')); 
		
		/*! jika Box Id tidak ada Artinya tidak ada conversation yang perlu di info */
		if ( !$out->boxid ){
			break;
		}
		
		/*! \validation Jika channel tujuan tidak ada tidak perlu di teruskan langsung skip */
		if ( !$out->chan2 ){
			break;
		}
		
		/*! \remark validasi BoxId jika masih tersedia Boleh di lanjut tapi kalau sudah ksosong exit bro */
		if ( !app_confridge_box_session($m, $out->chan1, $out->chan2)) {
			break;
		}
		
		/*! \remark ketika channel 1 mengetik maka informasi hanya akan di kirim ke channel2 atau 
		yang akan di kirimi pesan */
		
		if ( app_user_session_global($m, $out->chan2, $f, $c) ){
			
			/*! get data How Im ? */
			app_func_copy($out->from, NULL);
			app_func_copy($out->to, $f->get('name'));
			if ( app_user_session_find($out->chan1, $o) ) 
			{
				app_func_copy($out->from, $o->get('name'));
				app_func_free($o);
			}
			
			
			app_func_copy($meta->type  , 'event');
			app_func_copy($meta->event , 'typing');  
			app_func_copy($meta->error , 0);
			
			
			app_func_free($meta->data);
			app_func_alloc($meta->data);
			app_func_copy($meta->data, 'msgbox', app_func_str($out->boxid));
			app_func_copy($meta->data, 'chan1' , app_func_str($out->chan1));
			app_func_copy($meta->data, 'chan2' , app_func_str($out->chan2));
			app_func_copy($meta->data, 'from'  , app_func_str($out->from));
			app_func_copy($meta->data, 'to'	   , app_func_str($out->to));
			app_func_copy($meta->data, 'type'  , app_func_str($out->type));
			app_func_copy($meta->data, 'text'  , app_func_str($out->text)); 
			app_func_copy($meta->data, 'flag'  , app_func_str($out->flag));
					
					
			if( $c->local ){		
				
				if ( app_websock_visitor_events_meta_message($c, $meta) )  
				{
					app_func_free($meta->data);
					app_func_next($out->retval);
				}
				
			} else if( !$c->local ){
				
				if ( app_res_peer_triger_action_typing($m, $f, $meta) )  
				{
					app_func_free($meta->data);
					app_func_next($out->retval);
				}
			} 
			break;
		}
		
		/*! \remark break and stop */
		break;
	}
	
	/*! \retval to this method */
	app_func_free($eval);
	return $s->id;   
} 