<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * flow : meta ==> action ==> event 
 */
 
/*! \retval app_websock_visitor_meta_user */ 
 function app_websock_visitor_meta_user(&$m, &$s, &$meta)
{
	if (!strcmp($meta->type, 'action') && $meta->action == 'register' )
	{
		$eval = new evaluate($meta->data);
		app_websock_visitor_action_user_register($m, $s, $meta, $eval);
	}
	return 0; 
}

/*! \retval app_websock_visitor_meta_message */
 function app_websock_visitor_meta_message(&$m, &$s, &$meta) 
{
	if (!strcmp($meta->type, 'action') && $meta->action == 'sent' )
	{
		$eval = new evaluate($meta->data);
		app_websock_visitor_action_sent_message($m, $s, $meta, $eval);
		return 0;
	}
	
	/*! \remark even user typing on message */
	if (!strcmp($meta->type, 'action') && $meta->action == 'typing' )
	{
		$eval = new evaluate($meta->data);
		app_websock_visitor_action_sent_typing($m, $s, $meta, $eval);
		return 0;
	}
	
	return 0;
}
