<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
 function app_websock_visitor_handler(&$m, &$s)
{
	/*! \remark creata local object for attr thread */
	app_func_struct($out);
	app_func_copy($out->ping_time, time());
	app_func_copy($out->recv_time, time());
	
	
   /*! \remark 
	* Buat koneksi Untuk Pertukarara data antara thread dan session master 
	* tujuannya ketika nanti dapet pesan atau process apa saja langsung di triger ke server 
	* untuk melakukan drive pocess 
	*
	*/
	
	app_func_struct($udp);
	app_func_copy($udp->sock, -1);
	app_func_copy($udp->host, $m->MgrUdpHost);
	app_func_copy($udp->port, $m->MgrUdpPort);
	
	/*! \remark create client for UDP test */
	if ( sockUdpClientCreate($udp->sock) < 0) 
	{
		scp_log_app("Create 'sockUdpClientCreate()' Fail.");
		if ( sockUdpClose($udp->sock) ){
			app_func_copy($udp->sock,0);
		}
	}
	
	/*! \remark for socks Udp socket Manager */
	app_func_alloc($udp->socks);
	app_func_copy($s->errsock, 0);
	app_func_copy($s->state  , 0);
	app_func_copy($s->status , 0);  
		
	/*! \remark Live forever Thread share */
	$s->pid = @getmypid(); 
	while ($s->pid)
	{
		/*! create for message Id */
		app_func_copy($s->msgid , 
			sprintf("%s%06d", strtotime('now'), $s->pid)
		);
		
		
		/*! \remark jika server mati maka 'KILL' thread ini */
		if (!app_fork_pid($m->AppProgPid) ) 
		{
			sockUdpClose($udp->sock);
			app_fork_kill($s->pid, SIGKILL);
		}
	   
	    /*! \remark iterasi event socket Udp On Stream */
		$udp->socks[0] = $udp->sock; 
		
		
		/*! \remark look of socket changed */
		$udp->except = $udp->write = NULL; $udp->read= $udp->socks;
		@socket_select($udp->read, $udp->write, $udp->except, 1, 0);
		if ( @socket_select($udp->socks, $udp->write, $udp->except, 1, 0) >0) {
			if (app_func_sock($udp->sock) &&(app_func_search($udp->sock, $udp->socks))) { 
			
				app_func_free($rcv);
				if (sockUdpRead($udp->sock, $buf, $ret)) {
					if (app_func_decrypt($buf) &&( onTraceUdpReceive($buf, $rcv) ))
					{
						/*! \remark 
						* get Error state Jika Terindikasi Error Artinya PID yang ada 
						* sudah berganti pad session tersebut kirim Pesan ke Thread Untuk 
						* close socket Tersebut . Tandai Socket Tersebut Untuk di close 
						*/
						
						app_func_copy($s->error, $rcv->error);
						if ($s->error)
						{
							app_func_next($s->errsock);
						}
						
						
						/*! \remark for changed state Monitor */ 
						if ($rcv->channel &&(strcmp($s->state, $rcv->state)))
						{
							app_global_loger($s, sprintf("channel %s changed state to '%d'", $rcv->channel, $rcv->state));
							app_func_copy($s->state, $rcv->state);
						}
						
						/*!\remark for changed state Monitor */
						if ($rcv->channel &&(strcmp($s->status, $rcv->status)))
						{
							app_global_loger($s,sprintf("channel %s changed status to '%d'", $rcv->channel, $rcv->status));
							app_func_copy($s->status, $rcv->status);
						}
						
						/*!\remark for changed all data every time*/
						app_func_copy($s->appagent,$rcv->appagent); 
						app_func_copy($s->session ,$rcv->session); 
						app_func_copy($s->channel ,$rcv->channel); 
						app_func_copy($s->userid  ,$rcv->userid); 
						app_func_copy($s->domain  ,$rcv->domain); 
						app_func_copy($s->regtime ,$rcv->regtime);
						app_func_copy($s->email   ,$rcv->email); 
						app_func_copy($s->name    ,$rcv->name); 
						app_func_copy($s->uid	  ,$rcv->uid);	
					}  
					
					/*! \remark clean up after process */
					app_func_free($buf); 
				}
			}
		}
		
		
	   /*! \remark 
		* buat interval waktu 'pong' Untuk Membberitahukan jika saya / thread masih terkoneksi 
		* dengan catatan socket masih active jika tidak stop process PONG
		*/
		
		if (!$s->errsock &&(app_func_tick($out->ping_time) >= 2))
		{
			app_func_copy($out->ping_time, time());
			app_func_copy($s->now, strtotime('now'));
			
			
			/*! \retval write message for sent */  
			$s->tick = app_func_interval(app_func_eval($s, 'regtime'), $s->now);
			$s->tick = app_func_duration($s->tick);
			
			/*! create Events */
			app_func_copy($s->ecode , 300);
			app_func_copy($s->etype , 'event');
			app_func_copy($s->event , 'pong');
			
			if (app_websock_write_handler($s))
			{
				app_func_free($s->msg); 
			}
		}
		
		/*! jika terindikasi 'errsock' */
		if ($s->errsock >= 3)
		{
		   /**
			* o. Kirim pesan ke client thread Im is Dead 
			* o. Close dari client dengan methode Interupted .
			*/
			
			app_func_copy($s->ecode  , 300);
			app_func_copy($s->etype  , 'event');
			app_func_copy($s->event  , 'dead'); 
			app_func_copy($s->status , -1);
			if ( app_websock_write_handler($s) )
			{
				app_func_free($s->msg); 
			}
			 
			
			/*! \remark close socket after success message */
			if ( sockUdpClose($udp->sock)< 1) 
			{	
				if (app_fork_kill($s->pid, SIGKILL)){
					exit(0);
				}
			} 
		}
		
			
	   /*! \remark Interval untuk check apakah ada user session yang stay di client socket ini 
		*  Memiliki Tugas Untuk Up / Down Informasi atau hal lain semisal info User sedang ketik 
		*  sibuk dan lain2 
		*/
		
		if (app_func_tick($out->recv_time) >= 2)
		{
			app_func_copy($out->recv_time, time()); 
			app_func_copy($s->tick, strtotime('now'));
			
			/*! \retval make message for manager */
			
			app_websock_stream_handler($s, $udp->buf);
			if (app_func_enter($udp->buf) &&( app_func_encrypt($udp->buf) ))
			{
				if (sockUdpWrite($udp->sock, $udp->buf, $udp->host, $udp->port))
				{
					app_func_free($udp->buf);
				}
			}
		}
		
		/*! \remark check message if exist for my acccount */
		app_func_delete($s, 'buf');
	}
	
	sockUdpClose($udp->sock);
	return 0;
}

/*! \retval app_websock_output_handler for sent to manager */
 function app_websock_write_handler(&$s = NULL)
{
	app_func_copy($s->msg, array
	( 
		'meta' 	=> $s->ecode, 
		'type'	=> $s->etype,	
		'event' => $s->event,
		'error' => 0,
		'data'  => array 
		(
			'cid'  	 => $s->id  , 	// current id 
			'pid'  	 => $s->pid , 	// current pid 	
			'host' 	 => $s->host, 	// current host 
			'port' 	 => $s->port, 	// current port 
			'time' 	 => $s->time, 	// register time client 
			'tick'   => $s->tick,	// duration from <reg-now>
			'msgid'  => $s->msgid,	// message Id for curent state 	
			'status' => $s->status
		)
	));
	
	/*! \remark convert to object 'JSON' */
	$s->msg = json_encode($s->msg);
	app_func_copy($s->byte, 0);
	if ( sockWebSocketOutput($s, $s->msg, $s->byte) ) 
	{
		if ( !$s->byte ) {
			app_fork_kill($s->pid, SIGKILL);
		}
	} 
	return $s->pid;
 }
 
/*! \retval app_websock_output_handler for sent to manager */
 function app_websock_stream_handler(&$s = NULL, &$buf = "")
{
	/*! \remark buat pesan untuk di olah di manager */
	app_func_stream($buf, sprintf("msg:%s" ,"mgr"));
	app_func_stream($buf, sprintf("type:%s" ,"action"));
	app_func_stream($buf, sprintf("action:%s" ,"thread"));
	app_func_stream($buf, sprintf("id:%s" , $s->id));
	app_func_stream($buf, sprintf("pid:%s" , $s->pid));
	app_func_stream($buf, sprintf("tick:%s" , $s->tick));
	app_func_stream($buf, sprintf("ctype:%s" , $s->type));
	
	return $s->id;
}