<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */

 /*! \retval app_websock_global_client_bypid <m* scp, s * client >*/  
 function app_websock_global_client_bypid(&$m = NULL, &$pid= NULL, &$socks = NULL)
{
	foreach( $m->AppProgClient as $id => $sock )
   {
		if ( isset($sock['pid']) &&( !strcmp( $sock['pid'], $pid) )) 
		{
			$socks = (object)$sock;
			break;
		}
	} 
	
	return $socks;
}

 
 /*! \retval app_websock_visitor_client_bypid <m* scp, s * client >*/  
 function app_websock_visitor_client_bypid(&$m = NULL, &$pid= NULL, &$socks = NULL)
{
	foreach( $m->AppProgClient as $id => $sock )
   {
		if ( isset($sock['pid']) &&( !strcmp( $sock['pid'], $pid) )) 
		{
			$socks = (object)$sock;
			break;
		}
	} 
	
	return $socks;
}
 
/*! \retval app_websock_visitor_client_thread <m* scp, s * client >*/  
 function app_websock_visitor_client_thread(&$m = NULL, &$s= NULL)
{
	// goto : app_websock_visitor_handler
	if (scpAppCreateThread($m,'app_websock_visitor_handler', array(&$m, &$s), $s->pid)) {
		return $s->pid;		
	}
	return 0;
 }
 
 /*! \retval app_websock_visitor_client_sighup <m* scp, s * client >*/ 
 function app_websock_visitor_client_sighup(&$m = NULL, &$s= NULL)
{
	if (app_websock_visitor_client_delete($m, $s))
	{
		unset($m->AppProgSocket[$s->id]); 
	}
	
	return $s->id; 
 }
 
/*! \retval app_websock_visitor_client_collected <m* scp, s * client >*/ 
 function app_websock_visitor_client_collected(&$m = NULL, &$s= NULL)
{
	/*! \def finaly return ==== false */
	if ( !function_exists( 'scpAppClientCollected' ) ) {
		return 0;
	}
	
	if ( scpAppClientCollected($m, $s) )
	{
		return $s->id;
	}
	
	return 0; 
 }
 
 /*! \retval app_websock_visitor_client_delete <m* scp, s * client >*/ 
 function app_websock_visitor_client_delete(&$m, &$s)
{
	if ( !sockTcpClose($s->sock) )
	{
		scpAppClientDeleted($m, $s);
	}
	
	return $s->id;  
 }
  