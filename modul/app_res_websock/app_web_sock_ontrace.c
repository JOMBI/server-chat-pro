<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
 /*
	privilege 
{
	user	: 100,
	system	: 200, 
	handler : 300
	message	: 400 
	
}
*/

 if ( !defined( 'PRIV_META_USER' )) define( 'PRIV_META_USER', 100 );
 if ( !defined( 'PRIV_META_SYST' )) define( 'PRIV_META_SYST', 200 );
 if ( !defined( 'PRIV_META_HAND' )) define( 'PRIV_META_HAND', 300 ); 
 if ( !defined( 'PRIV_META_MESG' )) define( 'PRIV_META_MESG', 400 );
 
 /*! \retval app_websock_visitor_ontrace < m* master, s* client > */
 function app_websock_visitor_ontrace(&$m, &$s) 
{ 
   /*! \remark 
    * 1. convert data / pesan yang di terima kebentuk text 
	* 2. parsing datanya menjadi line per event  kemudian loop procesnya 
	* 
	*/
	
	$s->buf = sockWebSocketDecoder($s->buf);
	app_func_copy($recv, false);
	if (!onTraceWssReceive($s->buf, $recv)) 
	{
		app_func_free($s->buf);
		return 0;
	}
	/*! \remark for feeback method */
	app_func_copy($s->ptr, 0); 
	foreach ($recv as $j => $r )
   {
		if (app_func_retval($r, $e))
		{
			/*! \retval for 'PRIV_META_USER' */
			if ($e->meta && (!strcmp($e->meta, PRIV_META_USER)))
			{
				app_func_event('app_websock_visitor_meta_user', array(&$m, &$s, &$e), 
				__FILE__, __LINE__ ); 
				app_func_next($s->ptr);
			}
			
			/*! \retval for 'PRIV_META_SYST' */
			else if ($e->meta && (!strcmp($e->meta, PRIV_META_SYST)))
			{
				app_func_event('app_websock_visitor_meta_system', array(&$m, &$s, &$e), 
				__FILE__, __LINE__ ); 
				app_func_next($s->ptr); 
			}
			
			/*! \retval for 'PRIV_META_HAND' */
			else if ($e->meta && (!strcmp($e->meta, PRIV_META_HAND)))
			{
				app_func_event('app_websock_visitor_meta_handler', array(&$m, &$s, &$e), 
				__FILE__, __LINE__ ); 
				app_func_next($s->ptr); 
			}
			
			/*! \retval for 'PRIV_META_MESG' */
			else if ($e->meta && (!strcmp($e->meta, PRIV_META_MESG)))
			{
				app_func_event('app_websock_visitor_meta_message', array(&$m, &$s, &$e), 
				__FILE__, __LINE__ );  
				app_func_next($s->ptr); 
			}
		}
		
		app_func_free($e);
	} 
	/*! \retval num of iteration */
	return $s->ptr;
}
 
 