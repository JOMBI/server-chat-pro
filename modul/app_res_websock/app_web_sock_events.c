<?php if(!defined('SCP_APP')) define( 'SCP_APP', dirname( __FILE__ ));
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
/*! \retval app_websock_visitor_events_meta_user <META USER> */ 
 function app_websock_visitor_events_meta_user(&$s, &$meta)
{
	/*! \retval write message for sent */ 
	app_func_copy($s->msg, NULL);	
	app_func_copy($s->msg, array
	( 
		'meta' 	=> $meta->meta, 
		'type'	=> $meta->type,	
		'event' => $meta->event,
		'error' => $meta->error,
		'data'  => $meta->data 
	));
	
	/*! \remark convert to object 'JSON' */
	$s->msg = json_encode($s->msg);
	app_func_copy($s->byte, 0);
	if ( sockWebSocketOutput($s, $s->msg, $s->byte) ) 
	{
		app_func_free($s->msg);
	} 
	
	return $s->byte;
}
 
 
/*! \retval app_websock_visitor_events_meta_message <META MESSAGE>*/ 
 function app_websock_visitor_events_meta_message(&$s, &$meta)
{
	/*! \retval write message for sent */ 
	app_func_copy($s->msg, NULL);	
	app_func_copy($s->msg, array
	( 
		'meta' 	=> $meta->meta, 
		'type'	=> $meta->type,	
		'event' => $meta->event,
		'error' => $meta->error,
		'data'  => $meta->data 
	));
	
	/*! \remark convert to object 'JSON' */
	$s->msg = json_encode($s->msg); 
	app_func_copy($s->byte, 0);
	if ( sockWebSocketOutput($s, $s->msg, $s->byte) ) 
	{
		app_func_free($s->msg);
	} 
	
	return $s->byte;
}
