<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
 /**
	user_session 
	{
		uid      : 1
		appagent : DF<default>, FF <firefox>, CH <chrome>, IE<Internet Explorer>, SF <Safari> 	
		session  : 1676767676767
		channel  : email <>
		domain   : 90990909090
		group    : visitor,
		type     : wss_vst,
		userid   : omen
		name     : omen	
		email    : omen@test 
		status   : 0,1,2,3,4  
		state    : 
		pid      : 0
		sock     : 0
		host     : 
		port     :
		boxes    : <object.boxes_session >,
		time     : NULL 
		regts    : NULL
		alive    : 
		error    : 0 
		confm    : 
		gender   : 
	}
*/	

/*! \retval app_user_session_unique(* ref) */
 function app_user_session_unique(&$ret = false) 
{
	Global $scp;
	
	/*! \remark creta Uid and Session Id */
	app_func_struct($out);
	app_func_copy($out->uid, app_func_createid()); 
	if ( $out->uid )
	{
		app_func_copy( $out->session, $out->uid);
	} 
	
	if ( app_func_copy($ret, $out) ){
		app_func_free($out);
	}
	
	return $ret;
 }
 
 
 /*! \retval app_user_session_roolback(email, omen@data.com) */
 function app_user_session_roolback($chan = NULL, &$retval = 0)
{
	if (!$retval){
		return 0;
	}		
	app_func_free($retval);
	if ( app_user_session_find($chan, $retval) ){
		return $retval;
	}
	return 0;
 }
 
 
/*! \retval app_user_session_find(email, omen@data.com) */
 function app_user_session_find($chan = NULL, &$retval = 0)
{
	Global $scp;
	
	/*! \remark cehck for disposition session */
	if ( !isset($scp->AppUserSesion[$chan]) )
	{
		return 0;
	}
	
	/*! \sent data to reference object */
	app_func_copy( $retval, 
		new evaluate( $scp->AppUserSesion[$chan])
	);
	return $retval;
}



/*! \retval app_user_session_find(email, omen@data.com) */
 function app_user_session_type($chan = NULL, &$retval = 0)
{
	Global $scp;
	
	app_func_copy($eval, false);
	if (app_user_session_find($chan, $eval) ) 
	{
		if (app_func_copy($retval, $eval->get('type')))
		{
			app_func_free($eval);	
		} 
	}
	return $retval;
}

/*! \retval app_user_session_fetch(email, omen@data.com) */
 function app_user_session_visitor(&$m= NULL, $chan= NULL, &$user = 0, &$client = false)
{
	/*! \remark get session user ALl */ 
	if ( !app_user_session_find($chan, $user) )
	{
		app_func_free($user);
		return 0;
	}
	
	/*! \remark get client session */
	if (!app_websock_visitor_client_bypid($m, $user->get('pid'), $client))
	{
		app_func_free($client);
		return 0;
	}
	
	return $client;
}

/*! \retval app_user_session_agent(email, omen@data.com) */
 function app_user_session_agent(&$m= NULL, $chan= NULL, &$user = 0, &$client = false)
{
	/*! \remark get session user ALl */ 
	if ( !app_user_session_find($chan, $user) )
	{
		app_func_free($user);
		return 0;
	}
	
	/*! \remark get client session */
	if (!app_websock_agent_client_bypid($m, $user->get('pid'), $client))
	{
		app_func_free($client);
		return 0;
	}
	
	return $client;
}

/*! \retval app_user_session_global< m Global , chan string,  user Object , client > */
 function app_user_session_global(&$m= NULL, $chan= NULL, &$user = 0, &$client = false)
{
	/*! \remark get session user ALl */ 
	if ( !app_user_session_find($chan, $user) )
	{
		app_func_free($user);
		return 0;
	} 
	while (true) 
	{
		/*! get client session from local server */
		if ( strcmp($user->get('type'), 'tcp_peer') ) 
		{
			if ( !app_websock_global_client_bypid($m, $user->get('pid'), $client) ) 
			{
				app_func_free($client);
				break;
			}
			
			// has many process 
			app_func_copy($client->local, 1);  
		}
		
		/*! get peers session from local server */
		if ( !strcmp($user->get('type'), 'tcp_peer') )  
		{
			if ( !res_peer_utils_findby_domain($m, $user->get('domain'), $client) )
			{
				app_func_free($client);
				break;
			} 
			
			// has many process 
			app_func_copy($client->local, 0);  
		} 
		
		break;
	} 
	/*! \remark finaly response */ 
	return $client;
}

/**
 * \brief  app_user_session_eval <key string, val string>
 * \remark find data by custmize field on session  
 * \retval true or false
 *
 */
 
 function app_user_session_eval($key = NULL, $val = '', &$retval = false)
{
	Global $scp;
	
	/*! \remark aksess for Global object */
	app_func_copy($retval, false);
	foreach( $scp->AppUserSesion as $session => $data ) 
	{
		if ( !is_array($data) )
		{
			return 0;
		}
		
		/*! \\ validation for array_session */
		
		$out = new evaluate($data);
		if ( !is_object($out) ){
			continue;
		}
		
		if( !$out->find($key) ){
			continue;
		}
		
		if ( !strcmp( $out->get($key) , $val) )
		{
			app_func_copy( $retval, $out );
			break;
		}
	}
	
	/*! \retval finaly data */
	return $retval;
}

/*! \retval app_user_session_create */
 function app_user_session_create($chan = NULL, &$retval = 0)
{
	Global $scp; 
	
	/*! \retval cretae Unique Session for User */
	$retval = sprintf("%s", $chan);
	
	/*! \retval if still exist will return of false */
	if ( isset($scp->AppUserSesion[$retval]) )
		return 0;
	else if ( !isset($scp->AppUserSesion[$retval]) )
	{
		$scp->AppUserSesion[$retval]['uid']      = NULL;
		$scp->AppUserSesion[$retval]['appagent'] = NULL;
		$scp->AppUserSesion[$retval]['session']  = NULL;
		$scp->AppUserSesion[$retval]['channel']  = $chan;
		$scp->AppUserSesion[$retval]['userid'] 	 = NULL;
		$scp->AppUserSesion[$retval]['type'] 	 = NULL;
		$scp->AppUserSesion[$retval]['domain'] 	 = NULL;
		$scp->AppUserSesion[$retval]['group'] 	 = NULL;
		$scp->AppUserSesion[$retval]['name'] 	 = NULL;
		$scp->AppUserSesion[$retval]['email'] 	 = NULL;
		$scp->AppUserSesion[$retval]['status'] 	 = 0;
		$scp->AppUserSesion[$retval]['state'] 	 = 0;
		$scp->AppUserSesion[$retval]['pid'] 	 = NULL;
		$scp->AppUserSesion[$retval]['sock'] 	 = NULL;
		$scp->AppUserSesion[$retval]['host']     = NULL;
		$scp->AppUserSesion[$retval]['port']     = NULL;
		$scp->AppUserSesion[$retval]['boxes'] 	 = NULL;
		$scp->AppUserSesion[$retval]['time'] 	 = strtotime('now');
		$scp->AppUserSesion[$retval]['regts'] 	 = strtotime('now');
		$scp->AppUserSesion[$retval]['alive']    = strtotime('now'); 
		$scp->AppUserSesion[$retval]['gender']   = 'M';
	}
	
	/*! \retval finaly return is object reference */
	return $retval;
 }
 
/*! \retval app_user_session_delete */ 
 function app_user_session_delete(&$chan = NULL )
{
	Global $scp;  
	
	
	/*! \retval create default object string */
	app_func_copy($retval, 0);
	if ( app_user_session_find($chan) )
	{
		unset($scp->AppUserSesion[$chan]);
		app_func_next($retval);
	}
	
	return $retval;
 }
 
 
/*! \retval app_user_session_set */ 
function app_user_session_set(&$uid = NULL, $key= NULL, $val= '')
{
	Global $scp; 
	
	/*! \remark create postpone data */
	app_func_copy($retval, 0);
	if ( !is_array($key) )
	{
		$key = array($key => $val );
	}
	
	if ( app_func_sizeof($uid)<1 )
	{
		return 0;
	}
	
	if ( !app_user_session_find($uid) ) {
		$scp->AppUserSesion[$uid] = array();
	}
	
	foreach ($key as $i => $j) 
	{
		if ( !isset($scp->AppUserSesion[$uid][$i]) )
		{
			app_func_next($retval);
			$scp->AppUserSesion[$uid][$i] = $j; 
			
		} else if ( isset($scp->AppUserSesion[$uid][$i]) ){
			
			app_func_next($retval);
			
			// delete before 
			unset($scp->AppUserSesion[$uid][$i]);
			
			// new update 
			$scp->AppUserSesion[$uid][$i] = $j; 
		}
	}
	return $retval;
}

