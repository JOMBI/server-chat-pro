<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
 /*
 box_channels
   {
	   channel {
		   
	   } 
   }
 */
 
 
/*! \retval app_create_chan_session */ 
 function app_create_chan_session(&$m= NULL, &$channel = NULL, &$boxid = NULL, &$ret = false) 
{
	app_func_struct($out);
	app_func_copy($out->retval, 0);
	app_func_copy($out->channel, $channel);
	app_func_copy($out->boxid, $boxid);
		
	if ( !isset($m->AppChanSession[$out->channel] ))
	{
		$m->AppChanSession[$out->channel] = array();	
		app_func_next($out->retval);
	}
	
	/*! \remark start create box_channels session */
	if ( !isset($m->AppChanSession[$out->channel][$out->boxid] ) )
	{
		$m->AppChanSession[$out->channel][$out->boxid] = array();
		app_func_next($out->retval);
	}
	
	/*! \retval if success will return of User sesion by channel */
	if ( isset($m->AppChanSession[$out->channel][$out->boxid] ))
	{
		if ( !app_user_session_find($out->channel, $ret) ){
			app_func_free($ret);
		}
		
		app_func_next($out->retval);
	}
	return $out->retval;
}


/*! 
 * \brief 	app_delete_chan_session 
 *
 * \remark 	delete after export sesion is done 
 *
 * \param 	<m*: reference > Globals object application 
 * \param   <channel: string> channel from user login data type string
 *
 * \retval 	<int :mixed>
 *
 */
 
 function app_delete_chan_session(&$m= NULL, &$channel = NULL, &$retval = 0)
{
	app_func_copy($retval, 0);
	if ( isset($m->AppChanSession[$channel]) )
	{
		/* \remark delete form One By One */
		$channel_session = $m->AppChanSession[$channel];
		if ( app_func_sizeof($channel_session) ) 
		foreach( $channel_session as $boxid => $s )
	   {
			unset($m->AppChanSession[$channel][$boxid]);
			unset($m->AppChanSession[$channel]);
			app_func_next($retval);
		} 
	}
	
	/*! \remark final of retval */
	return $retval;
	
}

/*! 
 * \brief 	app_unregister_chan_session 
 *
 * \remark 	delete after export sesion is done 
 *
 * \param 	<m*: reference > Globals object application 
 * \param   <channel: string> channel from user login data type string
 *
 * \retval 	<int :mixed>
 *
 */
 
 function app_unreg_chan_session(&$m= NULL, &$channel= NULL, $flag= false, &$retval = 0)
{
	app_func_copy($retval, 0);
	if (isset($m->AppChanSession[$channel]))
	{
		/*! \remark delete form One By One */
		$channel_session = $m->AppChanSession[$channel];
		if (app_func_sizeof($channel_session)) foreach ($channel_session as $boxid => $s) 
		{
			if (isset($m->AppChanSession[$channel][$boxid])) 
			{
			   /*! \cari channel dengan box channel yang masih kosong jika flag =0 */   
				if (!$flag)
				{
					if (sizeof($m->AppChanSession[$channel][$boxid]) < 1 )
					{
						unset($m->AppChanSession[$channel][$boxid]);
						app_func_next($retval);
					}
				}
				
				/*! \cari channel dengan box channel baik ada isinya maupun kosong  kondisi true */   
				if ($flag)
				{
					unset($m->AppChanSession[$channel][$boxid]);
					app_func_next($retval);
				}
			}
		}
	} 
	
	
	/*! \remark final of retval */
	return $retval;
	
}
