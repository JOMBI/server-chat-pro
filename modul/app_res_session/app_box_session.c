<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
 /**
   box_session 
   {
	   id :{
		0 : user1,
		1 : user2 
	   }  
   } 
 */
 
 
 /*! \retval app_create_box_uniqueid */ 
 function app_create_box_uniqueid() 
{
	app_func_copy( $retval,
		app_func_createboxid()
	);
	return $retval;
 }
  
  
/*! \retval app_find_box_session */ 
 function app_create_box_session(&$m= NULL, &$o = NULL, &$boxid = 0) 
{
	app_func_struct($out);
	app_func_copy($out->retval, 0);
	
	/*! \remark jika terindikasi Box_id belum ada */
	$out->boxid =& $boxid;
	if (!$out->boxid)
	{
		app_func_copy( $out->boxid, 
			app_create_box_uniqueid()
		);
	}
	
	if ($out->boxid)
	{
		$m->AppBoxSesion[$out->boxid] = array($o->channel1, $o->channel2);
		if (isset($m->AppBoxSesion[$out->boxid]))
		{
			if (!isset($m->AppBoxTime[$out->boxid])) {
				$m->AppBoxTime[$out->boxid] = strtotime('now');
			}
			
			app_func_next($out->retval);
		}
	}
	
	return $out->retval;
} 


/*! \retval app_confridge_box_session */
 function app_confridge_box_session(&$m, &$chan1 = NULL, &$chan2 = NULL, &$ret = false) 
{
	app_func_struct($out);
	app_func_copy($out->chan1, $chan1);
	app_func_copy($out->chan2, $chan2);
	
	/*! \remark next data */
	app_func_alloc($out->chan);
	app_func_copy($out->chan,0, $out->chan1);
	app_func_copy($out->chan,1, $out->chan2);
	
	
	/*! for retval after iterations */
	app_func_copy($out->enum , 0);
	app_func_copy($out->boxid, 0);
	
	foreach ($m->AppBoxSesion as $boxid  => $channels)
	{
		if (!is_array($channels)) 
		{
			continue;	
		}			
		
		/*! No diffrent == 0 then is true */
		if ( ! @array_diff($out->chan, $channels)) 
		{
			app_func_copy($out->boxid, $boxid);
			app_func_next($out->enum);
		}
		
	}
	
	/*! \retval if success */
	app_func_copy($ret, $out->boxid);
	
	return $out->enum; 
}

 /*! \retval app_direction_box_session */
 function app_direction_box_session(&$m, $boxid = 0, $chanel=NULL,  &$ret = NULL) 
{
	app_func_copy($boxs, false);
	app_func_copy($rets, 0);
	
	if (app_find_box_session($m, $boxid, $boxs))
	{
		foreach ($boxs as $i => $chan)
		{
			if ( !strcmp($chanel, $chan) )
			{
				if (!strcmp($i, 0)) app_func_copy($ret, RING_OUTBOUND);
				else if (!strcmp($i, 1)) app_func_copy($ret, RING_INBOUND);
				else {
					app_func_copy($ret, 0);
				}
				app_func_next($rets);
				break;
			}				
		}
	}
	/*! \remark final is retval */ 
	return $rets;
} 

/*! \retval app_find_box_session */
 function app_find_box_session(&$m, $boxid = 0, &$ret = NULL) 
{
	app_func_struct($out);
	if ( !isset($m->AppBoxSesion[$boxid]) )
	{
		return 0;
	}
	
	// next if OK 
	$out->retval = $m->AppBoxSesion[$boxid];
	if ( app_func_sizeof($out->retval)< 1 ){
		return 0;
	}
	
	if ( app_func_copy($ret,$out->retval) ){
		return $out->retval;
	}
	
	return 0; 
} 


/*! \retval app_eval_box_session */
 function app_eval_box_session(&$m, $chan = 0, &$ret = false) 
{
	app_func_struct($out);
	app_func_copy($out->chan, $chan);
	app_func_copy($out->rets, false);
	app_func_copy($out->enum, 0);
	
	/*! \remark next data */
	app_func_alloc($out->rets);
	foreach( $m->AppBoxSesion as $boxid => $channels )
	{
		if ( !is_array($channels) ){
			continue;
		}			
		
		// next if in_array 
		if (app_func_search($out->chan, $channels))
		{
			if (app_func_copy($out->rets, $out->enum, $boxid) )
			{
				app_func_next($out->enum);
			}
		}			
	}
	
	app_func_copy($ret, $out->rets);
	return $out->enum;
}


/*! \retval app_delete_box_session */ 
 function app_delete_box_session(&$m= NULL, &$channel = NULL, &$retval = 0) 
{
	
	app_func_copy($retval, 0);
	app_func_copy($boxs, false);
	
	if (app_eval_box_session($m, $channel, $boxs))
	{
		
		foreach ($boxs as $j => $boxid)
		{
			/*! \remark find all box_channels */
			if (!isset($m->AppBoxSesion[$boxid])) 
			{
				continue;	
			}
			
			/*! \remark iterations for all channel1 */
			$box_channels = $m->AppBoxSesion[$boxid];
			foreach ($box_channels as $i => $s)
			{
				if ($s &&(!strcmp($channel, $s)))
				{
					unset($m->AppBoxSesion[$boxid][$i]);
					app_func_next($retval);
				}
			}
			
			
			/*! \remark Periksa setiap terjadi pengurangan jika box Kosong delete */
			if (isset($m->AppBoxSesion[$boxid]))
			{
				if (!app_func_sizeof($m->AppBoxSesion[$boxid]))
				{
					unset($m->AppBoxSesion[$boxid]);
				}
			}
		}
	}
	
	/*! \retval final */
	return $retval;
} 

/*! 
 * \brief 	app_bridge_box_session 
 * \remark 	Mencari tahu apakah suatu channel memang sedang Bridge yang terdiri dari2 chanel atau hanya satu 
 *  channel saja  
 *
 * \param 	<m> Global object 
 * \param 	<channel> channel atau uniq dari session user 
 *
 * \retval 
 */
 function app_bridge_box_session(&$m =NULL, $channel = NULL)
{
	app_func_copy($retval, 0);
	app_func_copy($boxs, false);
	
	if (app_eval_box_session($m, $channel, $boxs)) 
	{
		foreach ($boxs as $j => $boxid)
		{
			if( sizeof($m->AppBoxSesion[$boxid] ) > 1 ){
				app_func_next($retval);
			}
		}	
	}
	
	return $retval;
}

/*! 
 * \brief 	app_delete_box_room 
 * \remark 	delete room atau unregister room
 *
 * \param 	<m> Global object 
 * \param 	<channel> channel atau uniq dari session user 
 *
 * \retval 
 */
 function app_unregister_box_session($m, $boxid = 0, &$retval = 0)
{
	app_func_copy($retval, 0);
	if ( isset($m->AppBoxSesion[$boxid]) )
	{
		unset($m->AppBoxSesion[$boxid]);
		app_func_next($retval);
	}
	
	return $retval;
}
 
 