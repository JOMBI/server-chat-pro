<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 

/*! 
 * \brief app_loger_box_session 
 *
 * \remark Mencatat Setiap Session Percakapan antar channel 
 * berdasarkan chatbox id 
 *
 * \param 	<$m , $boxid, $chat, $ret>
 * \retval  integer 
 *
 */
 
 function app_loger_box_session(&$m= NULL, &$boxid= 0, $chat= NULL,  &$ret= false) 
{
	app_func_struct($o);
	app_func_copy($o->boxid, $boxid);	
	app_func_copy($o->enum , 0);	 
	
	if ( !isset($m->AppBoxLoger[$o->boxid]) )
	{
		$m->AppBoxLoger[$o->boxid] = array();
	}
	
	/*! \remark Add every chat this messag */
	if ( isset($m->AppBoxLoger[$o->boxid]) ) 
	{
		app_func_sizeof($m->AppBoxLoger[$o->boxid], $o->enum);
		
		/*! \remark add to Loger Version */
		if ( $o->boxid &&(app_func_sizeof($chat)))
		{
			$m->AppBoxLoger[$o->boxid][$o->enum] = $chat; 	
		}
	}
	
	/*! \remark convert to object Item 'stdClass' */
	
	app_func_free($s);
	if ( app_func_retval($chat, $s) )
	{
		/*! \remark loger for session channel: 1 */
		
		if (app_func_copy($chat, 'sfm', 2)){
			app_loger_chan_session($m, $s->chan1, $o->boxid, $chat);
		}
		
		/*! \remark loger for session channel: 2 */
		if (app_func_copy($chat, 'sfm', 1))
		{
			app_loger_chan_session($m, $s->chan2, $o->boxid, $chat);
		}
	}
	
	app_func_copy($ret, $o->enum);
	return $o->enum;
} 

/*! 
 * \brief app_loger_box_session 
 *
 * \remark Mencatat Setiap Session Percakapan antar channel 
 * berdasarkan chatbox id 
 *
 * \param 	<$m , $boxid, $chat, $ret>
 * \retval  integer 
 *
 */
 function app_loger_chan_session(&$m= NULL, &$chan = NULL,  &$boxid= 0, $chat= NULL,  &$ret= false) 
{
	app_func_struct($o);
	app_func_copy($o->boxid, $boxid);	
	app_func_copy($o->chan, $chan);	
	app_func_copy($o->enum , 0);	
	
	
	if (!isset($m->AppChanSession[$o->chan]) )
	{
		$m->AppChanSession[$o->chan] = array();
	}
	
	if (!isset($m->AppChanSession[$o->chan][$o->boxid]) )
	{
		$m->AppChanSession[$o->chan][$o->boxid] = array();
	}
	
	
	/*! \remark Add every chat this messag */
	if ( isset($m->AppChanSession[$o->chan][$o->boxid]) ) 
	{
		app_func_sizeof($m->AppChanSession[$o->chan][$o->boxid], $o->enum);
		
		/*! \remark add to Loger Version */
		if ( $o->boxid &&(app_func_sizeof($chat)))
		{
			$m->AppChanSession[$o->chan][$o->boxid][$o->enum] = $chat; 	
		}
	}
	
	app_func_copy($ret, $o->enum);
	return $o->enum;
} 