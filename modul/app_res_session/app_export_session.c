<?php if(!defined('SCP_APP')) exit( __FILE__ );
/**
 * SCP -- An open source Server Chat Person
 *
 * Copyright (C) 2019 - 2021, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SMT project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 */
 
 
/*! 
 * \brief 	app_export_chan_conversation 
 *
 * \remark  mencatat hasil conversation setiap chanel yang sudah berakhir dengan 
 * format file JSON yang kemudian Destroy Datanya  
 
 * \param chanel 
 * \retval int 
 */

 function app_export_chan_conversation(&$m= NULL, &$chan = NULL) 
{
	/*! \remark check of dirs is exist or not */
	$exports_ret = 0; $exports_dir = $m->AppProgEnviroment['__EXP__'];
	if( !is_dir($exports_dir) )
	{
		return 0;
	}
	
	/*! \remark next check file of data */
	if ( isset($m->AppChanSession[$chan]) )
	{
		$exports_file = sprintf( "exports_%s_%s.json", $chan, strtotime('now') );
		if ( $exports_cahnnel = $m->AppChanSession[$chan] )
		{
			$exports_final = sprintf("%s/%s", $exports_dir, $exports_file);	
			if ( !file_exists( $exports_final ) )
			{
				app_func_next($exports_ret);
				@file_put_contents( $exports_final, 
					json_encode($exports_cahnnel)
				);
			}
		}
	}
	
	/*! \remark if true return is int ++ */
	return $exports_ret;
}